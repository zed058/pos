import Vue from 'vue'
// 在vue上挂载一个指量 preventReClick，防止重复提交按钮button
var vv = false;
const preventReClick = Vue.directive('preventReClick', {
  inserted: function (el, binding) {
    console.log(el.disabled)
    el.addEventListener('click', () => {
      /*if (!vv) {
        vv = true;
      } else {
        alert("请勿重复点击");
      }*/
      if (!el.disabled) {
        el.disabled = true;
        setTimeout(() => {
          el.disabled = false;
        }, binding.value || 60000)
        //binding.value可以自行设置。如果设置了则跟着设置的时间走
        //例如：v-preventReClick='500'
      }
    })
  }
});
export { preventReClick }
