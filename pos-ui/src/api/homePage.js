import request from '@/utils/request'
/*首页头部*/
export function quantitySum() {
  return request({
    url: '/pos/HomePage/quantitySum',
    method: 'get',
  })
}

/*用户统计图*/
export function queryUserChart() {
  return request({
    url: '/pos/HomePage/queryUserChart',
    method: 'get',
  })
}

/*订单统计图*/
export function queryOrderChart() {
  return request({
    url: '/pos/HomePage/queryOrderChart',
    method: 'get',
  })
}

/*采购商统计图*/
export function queryOrderTerminalChart() {
  return request({
    url: '/pos/HomePage/queryOrderTerminalChart',
    method: 'get',
  })
}
/*兑换商统计图*/
export function queryOrderExchangeChart() {
  return request({
    url: '/pos/HomePage/queryOrderExchangeChart',
    method: 'get',
  })
}
