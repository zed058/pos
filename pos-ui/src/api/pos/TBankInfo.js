import request from '@/utils/request'

// 查询银行信息维护列表
export function listTBankInfo(query) {
  return request({
    url: '/pos/TBankInfo/list',
    method: 'get',
    params: query
  })
}

// 查询银行信息维护详细
export function getTBankInfo(bankId) {
  return request({
    url: '/pos/TBankInfo/' + bankId,
    method: 'get'
  })
}

// 新增银行信息维护
export function addTBankInfo(data) {
  return request({
    url: '/pos/TBankInfo',
    method: 'post',
    data: data
  })
}

// 修改银行信息维护
export function updateTBankInfo(data) {
  return request({
    url: '/pos/TBankInfo',
    method: 'put',
    data: data
  })
}

// 删除银行信息维护
export function delTBankInfo(bankId) {
  return request({
    url: '/pos/TBankInfo/' + bankId,
    method: 'delete'
  })
}

// 导出银行信息维护
export function exportTBankInfo(query) {
  return request({
    url: '/pos/TBankInfo/export',
    method: 'get',
    params: query
  })
}