import request from '@/utils/request'

// 查询终端交易流水列表
export function listTTerminalTransFlowInfo(query) {
  return request({
    url: '/pos/TTerminalTransFlowInfo/list',
    method: 'get',
    params: query
  })
}

// 查询终端交易流水详细
export function getTTerminalTransFlowInfo(id) {
  return request({
    url: '/pos/TTerminalTransFlowInfo/' + id,
    method: 'get'
  })
}

// 新增终端交易流水
export function addTTerminalTransFlowInfo(data) {
  return request({
    url: '/pos/TTerminalTransFlowInfo',
    method: 'post',
    data: data
  })
}

// 修改终端交易流水
export function updateTTerminalTransFlowInfo(data) {
  return request({
    url: '/pos/TTerminalTransFlowInfo',
    method: 'put',
    data: data
  })
}

// 删除终端交易流水
export function delTTerminalTransFlowInfo(id) {
  return request({
    url: '/pos/TTerminalTransFlowInfo/' + id,
    method: 'delete'
  })
}

// 导出终端交易流水
export function exportTTerminalTransFlowInfo(query) {
  return request({
    url: '/pos/TTerminalTransFlowInfo/export',
    method: 'get',
    params: query
  })
}

// 查询审核状态下拉框
export function queryHandleStatus(data) {
  return request({
    url: '/pos/TTerminalTransFlowInfo/queryHandleStatus',
    method: 'post',
    data: data
  })
}

// 查询审核状态下拉框
export function queryPayType(data) {
  return request({
    url: '/pos/TTerminalTransFlowInfo/queryPayType',
    method: 'post',
    data: data
  })
}
