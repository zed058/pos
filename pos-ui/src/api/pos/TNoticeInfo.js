import request from '@/utils/request'

// 查询平台公告列表
export function listTNoticeInfo(query) {
  return request({
    url: '/pos/TNoticeInfo/list',
    method: 'get',
    params: query
  })
}

// 查询平台公告详细
export function getTNoticeInfo(id) {
  return request({
    url: '/pos/TNoticeInfo/' + id,
    method: 'get'
  })
}

// 新增平台公告
export function addTNoticeInfo(data) {
  return request({
    url: '/pos/TNoticeInfo',
    method: 'post',
    data: data
  })
}

// 修改平台公告
export function updateTNoticeInfo(data) {
  return request({
    url: '/pos/TNoticeInfo',
    method: 'put',
    data: data
  })
}

// 删除平台公告
export function delTNoticeInfo(id) {
  return request({
    url: '/pos/TNoticeInfo/' + id,
    method: 'delete'
  })
}

// 导出平台公告
export function exportTNoticeInfo(query) {
  return request({
    url: '/pos/TNoticeInfo/export',
    method: 'get',
    params: query
  })
}