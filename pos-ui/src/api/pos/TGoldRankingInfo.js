import request from '@/utils/request'

// 查询交易金/奖励金（排行榜）回台添加app显示的假数据列表
export function listTGoldRankingInfo(query) {
  return request({
    url: '/pos/TGoldRankingInfo/list',
    method: 'get',
    params: query
  })
}

// 查询交易金/奖励金（排行榜）回台添加app显示的假数据详细
export function getTGoldRankingInfo(id) {
  return request({
    url: '/pos/TGoldRankingInfo/' + id,
    method: 'get'
  })
}

// 新增交易金/奖励金（排行榜）回台添加app显示的假数据
export function addTGoldRankingInfo(data) {
  return request({
    url: '/pos/TGoldRankingInfo',
    method: 'post',
    data: data
  })
}

// 修改交易金/奖励金（排行榜）回台添加app显示的假数据
export function updateTGoldRankingInfo(data) {
  return request({
    url: '/pos/TGoldRankingInfo',
    method: 'put',
    data: data
  })
}

// 删除交易金/奖励金（排行榜）回台添加app显示的假数据
export function delTGoldRankingInfo(id) {
  return request({
    url: '/pos/TGoldRankingInfo/' + id,
    method: 'delete'
  })
}

// 导出交易金/奖励金（排行榜）回台添加app显示的假数据
export function exportTGoldRankingInfo(query) {
  return request({
    url: '/pos/TGoldRankingInfo/export',
    method: 'get',
    params: query
  })
}