import request from '@/utils/request'

// 查询平台设置（关于我们）列表
export function listTAboutUsInfo(query) {
  return request({
    url: '/pos/TAboutUsInfo/list',
    method: 'get',
    params: query
  })
}

// 查询平台设置（关于我们）详细
export function getTAboutUsInfo(id) {
  return request({
    url: '/pos/TAboutUsInfo/' + id,
    method: 'get'
  })
}

// 新增平台设置（关于我们）
export function addTAboutUsInfo(data) {
  return request({
    url: '/pos/TAboutUsInfo',
    method: 'post',
    data: data
  })
}

// 修改平台设置（关于我们）
export function updateTAboutUsInfo(data) {
  return request({
    url: '/pos/TAboutUsInfo',
    method: 'put',
    data: data
  })
}

// 删除平台设置（关于我们）
export function delTAboutUsInfo(id) {
  return request({
    url: '/pos/TAboutUsInfo/' + id,
    method: 'delete'
  })
}

// 导出平台设置（关于我们）
export function exportTAboutUsInfo(query) {
  return request({
    url: '/pos/TAboutUsInfo/export',
    method: 'get',
    params: query
  })
}