import request from '@/utils/request'

// 查询商品基础列表
export function listTTerminalGoodsInfo(query) {
  return request({
    url: '/pos/TTerminalGoodsInfo/list',
    method: 'get',
    params: query
  })
}

// 查询商品品牌列表
export function listTTerminalBrandInfo() {
  return request({
    url: '/pos/TTerminalGoodsInfo/queryBrand',
    method: 'get'
  })
}

// 已实名认证的用户
export function auditUserList() {
  return request({
    url: '/pos/TTerminalGoodsInfo/auditUserList',
    method: 'get'
  })
}

// 金额状态
export function queryGoodsStatus() {
  return request({
    url: '/pos/TTerminalGoodsInfo/queryGoodsStatus',
    method: 'post'
  })
}

// 查询终端商品基础列表
export function listTTerminalGoodsInfoTerminal(query) {
  return request({
    url: '/pos/TTerminalGoodsInfo/terminal',
    method: 'get',
    params: query
  })
}


// 查询商品基础详细
export function getTTerminalGoodsInfo(id) {
  return request({
    url: '/pos/TTerminalGoodsInfo/' + id,
    method: 'get'
  })
}
  // 添加商品SN码
  export function addTerminalSn(data) {
    return request({
      url: '/pos/TTerminalGoodsInfo/addTerminalSn',
      method: 'post',
      data: data
    })
  }

// 新增商品基础
export function addTTerminalGoodsInfo(data) {
  return request({
    url: '/pos/TTerminalGoodsInfo',
    method: 'post',
    data: data
  })
}

// 修改商品基础
export function updateTTerminalGoodsInfo(data) {
  return request({
    url: '/pos/TTerminalGoodsInfo',
    method: 'put',
    data: data
  })
}

// 删除商品基础
export function delTTerminalGoodsInfo(id) {
  return request({
    url: '/pos/TTerminalGoodsInfo/' + id,
    method: 'delete'
  })
}

// 导出商品基础
export function exportTTerminalGoodsInfo(query) {
  return request({
    url: '/pos/TTerminalGoodsInfo/export',
    method: 'get',
    params: query
  })


}
