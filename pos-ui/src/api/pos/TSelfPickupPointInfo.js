import request from '@/utils/request'

// 查询商品自提点列表
export function listTSelfPickupPointInfo(query) {
  return request({
    url: '/pos/TSelfPickupPointInfo/list',
    method: 'get',
    params: query
  })
}

// 查询商品自提点详细
export function getTSelfPickupPointInfo(id) {
  return request({
    url: '/pos/TSelfPickupPointInfo/' + id,
    method: 'get'
  })
}

// 新增商品自提点
export function addTSelfPickupPointInfo(data) {
  return request({
    url: '/pos/TSelfPickupPointInfo',
    method: 'post',
    data: data
  })
}

// 修改商品自提点
export function updateTSelfPickupPointInfo(data) {
  return request({
    url: '/pos/TSelfPickupPointInfo',
    method: 'put',
    data: data
  })
}

// 删除商品自提点
export function delTSelfPickupPointInfo(id) {
  return request({
    url: '/pos/TSelfPickupPointInfo/' + id,
    method: 'delete'
  })
}

// 导出商品自提点
export function exportTSelfPickupPointInfo(query) {
  return request({
    url: '/pos/TSelfPickupPointInfo/export',
    method: 'get',
    params: query
  })
}