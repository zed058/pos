import request from '@/utils/request'

// 查询用户阅读信息列表
export function listTUserNoticeInfo(query) {
  return request({
    url: '/pos/TUserNoticeInfo/list',
    method: 'get',
    params: query
  })
}

// 查询用户阅读信息详细
export function getTUserNoticeInfo(id) {
  return request({
    url: '/pos/TUserNoticeInfo/' + id,
    method: 'get'
  })
}

// 新增用户阅读信息
export function addTUserNoticeInfo(data) {
  return request({
    url: '/pos/TUserNoticeInfo',
    method: 'post',
    data: data
  })
}

// 修改用户阅读信息
export function updateTUserNoticeInfo(data) {
  return request({
    url: '/pos/TUserNoticeInfo',
    method: 'put',
    data: data
  })
}

// 删除用户阅读信息
export function delTUserNoticeInfo(id) {
  return request({
    url: '/pos/TUserNoticeInfo/' + id,
    method: 'delete'
  })
}

// 导出用户阅读信息
export function exportTUserNoticeInfo(query) {
  return request({
    url: '/pos/TUserNoticeInfo/export',
    method: 'get',
    params: query
  })
}