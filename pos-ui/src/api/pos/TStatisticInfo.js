import request from '@/utils/request'

// 查询统计信息列表
export function listTStatisticInfo(query) {
  return request({
    url: '/pos/TStatisticInfo/list',
    method: 'get',
    params: query
  })
}

// 查询统计信息详细
export function getTStatisticInfo(id) {
  return request({
    url: '/pos/TStatisticInfo/' + id,
    method: 'get'
  })
}

// 新增统计信息
export function addTStatisticInfo(data) {
  return request({
    url: '/pos/TStatisticInfo',
    method: 'post',
    data: data
  })
}

// 修改统计信息
export function updateTStatisticInfo(data) {
  return request({
    url: '/pos/TStatisticInfo',
    method: 'put',
    data: data
  })
}

// 删除统计信息
export function delTStatisticInfo(id) {
  return request({
    url: '/pos/TStatisticInfo/' + id,
    method: 'delete'
  })
}

// 导出统计信息
export function exportTStatisticInfo(query) {
  return request({
    url: '/pos/TStatisticInfo/export',
    method: 'get',
    params: query
  })
}