import request from '@/utils/request'

// 查询用户vip等级列表
export function listTGradeInfo(query) {
  return request({
    url: '/pos/TGradeInfo/list',
    method: 'get',
    params: query
  })
}

// 查询用户vip等级详细
export function getTGradeInfo(id) {
  return request({
    url: '/pos/TGradeInfo/' + id,
    method: 'get'
  })
}

// 新增用户vip等级
export function addTGradeInfo(data) {
  return request({
    url: '/pos/TGradeInfo',
    method: 'post',
    data: data
  })
}

// 修改用户vip等级
export function updateTGradeInfo(data) {
  return request({
    url: '/pos/TGradeInfo',
    method: 'put',
    data: data
  })
}

// 删除用户vip等级
export function delTGradeInfo(id) {
  return request({
    url: '/pos/TGradeInfo/' + id,
    method: 'delete'
  })
}

// 导出用户vip等级
export function exportTGradeInfo(query) {
  return request({
    url: '/pos/TGradeInfo/export',
    method: 'get',
    params: query
  })
}
