import request from '@/utils/request'

// 查询用户出入账信息列表
export function listTUserDefaultInfo(query) {
  return request({
    url: '/pos/TUserDefaultInfo/list',
    method: 'get',
    params: query
  })
}

// 查询用户出入账信息详细
export function getTUserDefaultInfo(id) {
  return request({
    url: '/pos/TUserDefaultInfo/' + id,
    method: 'get'
  })
}

// 新增用户出入账信息
export function addTUserDefaultInfo(data) {
  return request({
    url: '/pos/TUserDefaultInfo',
    method: 'post',
    data: data
  })
}

// 修改用户出入账信息
export function updateTUserDefaultInfo(data) {
  return request({
    url: '/pos/TUserDefaultInfo',
    method: 'put',
    data: data
  })
}

// 删除用户出入账信息
export function delTUserDefaultInfo(id) {
  return request({
    url: '/pos/TUserDefaultInfo/' + id,
    method: 'delete'
  })
}

// 导出用户出入账信息
export function exportTUserDefaultInfo(query) {
  return request({
    url: '/pos/TUserDefaultInfo/export',
    method: 'get',
    params: query
  })
}

// 查询审核状态下拉框
export function queryCheckStatus(data) {
  return request({
    url: '/pos/TUserDefaultInfo/queryCheckStatus',
    method: 'post',
    data: data
  })
}

// 审核通过
export function checkPass(data) {
  return request({
    url: '/pos/TUserDefaultInfo/checkPass',
    method: 'post',
    data: data
  })
}

// 审核不通过
export function checkRefuse(data) {
  return request({
    url: '/pos/TUserDefaultInfo/checkRefuse',
    method: 'post',
    data: data
  })
}
// 查询用户 积分奖励记录&激活奖励记录
export function recordList(query){
  return request({
    url: '/pos/TUserDefaultInfo/recordList',
    method: 'get',
    params: query
  })
}
/* 兑换记录 */
export function exchangeRecordList(query){
  return request({
    url: '/pos/TUserDefaultInfo/exchangeRecordList',
    method: 'get',
    params: query
  })
}
