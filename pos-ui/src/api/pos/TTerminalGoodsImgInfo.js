import request from '@/utils/request'

// 查询商品轮播图，产品说明列表
export function listTTerminalGoodsImgInfo(query) {
  return request({
    url: '/pos/TTerminalGoodsImgInfo/list',
    method: 'get',
    params: query
  })
}

// 查询商品轮播图，产品说明详细
export function getTTerminalGoodsImgInfo(id) {
  return request({
    url: '/pos/TTerminalGoodsImgInfo/' + id,
    method: 'get'
  })
}

// 新增商品轮播图，产品说明
export function addTTerminalGoodsImgInfo(data) {
  return request({
    url: '/pos/TTerminalGoodsImgInfo',
    method: 'post',
    data: data
  })
}

// 修改商品轮播图，产品说明
export function updateTTerminalGoodsImgInfo(data) {
  return request({
    url: '/pos/TTerminalGoodsImgInfo',
    method: 'put',
    data: data
  })
}

// 删除商品轮播图，产品说明
export function delTTerminalGoodsImgInfo(id) {
  return request({
    url: '/pos/TTerminalGoodsImgInfo/' + id,
    method: 'delete'
  })
}

// 导出商品轮播图，产品说明
export function exportTTerminalGoodsImgInfo(query) {
  return request({
    url: '/pos/TTerminalGoodsImgInfo/export',
    method: 'get',
    params: query
  })
}