import request from '@/utils/request'

// 查询用户收货地址列表
export function listTUserAddressInfo(query) {
  return request({
    url: '/pos/TUserAddressInfo/list',
    method: 'get',
    params: query
  })
}

// 查询用户收货地址详细
export function getTUserAddressInfo(id) {
  return request({
    url: '/pos/TUserAddressInfo/' + id,
    method: 'get'
  })
}

// 新增用户收货地址
export function addTUserAddressInfo(data) {
  return request({
    url: '/pos/TUserAddressInfo',
    method: 'post',
    data: data
  })
}

// 修改用户收货地址
export function updateTUserAddressInfo(data) {
  return request({
    url: '/pos/TUserAddressInfo',
    method: 'put',
    data: data
  })
}

// 删除用户收货地址
export function delTUserAddressInfo(id) {
  return request({
    url: '/pos/TUserAddressInfo/' + id,
    method: 'delete'
  })
}

// 导出用户收货地址
export function exportTUserAddressInfo(query) {
  return request({
    url: '/pos/TUserAddressInfo/export',
    method: 'get',
    params: query
  })
}