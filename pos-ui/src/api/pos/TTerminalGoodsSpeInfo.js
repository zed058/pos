import request from '@/utils/request'

// 查询商品规格列表
export function listTTerminalGoodsSpeInfo(query) {
  return request({
    url: '/pos/TTerminalGoodsSpeInfo/list',
    method: 'get',
    params: query
  })
}

// 查询商品规格列表
export function queryTerminalGoods() {
  return request({
    url: '/pos/TTerminalGoodsSpeInfo/queryTerminalGoods',
    method: 'get',
  })
}


// 查询商品规格详细
export function getTTerminalGoodsSpeInfo(id) {
  return request({
    url: '/pos/TTerminalGoodsSpeInfo/' + id,
    method: 'get'
  })
}

// 新增商品规格
export function addTTerminalGoodsSpeInfo(data) {
  return request({
    url: '/pos/TTerminalGoodsSpeInfo',
    method: 'post',
    data: data
  })
}

// 修改商品规格
export function updateTTerminalGoodsSpeInfo(data) {
  return request({
    url: '/pos/TTerminalGoodsSpeInfo',
    method: 'put',
    data: data
  })
}

// 删除商品规格
export function delTTerminalGoodsSpeInfo(id) {
  return request({
    url: '/pos/TTerminalGoodsSpeInfo/' + id,
    method: 'delete'
  })
}

// 导出商品规格
export function exportTTerminalGoodsSpeInfo(query) {
  return request({
    url: '/pos/TTerminalGoodsSpeInfo/export',
    method: 'get',
    params: query
  })
}
