import request from '@/utils/request'

// 查询基础配置列表
export function listTRebateSetInfo(query) {
  return request({
    url: '/pos/TRebateSetInfo/list',
    method: 'get',
    params: query
  })
}

// 查询基础配置详细
export function getTRebateSetInfo(id) {
  return request({
    url: '/pos/TRebateSetInfo/' + id,
    method: 'get'
  })
}

// 新增基础配置
export function addTRebateSetInfo(data) {
  return request({
    url: '/pos/TRebateSetInfo',
    method: 'post',
    data: data
  })
}

// 修改基础配置
export function updateTRebateSetInfo(data) {
  return request({
    url: '/pos/TRebateSetInfo',
    method: 'put',
    data: data
  })
}

// 删除基础配置
export function delTRebateSetInfo(id) {
  return request({
    url: '/pos/TRebateSetInfo/' + id,
    method: 'delete'
  })
}

// 导出基础配置
export function exportTRebateSetInfo(query) {
  return request({
    url: '/pos/TRebateSetInfo/export',
    method: 'get',
    params: query
  })
}