import request from '@/utils/request'

// 查询终端划拨记录列表
export function listTTerminalTransInfo(query) {
  return request({
    url: '/pos/TTerminalTransInfo/list',
    method: 'get',
    params: query
  })
}

// 查询终端划拨记录详细
export function getTTerminalTransInfo(id) {
  return request({
    url: '/pos/TTerminalTransInfo/' + id,
    method: 'get'
  })
}

// 新增终端划拨记录
export function addTTerminalTransInfo(data) {
  return request({
    url: '/pos/TTerminalTransInfo',
    method: 'post',
    data: data
  })
}

// 修改终端划拨记录
export function updateTTerminalTransInfo(data) {
  return request({
    url: '/pos/TTerminalTransInfo',
    method: 'put',
    data: data
  })
}

// 删除终端划拨记录
export function delTTerminalTransInfo(id) {
  return request({
    url: '/pos/TTerminalTransInfo/' + id,
    method: 'delete'
  })
}

// 导出终端划拨记录
export function exportTTerminalTransInfo(query) {
  return request({
    url: '/pos/TTerminalTransInfo/export',
    method: 'get',
    params: query
  })
}