import request from '@/utils/request'

// 查询平台政策/商学院列表
export function listTPolicyInfo(query) {
  return request({
    url: '/pos/TPolicyInfo/list',
    method: 'get',
    params: query
  })
}

// 查询平台政策/商学院详细
export function getTPolicyInfo(id) {
  return request({
    url: '/pos/TPolicyInfo/' + id,
    method: 'get'
  })
}

// 新增平台政策/商学院
export function addTPolicyInfo(data) {
  return request({
    url: '/pos/TPolicyInfo',
    method: 'post',
    data: data
  })
}

// 修改平台政策/商学院
export function updateTPolicyInfo(data) {
  return request({
    url: '/pos/TPolicyInfo',
    method: 'put',
    data: data
  })
}

// 删除平台政策/商学院
export function delTPolicyInfo(id) {
  return request({
    url: '/pos/TPolicyInfo/' + id,
    method: 'delete'
  })
}

// 导出平台政策/商学院
export function exportTPolicyInfo(query) {
  return request({
    url: '/pos/TPolicyInfo/export',
    method: 'get',
    params: query
  })
}