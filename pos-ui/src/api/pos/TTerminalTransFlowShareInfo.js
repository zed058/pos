import request from '@/utils/request'

// 查询第三方交易流水分润详情列表
export function listTTerminalTransFlowShareInfo(query) {
  return request({
    url: '/pos/TTerminalTransFlowShareInfo/list',
    method: 'get',
    params: query
  })
}

// 查询第三方交易流水分润详情详细
export function getTTerminalTransFlowShareInfo(id) {
  return request({
    url: '/pos/TTerminalTransFlowShareInfo/' + id,
    method: 'get'
  })
}

// 新增第三方交易流水分润详情
export function addTTerminalTransFlowShareInfo(data) {
  return request({
    url: '/pos/TTerminalTransFlowShareInfo',
    method: 'post',
    data: data
  })
}

// 修改第三方交易流水分润详情
export function updateTTerminalTransFlowShareInfo(data) {
  return request({
    url: '/pos/TTerminalTransFlowShareInfo',
    method: 'put',
    data: data
  })
}

// 删除第三方交易流水分润详情
export function delTTerminalTransFlowShareInfo(id) {
  return request({
    url: '/pos/TTerminalTransFlowShareInfo/' + id,
    method: 'delete'
  })
}

// 导出第三方交易流水分润详情
export function exportTTerminalTransFlowShareInfo(query) {
  return request({
    url: '/pos/TTerminalTransFlowShareInfo/export',
    method: 'get',
    params: query
  })
}