import request from '@/utils/request'

// 查询用户商户终端关系列表
export function listTTerminalInfo(query) {
  return request({
    url: '/pos/TTerminalInfo/list',
    method: 'get',
    params: query
  })
}

// 查询用户商户终端关系详细
export function getTTerminalInfo(id) {
  return request({
    url: '/pos/TTerminalInfo/' + id,
    method: 'get'
  })
}

// 新增用户商户终端关系
export function addTTerminalInfo(data) {
  return request({
    url: '/pos/TTerminalInfo',
    method: 'post',
    data: data
  })
}

// 修改用户商户终端关系
export function updateTTerminalInfo(data) {
  return request({
    url: '/pos/TTerminalInfo',
    method: 'put',
    data: data
  })
}

// 删除用户商户终端关系
export function delTTerminalInfo(id) {
  return request({
    url: '/pos/TTerminalInfo/' + id,
    method: 'delete'
  })
}

// 导出用户商户终端关系
export function exportTTerminalInfo(query) {
  return request({
    url: '/pos/TTerminalInfo/export',
    method: 'get',
    params: query
  })
}

// 划拨
export function transfer(data) {
  return request({
    url: '/pos/TTerminalInfo/transfer',
    method: 'put',
    data: data
  })
}

// 查询审核状态下拉框
export function queryStatus(data) {
  return request({
    url: '/pos/TTerminalInfo/queryStatus',
    method: 'post',
    data: data
  })
}
