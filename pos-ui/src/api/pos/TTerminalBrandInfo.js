import request from '@/utils/request'

// 查询商品品牌列表
export function listTTerminalBrandInfo(query) {
  return request({
    url: '/pos/TTerminalBrandInfo/list',
    method: 'get',
    params: query
  })
}

// 查询商品品牌详细
export function getTTerminalBrandInfo(id) {
  return request({
    url: '/pos/TTerminalBrandInfo/' + id,
    method: 'get'
  })
}

// 新增商品品牌
export function addTTerminalBrandInfo(data) {
  return request({
    url: '/pos/TTerminalBrandInfo',
    method: 'post',
    data: data
  })
}

// 修改商品品牌
export function updateTTerminalBrandInfo(data) {
  return request({
    url: '/pos/TTerminalBrandInfo',
    method: 'put',
    data: data
  })
}

// 删除商品品牌
export function delTTerminalBrandInfo(id) {
  return request({
    url: '/pos/TTerminalBrandInfo/' + id,
    method: 'delete'
  })
}

// 导出商品品牌
export function exportTTerminalBrandInfo(query) {
  return request({
    url: '/pos/TTerminalBrandInfo/export',
    method: 'get',
    params: query
  })
}