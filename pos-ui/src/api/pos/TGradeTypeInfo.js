import request from '@/utils/request'

// 查询vip跨级分润，直营分润，类型配置列表
export function listTGradeTypeInfo(query) {
  return request({
    url: '/pos/TGradeTypeInfo/list',
    method: 'get',
    params: query
  })
}

// 查询vip跨级分润，直营分润，类型配置详细
export function getTGradeTypeInfo(id) {
  return request({
    url: '/pos/TGradeTypeInfo/' + id,
    method: 'get'
  })
}

// 新增vip跨级分润，直营分润，类型配置
export function addTGradeTypeInfo(data) {
  return request({
    url: '/pos/TGradeTypeInfo',
    method: 'post',
    data: data
  })
}

// 修改vip跨级分润，直营分润，类型配置
export function updateTGradeTypeInfo(data) {
  return request({
    url: '/pos/TGradeTypeInfo',
    method: 'put',
    data: data
  })
}

// 删除vip跨级分润，直营分润，类型配置
export function delTGradeTypeInfo(id) {
  return request({
    url: '/pos/TGradeTypeInfo/' + id,
    method: 'delete'
  })
}

// 导出vip跨级分润，直营分润，类型配置
export function exportTGradeTypeInfo(query) {
  return request({
    url: '/pos/TGradeTypeInfo/export',
    method: 'get',
    params: query
  })
}