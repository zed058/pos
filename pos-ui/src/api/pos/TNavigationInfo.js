import request from '@/utils/request'

// 查询金刚导航栏列表
export function listTNavigationInfo(query) {
  return request({
    url: '/pos/TNavigationInfo/list',
    method: 'get',
    params: query
  })
}

// 查询金刚导航栏详细
export function getTNavigationInfo(id) {
  return request({
    url: '/pos/TNavigationInfo/' + id,
    method: 'get'
  })
}

// 新增金刚导航栏
export function addTNavigationInfo(data) {
  return request({
    url: '/pos/TNavigationInfo',
    method: 'post',
    data: data
  })
}

// 修改金刚导航栏
export function updateTNavigationInfo(data) {
  return request({
    url: '/pos/TNavigationInfo',
    method: 'put',
    data: data
  })
}

// 删除金刚导航栏
export function delTNavigationInfo(id) {
  return request({
    url: '/pos/TNavigationInfo/' + id,
    method: 'delete'
  })
}

// 导出金刚导航栏
export function exportTNavigationInfo(query) {
  return request({
    url: '/pos/TNavigationInfo/export',
    method: 'get',
    params: query
  })
}