import request from '@/utils/request'

// 查询终端激活配置列表
export function listTTerminalActivateSetInfo(query) {
  return request({
    url: '/pos/TTerminalActivateSetInfo/list',
    method: 'get',
    params: query
  })
}

// 查询终端激活配置详细
export function getTTerminalActivateSetInfo(id) {
  return request({
    url: '/pos/TTerminalActivateSetInfo/' + id,
    method: 'get'
  })
}

// 新增终端激活配置
export function addTTerminalActivateSetInfo(data) {
  return request({
    url: '/pos/TTerminalActivateSetInfo',
    method: 'post',
    data: data
  })
}

// 修改终端激活配置
export function updateTTerminalActivateSetInfo(data) {
  return request({
    url: '/pos/TTerminalActivateSetInfo',
    method: 'put',
    data: data
  })
}

// 删除终端激活配置
export function delTTerminalActivateSetInfo(id) {
  return request({
    url: '/pos/TTerminalActivateSetInfo/' + id,
    method: 'delete'
  })
}

// 导出终端激活配置
export function exportTTerminalActivateSetInfo(query) {
  return request({
    url: '/pos/TTerminalActivateSetInfo/export',
    method: 'get',
    params: query
  })
}