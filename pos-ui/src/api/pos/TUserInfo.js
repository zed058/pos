import request from '@/utils/request'

// 查询用户基础数据列表
export function listTUserInfo(query) {
  return request({
    url: '/pos/TUserInfo/list',
    method: 'get',
    params: query
  })
}

// 查询用户基础数据列表
export function listForTrans(query) {
  return request({
    url: '/pos/TUserInfo/listForTrans',
    method: 'get',
    params: query
  })
}

// 查询用户基础数据详细
export function getTUserInfo(userId) {
  return request({
    url: '/pos/TUserInfo/' + userId,
    method: 'get'
  })
}

// 新增用户基础数据
export function addTUserInfo(data) {
  return request({
    url: '/pos/TUserInfo',
    method: 'post',
    data: data
  })
}

// 修改用户基础数据
export function updateTUserInfo(data) {
  return request({
    url: '/pos/TUserInfo',
    method: 'put',
    data: data
  })
}

// 删除用户基础数据
export function delTUserInfo(userId) {
  return request({
    url: '/pos/TUserInfo/' + userId,
    method: 'delete'
  })
}

// 导出用户基础数据
export function exportTUserInfo(query) {
  return request({
    url: '/pos/TUserInfo/export',
    method: 'get',
    params: query
  })
}

// 禁用/启用用户
export function handleStatusChange(userId, userStatus) {
  const data = {
    userId,
    userStatus
  }
  return request({
    url: '/pos/TUserInfo/changeUserStatus',
    method: 'put',
    data: data
  })
}

// 查询会员等级基础数据详细
export function getGradeInfoList(userId) {
  const data = {
    userId
  }
  return request({
    url: '/pos/TUserInfo/queryGradeInfoList',
    method: 'put',
    data: data
  })
}

// 查询会员等级基础数据详细
export function changeUserTeam(data) {
  return request({
    url: '/pos/TUserInfo/changeUserTeam',
    method: 'put',
    data: data
  })
}

// 变更趣工宝银行卡信息
export function updateBankCard(data) {
  return request({
    url: '/pos/TUserInfo/updateBankCard',
    method: 'post',
    data: data
  })
}

// 查询实名认证列表
export function checkListTUserInfo(query) {
  return request({
    url: '/pos/TUserInfo/check',
    method: 'get',
    params: query
  })
}

// 导出实名认证列表
export function exportCheckTUserInfo(query) {
  return request({
    url: '/pos/TUserInfo/checkExport',
    method: 'get',
    params: query
  })
}

// 审核通过
export function checkPass(data) {
  return request({
    url: '/pos/TUserInfo/checkPass',
    method: 'post',
    data: data
  })
}

// 审核不通过
export function checkRefuse(data) {
  return request({
    url: '/pos/TUserInfo/checkRefuse',
    method: 'post',
    data: data
  })
}
// 商户列表
export function merchantList(query){
  return request({
    url: '/pos/TUserInfo/merchantList',
    method: 'get',
    params: query
  })
}

// 查询下拉框
export function queryUserStatus(data) {
  return request({
    url: '/pos/TUserInfo/queryUserStatus',
    method: 'post',
    data: data
  })
}
