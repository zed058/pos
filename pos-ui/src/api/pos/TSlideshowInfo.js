import request from '@/utils/request'

// 查询引导页面列表
export function listTSlideshowInfo(query) {
  return request({
    url: '/pos/TSlideshowInfo/list',
    method: 'get',
    params: query
  })
}

// 查询引导页面详细
export function getTSlideshowInfo(id) {
  return request({
    url: '/pos/TSlideshowInfo/' + id,
    method: 'get'
  })
}

// 新增引导页面
export function addTSlideshowInfo(data) {
  return request({
    url: '/pos/TSlideshowInfo',
    method: 'post',
    data: data
  })
}

// 修改引导页面
export function updateTSlideshowInfo(data) {
  return request({
    url: '/pos/TSlideshowInfo',
    method: 'put',
    data: data
  })
}

// 删除引导页面
export function delTSlideshowInfo(id) {
  return request({
    url: '/pos/TSlideshowInfo/' + id,
    method: 'delete'
  })
}

// 导出引导页面
export function exportTSlideshowInfo(query) {
  return request({
    url: '/pos/TSlideshowInfo/export',
    method: 'get',
    params: query
  })
}