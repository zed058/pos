import request from '@/utils/request'

// 查询工单管理类型列表
export function listTWordOrderTypeInfo(query) {
  return request({
    url: '/pos/TWordOrderTypeInfo/list',
    method: 'get',
    params: query
  })
}

// 查询工单管理类型详细
export function getTWordOrderTypeInfo(id) {
  return request({
    url: '/pos/TWordOrderTypeInfo/' + id,
    method: 'get'
  })
}

// 新增工单管理类型
export function addTWordOrderTypeInfo(data) {
  return request({
    url: '/pos/TWordOrderTypeInfo',
    method: 'post',
    data: data
  })
}

// 修改工单管理类型
export function updateTWordOrderTypeInfo(data) {
  return request({
    url: '/pos/TWordOrderTypeInfo',
    method: 'put',
    data: data
  })
}

// 删除工单管理类型
export function delTWordOrderTypeInfo(id) {
  return request({
    url: '/pos/TWordOrderTypeInfo/' + id,
    method: 'delete'
  })
}

// 导出工单管理类型
export function exportTWordOrderTypeInfo(query) {
  return request({
    url: '/pos/TWordOrderTypeInfo/export',
    method: 'get',
    params: query
  })
}


// 状态修改
export function changeUserStatus(id, isValid) {
  const data = {
    id,
    isValid
  }
  return request({
    url: '/pos/TWordOrderTypeInfo',
    method: 'put',
    data: data
  })
}
