import request from '@/utils/request'

// 查询订单列表
export function listTOrderInfo(query) {
  return request({
    url: '/pos/TOrderInfo/list',
    method: 'get',
    params: query
  })
}
// 查询兑换记录
export function exchangeOrderList(query) {
  return request({
    url: '/pos/TOrderInfo/exchangeOrderList',
    method: 'get',
    params: query
  })
}

// 查询订单详细
export function getTOrderInfo(id) {
  return request({
    url: '/pos/TOrderInfo/' + id,
    method: 'get'
  })
}

// 新增订单
export function addTOrderInfo(data) {
  return request({
    url: '/pos/TOrderInfo',
    method: 'post',
    data: data
  })
}

// 修改订单
export function updateTOrderInfo(data) {
  return request({
    url: '/pos/TOrderInfo',
    method: 'put',
    data: data
  })
}

// 删除订单
export function delTOrderInfo(id) {
  return request({
    url: '/pos/TOrderInfo/' + id,
    method: 'delete'
  })
}

// 导出订单
export function exportTOrderInfo(query) {
  return request({
    url: '/pos/TOrderInfo/export',
    method: 'get',
    params: query
  })
}

// 发货
export function shipTOrder(data){
  return request({
    url: '/pos/TOrderInfo/ship',
    method: 'put',
    data: data
  })
}
