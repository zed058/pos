import request from '@/utils/request'

// 查询用户团队关系列表
export function listTUserTeamInfo(query) {
  return request({
    url: '/pos/TUserTeamInfo/list',
    method: 'get',
    params: query
  })
}

// 查询用户团队关系详细
export function getTUserTeamInfo(id) {
  return request({
    url: '/pos/TUserTeamInfo/' + id,
    method: 'get'
  })
}

// 新增用户团队关系
export function addTUserTeamInfo(data) {
  return request({
    url: '/pos/TUserTeamInfo',
    method: 'post',
    data: data
  })
}

// 修改用户团队关系
export function updateTUserTeamInfo(data) {
  return request({
    url: '/pos/TUserTeamInfo',
    method: 'put',
    data: data
  })
}

// 删除用户团队关系
export function delTUserTeamInfo(id) {
  return request({
    url: '/pos/TUserTeamInfo/' + id,
    method: 'delete'
  })
}

// 导出用户团队关系
export function exportTUserTeamInfo(query) {
  return request({
    url: '/pos/TUserTeamInfo/export',
    method: 'get',
    params: query
  })
}