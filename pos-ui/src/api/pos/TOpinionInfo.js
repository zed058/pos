import request from '@/utils/request'

// 查询意见反馈列表
export function listTOpinionInfo(query) {
  return request({
    url: '/pos/TOpinionInfo/list',
    method: 'get',
    params: query
  })
}

// 查询意见反馈详细
export function getTOpinionInfo(id) {
  return request({
    url: '/pos/TOpinionInfo/' + id,
    method: 'get'
  })
}

// 新增意见反馈
export function addTOpinionInfo(data) {
  return request({
    url: '/pos/TOpinionInfo',
    method: 'post',
    data: data
  })
}

// 修改意见反馈
export function updateTOpinionInfo(data) {
  return request({
    url: '/pos/TOpinionInfo',
    method: 'put',
    data: data
  })
}

// 删除意见反馈
export function delTOpinionInfo(id) {
  return request({
    url: '/pos/TOpinionInfo/' + id,
    method: 'delete'
  })
}

// 导出意见反馈
export function exportTOpinionInfo(query) {
  return request({
    url: '/pos/TOpinionInfo/export',
    method: 'get',
    params: query
  })
}