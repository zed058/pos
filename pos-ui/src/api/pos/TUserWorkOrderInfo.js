import request from '@/utils/request'

// 查询工单管理列表
export function listTUserWorkOrderInfo(query) {
  return request({
    url: '/pos/TUserWorkOrderInfo/list',
    method: 'get',
    params: query
  })
}

// 查询工单管理详细
export function getTUserWorkOrderInfo(id) {
  return request({
    url: '/pos/TUserWorkOrderInfo/' + id,
    method: 'get'
  })
}

// 新增工单管理
export function addTUserWorkOrderInfo(data) {
  return request({
    url: '/pos/TUserWorkOrderInfo',
    method: 'post',
    data: data
  })
}

// 修改工单管理
export function updateTUserWorkOrderInfo(data) {
  return request({
    url: '/pos/TUserWorkOrderInfo',
    method: 'put',
    data: data
  })
}

// 工单审核通关
export function auditPass(id,audit) {
  return request({
    url: '/pos/TUserWorkOrderInfo',
    method: 'put',
    data: {
      "id": id,
      "audit":audit
    }
  })
}

// 删除工单管理
export function delTUserWorkOrderInfo(id) {
  return request({
    url: '/pos/TUserWorkOrderInfo/' + id,
    method: 'delete'
  })
}

// 导出工单管理
export function exportTUserWorkOrderInfo(query) {
  return request({
    url: '/pos/TUserWorkOrderInfo/export',
    method: 'get',
    params: query
  })
}


// 工单单类型列表
export function listTWordOrderTypeInfo() {
  return request({
    url: '/pos/TUserWorkOrderInfo/queryWordorderTypeList',
    method: 'get',
  })
}
