import request from '@/utils/request'

// 查询兑换商品基础列表
export function listTTerminalGoodsInfoExchange(query) {
  return request({
    url: '/pos/TTerminalGoodsInfo/exchange',
    method: 'get',
    params: query
  })
}

// 查询商品基础详细
export function getTTerminalGoodsInfo(id) {
  return request({
    url: '/pos/TTerminalGoodsInfo/' + id,
    method: 'get'
  })
}

// 新增商品基础
export function addTTerminalGoodsInfo(data) {
  return request({
    url: '/pos/TTerminalGoodsInfo',
    method: 'post',
    data: data
  })
}

// 修改商品基础
export function updateTTerminalGoodsInfo(data) {
  return request({
    url: '/pos/TTerminalGoodsInfo',
    method: 'put',
    data: data
  })
}

// 删除商品基础
export function delTTerminalGoodsInfo(id) {
  return request({
    url: '/pos/TTerminalGoodsInfo/' + id,
    method: 'delete'
  })
}

// 导出商品基础
export function exportTTerminalGoodsInfo(query) {
  return request({
    url: '/pos/TTerminalGoodsInfo/export',
    method: 'get',
    params: query
  })
}
