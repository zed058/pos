package com.ruoyi.web.controller.pos;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.AuditStatusEnum;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.pos.web.domain.TUserDefaultInfo;
import com.ruoyi.pos.web.service.ITUserDefaultInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户出入账信息Controller
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@RestController
@RequestMapping("/pos/TUserDefaultInfo")
public class TUserDefaultInfoController extends BaseController
{
    @Autowired
    private ITUserDefaultInfoService tUserDefaultInfoService;

    /**
     * 查询用户出入账信息列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserDefaultInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TUserDefaultInfo tUserDefaultInfo)
    {
        startPage();
        List<TUserDefaultInfo> list = tUserDefaultInfoService.selectTUserDefaultInfoList(tUserDefaultInfo);
        return getDataTable(list);
    }

    /**
     * 导出用户出入账信息列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserDefaultInfo:export')")
    @Log(title = "用户出入账信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TUserDefaultInfo tUserDefaultInfo)
    {
        List<TUserDefaultInfo> list = tUserDefaultInfoService.selectTUserDefaultInfoList(tUserDefaultInfo);
        ExcelUtil<TUserDefaultInfo> util = new ExcelUtil<TUserDefaultInfo>(TUserDefaultInfo.class);
        return util.exportExcel(list, "提现数据");
    }

    /**
     * 获取用户出入账信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserDefaultInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tUserDefaultInfoService.selectTUserDefaultInfoById(id));
    }

    /**
     * 新增用户出入账信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserDefaultInfo:add')")
    @Log(title = "用户出入账信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TUserDefaultInfo tUserDefaultInfo)
    {
        return toAjax(tUserDefaultInfoService.insertTUserDefaultInfo(tUserDefaultInfo));
    }

    /**
     * 修改用户出入账信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserDefaultInfo:edit')")
    @Log(title = "用户出入账信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TUserDefaultInfo tUserDefaultInfo)
    {
        return toAjax(tUserDefaultInfoService.updateTUserDefaultInfo(tUserDefaultInfo));
    }

    /**
     * 删除用户出入账信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserDefaultInfo:remove')")
    @Log(title = "用户出入账信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tUserDefaultInfoService.deleteTUserDefaultInfoByIds(ids));
    }

    /**
     * 审核通过
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserDefaultInfo:edit')")
    @Log(title = "审核通过", businessType = BusinessType.UPDATE)
    @PostMapping("/checkPass")
    public AjaxResult checkPass(@RequestBody TUserDefaultInfo tUserDefaultInfo) {
        try {
            return toAjax(tUserDefaultInfoService.checkPass(tUserDefaultInfo));
        } catch (Exception e) {
            e.printStackTrace();
            return error(e.getMessage());
        }
    }

    /**
     * 审核不通通过
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserDefaultInfo:edit')")
    @Log(title = "审核不通通过", businessType = BusinessType.UPDATE)
    @PostMapping("/checkRefuse")
    public AjaxResult checkRefuse(@RequestBody TUserDefaultInfo tUserDefaultInfo) {
        try {
            return toAjax(tUserDefaultInfoService.checkRefuse(tUserDefaultInfo));
        } catch (Exception e) {
            e.printStackTrace();
            return toAjax(0);
        }
    }

    //vip说明--已测试
    @RequestMapping(value = "/queryCheckStatus", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public AjaxResult queryCheckStatus() {
        try {
            return AjaxResult.success(AuditStatusEnum.toList());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("错了");
        }
    }
    // 查询用户 积分奖励记录&激活奖励记录
    @GetMapping("recordList")
    public AjaxResult selectRecordList(TUserDefaultInfo userDefaultInfo){
        return AjaxResult.success(tUserDefaultInfoService.selectRecordList(userDefaultInfo));
    }

    /* 兑换记录 */
    @GetMapping("exchangeRecordList")
    public TableDataInfo exchangeRecordList(TUserDefaultInfo userDefaultInfo){
        startPage();
        List<TUserDefaultInfo> list = tUserDefaultInfoService.exchangeRecordList(userDefaultInfo);
        return getDataTable(list);
    }

}
