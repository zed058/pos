package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TGoldRankingInfo;
import com.ruoyi.pos.web.service.ITGoldRankingInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 交易金/奖励金（排行榜）回台添加app显示的假数据Controller
 * 
 * @author ruoyi
 * @date 2021-11-20
 */
@RestController
@RequestMapping("/pos/TGoldRankingInfo")
public class TGoldRankingInfoController extends BaseController
{
    @Autowired
    private ITGoldRankingInfoService tGoldRankingInfoService;

    /**
     * 查询交易金/奖励金（排行榜）回台添加app显示的假数据列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldRankingInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TGoldRankingInfo tGoldRankingInfo)
    {
        startPage();
        List<TGoldRankingInfo> list = tGoldRankingInfoService.selectTGoldRankingInfoList(tGoldRankingInfo);
        return getDataTable(list);
    }

    /**
     * 导出交易金/奖励金（排行榜）回台添加app显示的假数据列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldRankingInfo:export')")
    @Log(title = "交易金/奖励金（排行榜）回台添加app显示的假数据", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TGoldRankingInfo tGoldRankingInfo)
    {
        List<TGoldRankingInfo> list = tGoldRankingInfoService.selectTGoldRankingInfoList(tGoldRankingInfo);
        ExcelUtil<TGoldRankingInfo> util = new ExcelUtil<TGoldRankingInfo>(TGoldRankingInfo.class);
        return util.exportExcel(list, "交易金/奖励金（排行榜）回台添加app显示的假数据数据");
    }

    /**
     * 获取交易金/奖励金（排行榜）回台添加app显示的假数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldRankingInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tGoldRankingInfoService.selectTGoldRankingInfoById(id));
    }

    /**
     * 新增交易金/奖励金（排行榜）回台添加app显示的假数据
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldRankingInfo:add')")
    @Log(title = "交易金/奖励金（排行榜）回台添加app显示的假数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TGoldRankingInfo tGoldRankingInfo)
    {
        return toAjax(tGoldRankingInfoService.insertTGoldRankingInfo(tGoldRankingInfo));
    }

    /**
     * 修改交易金/奖励金（排行榜）回台添加app显示的假数据
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldRankingInfo:edit')")
    @Log(title = "交易金/奖励金（排行榜）回台添加app显示的假数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TGoldRankingInfo tGoldRankingInfo)
    {
        return toAjax(tGoldRankingInfoService.updateTGoldRankingInfo(tGoldRankingInfo));
    }

    /**
     * 删除交易金/奖励金（排行榜）回台添加app显示的假数据
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldRankingInfo:remove')")
    @Log(title = "交易金/奖励金（排行榜）回台添加app显示的假数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tGoldRankingInfoService.deleteTGoldRankingInfoByIds(ids));
    }
}
