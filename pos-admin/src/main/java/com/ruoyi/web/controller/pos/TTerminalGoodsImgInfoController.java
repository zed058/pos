package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TTerminalGoodsImgInfo;
import com.ruoyi.pos.web.service.ITTerminalGoodsImgInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品轮播图，产品说明Controller
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@RestController
@RequestMapping("/pos/TTerminalGoodsImgInfo")
public class TTerminalGoodsImgInfoController extends BaseController
{
    @Autowired
    private ITTerminalGoodsImgInfoService tTerminalGoodsImgInfoService;

    /**
     * 查询商品轮播图，产品说明列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsImgInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTerminalGoodsImgInfo tTerminalGoodsImgInfo)
    {
        startPage();
        List<TTerminalGoodsImgInfo> list = tTerminalGoodsImgInfoService.selectTTerminalGoodsImgInfoList(tTerminalGoodsImgInfo);
        return getDataTable(list);
    }

    /**
     * 导出商品轮播图，产品说明列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsImgInfo:export')")
    @Log(title = "商品轮播图，产品说明", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TTerminalGoodsImgInfo tTerminalGoodsImgInfo)
    {
        List<TTerminalGoodsImgInfo> list = tTerminalGoodsImgInfoService.selectTTerminalGoodsImgInfoList(tTerminalGoodsImgInfo);
        ExcelUtil<TTerminalGoodsImgInfo> util = new ExcelUtil<TTerminalGoodsImgInfo>(TTerminalGoodsImgInfo.class);
        return util.exportExcel(list, "商品轮播图，产品说明数据");
    }

    /**
     * 获取商品轮播图，产品说明详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsImgInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tTerminalGoodsImgInfoService.selectTTerminalGoodsImgInfoById(id));
    }

    /**
     * 新增商品轮播图，产品说明
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsImgInfo:add')")
    @Log(title = "商品轮播图，产品说明", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTerminalGoodsImgInfo tTerminalGoodsImgInfo)
    {
        return toAjax(tTerminalGoodsImgInfoService.insertTTerminalGoodsImgInfo(tTerminalGoodsImgInfo));
    }

    /**
     * 修改商品轮播图，产品说明
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsImgInfo:edit')")
    @Log(title = "商品轮播图，产品说明", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTerminalGoodsImgInfo tTerminalGoodsImgInfo)
    {
        return toAjax(tTerminalGoodsImgInfoService.updateTTerminalGoodsImgInfo(tTerminalGoodsImgInfo));
    }

    /**
     * 删除商品轮播图，产品说明
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsImgInfo:remove')")
    @Log(title = "商品轮播图，产品说明", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tTerminalGoodsImgInfoService.deleteTTerminalGoodsImgInfoByIds(ids));
    }
}
