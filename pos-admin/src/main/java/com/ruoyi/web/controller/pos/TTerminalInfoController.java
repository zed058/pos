package com.ruoyi.web.controller.pos;

import java.util.List;

import com.ruoyi.common.enums.ActivateStatusEnum;
import com.ruoyi.common.enums.AuditStatusEnum;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TTerminalInfo;
import com.ruoyi.pos.web.service.ITTerminalInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户商户终端关系Controller
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@RestController
@RequestMapping("/pos/TTerminalInfo")
public class TTerminalInfoController extends BaseController
{
    @Autowired
    private ITTerminalInfoService tTerminalInfoService;

    /**
     * 查询用户商户终端关系列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTerminalInfo tTerminalInfo)
    {
        startPage();
        List<TTerminalInfo> list = tTerminalInfoService.selectTTerminalInfoList(tTerminalInfo);
        return getDataTable(list);
    }

    /**
     * 导出用户商户终端关系列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalInfo:export')")
    @Log(title = "用户商户终端关系", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TTerminalInfo tTerminalInfo)
    {
        startPage();
        List<TTerminalInfo> list = tTerminalInfoService.selectTTerminalInfoList(tTerminalInfo);
        ExcelUtil<TTerminalInfo> util = new ExcelUtil<TTerminalInfo>(TTerminalInfo.class);
        return util.exportExcel(list, "用户终端数据表");
    }

    /**
     * 获取用户商户终端关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tTerminalInfoService.selectTTerminalInfoById(id));
    }

    /**
     * 新增用户商户终端关系
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalInfo:add')")
    @Log(title = "用户商户终端关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTerminalInfo tTerminalInfo)
    {
        return toAjax(tTerminalInfoService.insertTTerminalInfo(tTerminalInfo));
    }

    /**
     * 修改用户商户终端关系
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalInfo:edit')")
    @Log(title = "用户商户终端关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTerminalInfo tTerminalInfo)
    {
        return toAjax(tTerminalInfoService.updateTTerminalInfo(tTerminalInfo));
    }

    /**
     * 删除用户商户终端关系
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalInfo:remove')")
    @Log(title = "用户商户终端关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tTerminalInfoService.deleteTTerminalInfoByIds(ids));
    }

    /**
     * 划拨
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalInfo:edit')")
    @Log(title = "用户商户终端关系", businessType = BusinessType.UPDATE)
    @PutMapping("transfer")
    public AjaxResult transfer(@RequestBody TTerminalInfo tTerminalInfo)
    {
        return toAjax(tTerminalInfoService.transfer(tTerminalInfo));
    }

    @PostMapping("/queryStatus")
    @ResponseBody
    public AjaxResult queryStatus() {
        try {
            return AjaxResult.success(ActivateStatusEnum.toList());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("错了");
        }
    }
}
