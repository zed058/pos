package com.ruoyi.web.controller.pos;

import java.util.List;

import com.ruoyi.common.enums.HandleStausEnum;
import com.ruoyi.common.enums.PayTypeEnum;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TTerminalTransFlowInfo;
import com.ruoyi.pos.web.service.ITTerminalTransFlowInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 终端交易流水Controller
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@RestController
@RequestMapping("/pos/TTerminalTransFlowInfo")
public class TTerminalTransFlowInfoController extends BaseController
{
    @Autowired
    private ITTerminalTransFlowInfoService tTerminalTransFlowInfoService;

    /**
     * 查询终端交易流水列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransFlowInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTerminalTransFlowInfo tTerminalTransFlowInfo)
    {
        startPage();
        List<TTerminalTransFlowInfo> list = tTerminalTransFlowInfoService.selectTTerminalTransFlowInfoList(tTerminalTransFlowInfo);
        return getDataTable(list);
    }

    /**
     * 导出终端交易流水列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransFlowInfo:export')")
    @Log(title = "终端交易流水", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TTerminalTransFlowInfo tTerminalTransFlowInfo)
    {
        startPage();
        List<TTerminalTransFlowInfo> list = tTerminalTransFlowInfoService.selectTTerminalTransFlowInfoList(tTerminalTransFlowInfo);
        ExcelUtil<TTerminalTransFlowInfo> util = new ExcelUtil<TTerminalTransFlowInfo>(TTerminalTransFlowInfo.class);
        return util.exportExcel(list, "终端交易流水数据");
    }

    /**
     * 获取终端交易流水详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransFlowInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tTerminalTransFlowInfoService.selectTTerminalTransFlowInfoById(id));
    }

    /**
     * 新增终端交易流水
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransFlowInfo:add')")
    @Log(title = "终端交易流水", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTerminalTransFlowInfo tTerminalTransFlowInfo)
    {
        return toAjax(tTerminalTransFlowInfoService.insertTTerminalTransFlowInfo(tTerminalTransFlowInfo));
    }

    /**
     * 修改终端交易流水
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransFlowInfo:edit')")
    @Log(title = "终端交易流水", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTerminalTransFlowInfo tTerminalTransFlowInfo)
    {
        return toAjax(tTerminalTransFlowInfoService.updateTTerminalTransFlowInfo(tTerminalTransFlowInfo));
    }

    /**
     * 删除终端交易流水
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransFlowInfo:remove')")
    @Log(title = "终端交易流水", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tTerminalTransFlowInfoService.deleteTTerminalTransFlowInfoByIds(ids));
    }

    @RequestMapping(value = "/queryHandleStatus", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public AjaxResult queryHandleStatus() {
        try {
            return AjaxResult.success(HandleStausEnum.toMap());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("错了");
        }
    }

    @PostMapping("/queryPayType")
    @ResponseBody
    public AjaxResult queryPayType() {
        try {
            return AjaxResult.success(PayTypeEnum.toMap());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("错了");
        }
    }
}
