package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TGoldAwardSerialInfo;
import com.ruoyi.pos.web.service.ITGoldAwardSerialInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * (金币序列号Controller
 * 
 * @author ruoyi
 * @date 2021-11-20
 */
@RestController
@RequestMapping("/pos/TGoldAwardSerialInfo")
public class TGoldAwardSerialInfoController extends BaseController
{
    @Autowired
    private ITGoldAwardSerialInfoService tGoldAwardSerialInfoService;

    /**
     * 查询(金币序列号列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldAwardSerialInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TGoldAwardSerialInfo tGoldAwardSerialInfo)
    {
        startPage();
        List<TGoldAwardSerialInfo> list = tGoldAwardSerialInfoService.selectTGoldAwardSerialInfoList(tGoldAwardSerialInfo);
        return getDataTable(list);
    }

    /**
     * 导出(金币序列号列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldAwardSerialInfo:export')")
    @Log(title = "(金币序列号", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TGoldAwardSerialInfo tGoldAwardSerialInfo)
    {
        List<TGoldAwardSerialInfo> list = tGoldAwardSerialInfoService.selectTGoldAwardSerialInfoList(tGoldAwardSerialInfo);
        ExcelUtil<TGoldAwardSerialInfo> util = new ExcelUtil<TGoldAwardSerialInfo>(TGoldAwardSerialInfo.class);
        return util.exportExcel(list, "(金币序列号数据");
    }

    /**
     * 获取(金币序列号详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldAwardSerialInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tGoldAwardSerialInfoService.selectTGoldAwardSerialInfoById(id));
    }

    /**
     * 新增(金币序列号
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldAwardSerialInfo:add')")
    @Log(title = "(金币序列号", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TGoldAwardSerialInfo tGoldAwardSerialInfo)
    {
        return toAjax(tGoldAwardSerialInfoService.insertTGoldAwardSerialInfo(tGoldAwardSerialInfo));
    }

    /**
     * 修改(金币序列号
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldAwardSerialInfo:edit')")
    @Log(title = "(金币序列号", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TGoldAwardSerialInfo tGoldAwardSerialInfo)
    {
        return toAjax(tGoldAwardSerialInfoService.updateTGoldAwardSerialInfo(tGoldAwardSerialInfo));
    }

    /**
     * 删除(金币序列号
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldAwardSerialInfo:remove')")
    @Log(title = "(金币序列号", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tGoldAwardSerialInfoService.deleteTGoldAwardSerialInfoByIds(ids));
    }
}
