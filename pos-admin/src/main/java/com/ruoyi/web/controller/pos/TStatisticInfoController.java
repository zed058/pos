package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TStatisticInfo;
import com.ruoyi.pos.web.service.ITStatisticInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 统计信息Controller
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
@RestController
@RequestMapping("/pos/TStatisticInfo")
public class TStatisticInfoController extends BaseController
{
    @Autowired
    private ITStatisticInfoService tStatisticInfoService;

    /**
     * 查询统计信息列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TStatisticInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStatisticInfo tStatisticInfo)
    {
        startPage();
        List<TStatisticInfo> list = tStatisticInfoService.selectTStatisticInfoList(tStatisticInfo);
        return getDataTable(list);
    }

    /**
     * 导出统计信息列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TStatisticInfo:export')")
    @Log(title = "统计信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TStatisticInfo tStatisticInfo)
    {
        List<TStatisticInfo> list = tStatisticInfoService.selectTStatisticInfoList(tStatisticInfo);
        ExcelUtil<TStatisticInfo> util = new ExcelUtil<TStatisticInfo>(TStatisticInfo.class);
        return util.exportExcel(list, "统计信息数据");
    }

    /**
     * 获取统计信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TStatisticInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tStatisticInfoService.selectTStatisticInfoById(id));
    }

    /**
     * 新增统计信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TStatisticInfo:add')")
    @Log(title = "统计信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStatisticInfo tStatisticInfo)
    {
        return toAjax(tStatisticInfoService.insertTStatisticInfo(tStatisticInfo));
    }

    /**
     * 修改统计信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TStatisticInfo:edit')")
    @Log(title = "统计信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStatisticInfo tStatisticInfo)
    {
        return toAjax(tStatisticInfoService.updateTStatisticInfo(tStatisticInfo));
    }

    /**
     * 删除统计信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TStatisticInfo:remove')")
    @Log(title = "统计信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tStatisticInfoService.deleteTStatisticInfoByIds(ids));
    }
}
