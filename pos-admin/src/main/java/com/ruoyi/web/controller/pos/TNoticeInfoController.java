package com.ruoyi.web.controller.pos;

import java.util.List;

import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TNoticeInfo;
import com.ruoyi.pos.web.service.ITNoticeInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 平台公告Controller
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@RestController
@RequestMapping("/pos/TNoticeInfo")
public class TNoticeInfoController extends BaseController
{
    @Autowired
    private ITNoticeInfoService tNoticeInfoService;

    /**
     * 查询平台公告列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TNoticeInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TNoticeInfo tNoticeInfo)
    {
        startPage();
        List<TNoticeInfo> list = tNoticeInfoService.selectTNoticeInfoList(tNoticeInfo);
        return getDataTable(list);
    }

    /**
     * 导出平台公告列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TNoticeInfo:export')")
    @Log(title = "平台公告", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TNoticeInfo tNoticeInfo)
    {
        List<TNoticeInfo> list = tNoticeInfoService.selectTNoticeInfoList(tNoticeInfo);
        ExcelUtil<TNoticeInfo> util = new ExcelUtil<TNoticeInfo>(TNoticeInfo.class);
        return util.exportExcel(list, "平台公告数据");
    }

    /**
     * 获取平台公告详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TNoticeInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tNoticeInfoService.selectTNoticeInfoById(id));
    }

    /**
     * 新增平台公告
     */
    @PreAuthorize("@ss.hasPermi('pos:TNoticeInfo:add')")
    @Log(title = "平台公告", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TNoticeInfo tNoticeInfo)
    {
        return toAjax(tNoticeInfoService.insertTNoticeInfo(tNoticeInfo));
    }

    /**
     * 修改平台公告
     */
    @PreAuthorize("@ss.hasPermi('pos:TNoticeInfo:edit')")
    @Log(title = "平台公告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TNoticeInfo tNoticeInfo)
    {
        return toAjax(tNoticeInfoService.updateTNoticeInfo(tNoticeInfo));
    }

    /**
     * 删除平台公告
     */
    @PreAuthorize("@ss.hasPermi('pos:TNoticeInfo:remove')")
    @Log(title = "平台公告", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tNoticeInfoService.deleteTNoticeInfoByIds(ids));
    }
}
