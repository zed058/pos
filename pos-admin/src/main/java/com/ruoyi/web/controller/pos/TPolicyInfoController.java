package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TPolicyInfo;
import com.ruoyi.pos.web.service.ITPolicyInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 平台政策/商学院Controller
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@RestController
@RequestMapping("/pos/TPolicyInfo")
public class TPolicyInfoController extends BaseController
{
    @Autowired
    private ITPolicyInfoService tPolicyInfoService;

    /**
     * 查询平台政策/商学院列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TPolicyInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TPolicyInfo tPolicyInfo)
    {
        startPage();
        List<TPolicyInfo> list = tPolicyInfoService.selectTPolicyInfoList(tPolicyInfo);
        return getDataTable(list);
    }

    /**
     * 导出平台政策/商学院列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TPolicyInfo:export')")
    @Log(title = "平台政策/商学院", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TPolicyInfo tPolicyInfo)
    {
        List<TPolicyInfo> list = tPolicyInfoService.selectTPolicyInfoList(tPolicyInfo);
        ExcelUtil<TPolicyInfo> util = new ExcelUtil<TPolicyInfo>(TPolicyInfo.class);
        return util.exportExcel(list, "平台政策/商学院数据");
    }

    /**
     * 获取平台政策/商学院详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TPolicyInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tPolicyInfoService.selectTPolicyInfoById(id));
    }

    /**
     * 新增平台政策/商学院
     */
    @PreAuthorize("@ss.hasPermi('pos:TPolicyInfo:add')")
    @Log(title = "平台政策/商学院", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TPolicyInfo tPolicyInfo)
    {
        return toAjax(tPolicyInfoService.insertTPolicyInfo(tPolicyInfo));
    }

    /**
     * 修改平台政策/商学院
     */
    @PreAuthorize("@ss.hasPermi('pos:TPolicyInfo:edit')")
    @Log(title = "平台政策/商学院", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TPolicyInfo tPolicyInfo)
    {
        return toAjax(tPolicyInfoService.updateTPolicyInfo(tPolicyInfo));
    }

    /**
     * 删除平台政策/商学院
     */
    @PreAuthorize("@ss.hasPermi('pos:TPolicyInfo:remove')")
    @Log(title = "平台政策/商学院", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tPolicyInfoService.deleteTPolicyInfoByIds(ids));
    }
}
