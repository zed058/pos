package com.ruoyi.web.controller.pos;

import java.util.List;

import com.ruoyi.common.enums.GoodsStatusBean;
import com.ruoyi.common.enums.GoodsStatusEnum;
import com.ruoyi.pos.api.vo.user.UserInfoVo;
import com.ruoyi.pos.web.domain.TTerminalBrandInfo;
import com.ruoyi.pos.web.domain.TTerminalInfo;
import com.ruoyi.pos.web.webDto.TTerminalInfoWebDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TTerminalGoodsInfo;
import com.ruoyi.pos.web.service.ITTerminalGoodsInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品基础Controller
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
@RestController
@RequestMapping("/pos/TTerminalGoodsInfo")
public class TTerminalGoodsInfoController extends BaseController
{
    @Autowired
    private ITTerminalGoodsInfoService tTerminalGoodsInfoService;

    /**
     * 查询兑换商品列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsInfo:exchange')")
    @GetMapping("/exchange")
    @Transactional
    public TableDataInfo exchange(TTerminalGoodsInfo tTerminalGoodsInfo)
    {
        startPage();
        List<TTerminalGoodsInfo> list = tTerminalGoodsInfoService.queryTerminalAndExchange(tTerminalGoodsInfo);
        return getDataTable(list);
    }


    /**
     * 查询终端商品列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsInfo:terminal')")
    @GetMapping("/terminal")
    public TableDataInfo terminal(TTerminalGoodsInfo tTerminalGoodsInfo)
    {
        startPage();
        List<TTerminalGoodsInfo> list = tTerminalGoodsInfoService.queryTerminalAndExchange(tTerminalGoodsInfo);
        return getDataTable(list);
    }

    /**
     * 导出商品基础列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsInfo:export')")
    @Log(title = "商品基础", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TTerminalGoodsInfo tTerminalGoodsInfo)
    {
        List<TTerminalGoodsInfo> list = tTerminalGoodsInfoService.selectTTerminalGoodsInfoList(tTerminalGoodsInfo);
        ExcelUtil<TTerminalGoodsInfo> util = new ExcelUtil<TTerminalGoodsInfo>(TTerminalGoodsInfo.class);
        return util.exportExcel(list, "商品基础数据");
    }

    /**
     * 获取商品基础详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tTerminalGoodsInfoService.selectTTerminalGoodsInfoById(id));
    }

    /**
     * 新增商品基础
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsInfo:add')")
    @Log(title = "商品基础", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTerminalGoodsInfo tTerminalGoodsInfo)
    {
        return toAjax(tTerminalGoodsInfoService.insertTTerminalGoodsInfo(tTerminalGoodsInfo));
    }

    /**
     * 修改商品基础
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsInfo:edit')")
    @Log(title = "商品基础", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTerminalGoodsInfo tTerminalGoodsInfo)
    {
        return toAjax(tTerminalGoodsInfoService.updateTTerminalGoodsInfo(tTerminalGoodsInfo));
    }

    /**
     * 删除商品基础
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsInfo:remove')")
    @Log(title = "商品基础", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tTerminalGoodsInfoService.deleteTTerminalGoodsInfoByIds(ids));
    }

    /*查询商品品牌*/
    @GetMapping("queryBrand")
    @ResponseBody
    public List<TTerminalBrandInfo> queryBrand(){
        return tTerminalGoodsInfoService.queryBrand();
    }

    /*查询商品品牌*/
    @RequestMapping(value = "/queryGoodsStatus", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public List<GoodsStatusBean> queryGoodsStatus(){
        return  GoodsStatusEnum.toList();
    }


    /**
     * 新增用户商户终端关系
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsInfo:addTerminalSn')")
    @Log(title = "添加终端SN码", businessType = BusinessType.INSERT)
    @PostMapping("addTerminalSn")
    public AjaxResult insertList(@RequestBody TTerminalInfoWebDto dto)
    {
        return toAjax(tTerminalGoodsInfoService.insertList(dto));
    }

    /*已实名认证的用户*/
    @GetMapping("auditUserList")
    @ResponseBody
    public List<UserInfoVo> auditUserList()
    {
        List<UserInfoVo> userInfoVos = tTerminalGoodsInfoService.auditUserList();
        return userInfoVos;
    }

}
