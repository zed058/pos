package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TSelfPickupPointInfo;
import com.ruoyi.pos.web.service.ITSelfPickupPointInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品自提点Controller
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@RestController
@RequestMapping("/pos/TSelfPickupPointInfo")
public class TSelfPickupPointInfoController extends BaseController
{
    @Autowired
    private ITSelfPickupPointInfoService tSelfPickupPointInfoService;

    /**
     * 查询商品自提点列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TSelfPickupPointInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TSelfPickupPointInfo tSelfPickupPointInfo)
    {
        startPage();
        List<TSelfPickupPointInfo> list = tSelfPickupPointInfoService.selectTSelfPickupPointInfoList(tSelfPickupPointInfo);
        return getDataTable(list);
    }

    /**
     * 导出商品自提点列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TSelfPickupPointInfo:export')")
    @Log(title = "商品自提点", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TSelfPickupPointInfo tSelfPickupPointInfo)
    {
        List<TSelfPickupPointInfo> list = tSelfPickupPointInfoService.selectTSelfPickupPointInfoList(tSelfPickupPointInfo);
        ExcelUtil<TSelfPickupPointInfo> util = new ExcelUtil<TSelfPickupPointInfo>(TSelfPickupPointInfo.class);
        return util.exportExcel(list, "商品自提点数据");
    }

    /**
     * 获取商品自提点详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TSelfPickupPointInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tSelfPickupPointInfoService.selectTSelfPickupPointInfoById(id));
    }

    /**
     * 新增商品自提点
     */
    @PreAuthorize("@ss.hasPermi('pos:TSelfPickupPointInfo:add')")
    @Log(title = "商品自提点", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TSelfPickupPointInfo tSelfPickupPointInfo)
    {
        return toAjax(tSelfPickupPointInfoService.insertTSelfPickupPointInfo(tSelfPickupPointInfo));
    }

    /**
     * 修改商品自提点
     */
    @PreAuthorize("@ss.hasPermi('pos:TSelfPickupPointInfo:edit')")
    @Log(title = "商品自提点", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TSelfPickupPointInfo tSelfPickupPointInfo)
    {
        return toAjax(tSelfPickupPointInfoService.updateTSelfPickupPointInfo(tSelfPickupPointInfo));
    }

    /**
     * 删除商品自提点
     */
    @PreAuthorize("@ss.hasPermi('pos:TSelfPickupPointInfo:remove')")
    @Log(title = "商品自提点", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tSelfPickupPointInfoService.deleteTSelfPickupPointInfoByIds(ids));
    }
}
