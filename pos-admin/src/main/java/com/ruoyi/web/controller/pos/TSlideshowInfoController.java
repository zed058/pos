package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TSlideshowInfo;
import com.ruoyi.pos.web.service.ITSlideshowInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 引导页面Controller
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@RestController
@RequestMapping("/pos/TSlideshowInfo")
public class TSlideshowInfoController extends BaseController
{
    @Autowired
    private ITSlideshowInfoService tSlideshowInfoService;

    /**
     * 查询引导页面列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TSlideshowInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TSlideshowInfo tSlideshowInfo)
    {
        startPage();
        List<TSlideshowInfo> list = tSlideshowInfoService.selectTSlideshowInfoList(tSlideshowInfo);
        return getDataTable(list);
    }

    /**
     * 导出引导页面列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TSlideshowInfo:export')")
    @Log(title = "引导页面", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TSlideshowInfo tSlideshowInfo)
    {
        List<TSlideshowInfo> list = tSlideshowInfoService.selectTSlideshowInfoList(tSlideshowInfo);
        ExcelUtil<TSlideshowInfo> util = new ExcelUtil<TSlideshowInfo>(TSlideshowInfo.class);
        return util.exportExcel(list, "引导页面数据");
    }

    /**
     * 获取引导页面详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TSlideshowInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tSlideshowInfoService.selectTSlideshowInfoById(id));
    }

    /**
     * 新增引导页面
     */
    @PreAuthorize("@ss.hasPermi('pos:TSlideshowInfo:add')")
    @Log(title = "引导页面", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TSlideshowInfo tSlideshowInfo)
    {
        return toAjax(tSlideshowInfoService.insertTSlideshowInfo(tSlideshowInfo));
    }

    /**
     * 修改引导页面
     */
    @PreAuthorize("@ss.hasPermi('pos:TSlideshowInfo:edit')")
    @Log(title = "引导页面", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TSlideshowInfo tSlideshowInfo)
    {
        return toAjax(tSlideshowInfoService.updateTSlideshowInfo(tSlideshowInfo));
    }

    /**
     * 删除引导页面
     */
    @PreAuthorize("@ss.hasPermi('pos:TSlideshowInfo:remove')")
    @Log(title = "引导页面", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tSlideshowInfoService.deleteTSlideshowInfoByIds(ids));
    }
}
