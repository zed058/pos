package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TTerminalTransInfo;
import com.ruoyi.pos.web.service.ITTerminalTransInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 终端划拨记录Controller
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
@RestController
@RequestMapping("/pos/TTerminalTransInfo")
public class TTerminalTransInfoController extends BaseController
{
    @Autowired
    private ITTerminalTransInfoService tTerminalTransInfoService;

    /**
     * 查询终端划拨记录列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTerminalTransInfo tTerminalTransInfo)
    {
        startPage();
        List<TTerminalTransInfo> list = tTerminalTransInfoService.selectTTerminalTransInfoList(tTerminalTransInfo);
        return getDataTable(list);
    }

    /**
     * 导出终端划拨记录列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransInfo:export')")
    @Log(title = "终端划拨记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TTerminalTransInfo tTerminalTransInfo)
    {
        List<TTerminalTransInfo> list = tTerminalTransInfoService.selectTTerminalTransInfoList(tTerminalTransInfo);
        ExcelUtil<TTerminalTransInfo> util = new ExcelUtil<TTerminalTransInfo>(TTerminalTransInfo.class);
        return util.exportExcel(list, "终端划拨记录数据");
    }

    /**
     * 获取终端划拨记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tTerminalTransInfoService.selectTTerminalTransInfoById(id));
    }

    /**
     * 新增终端划拨记录
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransInfo:add')")
    @Log(title = "终端划拨记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTerminalTransInfo tTerminalTransInfo)
    {
        return toAjax(tTerminalTransInfoService.insertTTerminalTransInfo(tTerminalTransInfo));
    }

    /**
     * 修改终端划拨记录
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransInfo:edit')")
    @Log(title = "终端划拨记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTerminalTransInfo tTerminalTransInfo)
    {
        return toAjax(tTerminalTransInfoService.updateTTerminalTransInfo(tTerminalTransInfo));
    }

    /**
     * 删除终端划拨记录
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransInfo:remove')")
    @Log(title = "终端划拨记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tTerminalTransInfoService.deleteTTerminalTransInfoByIds(ids));
    }

}
