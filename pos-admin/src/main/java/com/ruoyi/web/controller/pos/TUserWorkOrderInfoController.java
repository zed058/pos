package com.ruoyi.web.controller.pos;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.pos.api.vo.user.WorkOrderVo;
import com.ruoyi.pos.web.domain.TUserWorkOrderInfo;
import com.ruoyi.pos.web.domain.TWordOrderTypeInfo;
import com.ruoyi.pos.web.service.ITUserWorkOrderInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 工单管理Controller
 *
 * @author ruoyi
 * @date 2021-11-12
 */
@RestController
@RequestMapping("/pos/TUserWorkOrderInfo")
public class TUserWorkOrderInfoController extends BaseController {
    @Autowired
    private ITUserWorkOrderInfoService tUserWorkOrderInfoService;

    /**
     * 查询工单管理列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserWorkOrderInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TUserWorkOrderInfo tUserWorkOrderInfo) {
        startPage();
        List<WorkOrderVo> list = tUserWorkOrderInfoService.queryTUserWorkOrderListApi(tUserWorkOrderInfo);
        return getDataTable(list);
    }

    /**
     * 导出工单管理列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserWorkOrderInfo:export')")
    @Log(title = "工单管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TUserWorkOrderInfo tUserWorkOrderInfo) {
        List<TUserWorkOrderInfo> list = tUserWorkOrderInfoService.selectTUserWorkOrderInfoList(tUserWorkOrderInfo);
        ExcelUtil<TUserWorkOrderInfo> util = new ExcelUtil<TUserWorkOrderInfo>(TUserWorkOrderInfo.class);
        return util.exportExcel(list, "工单管理数据");
    }

    /**
     * 获取工单管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserWorkOrderInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(tUserWorkOrderInfoService.selectTUserWorkOrderInfoById(id));
    }

    /**
     * 新增工单管理
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserWorkOrderInfo:add')")
    @Log(title = "工单管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TUserWorkOrderInfo tUserWorkOrderInfo) {
        return toAjax(tUserWorkOrderInfoService.insertTUserWorkOrderInfo(tUserWorkOrderInfo));
    }

    /**
     * 修改工单管理
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserWorkOrderInfo:edit')")
    @Log(title = "工单管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TUserWorkOrderInfo tUserWorkOrderInfo) {
        return toAjax(tUserWorkOrderInfoService.updateTUserWorkOrderInfo(tUserWorkOrderInfo));
    }

    /**
     * 删除工单管理
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserWorkOrderInfo:remove')")
    @Log(title = "工单管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(tUserWorkOrderInfoService.deleteTUserWorkOrderInfoByIds(ids));
    }
    // 工单类型

    @GetMapping("/queryWordorderTypeList")
    @ResponseBody
    public List<TWordOrderTypeInfo> queryWordorderTypeList() {
        return tUserWorkOrderInfoService.queryWordorderTypeList();
    }

}
