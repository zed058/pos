package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TGoldInfo;
import com.ruoyi.pos.web.service.ITGoldInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 交易金序列号Controller
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@RestController
@RequestMapping("/pos/TGoldInfo")
public class TGoldInfoController extends BaseController
{
    @Autowired
    private ITGoldInfoService tGoldInfoService;

    /**
     * 查询交易金序列号列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TGoldInfo tGoldInfo)
    {
        startPage();
        List<TGoldInfo> list = tGoldInfoService.selectTGoldInfoList(tGoldInfo);
        return getDataTable(list);
    }

    /**
     * 导出交易金序列号列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldInfo:export')")
    @Log(title = "交易金序列号", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TGoldInfo tGoldInfo)
    {
        List<TGoldInfo> list = tGoldInfoService.selectTGoldInfoList(tGoldInfo);
        ExcelUtil<TGoldInfo> util = new ExcelUtil<TGoldInfo>(TGoldInfo.class);
        return util.exportExcel(list, "交易金序列号数据");
    }

    /**
     * 获取交易金序列号详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tGoldInfoService.selectTGoldInfoById(id));
    }

    /**
     * 新增交易金序列号
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldInfo:add')")
    @Log(title = "交易金序列号", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TGoldInfo tGoldInfo)
    {
        return toAjax(tGoldInfoService.insertTGoldInfo(tGoldInfo));
    }

    /**
     * 修改交易金序列号
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldInfo:edit')")
    @Log(title = "交易金序列号", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TGoldInfo tGoldInfo)
    {
        return toAjax(tGoldInfoService.updateTGoldInfo(tGoldInfo));
    }

    /**
     * 删除交易金序列号
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldInfo:remove')")
    @Log(title = "交易金序列号", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tGoldInfoService.deleteTGoldInfoByIds(ids));
    }
}
