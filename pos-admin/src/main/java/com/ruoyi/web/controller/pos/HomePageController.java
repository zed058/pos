package com.ruoyi.web.controller.pos;


import com.ruoyi.api.controller.common.CommonsController;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.api.ApiResult;
import com.ruoyi.common.core.domain.api.CodeMsg;
import com.ruoyi.pos.web.service.HomePageService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author Administrator
 * @title: 首页统计图，统计数据页面
 * @projectName ruoyi
 * @description: TODO
 */
@RestController
@RequestMapping("/pos/HomePage")
public class HomePageController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(CommonsController.class);

    @Autowired
    private HomePageService homePageServicel;

    /**
     * 首页统计图头部数据
     */
    @GetMapping("/quantitySum")
    @RequestBody
    private AjaxResult quantitySum() {
        try {
            return AjaxResult.success(homePageServicel.quantitySum());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }


    /**
     * 用戶統計圖
     */
    @GetMapping("/queryUserChart")
    @RequestBody
    private AjaxResult queryUserChart() {
        try {
            return AjaxResult.success(homePageServicel.queryUserChart());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 订单统计图
     */
    @GetMapping("/queryOrderChart")
    @RequestBody
    private AjaxResult queryOrderChart() {
        try {
            return AjaxResult.success(homePageServicel.queryOrderChart());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

    /*采购商统计图*/
    @GetMapping("/queryOrderTerminalChart")
    @RequestBody
    private AjaxResult queryOrderTerminalChart() {
        try {
            return AjaxResult.success(homePageServicel.queryOrderTerminalChart());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

    /*兑换商统计图*/
    @GetMapping("/queryOrderExchangeChart")
    @RequestBody
    private AjaxResult queryOrderExchangeChart() {
        try {
            return AjaxResult.success(homePageServicel.queryOrderExchangeChart());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }
}