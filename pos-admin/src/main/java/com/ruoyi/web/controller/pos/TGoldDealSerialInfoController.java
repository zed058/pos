package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TGoldDealSerialInfo;
import com.ruoyi.pos.web.service.ITGoldDealSerialInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * (金币序列号Controller
 * 
 * @author ruoyi
 * @date 2021-11-20
 */
@RestController
@RequestMapping("/pos/TGoldDealSerialInfo")
public class TGoldDealSerialInfoController extends BaseController
{
    @Autowired
    private ITGoldDealSerialInfoService tGoldDealSerialInfoService;

    /**
     * 查询(金币序列号列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldDealSerialInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TGoldDealSerialInfo tGoldDealSerialInfo)
    {
        startPage();
        List<TGoldDealSerialInfo> list = tGoldDealSerialInfoService.selectTGoldDealSerialInfoList(tGoldDealSerialInfo);
        return getDataTable(list);
    }

    /**
     * 导出(金币序列号列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldDealSerialInfo:export')")
    @Log(title = "(金币序列号", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TGoldDealSerialInfo tGoldDealSerialInfo)
    {
        List<TGoldDealSerialInfo> list = tGoldDealSerialInfoService.selectTGoldDealSerialInfoList(tGoldDealSerialInfo);
        ExcelUtil<TGoldDealSerialInfo> util = new ExcelUtil<TGoldDealSerialInfo>(TGoldDealSerialInfo.class);
        return util.exportExcel(list, "(金币序列号数据");
    }

    /**
     * 获取(金币序列号详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldDealSerialInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tGoldDealSerialInfoService.selectTGoldDealSerialInfoById(id));
    }

    /**
     * 新增(金币序列号
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldDealSerialInfo:add')")
    @Log(title = "(金币序列号", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TGoldDealSerialInfo tGoldDealSerialInfo)
    {
        return toAjax(tGoldDealSerialInfoService.insertTGoldDealSerialInfo(tGoldDealSerialInfo));
    }

    /**
     * 修改(金币序列号
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldDealSerialInfo:edit')")
    @Log(title = "(金币序列号", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TGoldDealSerialInfo tGoldDealSerialInfo)
    {
        return toAjax(tGoldDealSerialInfoService.updateTGoldDealSerialInfo(tGoldDealSerialInfo));
    }

    /**
     * 删除(金币序列号
     */
    @PreAuthorize("@ss.hasPermi('pos:TGoldDealSerialInfo:remove')")
    @Log(title = "(金币序列号", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tGoldDealSerialInfoService.deleteTGoldDealSerialInfoByIds(ids));
    }
}
