package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TUserAddressInfo;
import com.ruoyi.pos.web.service.ITUserAddressInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户收货地址Controller
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@RestController
@RequestMapping("/pos/TUserAddressInfo")
public class TUserAddressInfoController extends BaseController
{
    @Autowired
    private ITUserAddressInfoService tUserAddressInfoService;

    /**
     * 查询用户收货地址列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserAddressInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TUserAddressInfo tUserAddressInfo)
    {
        startPage();
        List<TUserAddressInfo> list = tUserAddressInfoService.selectTUserAddressInfoList(tUserAddressInfo);
        return getDataTable(list);
    }

    /**
     * 导出用户收货地址列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserAddressInfo:export')")
    @Log(title = "用户收货地址", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TUserAddressInfo tUserAddressInfo)
    {
        List<TUserAddressInfo> list = tUserAddressInfoService.selectTUserAddressInfoList(tUserAddressInfo);
        ExcelUtil<TUserAddressInfo> util = new ExcelUtil<TUserAddressInfo>(TUserAddressInfo.class);
        return util.exportExcel(list, "用户收货地址数据");
    }

    /**
     * 获取用户收货地址详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserAddressInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tUserAddressInfoService.selectTUserAddressInfoById(id));
    }

    /**
     * 新增用户收货地址
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserAddressInfo:add')")
    @Log(title = "用户收货地址", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TUserAddressInfo tUserAddressInfo)
    {
        return toAjax(tUserAddressInfoService.insertTUserAddressInfo(tUserAddressInfo));
    }

    /**
     * 修改用户收货地址
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserAddressInfo:edit')")
    @Log(title = "用户收货地址", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TUserAddressInfo tUserAddressInfo)
    {
        return toAjax(tUserAddressInfoService.updateTUserAddressInfo(tUserAddressInfo));
    }

    /**
     * 删除用户收货地址
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserAddressInfo:remove')")
    @Log(title = "用户收货地址", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tUserAddressInfoService.deleteTUserAddressInfoByIds(ids));
    }
}
