package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TTerminalBindInfo;
import com.ruoyi.pos.web.service.ITTerminalBindInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 终端绑定临时（第三方杉德）Controller
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@RestController
@RequestMapping("/pos/TTerminalBindInfo")
public class TTerminalBindInfoController extends BaseController
{
    @Autowired
    private ITTerminalBindInfoService tTerminalBindInfoService;

    /**
     * 查询终端绑定临时（第三方杉德）列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalBindInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTerminalBindInfo tTerminalBindInfo)
    {
        startPage();
        List<TTerminalBindInfo> list = tTerminalBindInfoService.selectTTerminalBindInfoList(tTerminalBindInfo);
        return getDataTable(list);
    }

    /**
     * 导出终端绑定临时（第三方杉德）列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalBindInfo:export')")
    @Log(title = "终端绑定临时（第三方杉德）", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TTerminalBindInfo tTerminalBindInfo)
    {
        List<TTerminalBindInfo> list = tTerminalBindInfoService.selectTTerminalBindInfoList(tTerminalBindInfo);
        ExcelUtil<TTerminalBindInfo> util = new ExcelUtil<TTerminalBindInfo>(TTerminalBindInfo.class);
        return util.exportExcel(list, "终端绑定临时（第三方杉德）数据");
    }

    /**
     * 获取终端绑定临时（第三方杉德）详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalBindInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tTerminalBindInfoService.selectTTerminalBindInfoById(id));
    }

    /**
     * 新增终端绑定临时（第三方杉德）
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalBindInfo:add')")
    @Log(title = "终端绑定临时（第三方杉德）", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTerminalBindInfo tTerminalBindInfo)
    {
        return toAjax(tTerminalBindInfoService.insertTTerminalBindInfo(tTerminalBindInfo));
    }

    /**
     * 修改终端绑定临时（第三方杉德）
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalBindInfo:edit')")
    @Log(title = "终端绑定临时（第三方杉德）", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTerminalBindInfo tTerminalBindInfo)
    {
        return toAjax(tTerminalBindInfoService.updateTTerminalBindInfo(tTerminalBindInfo));
    }

    /**
     * 删除终端绑定临时（第三方杉德）
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalBindInfo:remove')")
    @Log(title = "终端绑定临时（第三方杉德）", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tTerminalBindInfoService.deleteTTerminalBindInfoByIds(ids));
    }
}
