package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TTerminalActivateSetInfo;
import com.ruoyi.pos.web.service.ITTerminalActivateSetInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 终端激活配置Controller
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@RestController
@RequestMapping("/pos/TTerminalActivateSetInfo")
public class TTerminalActivateSetInfoController extends BaseController
{
    @Autowired
    private ITTerminalActivateSetInfoService tTerminalActivateSetInfoService;

    /**
     * 查询终端激活配置列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalActivateSetInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTerminalActivateSetInfo tTerminalActivateSetInfo)
    {
        startPage();
        List<TTerminalActivateSetInfo> list = tTerminalActivateSetInfoService.selectTTerminalActivateSetInfoList(tTerminalActivateSetInfo);
        return getDataTable(list);
    }

    /**
     * 导出终端激活配置列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalActivateSetInfo:export')")
    @Log(title = "终端激活配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TTerminalActivateSetInfo tTerminalActivateSetInfo)
    {
        List<TTerminalActivateSetInfo> list = tTerminalActivateSetInfoService.selectTTerminalActivateSetInfoList(tTerminalActivateSetInfo);
        ExcelUtil<TTerminalActivateSetInfo> util = new ExcelUtil<TTerminalActivateSetInfo>(TTerminalActivateSetInfo.class);
        return util.exportExcel(list, "终端激活配置数据");
    }

    /**
     * 获取终端激活配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalActivateSetInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tTerminalActivateSetInfoService.selectTTerminalActivateSetInfoById(id));
    }

    /**
     * 新增终端激活配置
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalActivateSetInfo:add')")
    @Log(title = "终端激活配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTerminalActivateSetInfo tTerminalActivateSetInfo)
    {
        return toAjax(tTerminalActivateSetInfoService.insertTTerminalActivateSetInfo(tTerminalActivateSetInfo));
    }

    /**
     * 修改终端激活配置
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalActivateSetInfo:edit')")
    @Log(title = "终端激活配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTerminalActivateSetInfo tTerminalActivateSetInfo)
    {
        return toAjax(tTerminalActivateSetInfoService.updateTTerminalActivateSetInfo(tTerminalActivateSetInfo));
    }

    /**
     * 删除终端激活配置
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalActivateSetInfo:remove')")
    @Log(title = "终端激活配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tTerminalActivateSetInfoService.deleteTTerminalActivateSetInfoByIds(ids));
    }
}
