package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TOpinionInfo;
import com.ruoyi.pos.web.service.ITOpinionInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 意见反馈Controller
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@RestController
@RequestMapping("/pos/TOpinionInfo")
public class TOpinionInfoController extends BaseController
{
    @Autowired
    private ITOpinionInfoService tOpinionInfoService;

    /**
     * 查询意见反馈列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TOpinionInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TOpinionInfo tOpinionInfo)
    {
        startPage();
        List<TOpinionInfo> list = tOpinionInfoService.selectTOpinionInfoList(tOpinionInfo);
        return getDataTable(list);
    }

    /**
     * 导出意见反馈列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TOpinionInfo:export')")
    @Log(title = "意见反馈", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TOpinionInfo tOpinionInfo)
    {
        List<TOpinionInfo> list = tOpinionInfoService.selectTOpinionInfoList(tOpinionInfo);
        ExcelUtil<TOpinionInfo> util = new ExcelUtil<TOpinionInfo>(TOpinionInfo.class);
        return util.exportExcel(list, "意见反馈数据");
    }

    /**
     * 获取意见反馈详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TOpinionInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tOpinionInfoService.selectTOpinionInfoById(id));
    }

    /**
     * 新增意见反馈
     */
    @PreAuthorize("@ss.hasPermi('pos:TOpinionInfo:add')")
    @Log(title = "意见反馈", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TOpinionInfo tOpinionInfo)
    {
        return toAjax(tOpinionInfoService.insertTOpinionInfo(tOpinionInfo));
    }

    /**
     * 修改意见反馈
     */
    @PreAuthorize("@ss.hasPermi('pos:TOpinionInfo:edit')")
    @Log(title = "意见反馈", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TOpinionInfo tOpinionInfo)
    {
        return toAjax(tOpinionInfoService.updateTOpinionInfo(tOpinionInfo));
    }

    /**
     * 删除意见反馈
     */
    @PreAuthorize("@ss.hasPermi('pos:TOpinionInfo:remove')")
    @Log(title = "意见反馈", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tOpinionInfoService.deleteTOpinionInfoByIds(ids));
    }
}
