package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TOpinionImgInfo;
import com.ruoyi.pos.web.service.ITOpinionImgInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 意见反馈图片Controller
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@RestController
@RequestMapping("/pos/TOpinionImgInfo")
public class TOpinionImgInfoController extends BaseController
{
    @Autowired
    private ITOpinionImgInfoService tOpinionImgInfoService;

    /**
     * 查询意见反馈图片列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TOpinionImgInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TOpinionImgInfo tOpinionImgInfo)
    {
        startPage();
        List<TOpinionImgInfo> list = tOpinionImgInfoService.selectTOpinionImgInfoList(tOpinionImgInfo);
        return getDataTable(list);
    }

    /**
     * 导出意见反馈图片列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TOpinionImgInfo:export')")
    @Log(title = "意见反馈图片", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TOpinionImgInfo tOpinionImgInfo)
    {
        List<TOpinionImgInfo> list = tOpinionImgInfoService.selectTOpinionImgInfoList(tOpinionImgInfo);
        ExcelUtil<TOpinionImgInfo> util = new ExcelUtil<TOpinionImgInfo>(TOpinionImgInfo.class);
        return util.exportExcel(list, "意见反馈图片数据");
    }

    /**
     * 获取意见反馈图片详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TOpinionImgInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tOpinionImgInfoService.selectTOpinionImgInfoById(id));
    }

    /**
     * 新增意见反馈图片
     */
    @PreAuthorize("@ss.hasPermi('pos:TOpinionImgInfo:add')")
    @Log(title = "意见反馈图片", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TOpinionImgInfo tOpinionImgInfo)
    {
        return toAjax(tOpinionImgInfoService.insertTOpinionImgInfo(tOpinionImgInfo));
    }

    /**
     * 修改意见反馈图片
     */
    @PreAuthorize("@ss.hasPermi('pos:TOpinionImgInfo:edit')")
    @Log(title = "意见反馈图片", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TOpinionImgInfo tOpinionImgInfo)
    {
        return toAjax(tOpinionImgInfoService.updateTOpinionImgInfo(tOpinionImgInfo));
    }

    /**
     * 删除意见反馈图片
     */
    @PreAuthorize("@ss.hasPermi('pos:TOpinionImgInfo:remove')")
    @Log(title = "意见反馈图片", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tOpinionImgInfoService.deleteTOpinionImgInfoByIds(ids));
    }
}
