package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TOrderInfo;
import com.ruoyi.pos.web.service.ITOrderInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单Controller
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
@RestController
@RequestMapping("/pos/TOrderInfo")
public class TOrderInfoController extends BaseController
{
    @Autowired
    private ITOrderInfoService tOrderInfoService;

    /**
     * 查询订单列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TOrderInfo tOrderInfo)
    {
        startPage();
        List<TOrderInfo> list = tOrderInfoService.selectTOrderInfoList(tOrderInfo);
        return getDataTable(list);
    }

    /**
     * 导出订单列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderInfo:export')")
    @Log(title = "订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TOrderInfo tOrderInfo)
    {
        List<TOrderInfo> list = tOrderInfoService.selectTOrderInfoList(tOrderInfo);
        ExcelUtil<TOrderInfo> util = new ExcelUtil<TOrderInfo>(TOrderInfo.class);
        return util.exportExcel(list, "订单数据");
    }

    /**
     * 获取订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tOrderInfoService.selectTOrderInfoById(id));
    }

    /**
     * 新增订单
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderInfo:add')")
    @Log(title = "订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TOrderInfo tOrderInfo)
    {
        return toAjax(tOrderInfoService.insertTOrderInfo(tOrderInfo));
    }

    /**
     * 修改订单
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderInfo:edit')")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TOrderInfo tOrderInfo)
    {
        return toAjax(tOrderInfoService.updateTOrderInfo(tOrderInfo));
    }

    /**
     * 删除订单
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderInfo:remove')")
    @Log(title = "订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tOrderInfoService.deleteTOrderInfoByIds(ids));
    }

    /**
     * 订单发货
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderInfo:edit')")
    @Log(title = "订单", businessType = BusinessType.UPDATE)
    @PutMapping("/ship")
    public AjaxResult ship(@RequestBody TOrderInfo tOrderInfo)
    {
        return toAjax(tOrderInfoService.ship(tOrderInfo));
    }

    @GetMapping("/exchangeOrderList")
    @ResponseBody
    public TableDataInfo exchangeOrderList(TOrderInfo tOrderInfo){
        startPage();
        List<TOrderInfo> list = tOrderInfoService.exchangeOrderList(tOrderInfo);
        return  getDataTable(list);
    }
}
