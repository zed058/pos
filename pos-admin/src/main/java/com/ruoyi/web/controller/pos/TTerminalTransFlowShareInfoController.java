package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TTerminalTransFlowShareInfo;
import com.ruoyi.pos.web.service.ITTerminalTransFlowShareInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 第三方交易流水分润详情Controller
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@RestController
@RequestMapping("/pos/TTerminalTransFlowShareInfo")
public class TTerminalTransFlowShareInfoController extends BaseController
{
    @Autowired
    private ITTerminalTransFlowShareInfoService tTerminalTransFlowShareInfoService;

    /**
     * 查询第三方交易流水分润详情列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransFlowShareInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTerminalTransFlowShareInfo tTerminalTransFlowShareInfo)
    {
        startPage();
        List<TTerminalTransFlowShareInfo> list = tTerminalTransFlowShareInfoService.selectTTerminalTransFlowShareInfoList(tTerminalTransFlowShareInfo);
        return getDataTable(list);
    }

    /**
     * 导出第三方交易流水分润详情列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransFlowShareInfo:export')")
    @Log(title = "第三方交易流水分润详情", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TTerminalTransFlowShareInfo tTerminalTransFlowShareInfo)
    {
        startPage();
        List<TTerminalTransFlowShareInfo> list = tTerminalTransFlowShareInfoService.selectTTerminalTransFlowShareInfoList(tTerminalTransFlowShareInfo);
        ExcelUtil<TTerminalTransFlowShareInfo> util = new ExcelUtil<TTerminalTransFlowShareInfo>(TTerminalTransFlowShareInfo.class);
        return util.exportExcel(list, "终端交易流水分润详情");
    }

    /**
     * 获取第三方交易流水分润详情详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransFlowShareInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tTerminalTransFlowShareInfoService.selectTTerminalTransFlowShareInfoById(id));
    }

    /**
     * 新增第三方交易流水分润详情
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransFlowShareInfo:add')")
    @Log(title = "第三方交易流水分润详情", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTerminalTransFlowShareInfo tTerminalTransFlowShareInfo)
    {
        return toAjax(tTerminalTransFlowShareInfoService.insertTTerminalTransFlowShareInfo(tTerminalTransFlowShareInfo));
    }

    /**
     * 修改第三方交易流水分润详情
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransFlowShareInfo:edit')")
    @Log(title = "第三方交易流水分润详情", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTerminalTransFlowShareInfo tTerminalTransFlowShareInfo)
    {
        return toAjax(tTerminalTransFlowShareInfoService.updateTTerminalTransFlowShareInfo(tTerminalTransFlowShareInfo));
    }

    /**
     * 删除第三方交易流水分润详情
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalTransFlowShareInfo:remove')")
    @Log(title = "第三方交易流水分润详情", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tTerminalTransFlowShareInfoService.deleteTTerminalTransFlowShareInfoByIds(ids));
    }
}
