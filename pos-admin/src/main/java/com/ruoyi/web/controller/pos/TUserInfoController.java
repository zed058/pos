package com.ruoyi.web.controller.pos;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.enums.AuditStatusEnum;
import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.pos.web.TUserInfoBean;
import com.ruoyi.quartz.domain.SysJob;
import org.quartz.SchedulerException;
import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.service.ITUserInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户基础数据Controller
 * 
 * @author ruoyi
 * @date 2021-11-10
 */
@RestController
@RequestMapping("/pos/TUserInfo")
public class TUserInfoController extends BaseController
{
    @Autowired
    private ITUserInfoService tUserInfoService;

    /**
     * 查询用户基础数据列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TUserInfo tUserInfo)
    {
        startPage();
        List<TUserInfo> list = tUserInfoService.selectTUserInfoList(tUserInfo);
        return getDataTable(list);
    }

    /**
     * 查询用户基础数据列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:list')")
    @GetMapping("/listForTrans")
    public TableDataInfo listForTrans(TUserInfo tUserInfo)
    {
        startPage();
        List<TUserInfo> list = tUserInfoService.selectTUserInfoListForTrans(tUserInfo);
        return getDataTable(list);
    }

    /**
     * 导出用户基础数据列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:export')")
    @Log(title = "用户基础数据", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TUserInfo tUserInfo)
    {
        List<TUserInfo> list = tUserInfoService.selectTUserInfoList(tUserInfo);
        ExcelUtil<TUserInfo> util = new ExcelUtil<TUserInfo>(TUserInfo.class);
        return util.exportExcel(list, "用户基础数据数据");
    }

    /**
     * 获取用户基础数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") String userId)
    {
        return AjaxResult.success(tUserInfoService.selectTUserInfoByUserId(userId));
    }

    /**
     * 查询用户详情
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:query')")
    @GetMapping(value = "getUserDetail/{userId}")
    public AjaxResult getUserDetail(@PathVariable("userId") String userId)
    {
        return AjaxResult.success(tUserInfoService.selectTUserInfoByUserId(userId));
    }

    /**
     * 新增用户基础数据
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:add')")
    @Log(title = "用户基础数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TUserInfo tUserInfo)
    {
        return toAjax(tUserInfoService.insertTUserInfo(tUserInfo));
    }

    /**
     * 修改用户基础数据
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:edit')")
    @Log(title = "用户基础数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TUserInfo tUserInfo)
    {
        try {
            return toAjax(tUserInfoService.updateTUserInfo(tUserInfo));
        } catch (Exception e) {
            e.printStackTrace();
            return toAjax(0);
        }

    }

    /**
     * 删除用户基础数据
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:remove')")
    @Log(title = "用户基础数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable String[] userIds)
    {
        return toAjax(tUserInfoService.deleteTUserInfoByUserIds(userIds));
    }

    /**
     * 启用/禁用用户
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:edit')")
    @PutMapping("/changeUserStatus")
    public AjaxResult changeUserStatus(@RequestBody TUserInfo tUserInfo) {
        try {
            return toAjax(tUserInfoService.changeUserStatus(tUserInfo));
        } catch (Exception e) {
            return toAjax(0);
        }
    }

    /**
     * 查询会员等级列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:edit')")
    @PutMapping("/queryGradeInfoList")
    public AjaxResult queryGradeInfoList(@RequestBody TUserInfo tUserInfo){
        try {
            System.out.println(tUserInfo.getUserId());
            return AjaxResult.success(tUserInfoService.queryGradeInfoList());
        } catch (Exception e) {
            e.printStackTrace();
            return toAjax(0);
        }
    }

    /**
     * 变更用户团队
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:edit')")
    @PutMapping("/changeUserTeam")
    public AjaxResult changeUserTeam(@RequestBody TUserInfo tUserInfo) {
        try {
            return toAjax(tUserInfoService.changeUserTeam(tUserInfo));
        } catch (Exception e) {
            e.printStackTrace();
            return toAjax(0);
        }
    }

    /**
     * 实名认证通过
     * @param tUserInfo
     * @return
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:edit')")
    @PostMapping("/updateBankCard")
    public AjaxResult updateBankCard(@RequestBody TUserInfo tUserInfo) {
        try {
            return toAjax(tUserInfoService.updateBankCard(tUserInfo));
        } catch (Exception e) {
            e.printStackTrace();
            return toAjax(false);
        }
    }

    /**
     * 实名认证列表查询
     * @param tUserInfo
     * @return
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:check')")
    @GetMapping("/check")
    public TableDataInfo check(TUserInfo tUserInfo)
    {
        startPage();
        List<TUserInfo> list = tUserInfoService.selectTUserInfoCheckList(tUserInfo);
        return getDataTable(list);
    }

    /**
     * 实名认证列表导出
     * @param tUserInfo
     * @return
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:check')")
    @GetMapping("/checkExport")
    public AjaxResult checkExport(TUserInfo tUserInfo)
    {
        List<TUserInfo> list = tUserInfoService.selectTUserInfoCheckList(tUserInfo);
        List<TUserInfoBean> resultList = new ArrayList<TUserInfoBean>();
        list.stream().forEach(bean ->{
            TUserInfoBean result = new TUserInfoBean();
            BeanUtils.copyProperties(bean, result);
            resultList.add(result);
        });
        ExcelUtil<TUserInfoBean> util = new ExcelUtil<TUserInfoBean>(TUserInfoBean.class);
        return util.exportExcel(resultList, "实名认证数据");
    }

    /**
     * 实名认证通过
     * @param tUserInfo
     * @return
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:checkPass')")
    @PostMapping("/checkPass")
    public AjaxResult checkPass(@RequestBody TUserInfo tUserInfo) {
        try {
            return toAjax(tUserInfoService.checkPass(tUserInfo));
        } catch (Exception e) {
            e.printStackTrace();
            return toAjax(0);
        }
    }

    /**
     * 实名认证不通过
     * @param tUserInfo
     * @return
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:checkRefuse')")
    @PostMapping("/checkRefuse")
    public AjaxResult checkRefuse(@RequestBody TUserInfo tUserInfo) {
        try {
            return toAjax(tUserInfoService.checkRefuse(tUserInfo));
        } catch (Exception e) {
            e.printStackTrace();
            return toAjax(0);
        }
    }

    @PreAuthorize("@ss.hasPermi('pos:TUserInfo:list')")
    @GetMapping("/merchantList")
    public TableDataInfo merchantList(TUserInfo userInfo){
        startPage();
        List<TUserInfo> list = tUserInfoService.merchantList(userInfo);
        return getDataTable(list);
    }

    //查询用户状态下拉框
    @RequestMapping(value = "/queryUserStatus", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public AjaxResult queryUserStatus() {
        try {
            return AjaxResult.success(YesOrNoEnums.toList());
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("错了");
        }
    }
}
