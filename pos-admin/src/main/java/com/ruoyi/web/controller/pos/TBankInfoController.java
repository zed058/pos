package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TBankInfo;
import com.ruoyi.pos.web.service.ITBankInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 银行信息维护Controller
 * 
 * @author ruoyi
 * @date 2021-12-06
 */
@RestController
@RequestMapping("/pos/TBankInfo")
public class TBankInfoController extends BaseController
{
    @Autowired
    private ITBankInfoService tBankInfoService;

    /**
     * 查询银行信息维护列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TBankInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBankInfo tBankInfo)
    {
        startPage();
        List<TBankInfo> list = tBankInfoService.selectTBankInfoList(tBankInfo);
        return getDataTable(list);
    }

    /**
     * 导出银行信息维护列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TBankInfo:export')")
    @Log(title = "银行信息维护", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TBankInfo tBankInfo)
    {
        List<TBankInfo> list = tBankInfoService.selectTBankInfoList(tBankInfo);
        ExcelUtil<TBankInfo> util = new ExcelUtil<TBankInfo>(TBankInfo.class);
        return util.exportExcel(list, "银行信息维护数据");
    }

    /**
     * 获取银行信息维护详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TBankInfo:query')")
    @GetMapping(value = "/{bankId}")
    public AjaxResult getInfo(@PathVariable("bankId") String bankId)
    {
        return AjaxResult.success(tBankInfoService.selectTBankInfoByBankId(bankId));
    }

    /**
     * 新增银行信息维护
     */
    @PreAuthorize("@ss.hasPermi('pos:TBankInfo:add')")
    @Log(title = "银行信息维护", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBankInfo tBankInfo)
    {
        return toAjax(tBankInfoService.insertTBankInfo(tBankInfo));
    }

    /**
     * 修改银行信息维护
     */
    @PreAuthorize("@ss.hasPermi('pos:TBankInfo:edit')")
    @Log(title = "银行信息维护", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBankInfo tBankInfo)
    {
        return toAjax(tBankInfoService.updateTBankInfo(tBankInfo));
    }

    /**
     * 删除银行信息维护
     */
    @PreAuthorize("@ss.hasPermi('pos:TBankInfo:remove')")
    @Log(title = "银行信息维护", businessType = BusinessType.DELETE)
	@DeleteMapping("/{bankIds}")
    public AjaxResult remove(@PathVariable String[] bankIds)
    {
        return toAjax(tBankInfoService.deleteTBankInfoByBankIds(bankIds));
    }
}
