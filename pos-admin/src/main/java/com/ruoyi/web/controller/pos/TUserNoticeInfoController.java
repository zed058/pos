package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TUserNoticeInfo;
import com.ruoyi.pos.web.service.ITUserNoticeInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户阅读信息Controller
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@RestController
@RequestMapping("/pos/TUserNoticeInfo")
public class TUserNoticeInfoController extends BaseController
{
    @Autowired
    private ITUserNoticeInfoService tUserNoticeInfoService;

    /**
     * 查询用户阅读信息列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserNoticeInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TUserNoticeInfo tUserNoticeInfo)
    {
        startPage();
        List<TUserNoticeInfo> list = tUserNoticeInfoService.selectTUserNoticeInfoList(tUserNoticeInfo);
        return getDataTable(list);
    }

    /**
     * 导出用户阅读信息列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserNoticeInfo:export')")
    @Log(title = "用户阅读信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TUserNoticeInfo tUserNoticeInfo)
    {
        List<TUserNoticeInfo> list = tUserNoticeInfoService.selectTUserNoticeInfoList(tUserNoticeInfo);
        ExcelUtil<TUserNoticeInfo> util = new ExcelUtil<TUserNoticeInfo>(TUserNoticeInfo.class);
        return util.exportExcel(list, "用户阅读信息数据");
    }

    /**
     * 获取用户阅读信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserNoticeInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tUserNoticeInfoService.selectTUserNoticeInfoById(id));
    }

    /**
     * 新增用户阅读信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserNoticeInfo:add')")
    @Log(title = "用户阅读信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TUserNoticeInfo tUserNoticeInfo)
    {
        return toAjax(tUserNoticeInfoService.insertTUserNoticeInfo(tUserNoticeInfo));
    }

    /**
     * 修改用户阅读信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserNoticeInfo:edit')")
    @Log(title = "用户阅读信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TUserNoticeInfo tUserNoticeInfo)
    {
        return toAjax(tUserNoticeInfoService.updateTUserNoticeInfo(tUserNoticeInfo));
    }

    /**
     * 删除用户阅读信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserNoticeInfo:remove')")
    @Log(title = "用户阅读信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tUserNoticeInfoService.deleteTUserNoticeInfoByIds(ids));
    }
}
