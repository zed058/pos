package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TNavigationInfo;
import com.ruoyi.pos.web.service.ITNavigationInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 金刚导航栏Controller
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@RestController
@RequestMapping("/pos/TNavigationInfo")
public class TNavigationInfoController extends BaseController
{
    @Autowired
    private ITNavigationInfoService tNavigationInfoService;

    /**
     * 查询金刚导航栏列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TNavigationInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TNavigationInfo tNavigationInfo)
    {
        startPage();
        List<TNavigationInfo> list = tNavigationInfoService.selectTNavigationInfoList(tNavigationInfo);
        return getDataTable(list);
    }

    /**
     * 导出金刚导航栏列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TNavigationInfo:export')")
    @Log(title = "金刚导航栏", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TNavigationInfo tNavigationInfo)
    {
        List<TNavigationInfo> list = tNavigationInfoService.selectTNavigationInfoList(tNavigationInfo);
        ExcelUtil<TNavigationInfo> util = new ExcelUtil<TNavigationInfo>(TNavigationInfo.class);
        return util.exportExcel(list, "金刚导航栏数据");
    }

    /**
     * 获取金刚导航栏详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TNavigationInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tNavigationInfoService.selectTNavigationInfoById(id));
    }

    /**
     * 新增金刚导航栏
     */
    @PreAuthorize("@ss.hasPermi('pos:TNavigationInfo:add')")
    @Log(title = "金刚导航栏", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TNavigationInfo tNavigationInfo)
    {
        return toAjax(tNavigationInfoService.insertTNavigationInfo(tNavigationInfo));
    }

    /**
     * 修改金刚导航栏
     */
    @PreAuthorize("@ss.hasPermi('pos:TNavigationInfo:edit')")
    @Log(title = "金刚导航栏", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TNavigationInfo tNavigationInfo)
    {
        return toAjax(tNavigationInfoService.updateTNavigationInfo(tNavigationInfo));
    }

    /**
     * 删除金刚导航栏
     */
    @PreAuthorize("@ss.hasPermi('pos:TNavigationInfo:remove')")
    @Log(title = "金刚导航栏", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tNavigationInfoService.deleteTNavigationInfoByIds(ids));
    }
}
