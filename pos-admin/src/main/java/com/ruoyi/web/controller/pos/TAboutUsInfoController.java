package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TAboutUsInfo;
import com.ruoyi.pos.web.service.ITAboutUsInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 平台设置（关于我们）Controller
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@RestController
@RequestMapping("/pos/TAboutUsInfo")
public class TAboutUsInfoController extends BaseController
{
    @Autowired
    private ITAboutUsInfoService tAboutUsInfoService;

    /**
     * 查询平台设置（关于我们）列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TAboutUsInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TAboutUsInfo tAboutUsInfo)
    {
        startPage();
        List<TAboutUsInfo> list = tAboutUsInfoService.selectTAboutUsInfoList(tAboutUsInfo);
        return getDataTable(list);
    }





    /**
     * 新增平台设置（关于我们）
     */
    @PreAuthorize("@ss.hasPermi('pos:TAboutUsInfo:add')")
    @Log(title = "平台设置（关于我们）", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TAboutUsInfo tAboutUsInfo)
    {
        return toAjax(tAboutUsInfoService.insertTAboutUsInfo(tAboutUsInfo));
    }

    /**
     * 修改平台设置（关于我们）
     */
    @PreAuthorize("@ss.hasPermi('pos:TAboutUsInfo:edit')")
    @Log(title = "平台设置（关于我们）", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TAboutUsInfo tAboutUsInfo)
    {
        return toAjax(tAboutUsInfoService.updateTAboutUsInfo(tAboutUsInfo));
    }

    /**
     * 删除平台设置（关于我们）
     */
    @PreAuthorize("@ss.hasPermi('pos:TAboutUsInfo:remove')")
    @Log(title = "平台设置（关于我们）", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tAboutUsInfoService.deleteTAboutUsInfoByIds(ids));
    }
}
