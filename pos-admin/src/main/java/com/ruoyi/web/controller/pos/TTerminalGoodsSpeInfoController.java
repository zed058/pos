package com.ruoyi.web.controller.pos;

import java.util.List;

import com.ruoyi.pos.web.domain.TTerminalGoodsInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TTerminalGoodsSpeInfo;
import com.ruoyi.pos.web.service.ITTerminalGoodsSpeInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品规格Controller
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@RestController
@RequestMapping("/pos/TTerminalGoodsSpeInfo")
public class TTerminalGoodsSpeInfoController extends BaseController
{
    @Autowired
    private ITTerminalGoodsSpeInfoService tTerminalGoodsSpeInfoService;

    /**
     * 查询商品规格列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsSpeInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo)
    {
        startPage();
        List<TTerminalGoodsSpeInfo> list = tTerminalGoodsSpeInfoService.querySpeInfoList(tTerminalGoodsSpeInfo);
        return getDataTable(list);
    }

    /**
     * 导出商品规格列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsSpeInfo:export')")
    @Log(title = "商品规格", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo)
    {
        List<TTerminalGoodsSpeInfo> list = tTerminalGoodsSpeInfoService.selectTTerminalGoodsSpeInfoList(tTerminalGoodsSpeInfo);
        ExcelUtil<TTerminalGoodsSpeInfo> util = new ExcelUtil<TTerminalGoodsSpeInfo>(TTerminalGoodsSpeInfo.class);
        return util.exportExcel(list, "商品规格数据");
    }

    /**
     * 获取商品规格详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsSpeInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tTerminalGoodsSpeInfoService.selectTTerminalGoodsSpeInfoById(id));
    }

    /**
     * 新增商品规格
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsSpeInfo:add')")
    @Log(title = "商品规格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo)
    {
        return toAjax(tTerminalGoodsSpeInfoService.insertTTerminalGoodsSpeInfo(tTerminalGoodsSpeInfo));
    }

    /**
     * 修改商品规格
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsSpeInfo:edit')")
    @Log(title = "商品规格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo)
    {
        return toAjax(tTerminalGoodsSpeInfoService.updateTTerminalGoodsSpeInfo(tTerminalGoodsSpeInfo));
    }

    /**
     * 删除商品规格
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalGoodsSpeInfo:remove')")
    @Log(title = "商品规格", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tTerminalGoodsSpeInfoService.deleteTTerminalGoodsSpeInfoByIds(ids));
    }

    /*查询商品的名称*/
    @GetMapping("/queryTerminalGoods")
    public List<TTerminalGoodsInfo> queryTerminalGoods()
    {
        return tTerminalGoodsSpeInfoService.queryTerminalGoods();
    }

}
