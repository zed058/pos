package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TGradeTypeInfo;
import com.ruoyi.pos.web.service.ITGradeTypeInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * vip跨级分润，直营分润，类型配置Controller
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@RestController
@RequestMapping("/pos/TGradeTypeInfo")
public class TGradeTypeInfoController extends BaseController
{
    @Autowired
    private ITGradeTypeInfoService tGradeTypeInfoService;

    /**
     * 查询vip跨级分润，直营分润，类型配置列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TGradeTypeInfo:list')")
    @GetMapping("/list")
    public AjaxResult list(TGradeTypeInfo tGradeTypeInfo)
    {
        List<TGradeTypeInfo> list = tGradeTypeInfoService.selectTGradeTypeInfoList(tGradeTypeInfo);
        return AjaxResult.success(list);
    }

    /**
     * 导出vip跨级分润，直营分润，类型配置列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TGradeTypeInfo:export')")
    @Log(title = "vip跨级分润，直营分润，类型配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TGradeTypeInfo tGradeTypeInfo)
    {
        List<TGradeTypeInfo> list = tGradeTypeInfoService.selectTGradeTypeInfoList(tGradeTypeInfo);
        ExcelUtil<TGradeTypeInfo> util = new ExcelUtil<TGradeTypeInfo>(TGradeTypeInfo.class);
        return util.exportExcel(list, "vip跨级分润，直营分润，类型配置数据");
    }

    /**
     * 获取vip跨级分润，直营分润，类型配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TGradeTypeInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tGradeTypeInfoService.selectTGradeTypeInfoById(id));
    }

    /**
     * 新增vip跨级分润，直营分润，类型配置
     */
    @PreAuthorize("@ss.hasPermi('pos:TGradeTypeInfo:add')")
    @Log(title = "vip跨级分润，直营分润，类型配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TGradeTypeInfo tGradeTypeInfo)
    {
        return toAjax(tGradeTypeInfoService.insertTGradeTypeInfo(tGradeTypeInfo));
    }

    /**
     * 修改vip跨级分润，直营分润，类型配置
     */
    @PreAuthorize("@ss.hasPermi('pos:TGradeTypeInfo:edit')")
    @Log(title = "vip跨级分润，直营分润，类型配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TGradeTypeInfo tGradeTypeInfo)
    {
        return toAjax(tGradeTypeInfoService.updateTGradeTypeInfo(tGradeTypeInfo));
    }

    /**
     * 删除vip跨级分润，直营分润，类型配置
     */
    @PreAuthorize("@ss.hasPermi('pos:TGradeTypeInfo:remove')")
    @Log(title = "vip跨级分润，直营分润，类型配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tGradeTypeInfoService.deleteTGradeTypeInfoByIds(ids));
    }
}
