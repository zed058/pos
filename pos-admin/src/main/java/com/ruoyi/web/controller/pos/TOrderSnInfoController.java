package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TOrderSnInfo;
import com.ruoyi.pos.web.service.ITOrderSnInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单终端SN码（字）Controller
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
@RestController
@RequestMapping("/pos/TOrderSnInfo")
public class TOrderSnInfoController extends BaseController
{
    @Autowired
    private ITOrderSnInfoService tOrderSnInfoService;

    /**
     * 查询订单终端SN码（字）列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderSnInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TOrderSnInfo tOrderSnInfo)
    {
        startPage();
        List<TOrderSnInfo> list = tOrderSnInfoService.selectTOrderSnInfoList(tOrderSnInfo);
        return getDataTable(list);
    }

    /**
     * 导出订单终端SN码（字）列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderSnInfo:export')")
    @Log(title = "订单终端SN码（字）", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TOrderSnInfo tOrderSnInfo)
    {
        List<TOrderSnInfo> list = tOrderSnInfoService.selectTOrderSnInfoList(tOrderSnInfo);
        ExcelUtil<TOrderSnInfo> util = new ExcelUtil<TOrderSnInfo>(TOrderSnInfo.class);
        return util.exportExcel(list, "订单终端SN码（字）数据");
    }

    /**
     * 获取订单终端SN码（字）详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderSnInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tOrderSnInfoService.selectTOrderSnInfoById(id));
    }

    /**
     * 新增订单终端SN码（字）
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderSnInfo:add')")
    @Log(title = "订单终端SN码（字）", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TOrderSnInfo tOrderSnInfo)
    {
        return toAjax(tOrderSnInfoService.insertTOrderSnInfo(tOrderSnInfo));
    }

    /**
     * 修改订单终端SN码（字）
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderSnInfo:edit')")
    @Log(title = "订单终端SN码（字）", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TOrderSnInfo tOrderSnInfo)
    {
        return toAjax(tOrderSnInfoService.updateTOrderSnInfo(tOrderSnInfo));
    }

    /**
     * 删除订单终端SN码（字）
     */
    @PreAuthorize("@ss.hasPermi('pos:TOrderSnInfo:remove')")
    @Log(title = "订单终端SN码（字）", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tOrderSnInfoService.deleteTOrderSnInfoByIds(ids));
    }
}
