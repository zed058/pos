package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TUserTeamInfo;
import com.ruoyi.pos.web.service.ITUserTeamInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户团队关系Controller
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@RestController
@RequestMapping("/pos/TUserTeamInfo")
public class TUserTeamInfoController extends BaseController
{
    @Autowired
    private ITUserTeamInfoService tUserTeamInfoService;

    /**
     * 查询用户团队关系列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserTeamInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TUserTeamInfo tUserTeamInfo)
    {
        startPage();
        List<TUserTeamInfo> list = tUserTeamInfoService.selectTUserTeamInfoList(tUserTeamInfo);
        return getDataTable(list);
    }

    /**
     * 导出用户团队关系列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserTeamInfo:export')")
    @Log(title = "用户团队关系", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TUserTeamInfo tUserTeamInfo)
    {
        List<TUserTeamInfo> list = tUserTeamInfoService.selectTUserTeamInfoList(tUserTeamInfo);
        ExcelUtil<TUserTeamInfo> util = new ExcelUtil<TUserTeamInfo>(TUserTeamInfo.class);
        return util.exportExcel(list, "用户团队关系数据");
    }

    /**
     * 获取用户团队关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserTeamInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tUserTeamInfoService.selectTUserTeamInfoById(id));
    }

    /**
     * 新增用户团队关系
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserTeamInfo:add')")
    @Log(title = "用户团队关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TUserTeamInfo tUserTeamInfo)
    {
        return toAjax(tUserTeamInfoService.insertTUserTeamInfo(tUserTeamInfo));
    }

    /**
     * 修改用户团队关系
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserTeamInfo:edit')")
    @Log(title = "用户团队关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TUserTeamInfo tUserTeamInfo)
    {
        return toAjax(tUserTeamInfoService.updateTUserTeamInfo(tUserTeamInfo));
    }

    /**
     * 删除用户团队关系
     */
    @PreAuthorize("@ss.hasPermi('pos:TUserTeamInfo:remove')")
    @Log(title = "用户团队关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tUserTeamInfoService.deleteTUserTeamInfoByIds(ids));
    }
}
