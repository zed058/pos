package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TTerminalBrandInfo;
import com.ruoyi.pos.web.service.ITTerminalBrandInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商品品牌Controller
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@RestController
@RequestMapping("/pos/TTerminalBrandInfo")
public class TTerminalBrandInfoController extends BaseController
{
    @Autowired
    private ITTerminalBrandInfoService tTerminalBrandInfoService;

    /**
     * 查询商品品牌列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalBrandInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTerminalBrandInfo tTerminalBrandInfo)
    {
        startPage();
        List<TTerminalBrandInfo> list = tTerminalBrandInfoService.selectTTerminalBrandInfoList(tTerminalBrandInfo);
        return getDataTable(list);
    }

    /**
     * 导出商品品牌列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalBrandInfo:export')")
    @Log(title = "商品品牌", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TTerminalBrandInfo tTerminalBrandInfo)
    {
        List<TTerminalBrandInfo> list = tTerminalBrandInfoService.selectTTerminalBrandInfoList(tTerminalBrandInfo);
        ExcelUtil<TTerminalBrandInfo> util = new ExcelUtil<TTerminalBrandInfo>(TTerminalBrandInfo.class);
        return util.exportExcel(list, "商品品牌数据");
    }

    /**
     * 获取商品品牌详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalBrandInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tTerminalBrandInfoService.selectTTerminalBrandInfoById(id));
    }

    /**
     * 新增商品品牌
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalBrandInfo:add')")
    @Log(title = "商品品牌", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTerminalBrandInfo tTerminalBrandInfo)
    {
        return toAjax(tTerminalBrandInfoService.insertTTerminalBrandInfo(tTerminalBrandInfo));
    }

    /**
     * 修改商品品牌
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalBrandInfo:edit')")
    @Log(title = "商品品牌", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTerminalBrandInfo tTerminalBrandInfo)
    {
        return toAjax(tTerminalBrandInfoService.updateTTerminalBrandInfo(tTerminalBrandInfo));
    }

    /**
     * 删除商品品牌
     */
    @PreAuthorize("@ss.hasPermi('pos:TTerminalBrandInfo:remove')")
    @Log(title = "商品品牌", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tTerminalBrandInfoService.deleteTTerminalBrandInfoByIds(ids));
    }
}
