package com.ruoyi.web.controller.pos;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.web.domain.TRebateSetInfo;
import com.ruoyi.pos.web.service.ITRebateSetInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 基础配置Controller
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
@RestController
@RequestMapping("/pos/TRebateSetInfo")
public class TRebateSetInfoController extends BaseController
{
    @Autowired
    private ITRebateSetInfoService tRebateSetInfoService;

    /**
     * 查询基础配置列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TRebateSetInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TRebateSetInfo tRebateSetInfo)
    {
        startPage();
        List<TRebateSetInfo> list = tRebateSetInfoService.selectTRebateSetInfoList(tRebateSetInfo);
        return getDataTable(list);
    }

    /**
     * 导出基础配置列表
     */
    @PreAuthorize("@ss.hasPermi('pos:TRebateSetInfo:export')")
    @Log(title = "基础配置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TRebateSetInfo tRebateSetInfo)
    {
        List<TRebateSetInfo> list = tRebateSetInfoService.selectTRebateSetInfoList(tRebateSetInfo);
        ExcelUtil<TRebateSetInfo> util = new ExcelUtil<TRebateSetInfo>(TRebateSetInfo.class);
        return util.exportExcel(list, "基础配置数据");
    }

    /**
     * 获取基础配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('pos:TRebateSetInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tRebateSetInfoService.selectTRebateSetInfoById(id));
    }

    /**
     * 新增基础配置
     */
    @PreAuthorize("@ss.hasPermi('pos:TRebateSetInfo:add')")
    @Log(title = "基础配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TRebateSetInfo tRebateSetInfo)
    {
        return toAjax(tRebateSetInfoService.insertTRebateSetInfo(tRebateSetInfo));
    }

    /**
     * 修改基础配置
     */
    @PreAuthorize("@ss.hasPermi('pos:TRebateSetInfo:edit')")
    @Log(title = "基础配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TRebateSetInfo tRebateSetInfo)
    {
        return toAjax(tRebateSetInfoService.updateTRebateSetInfo(tRebateSetInfo));
    }

    /**
     * 删除基础配置
     */
    @PreAuthorize("@ss.hasPermi('pos:TRebateSetInfo:remove')")
    @Log(title = "基础配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tRebateSetInfoService.deleteTRebateSetInfoByIds(ids));
    }
}
