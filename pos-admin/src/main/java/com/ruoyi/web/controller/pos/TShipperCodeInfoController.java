package com.ruoyi.web.controller.pos;

import java.util.List;

import com.ruoyi.pos.web.domain.TShipperCodeInfo;
import com.ruoyi.pos.web.service.ITShipperCodeInfoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物流公司编码Controller
 * 
 * @author ruoyi
 * @date 2021-12-10
 */
@RestController
@RequestMapping("/system/info")
public class TShipperCodeInfoController extends BaseController
{
    @Autowired
    private ITShipperCodeInfoService tShipperCodeInfoService;

    /**
     * 查询物流公司编码列表
     */
    @PreAuthorize("@ss.hasPermi('system:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(TShipperCodeInfo tShipperCodeInfo)
    {
        startPage();
        List<TShipperCodeInfo> list = tShipperCodeInfoService.selectTShipperCodeInfoList(tShipperCodeInfo);
        return getDataTable(list);
    }

    /**
     * 导出物流公司编码列表
     */
    @PreAuthorize("@ss.hasPermi('system:info:export')")
    @Log(title = "物流公司编码", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TShipperCodeInfo tShipperCodeInfo)
    {
        List<TShipperCodeInfo> list = tShipperCodeInfoService.selectTShipperCodeInfoList(tShipperCodeInfo);
        ExcelUtil<TShipperCodeInfo> util = new ExcelUtil<TShipperCodeInfo>(TShipperCodeInfo.class);
        return util.exportExcel(list, "物流公司编码数据");
    }

    /**
     * 获取物流公司编码详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tShipperCodeInfoService.selectTShipperCodeInfoById(id));
    }

    /**
     * 新增物流公司编码
     */
    @PreAuthorize("@ss.hasPermi('system:info:add')")
    @Log(title = "物流公司编码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TShipperCodeInfo tShipperCodeInfo)
    {
        return toAjax(tShipperCodeInfoService.insertTShipperCodeInfo(tShipperCodeInfo));
    }

    /**
     * 修改物流公司编码
     */
    @PreAuthorize("@ss.hasPermi('system:info:edit')")
    @Log(title = "物流公司编码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TShipperCodeInfo tShipperCodeInfo)
    {
        return toAjax(tShipperCodeInfoService.updateTShipperCodeInfo(tShipperCodeInfo));
    }

    /**
     * 删除物流公司编码
     */
    @PreAuthorize("@ss.hasPermi('system:info:remove')")
    @Log(title = "物流公司编码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tShipperCodeInfoService.deleteTShipperCodeInfoByIds(ids));
    }
}
