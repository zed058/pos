package com.ruoyi.api.task;

import com.ruoyi.pos.api.service.task.IOrderTaskService;
import com.ruoyi.pos.web.domain.TOrderInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author luobo
 * @title: OrderTask
 * @projectName pos
 * @description: 订单处理定时任务
 * @date 2021-11-08 11:40:58
 */
@Component("orderTask")
public class OrderTask {
    private static final Logger log = LoggerFactory.getLogger(OrderTask.class);

    @Autowired
    private IOrderTaskService orderTaskService;

    /**
     * 关闭超时订单定时任务
     */
    @Scheduled()
    public void closeTimeoutOrdersTask() {
        try {
            log.info("****************关闭超时订单定时任务开始执行****************");
            //先查询出所有需要关闭的工单
            List<TOrderInfo> orderList = orderTaskService.queryCloseTimeoutOrders();
            if (null != orderList && orderList.size() > 0) {
                for (TOrderInfo order : orderList) {
                    try {
                        orderTaskService.closeTimeoutOrders(order.getId()); //进行关闭操作
                    } catch (Exception e) {
                        e.printStackTrace();
                        log.error(e.getMessage());
                    }
                }
            }
            log.info("****************关闭超时订单定时任务执行结束****************");
        } catch (Exception e) {
            e.printStackTrace();
            log.info("关闭超时订单定时任务执行异常：" + e.getMessage());
        }
    }
}
