package com.ruoyi.api.task;

import com.ruoyi.pos.api.service.task.IShandianbaoService;
import com.ruoyi.pos.web.domain.TTerminalBrandInfo;
import com.ruoyi.pos.web.domain.TUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Optional;

/**
 * @author luobo
 * @title: SandianbaoTask
 * @projectName pos
 * @description: 处理三点包相关业务逻辑
 * @date 2022-04-08 14:10:33
 */
@Component("shandianbaoTask")
@RequiredArgsConstructor
@Slf4j
public class ShandianbaoTask {
    private final IShandianbaoService sandianbaoService;

    //获取闪电宝注册商户号
    // 0 0/10 * * * ?     10分钟一次
    @Scheduled()
    @Async
    public void queryRegInfoList() {
        try {
            log.info("****************获取闪电宝注册商户号定时任务开始执行****************");
            TTerminalBrandInfo brand = sandianbaoService.queryTTerminalBrandInfo();
            if (null == brand) {
                log.info("系统还未维护闪电宝品牌信息");
                return;
            }
            List<TUserInfo> userList = sandianbaoService.queryUserList();
            Optional.ofNullable(userList).ifPresent(list ->{
                list.stream().forEach(user ->{
                    try {
                        log.info("******start*********当前用户userId：" + user.getUserId());
                        sandianbaoService.syncRegInfo(user, brand);
                        log.info("******end*********当前用户userId：" + user.getUserId());
                    } catch (Exception e) {
                        e.printStackTrace();
                        StringWriter sw = new StringWriter();
                        e.printStackTrace(new PrintWriter(sw, true));
                        log.error("获取闪电宝注册商户号失败，当前用户userId：" + user.getUserId() + "，异常信息：" + sw.toString());
                    }
                });
            });
            log.info("****************获取闪电宝注册商户号定时任务执行结束****************");
        } catch (Exception e) {
            e.printStackTrace();
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            log.error("获取闪电宝注册商户号定时任务程序异常" + "，异常信息：" + sw.toString());
        }
    }
    //获取闪电宝注册认证信息
    // 0 0 0/1 * * ?     一小时一次
    @Scheduled()
    @Async
    public void queryAuthInfoList() {
        try {
            log.info("****************获取闪电宝注册认证信息定时任务开始执行****************");
            TTerminalBrandInfo brand = sandianbaoService.queryTTerminalBrandInfo();
            List<TUserInfo> userList = sandianbaoService.queryUserList();
            Optional.ofNullable(userList).ifPresent(list ->{
                list.stream().forEach(user ->{
                    try {
                        log.info("******start*********当前用户userId：" + user.getUserId());
                        sandianbaoService.syncAuthInfo(user, brand);
                        log.info("******end*********当前用户userId：" + user.getUserId());
                    } catch (Exception e) {
                        e.printStackTrace();
                        StringWriter sw = new StringWriter();
                        e.printStackTrace(new PrintWriter(sw, true));
                        log.error("获取闪电宝注册商户号失败，当前用户userId：" + user.getUserId() + "，异常信息：" + sw.toString());
                    }
                });
            });
            log.info("****************获取闪电宝注册认证信息定时任务执行结束****************");
        } catch (Exception e) {
            e.printStackTrace();
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            log.error("获取闪电宝注册认证信息定时任务程序异常" + "，异常信息：" + sw.toString());
        }
    }
    //获取闪电宝交易流水
    // 0 0/5 * * * ?   5分钟一次
    @Scheduled()
    @Async
    public void queryTransInfoList() {
        try {
            log.info("****************获取闪电宝交易流水定时任务开始执行****************");
            TTerminalBrandInfo brand = sandianbaoService.queryTTerminalBrandInfo();
            if (null == brand) {
                log.info("系统还未维护闪电宝品牌信息");
                return;
            }
            sandianbaoService.syncTransInfo(brand);
            log.info("****************获取闪电宝交易流水定时任务执行结束****************");
        } catch (Exception e) {
            e.printStackTrace();
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            log.error("获取闪电宝交易流水定时任务程序异常" + "，异常信息：" + sw.toString());
        }
    }
}
