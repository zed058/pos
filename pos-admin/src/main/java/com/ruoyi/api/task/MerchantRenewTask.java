package com.ruoyi.api.task;

import com.ruoyi.pos.api.service.task.IMerchantRenewService;
import com.ruoyi.pos.web.domain.TTerminalBindInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * @author luobo
 * @title: MerchantRenewTask
 * @projectName pos
 * @description: 终端商户号更新定时任务
 * @date 2021-11-19 08:55:53
 */
@Component("merchantRenewTask")
public class MerchantRenewTask {
    private static final Logger log = LoggerFactory.getLogger(MerchantRenewTask.class);
    @Autowired
    private IMerchantRenewService merchantRenewService;

    /**
     * 更新终端商户号（杉德）
     */
    @Scheduled()
    public void renewTerminalBindStatus() {
        try {
            log.info("****************更新终端商户号定时任务开始执行****************");
            List<TTerminalBindInfo> list = merchantRenewService.queryTerminalBind();
            Optional.ofNullable(list).ifPresent(lists ->{
                lists.stream().forEach(bean ->{
                    merchantRenewService.renewTerminalBindStatus(bean);
                });
            });
            log.info("****************更新终端商户号定时任务执行结束****************");
        } catch (Exception e) {
            e.printStackTrace();
            log.info("更新终端商户号定时任务程序异常");
        }
    }
}
