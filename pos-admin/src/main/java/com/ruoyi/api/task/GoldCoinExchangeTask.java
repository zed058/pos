package com.ruoyi.api.task;

import com.ruoyi.pos.api.service.task.IGoldCoinExchangeService;
import com.ruoyi.pos.web.domain.TGoldInfo;
import com.ruoyi.pos.web.domain.TRebateSetInfo;
import com.ruoyi.pos.web.domain.TTerminalTransFlowInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * @author luobo
 * @title: GoldCoinExchangeTask
 * @projectName pos
 * @description: 金币兑换定时任务
 * @date 2021-11-06 16:31:48
 */
@Component("goldCoinExchangeTask")
public class GoldCoinExchangeTask {
    private static final Logger log = LoggerFactory.getLogger(GoldCoinExchangeTask.class);
    @Autowired
    private IGoldCoinExchangeService goldCoinExchangeService;
    /**
     * 金币兑换
     */
    @Scheduled()
    public void goldCoinExchange() {
        try {
            log.info("****************金币兑换定时任务开始执行****************");
            List<TGoldInfo> list = goldCoinExchangeService.queryGoldWaitConvertedList();
            Optional.ofNullable(list).ifPresent(lists ->{
                lists.stream().forEach(bean ->{
                    try {
                        goldCoinExchangeService.goldCoinExchange(bean);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            });
            log.info("****************金币兑换定时任务执行结束****************");
        } catch (Exception e) {
            e.printStackTrace();
            log.info("金币兑换定时任务程序异常");
        }
    }
}
