package com.ruoyi.api.task;

import com.ruoyi.pos.api.service.common.ICommonService;
import com.ruoyi.pos.api.service.task.ITransFlowService;
import com.ruoyi.pos.web.domain.TTerminalTransFlowInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Optional;

/**
 * @author luobo
 * @title: TransFlowTask
 * @projectName pos
 * @description: 第三方(付临门)交易流水逻辑处理定时任务
 * @date 2021-11-06 16:31:48
 */
@Component("transFlowTask")
public class TransFlowTask {
    private static final Logger log = LoggerFactory.getLogger(TransFlowTask.class);
    @Autowired
    private ITransFlowService transFlowService;
    /**
     * 处理第三方(付临门)交易流水（杉德）
     */
    @Scheduled()
    public void transFlow() {
        try {
            log.info("****************处理第三方(杉德畅刷)交易流水定时任务开始执行****************");
            List<TTerminalTransFlowInfo> list = transFlowService.queryTransFlow();
            Optional.ofNullable(list).ifPresent(lists ->{
                lists.stream().forEach(bean ->{
                    try {
                        log.info("******start*********当前交易流水号：" + bean.getOrderId() + "，当前终端号：" + bean.getSnCode());
                        transFlowService.handleLocalTransLogic(bean);
                        log.info("******end*********当前交易流水号：" + bean.getOrderId() + "，当前终端号：" + bean.getSnCode());
                    } catch (Exception e) {
                        e.printStackTrace();
                        StringWriter sw = new StringWriter();
                        e.printStackTrace(new PrintWriter(sw, true));
                        log.error("处理第三方(杉德畅刷)交易流水失败，第三方订单号：" + bean.getOrderId() + "，异常信息：" + sw.toString());
                    }
                });
            });
            log.info("****************处理第三方(杉德畅刷)交易流水定时任务执行结束****************");
        } catch (Exception e) {
            e.printStackTrace();
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            log.error("处理第三方(杉德畅刷)交易流水定时任务程序异常" + "，异常信息：" + sw.toString());
        }
    }
}
