package com.ruoyi.api.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author luobo
 * @title: SerialNumIssuanceTask
 * @projectName pos
 * @description: 序列号发放定时任务（已在交易流水定时任务处理）
 * @date 2021-11-06 16:31:48
 */
@Component("serialNumIssuanceTask")
public class SerialNumIssuanceTask {
    private static final Logger log = LoggerFactory.getLogger(SerialNumIssuanceTask.class);

    //发放交易金序列号
    /**
     * 发放交易金序列号
     */
    /*@Scheduled()
    public void transFeeserialNum() {
        try {
            log.info("****************发放交易金序列号定时任务开始执行****************");
            log.info("****************发放交易金序列号定时任务执行结束****************");
        } catch (Exception e) {
            e.printStackTrace();
            log.info("发放交易金序列号定时任务程序异常");
        }
    }*/

    //发放奖励金序列号
    /**
     * 发放奖励金序列号
     */
    /*@Scheduled()
    public void bonusSerialNum() {
        try {
            log.info("****************发放奖励金序列号定时任务开始执行****************");
            log.info("****************发放奖励金序列号定时任务执行结束****************");
        } catch (Exception e) {
            e.printStackTrace();
            log.info("发放奖励金序列号定时任务程序异常");
        }
    }*/

}
