package com.ruoyi.api.task;

import com.ruoyi.pos.api.service.task.IGoldCoinIssuanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author luobo
 * @title: GoldCoinIssuanceTask
 * @projectName pos
 * @description: 金币发放定时任务（已在交易流水定时任务处理）
 * @date 2021-11-06 16:31:48
 */
@Component("goldCoinIssuanceTask")
public class GoldCoinIssuanceTask {
    private static final Logger log = LoggerFactory.getLogger(GoldCoinIssuanceTask.class);
    @Autowired
    private IGoldCoinIssuanceService goldCoinIssuanceService;

    /**
     * 交易金金币发放
     */
    /*@Scheduled()
    public void transGoldCoinIssuance() {
        try {
            log.info("****************交易金金币发放定时任务开始执行****************");
            //查平台配置表
            //平台累计金额达到一定数目时发放，发放给最小的序列号
            goldCoinIssuanceService.transGoldCoinIssuance();
            log.info("****************交易金金币发放定时任务执行结束****************");
        } catch (Exception e) {
            e.printStackTrace();
            log.info("金币发放定时任务程序异常");
        }
    }*/

    /**
     * 奖励金金币发放
     */
    /*@Scheduled()
    public void bonusGoldCoinIssuance() {
        try {
            log.info("****************奖励金金币发放定时任务开始执行****************");
            //平台累计金额达到一定数目时发放，发放给最小的序列号
            log.info("****************奖励金金币发放定时任务执行结束****************");
        } catch (Exception e) {
            e.printStackTrace();
            log.info("金币发放定时任务程序异常");
        }
    }*/
}
