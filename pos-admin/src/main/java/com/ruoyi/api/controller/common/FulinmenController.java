package com.ruoyi.api.controller.common;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.annotation.RateLimiter;
import com.ruoyi.common.utils.fulinmen.ReturnData;
import com.ruoyi.pos.api.service.common.IFulinmenService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author luobo
 * @title: FulinmenController
 * @projectName pos
 * @description: 付临门接口定义类
 * @date 2022-06-13 08:43:59
 */
@RestController
@RequestMapping("/api/common")
@Slf4j
@RequiredArgsConstructor
public class FulinmenController {

    private final IFulinmenService fulinmenService;

    /**
     * 接收对方推送的实时交易数据
     * @param body
     * @return
     */
    @PostMapping("/realtimeTrans")
    @RateLimiter(time = 1, count = 10)
    public String realtimeTrans(@RequestBody String body) {
        ReturnData returnData = new ReturnData();
        log.info("transaction ----实时交易未解密数据:" + body);
        try{
            returnData = fulinmenService.realtimeTrans(body);
        }catch (Exception e) {
            log.error("实时交易解密异常!");
            e.printStackTrace();
        }
        return JSON.toJSONString(returnData);
    }

    /**
     * 终端绑定
     * @param body
     * @return
     */
    @PostMapping("/terminal")
    public String terminal(@RequestBody String body) {
        ReturnData returnData = new ReturnData();
        log.info("terminal ----终端绑定未解密数据:" + body);
        try{
            //final String str = PublicKeySecurityUtil.decript(body,des3Key,publicKey,sha1Key);
            //log.info("终端绑定解密数据:" + str);
        }catch (Exception e) {
            log.error("终端绑定解密异常!");
            e.printStackTrace();
        }
        return JSON.toJSONString(returnData);
    }

    /**
     * 心跳验证
     * @param body
     * @return
     */
    @PostMapping("/check")
    public String check(@RequestBody String body) {
        log.info("check ----心跳验证未解密数据：{}", body);
        ReturnData returnData = new ReturnData();
        try{
            returnData = fulinmenService.realtimeTrans(body);
        } catch (Exception e) {
            log.error("心跳验证解密异常!");
            e.printStackTrace();
        }
        return JSON.toJSONString(returnData);
    }

    /**
     * 全量交易
     * @param body
     * @return
     */
    @PostMapping("/fullTransaction")
    public String fullTransaction(@RequestBody String body) {
        ReturnData returnData = new ReturnData();
        log.info("fullTransaction--------------全量未解密数据 :" + body);
        try{
            //final String str = PublicKeySecurityUtil.decript(body,des3Key,publicKey,sha1Key);
            //log.info("全量解密数据:" + str);
        }catch (Exception e) {
            log.error("全量解密异常!");
            e.printStackTrace();
        }
        return JSON.toJSONString(returnData);
    }
}
