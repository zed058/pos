package com.ruoyi.api.controller.user;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.api.ApiResult;
import com.ruoyi.common.core.domain.api.CodeMsg;
import com.ruoyi.pos.api.dto.user.TerminalManageDto;
import com.ruoyi.pos.api.service.user.ITerminalManageService;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author luobo
 * @title: TerminalManageController
 * @projectName pos
 * @description: 终端管理controller
 * @date 2021-11-09 13:43:49
 */
@RestController
@RequestMapping("/api/terminalManage")
public class TerminalManageController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(TerminalManageController.class);
    @Autowired
    ITerminalManageService terminalManageService;

    //查询库存管理/库存记录-头信息
    @RequestMapping(value = "/queryInventoryRecord", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryInventoryRecord(@RequestBody TerminalManageDto terminalManageDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, terminalManageService.queryInventoryRecord(terminalManageDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询库存管理/库存记录-头信息失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询库存管理/库存记录-头信息失败：" + e.getMessage());
        }
    }

    //查询库存管理-根据品牌查询
    @RequestMapping(value = "/queryInventoryManagementByBrand", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryInventoryManagementByBrand(@RequestBody TerminalManageDto terminalManageDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, terminalManageService.queryInventoryManagementByBrand(terminalManageDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询库存管理-根据品牌查询失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询库存管理-根据品牌查询失败：" + e.getMessage());
        }
    }

    //查询库存记录-根据品牌查询
    @RequestMapping(value = "/queryInventoryRecordByBrand", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryInventoryRecordByBrand(@RequestBody TerminalManageDto terminalManageDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, terminalManageService.queryInventoryRecordByBrand(terminalManageDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询库存记录-根据品牌查询失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询库存记录-根据品牌查询失败：" + e.getMessage());
        }
    }

    //查询终端列表
    @RequestMapping(value = "/queryTerminalList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryTerminalList(@RequestBody TerminalManageDto terminalManageDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, getDataTableApi(terminalManageService.queryTerminalList(terminalManageDto)));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询终端列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询终端列表失败：" + e.getMessage());
        }
    }

    //划拨（划出）
    @RequestMapping(value = "/terminalTransferOut", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> terminalTransferOut(@RequestBody TerminalManageDto terminalManageDto) {
        try {
            terminalManageService.terminalTransferOut(terminalManageDto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("划拨（划出）失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "划拨（划出）失败：" + e.getMessage());
        }
    }

    //划拨（划回）
    @RequestMapping(value = "/terminalTransferBack", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> terminalTransferBack(@RequestBody TerminalManageDto terminalManageDto) {
        try {
            terminalManageService.terminalTransferBack(terminalManageDto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("划拨（划回）失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "划拨（划回）失败：" + e.getMessage());
        }
    }


}
