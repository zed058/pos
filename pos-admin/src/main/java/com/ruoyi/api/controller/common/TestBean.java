package com.ruoyi.api.controller.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;

/**
 * @author luobo
 * @title: TestBean
 * @projectName pos
 * @description: TODO
 * @date 2022-02-25 15:35:28
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TestBean {
    private String name;
    @Min(value = 20, message = "年龄不能小于20")
    private int age;
}
