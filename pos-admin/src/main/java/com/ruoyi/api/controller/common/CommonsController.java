package com.ruoyi.api.controller.common;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.credentials.http.HttpRequest;
import com.ruoyi.common.annotation.RateLimiter;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.api.ApiResult;
import com.ruoyi.common.core.domain.api.CodeMsg;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.common.utils.GetLocationUtil;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.json.JsonUtils;
import com.ruoyi.common.utils.kuaidiniao.KdApiUtil;
import com.ruoyi.common.utils.kuaidiniao.QueryOrderReq;
import com.ruoyi.common.utils.kuaidiniao.QueryOrderRes;
import com.ruoyi.common.utils.leshua.LeshuaApiBaseRequestBean;
import com.ruoyi.common.utils.sms.SmsUtil;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.weCat.PayUtil;
import com.ruoyi.framework.config.ServerConfig;
import com.ruoyi.pos.api.dto.BaseDto;
import com.ruoyi.pos.api.dto.common.*;
import com.ruoyi.pos.api.service.common.ICommonService;
import com.ruoyi.pos.api.vo.common.TAboutUsInfoVo;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author luobo
 * @title: CommonsController
 * @projectName pos
 * @description: 公共接口处理类
 * @date 2021-11-06 16:31:48
 */
@RestController
@RequestMapping("/api/common")
public class CommonsController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(CommonsController.class);
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private SmsUtil smsUtil;
    @Autowired
    private ICommonService commonService;
    @Autowired
    private Environment env;
    @Autowired
    private ServerConfig serverConfig;
    @Autowired
    KdApiUtil kdApiUtil;

    //查询会员等级列表-已测试
    @RequestMapping(value = "/queryGradeInfoList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryGradeInfoList(@RequestBody BaseDto baseDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, getDataTableApi(commonService.queryGradeInfoList(baseDto)));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询关于我们信息失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询关于我们信息失败：" + e.getMessage());
        }
    }

    //查询关于我们信息-已测试
    @RequestMapping(value = "/queryTAboutUsInfoInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<TAboutUsInfoVo> queryTAboutUsInfoInfo() {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.queryTAboutUsInfoInfo());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询关于我们信息失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询关于我们信息失败：" + e.getMessage());
        }
    }

    //查询终端品牌列表-已测试
    @RequestMapping(value = "/queryTerminalBrandList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryTerminalBrandList() {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.queryTerminalBrandList());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询终端品牌列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询终端品牌列表失败：" + e.getMessage());
        }
    }

    //查询终端商品/兑换商城商品列表-已测试
    @RequestMapping(value = "/queryTerminalGoodsList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryTerminalGoodsList(@RequestBody TerminalGoodsDto terminalGoodsDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, getDataTableApi(commonService.queryTerminalGoodsList(terminalGoodsDto)));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询终端型号列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询终端型号列表失败：" + e.getMessage());
        }
    }

    //查询终端商品详情信息-已测试
    @RequestMapping(value = "/queryTerminalGoodsDetail", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryTerminalGoodsDetail(@RequestBody TerminalGoodsDto terminalGoodsDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.queryTerminalGoodsDetail(terminalGoodsDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询终端商品列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询终端商品列表失败：" + e.getMessage());
        }
    }

    //查询金刚导航栏列表-已测试
    @RequestMapping(value = "/queryNavigationInfoList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryNavigationInfoList() {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.queryNavigationInfoList());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询金刚导航栏列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询金刚导航栏列表失败：" + e.getMessage());
        }
    }

    //根据经纬度获取城市信息-已测试
    @RequestMapping(value = "/queryLocationByLngAndLat", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<Map<String,Object>> queryLocationByLngAndLat(@RequestBody CommonDto commonDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS,  GetLocationUtil.getLocatin(commonDto.getLng() + "," + commonDto.getLat()));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "根据经纬度获取城市信息失败" + e.getMessage());
        }
    }

    //通用图片上传（支持多个）-已测试
    @PostMapping("/uploadFiles")
    @ResponseBody
    public ApiResult uploadFiles(MultipartFile[] files) {
        try {
            List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
            String filePath = RuoYiConfig.getUploadPath(); // 上传文件路径
            for (MultipartFile file : files) {
                Map<String, Object> map = new HashMap<String, Object>();
                String fileName = FileUploadUtils.upload(filePath, file); // 上传并返回新文件名称
                String url = serverConfig.getUrl() + fileName;
                map.put("fileName", fileName);
                map.put("url", url);
                list.add(map);
            }
            Map<String, Object> result = new HashMap<String, Object>();
            result.put("result", list);
            return ApiResult.success(CodeMsg.SUCCESS, result);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "图片上传失败：" + e.getMessage());
        }
    }

    //查询轮播图/广告展示列表-已测试
    @RequestMapping(value = "/queryTSlideshowInfoList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryTSlideshowInfoList(@RequestBody TSlideshowInfoDto slideshowInfoDto) {
        try {
            Map<String, Object> resultMap = commonService.queryTSlideshowInfoList(slideshowInfoDto);
            return ApiResult.success(CodeMsg.SUCCESS, resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询首页轮播图失败" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询首页轮播图失败" + e.getMessage());
        }
    }

    //查询平台政策/商学院列表-已测试
    @RequestMapping(value = "/queryPolicyInfoList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryPolicyInfoList(@RequestBody TPolicyInfoDto policyInfoDto) {
        try {
            Map<String, Object> resultMap = commonService.queryPolicyInfoList(policyInfoDto);
            return ApiResult.success(CodeMsg.SUCCESS, getDataTableApi(resultMap));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询终端规格失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询终端规格失败：" + e.getMessage());
        }
    }

    //查询自提点列表-已测试
    @RequestMapping(value = "/querySelfPickupPointList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> querySelfPickupPointList(@RequestBody CommonDto commonDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.querySelfPickupPointList(commonDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询自提点列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询自提点列表失败：" + e.getMessage());
        }
    }

    //查询快递物流
    @RequestMapping(value = "/queryExpressLogistics", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryExpressLogistics(@RequestBody CommonDto commonDto) {
        try {
            QueryOrderReq req = QueryOrderReq.builder().build();
            BeanUtils.copyProperties(commonDto, req);
            String ss = kdApiUtil.buildSystemParams(JsonUtils.pojoToJson(req), env.getProperty("kuaidiniao.query.dataType"), env.getProperty("kuaidiniao.query.url"), "2");
            return ApiResult.success(CodeMsg.SUCCESS, JsonUtils.jsonToPojo(ss, QueryOrderRes.class));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询快递物流失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询快递物流失败：" + e.getMessage());
        }
    }

    //获取手机验证码-已测试
    @RequestMapping(value = "/generateVerifyCode", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> generateVerifyCode(@RequestBody CommonDto commonDto) {
        try {
            String code = IdUtils.getRandomForSix();
            redisCache.setVerifyCode(commonDto.getUserPhone(), code);
            JSONObject jo = new JSONObject();
            jo.put("code", code);
            smsUtil.sendAliyunSms(commonDto.getUserPhone(), env.getProperty("sms.aliyun.template.verifyCode"), jo.toJSONString()); //发送短信验证码
            JSONObject result = new JSONObject();
            result.put("result", code);
            return ApiResult.success(CodeMsg.SUCCESS, result);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取手机验证码失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "获取手机验证码失败！");
        }
    }

    //校验手机验证码-已测试
    @RequestMapping(value = "/checkVerifyCode", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> checkVerifyCode(@RequestBody CommonDto commonDto) {
        try {
            String redisCode = redisCache.getVerifyCode(commonDto.getUserPhone());
            JSONObject jo = new JSONObject();
            //if (StringUtils.isNotEmpty(commonDto.getCode()) && commonDto.getCode().equals(redisCode)) {
                jo.put("result", "验证码校验通过！");
                return ApiResult.success(CodeMsg.SUCCESS_CLOSE, jo);
            /*} else {
                jo.put("result", "验证码已过期！");
                return ApiResult.error(CodeMsg.ERROR, jo);
            }*/
        } catch (Exception e) {
            e.printStackTrace();
            log.error("校验手机验证码失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "校验手机验证码失败！");
        }
    }

    //https://developers.weixin.qq.com/community/develop/doc/000c6a23010d582af6271283a5c800
    //https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=7_3&index=1
    //https://blog.csdn.net/x_san3/article/details/109848768
    //https://www.jb51.net/article/113362.htm
    //支付回调接口，微信支付成功后会自动调用
    /*@PostMapping(value = "/wxNotify")
    @ResponseBody
    public void wxNotify(HttpServletRequest request, HttpServletResponse response){
        log.info("ms pay callback message,status scaned");
        BufferedOutputStream out = null;
        try {
            String notityXml = PayUtil.getPostStr(request);
            log.info("微信支付返回结果：" + notityXml);
            String resXml = "";
            Map map = PayUtil.doXMLParse(notityXml);//回调返回的信息
            Map<String,String> validParams = PayUtil.paraFilter(map);//回调验签时需要去除sign和空值参数
            String prestr = PayUtil.createLinkString(validParams);//拼接参数
            String mysign = PayUtil.sign(prestr, env.getProperty("wx.paySignKey"), "utf-8").toUpperCase();
            System.out.println(mysign);
            if (mysign.equals((String) map.get("sign"))) {
                log.info("微信支付返回的签名信息：" + map.get("sign"));
                String returnCode = (String) map.get("return_code");
                if("SUCCESS".equals(returnCode)){
                    resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
                            + "<return_msg><![CDATA[OK]]></return_msg>" + "</xml>";
                }else{
                    resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
                            + "<return_msg><![CDATA[FAIL]]></return_msg>" + "</xml> ";
                }
                commonService.weCatPayNotify(map);
            } else { //验签不通过
                //返回的签名不正确
                resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
                        + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
                log.info("微信支付回调签名错误");
            }
            //缓冲输出流
            out = new BufferedOutputStream(response.getOutputStream());
            out.write(resXml.getBytes());
            out.flush();
        } catch (Exception e) {
            log.info("微信支付回调异常：" + e.getMessage());
            e.printStackTrace();
        } finally {
            if (null != out) {
                try {
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }*/

    @PostMapping(value = "/wxNotify")
    @ResponseBody
    @RateLimiter
    public void wxNotify(HttpServletRequest request, HttpServletResponse response){
        log.info("ms pay callback message,status scaned");
        try (
                BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
                ) {
            String notityXml = PayUtil.getPostStr(request);
            log.info("微信支付返回结果：" + notityXml);
            String resXml = "";
            Map map = PayUtil.doXMLParse(notityXml);//回调返回的信息
            Map<String,String> validParams = PayUtil.paraFilter(map);//回调验签时需要去除sign和空值参数
            String prestr = PayUtil.createLinkString(validParams);//拼接参数
            String mysign = PayUtil.sign(prestr, env.getProperty("wx.paySignKey"), "utf-8").toUpperCase();
            System.out.println(mysign);
            if (mysign.equals((String) map.get("sign"))) {
                log.info("微信支付返回的签名信息：" + map.get("sign"));
                String returnCode = (String) map.get("return_code");
                if("SUCCESS".equals(returnCode)){
                    resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
                            + "<return_msg><![CDATA[OK]]></return_msg>" + "</xml>";
                }else{
                    resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
                            + "<return_msg><![CDATA[FAIL]]></return_msg>" + "</xml> ";
                }
                commonService.weCatPayNotify(map);
            } else { //验签不通过
                //返回的签名不正确
                resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
                        + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
                log.info("微信支付回调签名错误");
            }
            //缓冲输出流
            out.write(resXml.getBytes());
            out.flush();
        } catch (Exception e) {
            log.info("微信支付回调异常：" + e.getMessage());
            e.printStackTrace();
        }
    }

    //终端交易结果通知，终端产生交易时，第三方平台调用
    @PostMapping(value = "/terminalTransResultNotify")
    @ResponseBody
    @RateLimiter(time = 1, count = 10)
    public JSONObject terminalTransResultNotify(@RequestParam(name = "sign") String sign, HttpServletRequest request){
        try {
            String bodyStr = getBodyStr(request);
            //log.info("terminalTransResultNotify接口获取的原始报文sign数据：" + sign);
            log.info("terminalTransResultNotify接口获取的原始报文数据：" + bodyStr);
            return commonService.terminalTransResultNotify(bodyStr, sign);
        } catch (Exception e) {
            log.info("终端交易结果通知回调异常：" + e.getMessage());
            e.printStackTrace();
            JSONObject result = new JSONObject();
            result.put("code", YesOrNoEnums.NO.getCode());
            result.put("desc", "程序校验失败！");
            return result;
        }
    }

    //新增/变更商户结果通知，新增/变更商户时，第三方平台调用
    @PostMapping(value = "/newMerchantNotify")
    @ResponseBody
    @RateLimiter(time = 2, count = 1)
    public JSONObject newMerchantNotify(@RequestParam(name = "sign") String sign, HttpServletRequest request){
        try {
            String bodyStr = getBodyStr(request);
            log.info("newMerchantNotify接口获取的原始报文sign数据：" + sign);
            log.info("newMerchantNotify接口获取的原始报文数据：" + bodyStr);
            return commonService.newMerchantNotify(bodyStr, sign);
        } catch (Exception e) {
            log.info("新增商户结果通知回调异常：" + e.getMessage());
            e.printStackTrace();
            JSONObject result = new JSONObject();
            result.put("code", YesOrNoEnums.NO.getCode());
            result.put("desc", "程序校验失败！");
            return result;
        }
    }

    //智强科技预制码交易结果通知，终端产生交易时，第三方平台调用
    @PostMapping(value = "/preCodeTransResultNotify")
    @ResponseBody
    @RateLimiter(time = 1, count = 10)
    public String preCodeTransResultNotify(HttpServletRequest request){
        try {
            return commonService.preCodeTransResultNotify(request);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            log.error("智强科技预制码交易结果通知，终端产生交易时，第三方平台调用异常" + "，异常信息：" + sw.toString());
            e.printStackTrace();
            return "程序校验失败！";
        }
    }

    //乐刷交易结果通知，终端产生交易时，第三方平台调用
    @PostMapping(value = "/leshuaTransResultNotify")
    @ResponseBody
    @RateLimiter(time = 1, count = 20)
    public String leshuaTransResultNotify(@RequestBody LeshuaApiBaseRequestBean bean){
        try {
            return commonService.leshuaTransResultNotify(bean);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            log.error("乐刷交易结果通知，终端产生交易时，第三方平台调用异常" + "，异常信息：" + sw.toString());
            e.printStackTrace();
            return "程序校验失败！";
        }
    }

    //乐刷激活交易结果通知，终端产生交易时，第三方平台调用
    @PostMapping(value = "/leshuaActiveResultNotify")
    @ResponseBody
    @RateLimiter(time = 1, count = 50)
    public String leshuaActiveResultNotify(@RequestBody LeshuaApiBaseRequestBean bean){
        try {
            return commonService.leshuaActiveResultNotify(bean);
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            log.error("乐刷激活交易结果通知，终端产生激活交易时，第三方平台调用异常" + "，异常信息：" + sw.toString());
            e.printStackTrace();
            return "程序校验失败！";
        }
    }

    //趣工宝注册接口
    @PostMapping(value = "/memberRegUrl")
    @ResponseBody
    public ApiResult<T> memberRegUrl(String userName, String userPhone, String idCardNo, String bankCardNo){
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.memberRegUrl(userName, userPhone, idCardNo, bankCardNo));
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResult.error(CodeMsg.ERROR, "趣工宝注册接口异常：" + e.getMessage());
        }
    }

    //趣工宝签约接口
    @PostMapping(value = "/signContract")
    @ResponseBody
    public ApiResult<T> signContract(String memberNo, String serverCallbackUrl, String pageCallbackUrl){
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.signContract(memberNo, serverCallbackUrl, pageCallbackUrl));
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResult.error(CodeMsg.ERROR, "趣工宝签约接口异常：" + e.getMessage());
        }
    }

    //趣工宝变更银行卡
    @PostMapping(value = "/updateBankCard")
    @ResponseBody
    public ApiResult<T> updateBankCard(String memberNo, String bankCardNo){
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.updateBankCard(memberNo, bankCardNo));
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResult.error(CodeMsg.ERROR, "趣工宝签约接口异常：" + e.getMessage());
        }
    }

    //趣工宝变更银行卡
    @PostMapping(value = "/getMemberInfo")
    @ResponseBody
    public ApiResult<T> getMemberInfo(String memberNo, String bankCardNo){
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.getMemberInfo(memberNo));
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResult.error(CodeMsg.ERROR, "趣工宝签约接口异常：" + e.getMessage());
        }
    }

    //趣工宝签约接口
    @PostMapping(value = "/queryContractStatus")
    @ResponseBody
    public ApiResult<T> queryContractStatus(String memberNo, String spCode){
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.queryContractStatus(memberNo, spCode));
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResult.error(CodeMsg.ERROR, "趣工宝签约接口异常：" + e.getMessage());
        }
    }

    //趣工宝提现接口
    @PostMapping(value = "/withdrawalForPay")
    @ResponseBody
    public ApiResult<T> withdrawalForPay(){
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.withdrawalForPay());
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResult.error(CodeMsg.ERROR, "趣工宝提现接口异常：" + e.getMessage());
        }
    }

    //趣工宝提现状态查询接口
    @PostMapping(value = "/withdrawalResultQuery")
    @ResponseBody
    public ApiResult<T> withdrawalResultQuery(){
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.withdrawalResultQuery());
        } catch (Exception e) {
            e.printStackTrace();
            return ApiResult.error(CodeMsg.ERROR, "趣工宝提现状态查询接口异常：" + e.getMessage());
        }
    }

    //趣工宝提现结果通知接口
    @PostMapping(value = "/withdrawalResultNotify")
    @ResponseBody
    @RateLimiter
    public JSONObject withdrawalResultNotify(@RequestBody WithdrawalResultNotifyDto dto){
        try {
            log.info("withdrawalResultNotify接口获取的原始报文数据：" + JsonUtils.pojoToJson(dto));
            return commonService.withdrawalResultNotify(dto);
        } catch (Exception e) {
            log.info("趣工宝提现结果通知接口异常：" + e.getMessage());
            e.printStackTrace();
            JSONObject result = new JSONObject();
            result.put("code", YesOrNoEnums.NO.getCode());
            result.put("desc", "程序校验失败！");
            return result;
        }
    }

    //趣工宝提现退票查询接口
    @PostMapping(value = "/withdrawalResultForRefund")
    @ResponseBody
    @RateLimiter
    public JSONObject withdrawalResultForRefund(@RequestBody WithdrawalRefundResultNotifyDto dto){
        try {
            log.info("withdrawalResultForRefund接口获取的原始报文数据：" + JsonUtils.pojoToJson(dto));
            return commonService.withdrawalResultForRefund(dto);
        } catch (Exception e) {
            log.info("趣工宝提现结果通知接口异常：" + e.getMessage());
            e.printStackTrace();
            JSONObject result = new JSONObject();
            result.put("code", YesOrNoEnums.NO.getCode());
            result.put("desc", "程序校验失败！");
            return result;
        }
    }

    //趣工宝签约结果通知接口
    @PostMapping(value = "/signContractResultNotify")
    @ResponseBody
    @RateLimiter
    public JSONObject signContractResultNotify(@RequestBody WithdrawalResultNotifyDto dto){
        try {
            log.info("signContractResultNotify：" + JsonUtils.pojoToJson(dto));
            return commonService.signContractResultNotify(dto);
        } catch (Exception e) {
            log.info("趣工宝提现结果通知接口异常：" + e.getMessage());
            e.printStackTrace();
            JSONObject result = new JSONObject();
            result.put("code", YesOrNoEnums.NO.getCode());
            result.put("desc", "程序校验失败！");
            return result;
        }
    }

    public String getBodyStr(HttpServletRequest request) throws Exception {
        BufferedReader bufferReader = new BufferedReader(request.getReader());
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = bufferReader.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

    //工单类型
    @RequestMapping(value = "/queryTWordOrderTypeInfoList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryTWordOrderTypeInfoList() {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.queryTWordOrderTypeInfoList());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询工单类型失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询工单类型失败：" + e.getMessage());
        }
    }


    //0交易金额/1奖励金--已测试
    @RequestMapping(value = "/queryTGoldRankingInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryTGoldRankingInfo(@RequestParam(name = "dealAwardType") String dealAwardType) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, getDataTable(commonService.queryTGoldRankingInfo(dealAwardType)));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询工单类型失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询工单类型失败：" + e.getMessage());
        }
    }

    //vip说明--已测试
    @RequestMapping(value = "/queryVipExplain", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryVipExplain() {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, getDataTable(commonService.queryVipExplain()));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询工单类型失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询工单类型失败：" + e.getMessage());
        }
    }

    //银行维护--已测试
    @RequestMapping(value = "/queryBankList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryBankList() {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, getDataTableApi(commonService.queryBankList()));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询银行失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询银行失败：" + e.getMessage());
        }
    }
    /*微信授权空白页域名*/
    @RequestMapping(value = "/getAuthorizationUrl", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> getAuthorizationUrl(@RequestBody Map<String,String> map) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.getAuthorizationUrl(map.get("url")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取静默授权地址失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "获取静默授权地址失败：" + e.getMessage());
        }
    }

    /*微信公众号签名授权*/
    @RequestMapping(value = "/getPermissionsToSign", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> getPermissionsToSign(@RequestBody Map<String,String> map) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.getPermissionsToSign(map.get("url")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取公众号权限签名失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "获取公众号权限签名失败：" + e.getMessage());
        }
    }

    /*code 换 openId*/
    @RequestMapping(value = "/queryWxOpenId", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryWxOpenId(@RequestBody Map<String,String> map) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, commonService.queryWxOpenId(map.get("code")));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取公众号权限签名失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "获取公众号权限签名失败：" + e.getMessage());
        }
    }




}
