package com.ruoyi.api.controller.user;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.api.ApiResult;
import com.ruoyi.common.core.domain.api.CodeMsg;
import com.ruoyi.pos.api.dto.user.UserDto;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * @author luobo
 * @title: WalletManageController
 * @projectName pos
 * @description: 钱包管理controller
 * @date 2021-11-09 13:51:37
 */
@RestController
@RequestMapping("/api/walletManage")
public class WalletManageController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(WalletManageController.class);

    //钱包首页查询
    @RequestMapping(value = "/qianbaoshouyechaxun", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> qianbaoshouyechaxun(@RequestBody UserDto userDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询库存管理失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询库存管理失败：" + e.getMessage());
        }
    }

    //查询收益明细
    @RequestMapping(value = "/chaxunshouyimingxi", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> chaxunshouyimingxi(@RequestBody UserDto userDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询库存管理失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询库存管理失败：" + e.getMessage());
        }
    }

    //提现
    @RequestMapping(value = "/tixian", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> tixian(@RequestBody UserDto userDto) {
        try {
            //提现成功，别忘记计算导师收益
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询库存管理失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询库存管理失败：" + e.getMessage());
        }
    }
}
