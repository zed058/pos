package com.ruoyi.api.controller.user;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.api.ApiResult;
import com.ruoyi.common.core.domain.api.CodeMsg;
import com.ruoyi.pos.api.dto.common.ExchangeDto;
import com.ruoyi.pos.api.dto.user.*;
import com.ruoyi.pos.api.service.user.IUserManageService;
import com.ruoyi.pos.api.utils.CommonUtils;
import com.ruoyi.pos.api.vo.user.UserNoticeVo;
import com.ruoyi.pos.api.vo.user.UserOpinionVo;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author luobo
 * @title: UserManageController
 * @projectName pos
 * @description: 用户管理
 * @date 2021-11-06 16:31:48
 */
@RestController
@RequestMapping("/api/userManage")
public class UserManageController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(UserManageController.class);
    @Autowired
    IUserManageService userManageService;
    @Autowired
    CommonUtils commonUtils;


    //查询用户信息-已测试
    @RequestMapping(value = "/queryUserInfoVo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryTUserInfoVo() {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.queryTUserInfoVo());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询用户信息失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询用户信息失败：" + e.getMessage());
        }
    }

    //保存用户信息（设置支付密码、实名认证）-已测试
    @RequestMapping(value = "/saveUserInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> saveUserInfo(@RequestBody UserDto userDto) {
        try {
            userManageService.saveUserInfo(userDto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存用户信息失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, e.getMessage());
        }
    }

    //保存收货地址信息-已测试
    @RequestMapping(value = "/saveUserAddressInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> saveUserAddressInfo(@RequestBody UserAddressDto userAddressDto) {
        try {
            userManageService.saveUserAddressInfo(userAddressDto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询收获地址列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询收获地址列表失败：" + e.getMessage());
        }
    }

    //查询收获地址列表//id查询 -已测试
    @RequestMapping(value = "/queryUserAddressList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryUserAddressList(@RequestBody UserAddressDto userAddressDto) {
        try {
            Map<String, Object> resultMap = userManageService.queryUserAddressList(userAddressDto);
            return ApiResult.success(CodeMsg.SUCCESS, resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询收获地址列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询收获地址列表失败：" + e.getMessage());
        }
    }

    //查询平台公告列表-已测试
    @RequestMapping(value = "/queryUserNoticeList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryUserNoticeList(@RequestBody UserNoticeDto userNoticeDto) {
        try {
            List<UserNoticeVo> list = userManageService.queryUserNoticeList(userNoticeDto);
            return ApiResult.success(CodeMsg.SUCCESS, getDataTableApi(list));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询平台公告列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询平台公告列表失败：" + e.getMessage());
        }
    }

    //查询平台公告详情-已测试
    @RequestMapping(value = "/queryUserNoticeDetail", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryUserNoticeDetail(@RequestBody UserNoticeDto userNoticeDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.queryUserNoticeDetail(userNoticeDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询平台公告列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询平台公告列表失败：" + e.getMessage());
        }
    }

    //发布意见-已测试
    @RequestMapping(value = "/saveUserOpinionInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> saveUserOpinionInfo(@RequestBody UserOpinionDto userOpinionDto) {
        try {
            userManageService.saveUserOpinionInfo(userOpinionDto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("发布意见失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "发布意见失败：" + e.getMessage());
        }
    }

    //查询意见列表-已测试
    @RequestMapping(value = "/queryUserOpinionList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryUserOpinionList(@RequestBody UserOpinionDto userOpinionDto) {
        try {
            List<UserOpinionVo> list = userManageService.queryUserOpinionList(userOpinionDto);
            return ApiResult.success(CodeMsg.SUCCESS, getDataTableApi(list));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询意见列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询意见列表失败：" + e.getMessage());
        }
    }

    //查询用我的团队/我的商户-已测试
    @RequestMapping(value = "/queryUserTeamList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryTeamUserList(@RequestBody UserTeamDto userTeamDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.queryTeamUserList(userTeamDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询用户团队列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询用户团队列表失败：" + e.getMessage());
        }
    }

    //查询兑换记录列表（兑换商城）
    @RequestMapping(value = "/queryExchangeRecordList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryExchangeRecordList(@RequestBody OrderDto orderDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, getDataTableApi(userManageService.queryExchangeRecordList(orderDto)));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询兑换记录列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询兑换记录列表失败" + e.getMessage());
        }
    }

    //查询交易金/奖励金序列号列表(商户奖励)
    @RequestMapping(value = "/queryGoldSerialNumberList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryGoldSerialNumberList(@RequestBody ExchangeDto exchangeDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.queryGoldSerialNumberList(exchangeDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询交易金/奖励金列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询交易金/奖励金列表失败" + e.getMessage());
        }
    }

    //校验支付密码-已测试
    @RequestMapping(value = "/checkPayPassword", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> checkPayPassword(@RequestBody UserDto userDto) {
        try {
            JSONObject jsonObject = userManageService.checkPayPassword(userDto);
            return ApiResult.success(CodeMsg.SUCCESS, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("校验支付密码失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "校验支付密码失败：" + e.getMessage());
        }
    }

    //查询划拨记录列表
    @RequestMapping(value = "/queryTerminalTransRecordList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryTerminalTransRecordList(@RequestBody TerminalTransRecordDto terminalTransRecordDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, getDataTableApi(userManageService.queryTerminalTransRecordList(terminalTransRecordDto)));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询划拨记录列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询划拨记录列表失败：" + e.getMessage());
        }
    }

    //划拨/划回（确认）
    @RequestMapping(value = "/terminalTransferConfirm", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> terminalTransferConfirm(@RequestBody TerminalManageDto terminalManageDto) {
        try {
            userManageService.terminalTransferConfirm(terminalManageDto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("划拨失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "划拨失败：" + e.getMessage());
        }
    }

    //划拨/划回（拒绝）
    @RequestMapping(value = "/terminalTransferRefuse", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> terminalTransferRefuse(@RequestBody TerminalManageDto terminalManageDto) {
        try {
            userManageService.terminalTransferRefuse(terminalManageDto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("划拨失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "划拨失败：" + e.getMessage());
        }
    }

    /*//查询我的等级
    @RequestMapping(value = "/checkPayPassword", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> checkPayPassword(@RequestBody UserDto userDto) {
        try {
            userManageService.checkPayPassword(userDto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("校验支付密码失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "校验支付密码失败：" + e.getMessage());
        }
    }

    //我的服务商
    @RequestMapping(value = "/checkPayPassword", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> checkPayPassword(@RequestBody UserDto userDto) {
        try {
            userManageService.checkPayPassword(userDto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("校验支付密码失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "校验支付密码失败：" + e.getMessage());
        }
    }*/

    //兑换商城我的余额
    @RequestMapping(value = "/userBalance", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> userBalance() {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.userBalance());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("余额查询失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "余额查询失败：" + e.getMessage());
        }
    }



    //(我的收益)收益明细
    @RequestMapping(value = "/queryMyIncome", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryMyIncome(@RequestBody UserDefaultDto userDefaultDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.queryMyIncome(userDefaultDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("我的收益（收益明细）：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询交易金/奖励金列表失败" + e.getMessage());
        }
    }


    //我的收益（我的金币详情）
    @RequestMapping(value = "/queryMyGoldDetails", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryMyGoldDetails(@RequestBody UserDefaultDto userDefaultDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.queryMyGoldDetails(userDefaultDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("我的收益（我的金币详情）：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "我的收益（我的金币详情）失败" + e.getMessage());
        }
    }

    //我的收益（我的积分详情）
    @RequestMapping(value = "/queryMyIntegralDetails", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryMyIntegralDetails(@RequestBody UserDefaultDto userDefaultDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.queryMyIntegralDetails(userDefaultDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("我的收益（我的积分详情）：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "我的收益（我的积分详情）失败" + e.getMessage());
        }
    }


    //我的收益（我的交易金序列号）
    @RequestMapping(value = "/queryMyDealAndGoldDetails", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryMyDealAndGoldDetails(@RequestBody UserDefaultDto userDefaultDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.queryMyDealAndGoldDetails(userDefaultDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("我的收益（我的交易金序列号）：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询我的收益（我的交易金序列号）失败" + e.getMessage());
        }
    }

    //我的收益（我的奖励金序列号）
    @RequestMapping(value = "/queryMyAwardAndGoldDetails", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryMyAwardAndGoldDetails(@RequestBody UserDefaultDto userDefaultDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.queryMyAwardAndGoldDetails(userDefaultDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("我的收益（我的奖励金序列号）：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询我的收益（我的奖励金序列号）失败" + e.getMessage());
        }
    }


    //我的收益（总收益分析）
    @RequestMapping(value = "/queryMyTotalRevenueDetails", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryMyTotalRevenueDetails(@RequestBody UserDefaultDto userDefaultDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.queryMyTotalRevenueDetails(userDefaultDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("我的收益（我的奖励金序列号）：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询我的收益（我的奖励金序列号）失败" + e.getMessage());
        }
    }

    //我的收益（收益明细）
    @RequestMapping(value = "/queryEarningsDetail", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryEarningsDetail(@RequestBody UserDefaultDto userDefaultDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.queryEarningsDetail(userDefaultDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("我的收益（收益明细）：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询我的收益（收益明细）失败" + e.getMessage());
        }
    }

    //用户等级
    @RequestMapping(value = "/queryUserVip", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryUserVip() {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, userManageService.queryUserVip());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("我的收益（收益明细）：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询我的收益（收益明细）失败" + e.getMessage());
        }
    }

    //用户提现
    @RequestMapping(value = "/userWithdrawDeposit", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> userWithdrawDeposit(@RequestBody UserDefaultDto dto) {
        try {
            userManageService.userWithdrawDeposit(dto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("提现失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "提现失败" + e.getMessage());
        }
    }

    //提现明细
    @RequestMapping(value = "/queryUserWithdrawDeposit", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryUserWithdrawDeposit(@RequestBody UserDefaultDto dto) {
        try {

            return ApiResult.success(CodeMsg.SUCCESS, getDataTableApi(userManageService.queryUserWithdrawDeposit(dto)));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("我的收益（收益明细）：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询我的收益（收益明细）失败" + e.getMessage());
        }
    }

    //趣工宝用户签约
    @RequestMapping(value = "/userSignContract", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> userSignContract(@RequestBody UserDto dto) {
        try {

            return ApiResult.success(CodeMsg.SUCCESS, userManageService.userSignContract(dto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("趣工包签约：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR,  e.getMessage());
        }
    }
}
