package com.ruoyi.api.controller.user;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.api.ApiResult;
import com.ruoyi.common.core.domain.api.CodeMsg;
import com.ruoyi.pos.api.service.user.IMainManageService;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author luobo
 * @title: MainManageController
 * @projectName pos
 * @description: 用户端首页
 * @date 2021-11-17 09:36:52
 */
@RestController
@RequestMapping("/api/mainManage")
public class MainManageController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(MainManageController.class);
    @Autowired
    IMainManageService mainManageService;

    //查询首页统计数据-已测试
    @RequestMapping(value = "/queryMainInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryMainInfo() {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, mainManageService.queryMainInfo());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询用户信息失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询用户信息失败：" + e.getMessage());
        }
    }

    //跳转闪电宝第三方注册页面
    @RequestMapping(value = "/toShandianbaoReg", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> toShandianbaoReg() {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, mainManageService.toShandianbaoReg());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询用户信息失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询用户信息失败：" + e.getMessage());
        }
    }
}
