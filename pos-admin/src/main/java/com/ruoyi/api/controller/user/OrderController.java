package com.ruoyi.api.controller.user;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.api.ApiResult;
import com.ruoyi.common.core.domain.api.CodeMsg;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.pos.api.dto.user.OrderDto;
import com.ruoyi.pos.api.service.user.IOrderService;
import com.ruoyi.pos.api.vo.user.OrderListVo;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author luobo
 * @title: OrderController
 * @projectName pos
 * @description: 【我的订单】controller
 * @date 2021-11-08 08:34:46
 */
@RestController
@RequestMapping("/api/orderManage")
public class OrderController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(UserManageController.class);
    @Autowired
    IOrderService orderService;

    //查询订单列表
    @RequestMapping(value = "/queryOrderList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryOrderList(@RequestBody OrderDto orderDto) {
        try {
            List<OrderListVo> list = orderService.queryOrderList(orderDto);
            return ApiResult.success(CodeMsg.SUCCESS, getDataTableApi(list));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询订单列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询订单列表失败" + e.getMessage());
        }
    }

    //保存订单详情
    @RequestMapping(value = "/saveOrderDetail", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> saveOrderDetail(@RequestBody OrderDto orderDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, orderService.saveOrderDetail(orderDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存订单详情失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "保存订单详情失败" + e.getMessage());
        }
    }

    //查询订单详情
    @RequestMapping(value = "/queryOrderDetail", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryOrderDetail(@RequestBody OrderDto orderDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, orderService.queryOrderDetail(orderDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询订单详情失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询订单详情失败" + e.getMessage());
        }
    }

    //订单支付
    @RequestMapping(value = "/orderPay", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> orderPay(@RequestBody OrderDto orderDto) {
        try {
            Map<String, Object> resultMap = orderService.orderPay(orderDto);
            if (StringUtils.isNotNull(resultMap.get("setPass"))){
                if (resultMap.get("setPass").equals(String.valueOf(CodeMsg.SETPASS.getCode()))){
                    return ApiResult.success(CodeMsg.SETPASS, "请设置支付密码");
                }
            }
            return ApiResult.success(CodeMsg.SUCCESS, resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("订单支付失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "订单支付失败" + e.getMessage());
        }
    }

    //取消订单
    @RequestMapping(value = "/cancelOrder", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> cancelOrder(@RequestBody OrderDto orderDto) {
        try {
            orderService.cancelOrder(orderDto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("取消订单失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "取消订单失败" + e.getMessage());
        }
    }

    //确认收货
    @RequestMapping(value = "/confirmReceipt", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<OrderListVo> confirmReceipt(@RequestBody OrderDto orderDto) {
        try {
            orderService.confirmReceipt(orderDto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("确认收货失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "确认收货失败" + e.getMessage());
        }
    }

    //查询订单详情
    @RequestMapping(value = "/queryConfirmOrder", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryConfirmOrder(@RequestBody OrderDto orderDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, orderService.queryConfirmOrder(orderDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询订单详情失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询订单详情失败" + e.getMessage());
        }
    }

}
