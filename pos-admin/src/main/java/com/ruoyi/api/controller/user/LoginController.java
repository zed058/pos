package com.ruoyi.api.controller.user;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.api.ApiResult;
import com.ruoyi.common.core.domain.api.CodeMsg;
import com.ruoyi.pos.api.dto.user.UserDto;
import com.ruoyi.pos.api.service.user.ILoginService;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author luobo
 * @title: LoginController
 * @projectName pos
 * @description: 用户注册登录
 * @date 2021-11-06 16:31:48
 */
@RestController
@RequestMapping("/api/login")
public class LoginController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(LoginController.class);
    @Autowired
    ILoginService loginService;

    //用户注册-已测试
    @RequestMapping(value = "/userRegister", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> userRegister(@RequestBody UserDto userDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, loginService.userRegister(userDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("用户注册失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "用户注册失败：" + e.getMessage());
        }
    }

    //openId登录-已测试
    @RequestMapping(value = "/loginByOpenId", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> loginByOpenId(@RequestBody UserDto userDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, loginService.loginByOpenId(userDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("手机号登录失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "手机号登录失败：" + e.getMessage());
        }
    }

    //手机号登录-已测试
    @RequestMapping(value = "/loginByPhone", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> loginByPhone(@RequestBody UserDto userDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, loginService.loginByPhone(userDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("手机号登录失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, e.getMessage());
        }
    }

    //账号密码登录-已测试
    @RequestMapping(value = "/loginByAccount", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> loginByAccount(@RequestBody UserDto userDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, loginService.loginByAccount(userDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("账号密码登录失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "账号密码登录失败：" + e.getMessage());
        }
    }

    //退出登录-已测试
    @RequestMapping(value = "/loginOut", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> loginOut() {
        try {
            loginService.loginOut();
            JSONObject jo = new JSONObject();
            jo.put("result", "退出登录成功");
            return ApiResult.success(CodeMsg.SUCCESS, jo);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("仓库管理员退出登录失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "仓库管理员退出登录失败：" + e.getMessage());
        }
    }
}
