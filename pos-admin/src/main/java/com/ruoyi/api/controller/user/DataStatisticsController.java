package com.ruoyi.api.controller.user;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.api.ApiResult;
import com.ruoyi.common.core.domain.api.CodeMsg;
import com.ruoyi.pos.api.dto.user.DataStatisticsDto;
import com.ruoyi.pos.api.service.user.DataStatisticsService;
import com.ruoyi.pos.api.vo.user.DataStatisticsSumVo;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author luobo
 * @title: DataStatisticsController
 * @projectName pos
 * @description: 数据统计controller
 * @date 2021-11-09 15:02:58
 */
@RestController
@RequestMapping("/api/dataStatistics")
public class DataStatisticsController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(DataStatisticsController.class);

    @Autowired
    private DataStatisticsService dataStatisticsService;

    //数据统计图--已测试
    @RequestMapping(value = "/mainStatistic", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> mainStatistic(@RequestBody DataStatisticsDto tStatisticInfo) {
        try {
            Map<String, Object> stringObjectMap = dataStatisticsService.mainStatistic(tStatisticInfo);
            return ApiResult.success(CodeMsg.SUCCESS_MSG,stringObjectMap);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("数据统计图查询失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "数据统计图查询失败！");
        }
    }


    //数据统计--已测试
    @RequestMapping(value = "/dataStatisticsSum", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> dataStatisticsSum(@RequestBody DataStatisticsDto tStatisticInfo) {
        try {
            DataStatisticsSumVo statisticsSumVo = dataStatisticsService.queryTStatisticSum(tStatisticInfo);
            return ApiResult.success(CodeMsg.SUCCESS_MSG, statisticsSumVo);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("数据统计查询失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "数据统计查询失败！");
        }
    }

    //数据统计月维度日纬度--已测试
    @RequestMapping(value = "/queryTStatisticMonthAndDay", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryTStatisticMonthAndDay(@RequestBody DataStatisticsDto tStatisticInfo) {
        try {
            Map<String, Object> map = dataStatisticsService.queryTStatisticMonthAndDay(tStatisticInfo);
            return ApiResult.success(CodeMsg.SUCCESS_MSG, map);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("数据统计月维度日纬度查询失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "数据统计月维度日纬度查询失败！");
        }
    }

    //数据统计更多--已测试
    @RequestMapping(value = "/queryTStatisticMonthAndDayMore", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryTStatisticMonthAndDayMore(@RequestBody DataStatisticsDto tStatisticInfo) {
        try {
            Map<String, Object> map = dataStatisticsService.queryTStatisticMonthAndDayMore(tStatisticInfo);
            return ApiResult.success(CodeMsg.SUCCESS_MSG, map);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("统计更多失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "统计更多失败！");
        }
    }


}
