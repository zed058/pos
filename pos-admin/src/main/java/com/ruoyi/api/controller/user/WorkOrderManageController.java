package com.ruoyi.api.controller.user;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.api.ApiResult;
import com.ruoyi.common.core.domain.api.CodeMsg;
import com.ruoyi.pos.api.dto.BaseDto;
import com.ruoyi.pos.api.dto.user.WorkOrderDto;
import com.ruoyi.pos.api.service.user.IWorkOrderManageService;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author luobo
 * @title: WorkOrderManageController
 * @projectName pos
 * @description: 工单管理controller
 * @date 2021-11-09 14:04:26
 */
@RestController
@RequestMapping("/api/workOrderManage")
public class WorkOrderManageController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(WorkOrderManageController.class);
    @Autowired
    private IWorkOrderManageService workOrderManageService;

    //发布工单
    @RequestMapping(value = "/saveWorkOrderInfo", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> saveWorkOrderInfo(@RequestBody WorkOrderDto workOrderDto) {
        try {
            workOrderManageService.saveWorkOrderInfo(workOrderDto);
            return ApiResult.success(CodeMsg.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("发布工单失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "发布工单失败：" + e.getMessage());
        }
    }

    //查询工单详情
    @RequestMapping(value = "/queryWorkOrderInfoDeatil", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryWorkOrderInfoDeatil(@RequestBody WorkOrderDto workOrderDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, workOrderManageService.queryWorkOrderInfoDeatil(workOrderDto));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询工单详情失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询工单详情失败：" + e.getMessage());
        }
    }

    //查询工单列表
    @RequestMapping(value = "/queryWorkOrderInfoList", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ApiResult<T> queryWorkOrderInfoList(@RequestBody WorkOrderDto workOrderDto) {
        try {
            return ApiResult.success(CodeMsg.SUCCESS, getDataTableApi(workOrderManageService.queryWorkOrderInfoList(workOrderDto)));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询工单列表失败：" + e.getMessage());
            return ApiResult.error(CodeMsg.ERROR, "查询工单列表失败：" + e.getMessage());
        }
    }
}
