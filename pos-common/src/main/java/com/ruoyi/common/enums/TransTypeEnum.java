package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author luobo
 * @title: TransStatusEnum
 * @projectName pos
 * @description: 出入账类型枚举类
 * @date 2021-11-16 09:10:36
 */
public enum TransTypeEnum {
    YUE("10", "余额"),
    JIFEN("20", "积分"),
    JINBI("30", "金币"),
    JIAOYIJIN("40", "交易金序列号"),
    JIANGLIJIN("50", "奖励金序列号");

    private final String code;
    private final String value;

    TransTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static TransTypeEnum getEnumsByCode(String code) {
        for (TransTypeEnum enums : TransTypeEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
}
