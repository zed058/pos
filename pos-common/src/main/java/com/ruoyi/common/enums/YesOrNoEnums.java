package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author luobo
 * @title: YesOrNoEnums
 * @projectName pos
 * @description: 是否类型美枚举类
 * @date 2021-11-08 08:46:55
 */
public enum YesOrNoEnums {
    NO("1", "否", "禁用"),
    YES("0", "是", "正常");

    private final String code;
    private final String value;
    private final String userStatus;

    YesOrNoEnums(String code, String value, String userStatus) {
        this.code = code;
        this.value = value;
        this.userStatus = userStatus;
    }
    public static YesOrNoEnums getEnumsByCode(String code) {
        for (YesOrNoEnums enums : YesOrNoEnums.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }

    public static List<YesOrNoBean> toList() {
        List<YesOrNoBean> list = new ArrayList<YesOrNoBean>();
        for (YesOrNoEnums enums : YesOrNoEnums.values()) {
            YesOrNoBean bean = YesOrNoBean.builder()
                    .code(enums.getCode())
                    .value(enums.getValue())
                    .userStatus(enums.getUserStatus())
                    .build();
            list.add(bean);
        }
        return list;
    }
    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public String getUserStatus() {
        return userStatus;
    }
}
