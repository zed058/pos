package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author luobo
 * @title: ActivateStatusEnum
 * @projectName pos
 * @description: 终端激活状态枚举类
 * @date 2021-11-16 16:46:14
 */
public enum ActivateStatusEnum {
    ACTIVATED("1", "已激活", "已绑定"),
    INACTIVATED("0", "未激活", "未绑定");

    private final String code;
    private final String value;
    private final String bindValue;

    ActivateStatusEnum(String code, String value, String bindValue) {
        this.code = code;
        this.value = value;
        this.bindValue = bindValue;
    }
    public static ActivateStatusEnum getEnumsByCode(String code) {
        for (ActivateStatusEnum enums : ActivateStatusEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }

    public static List<AuditStatusBean> toList() {
        List<AuditStatusBean> list = new ArrayList<AuditStatusBean>();
        for (ActivateStatusEnum enums : ActivateStatusEnum.values()) {
            AuditStatusBean bean = AuditStatusBean.builder()
                    .code(enums.getCode())
                    .value(enums.getValue())
                    .show(enums.getBindValue())
                    .build();
            list.add(bean);
        }
        return list;
    }
    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public String getBindValue() {
        return bindValue;
    }
}
