package com.ruoyi.common.enums;

import lombok.Builder;
import lombok.Data;

/**
 * @author Administrator
 * @title: CodeValueBean
 * @projectName ruoyi
 * @description: TODO
 */
@Data
@Builder
public class GoodsStatusBean {
    private String code;
    private String value;
}
