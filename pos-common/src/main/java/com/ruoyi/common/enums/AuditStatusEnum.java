package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author luobo
 * @title: AuditStatusEnum
 * @projectName pos
 * @description: 审核状态枚举类
 * @date 2021-11-10 14:52:01
 */
public enum AuditStatusEnum {
    WAIT_AUDIT("0", "待审核","审核中","待签约"),
    PASS_AUDIT("1", "审核通过","提现成功","签约成功"),
    REFUSE_AUDIT("2", "审核驳回","提现失败","签约失败");

    private final String code;
    private final String value;
    private final String show;
    private final String sing;

    AuditStatusEnum(String code, String value ,String show, String sing) {
        this.code = code;
        this.value = value;
        this.show = show;
        this.sing = sing;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public String getShow() {
        return show;
    }

    public String getSing() {
        return sing;
    }

    public static AuditStatusEnum getEnumsByCode(String code) {
        for (AuditStatusEnum enums : AuditStatusEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
    public static List<AuditStatusBean> toList() {
        List<AuditStatusBean> list = new ArrayList<AuditStatusBean>();
        for (AuditStatusEnum enums : AuditStatusEnum.values()) {
            AuditStatusBean bean = AuditStatusBean.builder()
                    .code(enums.getCode())
                    .value(enums.getValue())
                    .show(enums.getShow())
                    .build();
            list.add(bean);
        }
        return list;
    }
}
