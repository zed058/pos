package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * @author luobo
 * @title: HandleStausEnum
 * @projectName pos
 * @description: 第三方交易流水处理结果
 * @date 2021-12-14 14:15:57
 */
public enum HandleStausEnum {
    NO_HANDLE("0", "未处理"),
    HAS_HANDLE("1", "已处理");

    private final String code;
    private final String value;

    HandleStausEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static HandleStausEnum getEnumsByCode(String code) {
        for (HandleStausEnum enums : HandleStausEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }

    public static Map<String, String> toMap() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (HandleStausEnum enums : HandleStausEnum.values()) {
            map.put(enums.getCode(), enums.getValue());
        }
        return map;
    }
}
