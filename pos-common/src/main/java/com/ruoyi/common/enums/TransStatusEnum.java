package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author luobo
 * @title: TransStatusEnum
 * @projectName pos
 * @description: 划拨状态枚举类
 * @date 2021-11-16 09:10:36
 */
public enum TransStatusEnum {
    WAIT_TRANS("10", "未划拨"),
    WAIT_CONFIIRM("20", "待确认"),
    HAS_CONFIIRM("30", "已确认"),
    HAS_REFUSE("40", "拒绝");

    private final String code;
    private final String value;

    TransStatusEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static TransStatusEnum getEnumsByCode(String code) {
        for (TransStatusEnum enums : TransStatusEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
}
