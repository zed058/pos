package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Administrator
 * @title: GoodsTypeEnum
 * @projectName ruoyi
 * @description: TODO
 */
public enum GoodsTypeEnum {
    TERMINAL("0", "终端商城"),
    EXCHSNGE("1", "兑换商城");

    private final String code;
    private final String value;

    GoodsTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static GoodsTypeEnum getEnumsByCode(String code) {
        for (GoodsTypeEnum enums : GoodsTypeEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
}
