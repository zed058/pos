package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @Author: Fan
 * @Description TODO
 * @Date: 2021/12/16 9:40
 */
public enum ReplyStateEnums {
    Not_SOLD("0", "回复"),
    SOLD("1", "未回复");

    private final String code;
    private final String value;

    ReplyStateEnums(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static ReplyStateEnums getEnumsByCode(String code) {
        for (ReplyStateEnums enums : ReplyStateEnums.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
}
