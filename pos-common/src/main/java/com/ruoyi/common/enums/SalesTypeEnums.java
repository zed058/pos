package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @Author: Fan
 * @Description TODO
 * @Date: 2021/12/10 14:22
 */
public enum SalesTypeEnums {
    Not_SOLD("0", "未出售"),
    SOLD("1", "已出售");

    private final String code;
    private final String value;

    SalesTypeEnums(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
