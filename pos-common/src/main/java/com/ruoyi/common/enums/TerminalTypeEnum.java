package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author luobo
 * @title: TerminalTypeEnum
 * @projectName pos
 * @description: 终端获取方式
 * @date 2021-11-23 14:14:00
 */
public enum TerminalTypeEnum {
    BUY("10", "线上购买"),
    TRANS("20", "终端划拨"),
    EXCHSNGE("30", "兑换终端");

    private final String code;
    private final String value;

    TerminalTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static TerminalTypeEnum getEnumsByCode(String code) {
        for (TerminalTypeEnum enums : TerminalTypeEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
}
