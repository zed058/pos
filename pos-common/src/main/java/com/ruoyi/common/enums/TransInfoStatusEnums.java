package com.ruoyi.common.enums;

/**
 * @Author: Fan
 * @Description TODO
 * @Date: 2021/12/14 10:06
 */
public enum TransInfoStatusEnums {
    TO_BE_CONFIRMED("0","待确认"),
    CONFIRMED("0","已确认"),
    REFUSE("0","拒绝");

    private final String code;
    private final String value;

    TransInfoStatusEnums(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
