package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @Author: Fan
 * @Description TODO
 * @Date: 2021/12/15 21:15
 */
public enum IsReadEnums {

    UNREAD("0","未读"),
    HAVEREAD("1","已读");

    private String code;
    private String value;

    IsReadEnums(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static IsReadEnums getEnumsByCode(String code) {
        for (IsReadEnums enums : IsReadEnums.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
}
