package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author luobo
 * @title: OrderStatusEnums
 * @projectName pos
 * @description: 订单状态枚举类
 * @date 2021-11-08 10:02:50
 */
public enum OrderStatusEnums {
    WAIT_PAY("10", "待支付"),//（待兑换）
    WAIT_DELIVERED("20", "待发货"),
    HAS_RECEIPT("30", "已发货"),//（已兑换）
    HAS_OVER("40", "已完成"),
    TIMEOUT_CLOSE("50", "已取消");//已关闭

    private final String code;
    private final String value;

    OrderStatusEnums(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static OrderStatusEnums getEnumsByCode(String code) {
        for (OrderStatusEnums enums : OrderStatusEnums.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
}
