package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author luobo
 * @title: OrderTypeEnum
 * @projectName pos
 * @description: 订单类型枚举类
 * @date 2021-11-15 09:59:52
 */
public enum OrderTypeEnum {
    TERMINALPURCHASE("10", "终端购买"),
    INTEGRALEXCHANGE("20", "积分兑换"),
    TRANEXCHANGE("21", "交易金兑换"),
    BONUSEXCHANGE("22", "奖励金兑换"),
    GOLDEXCHANGE("23", "金币兑换");

    private final String code;
    private final String value;

    OrderTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static OrderTypeEnum getEnumsByCode(String code) {
        for (OrderTypeEnum enums : OrderTypeEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
}
