package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 * @title: GoodsStatusEnum
 * @projectName ruoyi
 * @description: TODO
 */
public enum  GoodsStatusEnum {

    TERMINALPURCHASE("10", "采购"),
    INTEGRALEXCHANGE("20", "积分兑换"),
    TRANEXCHANGE("21", "交易金兑换"),
    BONUSEXCHANGE("22", "奖励金兑换"),
    GOLDEXCHANGE("23", "金币兑换");

    private final String code;
    private final String value;

    GoodsStatusEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static GoodsStatusEnum getGoodsStatusEnum(String code) {
        for (GoodsStatusEnum enums : GoodsStatusEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }

    public static List<GoodsStatusBean> toList() {
        List<GoodsStatusBean> list = new ArrayList<GoodsStatusBean>();
        for (GoodsStatusEnum enums : GoodsStatusEnum.values()) {
            GoodsStatusBean bean = GoodsStatusBean.builder()
                    .code(enums.getCode())
                    .value(enums.getValue())
                    .build();
            list.add(bean);
        }
        return list;
    }
}
