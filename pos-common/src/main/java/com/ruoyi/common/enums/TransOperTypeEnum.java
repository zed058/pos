package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author luobo
 * @title: TransOperTypeEnum
 * @projectName pos
 * @description: 出入账操作类型枚举类
 * @date 2021-11-24 18:17:55
 */
public enum TransOperTypeEnum {
    YUE_1001("1001", "终端购买"), //（出）
    YUE_1002("1002", "商品兑换"), //（出）
    YUE_1003("1003", "提现"), //（出）
    YUE_1004("1004", "商户收益（激活奖励）"), //（入）
    YUE_1005("1005", "直接受益（分润收益）"), //（入）
    YUE_1006("1006", "下级收益（分润收益）"), //（入）
    YUE_1007("1007", "采购收益（导师收益）"), //（入）
    YUE_1008("1008", "提现收益（导师收益）"), //（入）
    YUE_1009("1009", "办公补贴（导师收益）"), //（入）
    YUE_1010("1010", "金币兑换"), //（入）
    YUE_1011("1011", "兑换邮费"), //（入）

    JIFEN_2001("2001", "激活奖励"), //（入）
    JIFEN_2002("2002", "出售商品"), //（入）
    JIFEN_2003("2003", "商品兑换"), //（出）

    JINBI_3001("3001", "序列号兑换"), //（入）
    JINBI_3002("3002", "出售商品"), //（入）
    JINBI_3003("3003", "商品兑换"), //（出）
    JINBI_3004("3004", "兑换现金"), //（出）

    JIAOYIJIN_4001("4001", "系统发放"), //（入）
    JIAOYIJIN_4002("4002", "出售商品"), //（入）
    JIAOYIJIN_4003("4003", "商品兑换"), //（出）
    JIAOYIJIN_4004("4004", "兑换金币"), //（出）

    JIANGLIJIN_5001("5001", "系统发放"), //（入）
    JIANGLIJIN_5002("5002", "出售商品"), //（入）
    JIANGLIJIN_5003("5003", "商品兑换"), //（出）
    JIANGLIJIN_5004("5004", "兑换金币"); //（出）

    private final String code;
    private final String value;

    TransOperTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static TransOperTypeEnum getEnumsByCode(String code) {
        for (TransOperTypeEnum enums : TransOperTypeEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
}
