package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Administrator
 * @title: ImgTypeEnums
 * @projectName ruoyi
 * @description: TODO
 */
public enum ImgTypeEnums {
    POPUP("1", "首页弹窗"),
    SLIDESHOW("2", "首页轮播图"),
    ACTIVATED("3", "广告展示"),
    ADVERTISING("4", "分享图片"),
    LOIN("5", "腰部广告图"),
    BACKDROP("6", "商户奖励背景图");

    private final String code;
    private final String value;

    ImgTypeEnums(String code, String value) {
        this.code = code;
        this.value = value;
    }
    public static ImgTypeEnums getEnumsByCode(String code) {
        for (ImgTypeEnums enums : ImgTypeEnums.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
