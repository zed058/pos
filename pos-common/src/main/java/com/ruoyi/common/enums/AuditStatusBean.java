package com.ruoyi.common.enums;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author luobo
 * @title: AuditStatusBean
 * @projectName pos
 * @description: TODO
 * @date 2021-12-06 10:23:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuditStatusBean {
    private String code;
    private String value;
    private String show;
}
