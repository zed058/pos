package com.ruoyi.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author luobo
 * @title: PayTypeEnum
 * @projectName pos
 * @description: 第三方交易交易类型美剧看
 * @date 2021-12-14 14:02:06
 */
@Getter
@AllArgsConstructor
public enum PayTypeEnum {
    SHUAKA_1("1", "刷卡"),
    SAOMA_2("2", "扫码"),
    SHANFU_3("3", "闪付"),

    //O110303，O110404是大额
    //O110606是刷脸   O110505是小额
    //O110909，O111515是POS交易  O111212是碰一碰交易
    //（03优惠大额、04正常大额、05小额、06刷脸、12碰一碰、09刷卡、09扫码、15刷卡、15扫码、激活交易）

    SHANDIAN_1_09_1("O110909_1", "09刷卡"),
    SHANDIAN_1_15_1("O111515_1", "15刷卡"),
    SHANDIAN_2_03("O110303", "03优惠大额"), //O110303
    SHANDIAN_2_04("O110404", "04正常大额"), //O110404
    SHANDIAN_2_05("O110505", "05小额"),
    SHANDIAN_2_06("O110606", "06刷脸"),
    SHANDIAN_2_12("O111212", "12碰一碰"),
    SHANDIAN_2_09_2("O110909_2", "09扫码"),
    SHANDIAN_2_15_2("O111515_2", "15扫码"),
    SHANDIAN_ACTIVATY("ACTIVATY", "激活交易"),

    LESHUA_1_SALE("SALE", "pos消费"),
    LESHUA_3_QSALE("QSALE", "闪付"),
    LESHUA_2_TFTQRACTV_WE("TFTQRACTV_WE", "微信主扫"),
    LESHUA_2_TFTQRACTV_AL("TFTQRACTV_AL", "阿里主扫"),
    LESHUA_2_TFTQRACTV_CU("TFTQRACTV_CU", "银联主扫"),
    LESHUA_2_TFTQRERACTV_WE("TFTQRERACTV_WE", "微信被扫"),
    LESHUA_2_TFTQRERACTV_AL("TFTQRERACTV_AL", "支付宝被扫"),
    LESHUA_2_TFTQRERACTV_CU("TFTQRERACTV_CU", "银联被扫");

    private final String code;
    private final String value;

    public static PayTypeEnum getEnumsByCode(String code) {
        for (PayTypeEnum enums : PayTypeEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
    public static Map<String, String> toMap() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (PayTypeEnum enums : PayTypeEnum.values()) {
            map.put(enums.getCode(), enums.getValue());
        }
        return map;
    }
    public static List<String> getSwipeList() {
        List<String> list = new ArrayList<>();
        list.add(PayTypeEnum.SHUAKA_1.getCode());
        //闪电宝
        list.add(PayTypeEnum.SHANDIAN_1_09_1.getCode());
        list.add(PayTypeEnum.SHANDIAN_1_15_1.getCode());
        //乐刷
        list.add(PayTypeEnum.LESHUA_1_SALE.getCode());
        return list;
    }
    public static List<String> getScanCodeList() {
        List<String> list = new ArrayList<>();
        list.add(PayTypeEnum.SAOMA_2.getCode());
        //闪电宝
        list.add(PayTypeEnum.SHANDIAN_2_03.getCode());
        list.add(PayTypeEnum.SHANDIAN_2_04.getCode());
        list.add(PayTypeEnum.SHANDIAN_2_05.getCode());
        list.add(PayTypeEnum.SHANDIAN_2_06.getCode());
        list.add(PayTypeEnum.SHANDIAN_2_12.getCode());
        list.add(PayTypeEnum.SHANDIAN_2_09_2.getCode());
        list.add(PayTypeEnum.SHANDIAN_2_15_2.getCode());
        //乐刷
        list.add(PayTypeEnum.LESHUA_2_TFTQRACTV_WE.getCode());
        list.add(PayTypeEnum.LESHUA_2_TFTQRACTV_AL.getCode());
        list.add(PayTypeEnum.LESHUA_2_TFTQRACTV_CU.getCode());
        list.add(PayTypeEnum.LESHUA_2_TFTQRERACTV_WE.getCode());
        list.add(PayTypeEnum.LESHUA_2_TFTQRERACTV_AL.getCode());
        list.add(PayTypeEnum.LESHUA_2_TFTQRERACTV_CU.getCode());
        return list;
    }
}
