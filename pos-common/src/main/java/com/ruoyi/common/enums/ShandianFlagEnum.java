package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author luobo
 * @title: ShandianFlagEnum
 * @projectName pos
 * @description: TODO
 * @date 2022-04-15 10:18:10
 */
public enum ShandianFlagEnum {

    WAIT_REG("10", "未注册"),
    WAIT_SNGY("11", "待同步"),
    HAS_SNGY("12", "已同步");

    private final String code;
    private final String value;

    ShandianFlagEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static ShandianFlagEnum getEnumsByCode(String code) {
        for (ShandianFlagEnum enums : ShandianFlagEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
    public static Map<String, String> toMap() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (ShandianFlagEnum enums : ShandianFlagEnum.values()) {
            map.put(enums.getCode(), enums.getValue());
        }
        return map;
    }
}
