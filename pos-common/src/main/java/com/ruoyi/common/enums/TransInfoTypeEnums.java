package com.ruoyi.common.enums;

/**
 * @Author: Fan
 * @Description TODO
 * @Date: 2021/12/14 10:06
 */
public enum TransInfoTypeEnums {
    TRANSFER("0","划拨"),
    RETRACE("1","划回");

    private final String code;
    private final String value;

    TransInfoTypeEnums(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
