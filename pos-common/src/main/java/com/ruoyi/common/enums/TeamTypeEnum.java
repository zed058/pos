package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author luobo
 * @title: TeamTypeEnum
 * @projectName pos
 * @description: 团队类型枚举类
 * @date 2021-11-16 09:52:09
 */
public enum TeamTypeEnum {
    DIRECTLY("0", "直营"),
    TEAM("1", "团队");

    private final String code;
    private final String value;

    TeamTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
    public static TeamTypeEnum getEnumsByCode(String code) {
        for (TeamTypeEnum enums : TeamTypeEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
