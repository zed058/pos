package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author luobo
 * @title: deliveryTypeEnum
 * @projectName pos
 * @description: 订单配送方式枚举类
 * @date 2021-11-17 14:40:40
 */
public enum DeliveryTypeEnum {
    LOGISTICS("1", "物流"),
    SELF_MENTION("0", "自提");

    private final String code;
    private final String value;

    DeliveryTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }
    public static DeliveryTypeEnum getEnumsByCode(String code) {
        for (DeliveryTypeEnum enums : DeliveryTypeEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
