package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @Author: Fan
 * @Description TODO
 * @Date: 2021/12/15 21:15
 */
public enum ValidEnums {

    NORMAL("0","正常"),
    DISABLE("1","禁用");

    private String code;
    private String value;

    ValidEnums(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static ValidEnums getEnumsByCode(String code) {
        for (ValidEnums enums : ValidEnums.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
}
