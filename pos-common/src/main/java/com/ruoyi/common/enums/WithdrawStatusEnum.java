package com.ruoyi.common.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * @author luobo
 * @title: WithdrawStatusEnum
 * @projectName pos
 * @description: 提现状态枚举类
 * @date 2021-12-01 16:09:35
 */
public enum WithdrawStatusEnum {
    HANDEL("10", "提现处理中"),
    SUCCESS("20", "提现成功"),
    FAIL("30", "提现失败"),
    FAILFORBANK("40", "因收款行原因失败，请更换卡");

    private final String code;
    private final String value;

    WithdrawStatusEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static WithdrawStatusEnum getEnumsByCode(String code) {
        for (WithdrawStatusEnum enums : WithdrawStatusEnum.values()) {
            if (StringUtils.equalsIgnoreCase(enums.getCode(), code)) {
                return enums;
            }
        }
        return null;
    }
}
