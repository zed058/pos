package com.ruoyi.common.core.domain.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.core.page.TableDataInfoApi;
import com.ruoyi.common.utils.StringUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @Author luobo
 * @Date 2021/6/29 16:01
 * @Describetion 操作消息提醒
 */
@ApiModel("结果集")
public class ApiResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private static Integer success = 200;
    private static Integer fail = 400;


    @ApiModelProperty("结果编码")
    private int code;
    @ApiModelProperty("结果消息")
    private String desc;
    @ApiModelProperty("结果数据")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;
    @ApiModelProperty("token仅调用登陆接口")
    private String token;

    /**
     * 初始化一个新创建的 Message 对象
     */
    public ApiResult() {
    }

    /**
     * 返回错误消息
     *
     * @return 错误消息
     * @param ERROR
     * @param msg
     */
    public static ApiResult error(CodeMsg ERROR, String msg) {
        return error(fail, msg);
    }
    public static ApiResult error(CodeMsg ERROR, Object data) {
        ApiResult json = new ApiResult();
        json.setCode(ERROR.getCode());
        json.setDesc(ERROR.getDesc());
        json.setData(data);
        return json;
    }

    /**
     * 返回错误消息
     *
     * @param msg 内容
     * @return 错误消息
     */
    public static ApiResult error(String msg) {
        return error(fail, msg);
    }

    /**
     * 返回错误消息
     *
     * @param code 错误码
     * @param msg  内容
     * @return 错误消息
     */
    public static ApiResult error(int code, String msg) {
        ApiResult json = new ApiResult();
        json.setCode(code);
        json.setDesc(msg);
        return json;
    }

    public static ApiResult error(CodeMsg codeMsg) {
        ApiResult json = new ApiResult();
        json.setCode(codeMsg.getCode());
        json.setDesc(codeMsg.getDesc());
        return json;
    }


    /**
     * 返回成功消息
     *
     * @param msg 内容
     * @return 成功消息
     */
    public static ApiResult success(String msg) {
        ApiResult json = new ApiResult();
        json.setCode(success);
        json.setDesc(msg);
        return json;
    }

    /**
     * 返回成功消息
     *
     * @param msg 内容
     * @return 成功消息
     */
    public static ApiResult success(String msg, Object data) {
        ApiResult json = new ApiResult();
        json.setCode(success);
        json.setDesc(msg);
        json.setData(data);
        return json;
    }

    public static ApiResult success(CodeMsg codeMsg, Object data) {
        ApiResult json = new ApiResult();
        json.setCode(codeMsg.getCode());
        json.setDesc(codeMsg.getDesc());
        json.setData(data);
        return json;
    }
    /**
     * 返回成功消息
     *
     * @param msg 内容
     * @return 成功消息
     */
    public static ApiResult success(String msg, Object data, Integer total) {
        ApiResult json = new ApiResult();
        json.setCode(success);
        json.setDesc(msg);
        json.setData(data);
        return json;
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static ApiResult success() {
        return ApiResult.success("操作成功");
    }

    /**
     * 返回默认成功消息
     *
     * @return 成功消息
     */
    public static ApiResult successDefaultMsg(Object data) {
        return success(CodeMsg.SUCCESS_MSG, data);
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static ApiResult success(Object data, Integer total) {
        return ApiResult.success("操作成功", data, total);
    }

    /**
     * 判断返回结果是否是成功消息
     */
    public static Boolean isResultSuccess(ApiResult ajaxResult) {
        if (ajaxResult.getCode() == success) {
            return true;
        } else {
            return false;
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}

