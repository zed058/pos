package com.ruoyi.common.core.domain.api;

import java.io.Serializable;

/**
 * @Author luobo
 * @Date 2021/6/29 16:01
 * @Describetion 提示信息
 */
public class CodeMsg implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String SUCCESS_MSG = "操作成功";
    private int code;
    private String desc;

    public static CodeMsg SUCCESS = new CodeMsg(200, "操作成功");
    public static CodeMsg SUCCESS_CLOSE = new CodeMsg(201, "成功并提示信息3秒内自动关闭");
    public static CodeMsg ERROR = new CodeMsg(400, "操作失败");
    public static CodeMsg SETPASS = new CodeMsg(301, "请设置密码");
    public static CodeMsg LOGIN_EXPIRED = new CodeMsg(500, "登录过期");

    private CodeMsg() {
    }

    private CodeMsg(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public CodeMsg fillDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public CodeMsg fillArgs(Object... args) {
        int code = this.code;
        String message = String.format(this.desc, args);
        return new CodeMsg(code, message);
    }

    @Override
    public String toString() {
        return "CodeMsg [code=" + code + ", desc=" + desc + "]";
    }
}

