package com.ruoyi.common.utils.shandianbao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author luobo
 * @title: ApiBaseResponseBean
 * @projectName pos
 * @description: TODO
 * @date 2022-04-07 10:41:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApiBaseResponseBean {
    private String resultCode;
    private String errorMsg;
    private String resultMsg;
}
