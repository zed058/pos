package com.ruoyi.common.utils.qugongbao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ApiBaseResponseBean implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    //"响应码 0000:成功 其他:失败", name = "code")
    private String code;

    //"响应信息", name = "msg")
    private String msg;

    //"请求流水号", name = "reqNo")
    private String reqNo;

    // "签名类型  name = "signType")
    @Builder.Default
    private String signType = "SHA256WithRSA";

    //"签名字符串", name = "signValue")
    private String signValue;

    //返回内容，服务调用成功后的返回结果，json字符串", name = "body")
    private String body;


}
