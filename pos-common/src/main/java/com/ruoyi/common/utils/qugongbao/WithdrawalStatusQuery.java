package com.ruoyi.common.utils.qugongbao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author luobo
 * @title: WithdrawalReq
 * @projectName pos
 * @description: 提现状态查询
 * @date 2021-12-01 10:49:13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WithdrawalStatusQuery {
    //1. 三选⼀
    //2. orderNo/custOrderNo 有值时查询列表
    //3. custTransId 有值时，查出单条
    private String orderNo; //批次订单号
    private String custOrderNo; //客户批次订单号
    private String custTransId; //客户系统交易流⽔号

}
