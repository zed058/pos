package com.ruoyi.common.utils.cipher;

import org.apache.shiro.codec.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;

/**
 * @author luobo
 * @title: CipherKeyUtils
 * @projectName pos
 * @description: TODO
 * @date 2022-05-11 15:40:22
 */
public class CipherKeyUtils {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        KeyGenerator keygen = KeyGenerator.getInstance("AES");
        SecretKey deskey = keygen.generateKey();
        System.out.println(Base64.encodeToString(deskey.getEncoded()));
    }
}
