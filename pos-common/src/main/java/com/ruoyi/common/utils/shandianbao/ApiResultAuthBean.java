package com.ruoyi.common.utils.shandianbao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author luobo
 * @title: APIRegResultbean
 * @projectName pos
 * @description: TODO
 * @date 2022-04-06 09:53:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApiResultAuthBean {
    @JsonProperty(value = "bankName")
    private String shandianBankName; //银行名称
    @JsonProperty(value = "bankNo")
    private String shandianBankNo; //卡号
    @JsonProperty(value = "certNo")
    private String shandianCertNo; //身份证号
    @JsonProperty(value = "name")
    private String shandianName; //姓名
    @JsonProperty(value = "saruLruid")
    private String saruLruid; //商户号
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty(value = "authTime")
    private Date shandianAuthTime; //认证时间
}
