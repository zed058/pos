package com.ruoyi.common.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author luobo
 * @title: UrlUtils
 * @projectName pos
 * @description: TODO
 * @date 2021-11-10 15:48:13
 */
public class UrlUtils {
    public static String getUrl() {
        HttpServletRequest request = ServletUtils.getRequest();
        return getDomain(request);
    }

    public static String getDomain(HttpServletRequest request) {
        StringBuffer url = request.getRequestURL();
        String contextPath = request.getServletContext().getContextPath();
        return url.delete(url.length() - request.getRequestURI().length(), url.length()).append(contextPath).toString();
    }
}
