package com.ruoyi.common.utils.shandianbao;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author luobo
 * @title: APITransResultBean
 * @projectName pos
 * @description: TODO
 * @date 2022-04-06 09:50:43
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApiResultTransBean {
    private String cardNum; //银行名称
    @JsonProperty(value = "cpiNo")
    private String cpiNo; //交易编号 O110303，O110404是大额   O110606是刷脸   O110505是小额   O110909，O111515是POS交易    O111212是碰一碰交易
    @JsonProperty(value = "settleFee")
    private BigDecimal extractionFee; //单笔手续费
    @JsonProperty(value = "handFee")
    private BigDecimal merSigeFee; //手续费
    @JsonProperty(value = "amount")
    private BigDecimal transAmount; //支付金额

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonProperty(value = "createTime")
    private Date transTime; //订单时间
    @JsonProperty(value = "bcconOrdernum")
    private String orderId; //订单号
    private String termNo; //sn号
    @JsonProperty(value = "bcconMark")
    private String bcconMark; //交易类型    CLOUDPAY:云闪付 SMALLFREEPAY:小额双免 VIPPAY:激活交易 CARDPAY:刷卡消费 QUICKPAY:快捷支付 WXQRPAY:微信扫码 ALIQRPAY:支付宝扫码 UNIONQRPAY:银联扫码
    private String vCode; //注册码

    private String saruLruid; //商户号
    @JsonProperty(value = "termNo")
    private String snCode; //商户号
}
