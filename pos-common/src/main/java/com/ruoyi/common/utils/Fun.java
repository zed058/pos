package com.ruoyi.common.utils;

import com.alibaba.fastjson.JSON;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.*;

public class Fun {

    /**
     * 编码格式转换(解码)
     *
     * @param content
     * @return
     */
    public static String decode(String content) {
        try {
            return URLDecoder.decode(content, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * String param = request.getQueryString();
     * Map<String,Object> dataMap = CommonUtils.paramsToMap(param);
     *
     * @param params
     * @return
     */
    public static Map<String, Object> paramsToMap(String params) {
        Map<String, Object> map = new LinkedHashMap<>();
        if (params != null && !params.equals("")) {
            String[] array = params.split("&");
            for (String pair : array) {
                if ("=".equals(pair.trim())) {
                    continue;
                }
                String[] entity = pair.split("=");
                if (entity.length == 1) {
                    map.put(decode(entity[0]), null);
                } else {
                    map.put(decode(entity[0]), decode(entity[1]));
                }
            }
        }
        return map;
    }

    /**
     * 将对象转成TreeMap,属性名为key,属性值为value
     *
     * @param object  对象
     * @param isEmpty 是否put空数值
     * @return
     */
    public static Map<String, Object> objToMap(Object object, boolean isEmpty) {
        Map<String, Object> map = new HashMap<>();
        if (object == null) {
            return map;
        }
        Class clazz = object.getClass();

        while (null != clazz.getSuperclass()) {
            Field[] declaredFields1 = clazz.getDeclaredFields();

            for (Field field : declaredFields1) {
                String name = field.getName();

                // 获取原来的访问控制权限
                boolean accessFlag = field.isAccessible();
                // 修改访问控制权限
                field.setAccessible(true);
                Object value = null;
                try {
                    value = field.get(object);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                // 恢复访问控制权限
                field.setAccessible(accessFlag);

                if (null != value && StringUtils.isNotBlank(value.toString())) {
                    //如果是List,将List转换为json字符串
                    if (value instanceof List) {
                        value = JSON.toJSONString(value);
                    }
                    if (!Objects.equals("serialVersionUID", name)) {
                        if (isEmpty) {
                            map.put(name, value);
                        } else {
                            if (
                                    !(
                                            (value instanceof Double && (Double) value == 0) ||
                                                    (value instanceof BigDecimal && ((BigDecimal) value).equals(BigDecimal.ZERO)) ||
                                                    (value instanceof Integer && (Integer) value == 0)
                                    )
                            ) {
                                map.put(name, value);
                            }
                        }
                    }
                }
            }

            clazz = clazz.getSuperclass();
        }
        return map;
    }

    /**
     * 计算两个数百分比
     *
     * @param num1
     * @param num2
     * @return
     */
    public static double calcPercentage(BigDecimal num1, BigDecimal num2) {
        if (num2.equals(BigDecimal.ZERO) || num2.doubleValue() == 0) {
            return 100.0;
        } else if (num1.equals(BigDecimal.ZERO) || num1.doubleValue() == 0) {
            return 0.0;
        }
//        DecimalFormat df = new DecimalFormat("0.00");//设置保留位数
//        return Double.parseDouble(df.format(num1.doubleValue() / num2.doubleValue())) * 100;
        BigDecimal decimal = new BigDecimal(num1.doubleValue() / num2.doubleValue() * 100).setScale(2, BigDecimal.ROUND_HALF_UP);
        return decimal.doubleValue();
    }



}