package com.ruoyi.common.utils.leshua;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author luobo
 * @title: LeshuaApiTransInfoBean
 * @projectName pos
 * @description: TODO
 * @date 2022-04-19 09:41:48
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LeshuaApiTransInfoBean implements Serializable {
    private static final long serialVersionUID = -1;
    @JsonProperty(value = "trans_id")
    private String orderId; //交易唯一ID
    @JsonProperty(value = "merchant_name")
    private String merName; //商户名称
    @JsonProperty(value = "merchant_no")
    private String merId; //商户编号
    private String term_no; //机具终端号
    @JsonProperty(value = "device_sn")
    private String snCode; //机具SN码
    @JsonProperty(value = "trans_time")
    private String transTime; //交易产生时间
    @JsonProperty(value = "amount")
    private BigDecimal transAmount; //交易金额
    private String fee; //总手续费
    private String transFee; //交易手续费
    private String expFee; //到账手续费
    @JsonProperty(value = "pay_type")
    private String payType; //支付方式 SALE pos消费，QSALE 闪付，TFTQRACTV_WE 微信主扫，TFTQRACTV_AL 阿里主扫，TFTQRACTV_CU  银联主扫，TFTQRERACTV_WE 微信被扫，TFTQRERACTV_AL 支付宝被扫，TFTQRERACTV_CU银联被扫
    @JsonProperty(value = "card_type")
    private String cardType; //卡类型 1借记卡，2信用卡，3 未知
    private String is_d0; //结算类型 1：D0，0：T1
    private String device_type; //机具类型
}
