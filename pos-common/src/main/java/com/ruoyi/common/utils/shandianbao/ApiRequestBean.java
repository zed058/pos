package com.ruoyi.common.utils.shandianbao;

import lombok.*;

/**
 * @author luobo
 * @title: APIRegResultbean
 * @projectName pos
 * @description: TODO
 * @date 2022-04-06 09:53:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApiRequestBean {
    private String rugrId; //渠道id
    private String beginTime; //查询开始时间
    private String endTime; //查询结束时间
    private String phoneNo; //手机号
    //private String saruLruid; //手机号
}
