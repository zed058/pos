package com.ruoyi.common.utils.qugongbao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author luobo
 * @title: WithdrawalRefundStatusQuery
 * @projectName pos
 * @description: 提现退票状态查询
 * @date 2021-12-01 10:49:13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WithdrawalRefundStatusQuery {
    private String orderNo; //批次订单号
    private String refundDate; //退票⽇期  yyyyMMdd

}
