package com.ruoyi.common.utils.kuaidiniao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author luobo
 * @title: GenerateOrderRes
 * @projectName pos
 * @description: 快递鸟下单接口响应参数
 * @date 2021-12-09 19:19:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GenerateOrderRes {
    @JsonProperty("EBusinessID")
    private String eBusinessID;
    @JsonProperty("Success")
    private String success;
    @JsonProperty("ResultCode")
    private String resultCode;
    @JsonProperty("Reason")
    private String reason;
    @JsonProperty("Order")
    private Order order;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class Order{
        @JsonProperty("OrderCode")
        private String orderCode;
        @JsonProperty("ShipperCode")
        private String shipperCode;
        @JsonProperty("LogisticCode")
        private String logisticCode;
    }
}
