package com.ruoyi.common.utils.zhiqiangkeji;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 * @author luobo
 * @title: SignUtil
 * @projectName pos
 * @description: 智强科技验签工具类
 * @date 2021-12-23 09:40:43
 */
@Component
public class SignUtil {
    private static final Logger log = LoggerFactory.getLogger(SignUtil.class);
    @Autowired
    private Environment env;
    private static String signKey;

    @PostConstruct
    public void init() {
        signKey = env.getProperty("zhiqiang.signKey");
    }
    private static String encodingCharset = "UTF-8";

    public static void checkSign(JSONObject reqPrams, String sign) {
        // 删除sign字段
        reqPrams.remove("sign");
        System.out.println("验签结果： " + sign.equalsIgnoreCase(getSign(reqPrams, "KEYKEYKEYKEYKEYKEYKEY")));

    }

    public static String toHex(byte input[]) {
        if (input == null)
            return null;
        StringBuffer output = new StringBuffer(input.length * 2);
        for (int i = 0; i < input.length; i++) {
            int current = input[i] & 0xff;
            if (current < 16)
                output.append("0");
            output.append(Integer.toString(current, 16));
        }
        return output.toString();
    }

    public static String md5(String value, String charset) {
        MessageDigest md = null;
        try {
            byte[] data = value.getBytes(charset);
            md = MessageDigest.getInstance("MD5");
            byte[] digestData = md.digest(data);
            return toHex(digestData);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getSign(Map<String,Object> map, String key){
        ArrayList<String> list = new ArrayList<String>();
        for(Map.Entry<String,Object> entry:map.entrySet()){
            if(null != entry.getValue() && !"".equals(entry.getValue())){
                if(null != entry.getValue() && !"".equals(entry.getValue())){
                    list.add(entry.getKey() + "=" + entry.getValue() + "&");
                }
            }
        }
        int size = list.size();
        String [] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < size; i ++) {
            sb.append(arrayToSort[i]);
        }
        String result = sb.toString();
        result += "key=" + key;
        result = md5(result, encodingCharset).toUpperCase();
        return result;
    }

    public static String getSign(Map<String,Object> map){
        return getSign(map, signKey);
    }

    public static void main(String[] args) {
        JSONObject reqPrams = new JSONObject();
        reqPrams.put("tradeOrderId", "tradeOrderIdtradeOrderId");
        reqPrams.put("payOrderId", "payOrderIdpayOrderId");
        reqPrams.put("mchId", "mchIdmchId");
        //reqPrams.put("55", "5555");
        //reqPrams.put("sign", "sign");
        //reqPrams.remove("sign");
        String str = getSign(reqPrams, "seCag0TU6Mv5J0rowAf5Xd20");
        System.out.println(str);
    }
}
