package com.ruoyi.common.utils.qugongbao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author luobo
 * @title: WithdrawalReq
 * @projectName pos
 * @description: 提现
 * @date 2021-12-01 10:49:13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WithdrawalReq {
    private String spProdCode; //服务商代码
    private String custOrderNo; //客户订单批次号
    private String notifyUrl; //交易结果通知地址

    private List<WithdrawalDetail> dataList; //提现

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class WithdrawalDetail {
        private String custTransId; //客户系统交易流⽔号
        private String memberNo; //创客手机号
        private Long amount; //amount
    }
}
