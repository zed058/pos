package com.ruoyi.common.utils.kuaidiniao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author luobo
 * @title: QueryOrderRes
 * @projectName pos
 * @description: 快递鸟订单查询接口响应参数
 * @date 2021-12-09 19:23:40
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QueryOrderRes {
    @JsonProperty("EBusinessID")
    private String eBusinessID;
    @JsonProperty("ShipperCode")
    private String shipperCode;
    @JsonProperty("Success")
    private String success;
    @JsonProperty("logisticCode")
    private String logisticCode;
    @JsonProperty("State")
    private String state;
    @JsonProperty("StateEx")
    private String stateEx;
    @JsonProperty("Location")
    private String location;
    @JsonProperty("Traces")
    private List<Traces> traces;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class Traces{
        @JsonProperty("AcceptTime")
        private String acceptTime;
        @JsonProperty("AcceptStation")
        private String acceptStation;
        @JsonProperty("Location")
        private String location;
        @JsonProperty("Action")
        private String action;
    }
}
