package com.ruoyi.common.utils.weCat.template;

import com.alibaba.fastjson.JSONObject;
import lombok.Builder;
import lombok.Data;

/**
 * @author luobo
 * @title: ShipTemplate
 * @projectName pos
 * @description: 发货消息通知模板
 * @date 2021-12-10 16:04:29
 */
@Data
@Builder
public class ShipTemplate {
    private String touser;
    private String template_id;
    private String url;
    private Params data;

    @Data
    @Builder
    public static class Params{
        private JSONObject first;
        private JSONObject delivername; //图文标题
        private JSONObject ordername; //发布时间
        private JSONObject remark;
    }
}
