package com.ruoyi.common.utils.leshua;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/***
 * @Description: API接口请求参数
 * @author gangwei.chen
 * @date 2019年6月24日上午10:35:36
 * @version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LeshuaApiBaseRequestBean implements Serializable {
    private static final long serialVersionUID = -1;
    private String request_id; //随机串，每次请求唯一
    private String sign; //签名
    private String date_time; //数据推送时间
    private String data; //参考对应接口 输出参数
}
