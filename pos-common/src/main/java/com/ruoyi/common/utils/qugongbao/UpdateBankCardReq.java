package com.ruoyi.common.utils.qugongbao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author luobo
 * @title: UpdateBankCardReq
 * @projectName pos
 * @description: TODO
 * @date 2022-03-01 12:19:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateBankCardReq {
    private String memberNo; //创客编号
    private String mobile; //1.仅⽤作信息验证。不会修改会原⼿机号。
    private String accountType; //取值： BANK/ALIPAY
    private String alipayLoginId; //邮箱和⼿机号格式 accountType = ALIPAY 时不能为空
    private String bankCardNo; //accountType = BANK 时不能为空
    private String bankName; //bankCardNo 不为空时，可选发卡⾏名称
}
