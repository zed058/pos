/****************************************************************
 *
 *    Copyright (c) 2001-2020 深圳南方电子口岸有限公司
 *    http://www.szeport.com.cn/
 *
 *    Package:     com.szeport.common.core.util
 *
 *    Filename:    RestTemplateUtil.java
 *
 *    Description: restTemplate工具类
 *
 *    @author:     duanhp
 *
 *    @version:    1.0.0
 *
 *    Create at:   2020年6月4日 下午1:51:09
 *
 *    Revision:
 *
 *    2020年6月4日 下午1:51:09
 *        - first revision
 *
 *****************************************************************/
package com.ruoyi.common.utils.rest;

import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * @ClassName RestTemplateUtil
 * @Description restTemplate工具类
 * @author luobo
 * @Date 2020年6月4日 下午1:51:09
 * @version 1.0.0
 */
public class RestTemplateUtil {

	RestTemplate restTemplate;

	public RestTemplateUtil(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	// ----------------------------------GET-------------------------------------------------------

	/**
	 * GET请求调用方式
	 * 
	 * @param url          请求URL
	 * @param responseType 返回对象类型
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> get(String url, Class<T> responseType) {
		return restTemplate.getForEntity(url, responseType);
	}

	/**
	 * GET请求调用方式
	 * 
	 * @param url          请求URL
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> get(String url, Class<T> responseType, Object... uriVariables) {
		return restTemplate.getForEntity(url, responseType, uriVariables);
	}

	/**
	 * GET请求调用方式
	 * 
	 * @param url          请求URL
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> get(String url, Class<T> responseType, Map<String, ?> uriVariables) {
		return restTemplate.getForEntity(url, responseType, uriVariables);
	}

	/**
	 * 带请求头的GET请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> get(String url, Map<String, String> headers, Class<T> responseType,
			Object... uriVariables) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAll(headers);
		return get(url, httpHeaders, responseType, uriVariables);
	}

	/**
	 * 带请求头的GET请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> get(String url, HttpHeaders headers, Class<T> responseType,
			Object... uriVariables) {
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		return exchange(url, HttpMethod.GET, requestEntity, responseType, uriVariables);
	}

	/**
	 * 带请求头的GET请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> get(String url, Map<String, String> headers, Class<T> responseType,
			Map<String, ?> uriVariables) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAll(headers);
		return get(url, httpHeaders, responseType, uriVariables);
	}

	/**
	 * 带请求头的GET请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> get(String url, HttpHeaders headers, Class<T> responseType,
			Map<String, ?> uriVariables) {
		HttpEntity<?> requestEntity = new HttpEntity<>(headers);
		return exchange(url, HttpMethod.GET, requestEntity, responseType, uriVariables);
	}

	// ----------------------------------POST-------------------------------------------------------

	/**
	 * POST请求调用方式
	 * 
	 * @param url          请求URL
	 * @param responseType 返回对象类型
	 * @return
	 */
	public <T> ResponseEntity<T> post(String url, Class<T> responseType) {
		return restTemplate.postForEntity(url, HttpEntity.EMPTY, responseType);
	}

	/**
	 * POST请求调用方式
	 * 
	 * @param url          请求URL
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> post(String url, Object requestBody, Class<T> responseType) {
		return restTemplate.postForEntity(url, requestBody, responseType);
	}

	/**
	 * POST请求调用方式
	 * 
	 * @param url          请求URL
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> post(String url, Object requestBody, Class<T> responseType,
			Object... uriVariables) {
		return restTemplate.postForEntity(url, requestBody, responseType, uriVariables);
	}

	/**
	 * POST请求调用方式
	 * 
	 * @param url          请求URL
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> post(String url, Object requestBody, Class<T> responseType,
			Map<String, ?> uriVariables) {
		return restTemplate.postForEntity(url, requestBody, responseType, uriVariables);
	}

	/**
	 * 带请求头的POST请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> post(String url, Map<String, String> headers, Object requestBody,
			Class<T> responseType, Object... uriVariables) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAll(headers);
		return post(url, httpHeaders, requestBody, responseType, uriVariables);
	}

	/**
	 * 带请求头的POST请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> post(String url, HttpHeaders headers, Object requestBody, Class<T> responseType,
			Object... uriVariables) {
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(requestBody, headers);
		return post(url, requestEntity, responseType, uriVariables);
	}

	/**
	 * 带请求头的POST请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> post(String url, Map<String, String> headers, Object requestBody,
			Class<T> responseType, Map<String, ?> uriVariables) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAll(headers);
		return post(url, httpHeaders, requestBody, responseType, uriVariables);
	}

	/**
	 * 带请求头的POST请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> post(String url, HttpHeaders headers, Object requestBody, Class<T> responseType,
			Map<String, ?> uriVariables) {
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(requestBody, headers);
		return post(url, requestEntity, responseType, uriVariables);
	}

	/**
	 * 自定义请求头和请求体的POST请求调用方式
	 * 
	 * @param url           请求URL
	 * @param requestEntity 请求头和请求体封装对象
	 * @param responseType  返回对象类型
	 * @param uriVariables  URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> post(String url, HttpEntity<?> requestEntity, Class<T> responseType,
			Object... uriVariables) {
		return restTemplate.exchange(url, HttpMethod.POST, requestEntity, responseType, uriVariables);
	}

	/**
	 * 自定义请求头和请求体的POST请求调用方式
	 * 
	 * @param url           请求URL
	 * @param requestEntity 请求头和请求体封装对象
	 * @param responseType  返回对象类型
	 * @param uriVariables  URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> post(String url, HttpEntity<?> requestEntity, Class<T> responseType,
			Map<String, ?> uriVariables) {
		return restTemplate.exchange(url, HttpMethod.POST, requestEntity, responseType, uriVariables);
	}

	/**
	 * POST表单请求调用方式
	 *
	 * @param url          请求URL
	 * @param params  请求表单参数
	 * @param responseType 返回对象类型
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> postForm(String url, Map<String, Object> params, Class<T> responseType) {
		HttpHeaders herader = new HttpHeaders();
		herader.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
		for (String key : params.keySet()) {
			bodyMap.add(key, params.get(key));
		}
		HttpEntity<MultiValueMap<String, Object>> multiValueMapHttpEntity = new HttpEntity<>(bodyMap, herader);
		return post(url, multiValueMapHttpEntity, responseType);
	}

	// ----------------------------------PUT-------------------------------------------------------

	/**
	 * PUT请求调用方式
	 * 
	 * @param url          请求URL
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> put(String url, Class<T> responseType, Object... uriVariables) {
		return put(url, HttpEntity.EMPTY, responseType, uriVariables);
	}

	/**
	 * PUT请求调用方式
	 * 
	 * @param url          请求URL
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> put(String url, Object requestBody, Class<T> responseType,
			Object... uriVariables) {
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(requestBody);
		return put(url, requestEntity, responseType, uriVariables);
	}

	/**
	 * PUT请求调用方式
	 * 
	 * @param url          请求URL
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> put(String url, Object requestBody, Class<T> responseType,
			Map<String, ?> uriVariables) {
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(requestBody);
		return put(url, requestEntity, responseType, uriVariables);
	}

	/**
	 * 带请求头的PUT请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> put(String url, Map<String, String> headers, Object requestBody,
			Class<T> responseType, Object... uriVariables) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAll(headers);
		return put(url, httpHeaders, requestBody, responseType, uriVariables);
	}

	/**
	 * 带请求头的PUT请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> put(String url, HttpHeaders headers, Object requestBody, Class<T> responseType,
			Object... uriVariables) {
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(requestBody, headers);
		return put(url, requestEntity, responseType, uriVariables);
	}

	/**
	 * 带请求头的PUT请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> put(String url, Map<String, String> headers, Object requestBody,
			Class<T> responseType, Map<String, ?> uriVariables) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAll(headers);
		return put(url, httpHeaders, requestBody, responseType, uriVariables);
	}

	/**
	 * 带请求头的PUT请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> put(String url, HttpHeaders headers, Object requestBody, Class<T> responseType,
			Map<String, ?> uriVariables) {
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(requestBody, headers);
		return put(url, requestEntity, responseType, uriVariables);
	}

	/**
	 * 自定义请求头和请求体的PUT请求调用方式
	 * 
	 * @param url           请求URL
	 * @param requestEntity 请求头和请求体封装对象
	 * @param responseType  返回对象类型
	 * @param uriVariables  URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> put(String url, HttpEntity<?> requestEntity, Class<T> responseType,
			Object... uriVariables) {
		return restTemplate.exchange(url, HttpMethod.PUT, requestEntity, responseType, uriVariables);
	}

	/**
	 * 自定义请求头和请求体的PUT请求调用方式
	 * 
	 * @param url           请求URL
	 * @param requestEntity 请求头和请求体封装对象
	 * @param responseType  返回对象类型
	 * @param uriVariables  URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> put(String url, HttpEntity<?> requestEntity, Class<T> responseType,
			Map<String, ?> uriVariables) {
		return restTemplate.exchange(url, HttpMethod.PUT, requestEntity, responseType, uriVariables);
	}

	// ----------------------------------DELETE-------------------------------------------------------

	/**
	 * DELETE请求调用方式
	 * 
	 * @param url          请求URL
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, Class<T> responseType, Object... uriVariables) {
		return delete(url, HttpEntity.EMPTY, responseType, uriVariables);
	}

	/**
	 * DELETE请求调用方式
	 * 
	 * @param url          请求URL
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, Class<T> responseType, Map<String, ?> uriVariables) {
		return delete(url, HttpEntity.EMPTY, responseType, uriVariables);
	}

	/**
	 * DELETE请求调用方式
	 * 
	 * @param url          请求URL
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, Object requestBody, Class<T> responseType,
			Object... uriVariables) {
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(requestBody);
		return delete(url, requestEntity, responseType, uriVariables);
	}

	/**
	 * DELETE请求调用方式
	 * 
	 * @param url          请求URL
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, Object requestBody, Class<T> responseType,
			Map<String, ?> uriVariables) {
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(requestBody);
		return delete(url, requestEntity, responseType, uriVariables);
	}

	/**
	 * 带请求头的DELETE请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, Map<String, String> headers, Class<T> responseType,
			Object... uriVariables) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAll(headers);
		return delete(url, httpHeaders, responseType, uriVariables);
	}

	/**
	 * 带请求头的DELETE请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, HttpHeaders headers, Class<T> responseType,
			Object... uriVariables) {
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
		return delete(url, requestEntity, responseType, uriVariables);
	}

	/**
	 * 带请求头的DELETE请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, Map<String, String> headers, Class<T> responseType,
			Map<String, ?> uriVariables) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAll(headers);
		return delete(url, httpHeaders, responseType, uriVariables);
	}

	/**
	 * 带请求头的DELETE请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, HttpHeaders headers, Class<T> responseType,
			Map<String, ?> uriVariables) {
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(headers);
		return delete(url, requestEntity, responseType, uriVariables);
	}

	/**
	 * 带请求头的DELETE请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, Map<String, String> headers, Object requestBody,
			Class<T> responseType, Object... uriVariables) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAll(headers);
		return delete(url, httpHeaders, requestBody, responseType, uriVariables);
	}

	/**
	 * 带请求头的DELETE请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, HttpHeaders headers, Object requestBody,
			Class<T> responseType, Object... uriVariables) {
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(requestBody, headers);
		return delete(url, requestEntity, responseType, uriVariables);
	}

	/**
	 * 带请求头的DELETE请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, Map<String, String> headers, Object requestBody,
			Class<T> responseType, Map<String, ?> uriVariables) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAll(headers);
		return delete(url, httpHeaders, requestBody, responseType, uriVariables);
	}

	/**
	 * 带请求头的DELETE请求调用方式
	 * 
	 * @param url          请求URL
	 * @param headers      请求头参数
	 * @param requestBody  请求参数体
	 * @param responseType 返回对象类型
	 * @param uriVariables URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, HttpHeaders headers, Object requestBody,
			Class<T> responseType, Map<String, ?> uriVariables) {
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(requestBody, headers);
		return delete(url, requestEntity, responseType, uriVariables);
	}

	/**
	 * 自定义请求头和请求体的DELETE请求调用方式
	 * 
	 * @param url           请求URL
	 * @param requestEntity 请求头和请求体封装对象
	 * @param responseType  返回对象类型
	 * @param uriVariables  URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, HttpEntity<?> requestEntity, Class<T> responseType,
			Object... uriVariables) {
		return restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, responseType, uriVariables);
	}

	/**
	 * 自定义请求头和请求体的DELETE请求调用方式
	 * 
	 * @param url           请求URL
	 * @param requestEntity 请求头和请求体封装对象
	 * @param responseType  返回对象类型
	 * @param uriVariables  URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> delete(String url, HttpEntity<?> requestEntity, Class<T> responseType,
			Map<String, ?> uriVariables) {
		return restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, responseType, uriVariables);
	}

	// ----------------------------------通用方法-------------------------------------------------------

	/**
	 * 通用调用方式
	 * 
	 * @param url           请求URL
	 * @param method        请求方法类型
	 * @param requestEntity 请求头和请求体封装对象
	 * @param responseType  返回对象类型
	 * @param uriVariables  URL中的变量，按顺序依次对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity,
			Class<T> responseType, Object... uriVariables) {
		return restTemplate.exchange(url, method, requestEntity, responseType, uriVariables);
	}

	/**
	 * 通用调用方式
	 * 
	 * @param url           请求URL
	 * @param method        请求方法类型
	 * @param requestEntity 请求头和请求体封装对象
	 * @param responseType  返回对象类型
	 * @param uriVariables  URL中的变量，与Map中的key对应
	 * @return ResponseEntity 响应对象封装类
	 */
	public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity,
			Class<T> responseType, Map<String, ?> uriVariables) {
		return restTemplate.exchange(url, method, requestEntity, responseType, uriVariables);
	}

	/**
	 * 获取RestTemplate实例对象，可自由调用其方法
	 * 
	 * @return RestTemplate实例对象
	 */
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}
}
