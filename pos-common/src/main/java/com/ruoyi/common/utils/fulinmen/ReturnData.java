package com.ruoyi.common.utils.fulinmen;

import lombok.Data;

/**
 * @Description: 统一返回格式
 * @Author: WangHui
 * @CreateDate: 2019/4/19
 * @Version: 1.0
 * <p>Copyright: Copyright (c) 2018</p>
 */
@Data
public class ReturnData {
    private String resultCode;
    private String resultContent;
}