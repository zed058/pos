package com.ruoyi.common.utils.leshua;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author luobo
 * @title: LeshuaApiTransInfoBean
 * @projectName pos
 * @description: TODO
 * @date 2022-04-19 09:41:48
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LeshuaApiActiveInfoBean implements Serializable {
    private static final long serialVersionUID = -1;
    private String term_no; //机具终端号
    private String device_sn; //机具SN码
    private String status; //机具状态 0 未激活，1 已激活
    private String activation_type; //激活类型 0押金激活，1达标激活，2押金超期激活，3达标超期激活
    private String activation_time; //激活时间
    private String merchant_name; //机具绑定的商户名称
    private String merchant_no; //商户编号
    private String device_type; //机具类型
    private String deposit_amount; //止付金额（元）
}
