package com.ruoyi.common.utils.weCat.template;

import com.alibaba.fastjson.JSONObject;
import lombok.Builder;
import lombok.Data;

/**
 * @author luobo
 * @title: RefuseTemplate
 * @projectName pos
 * @description: 提现审核不通过消息模板
 * @date 2021-12-10 16:04:39
 */
@Data
@Builder
public class RefuseTemplate {
    private String touser;
    private String template_id;
    private String url;
    private Params data;

    @Data
    @Builder
    public static class Params{
        private JSONObject first;
        private JSONObject keyword1; //用户姓名
        private JSONObject keyword2; //图文标题
        private JSONObject keyword3; //发布时间
        private JSONObject keyword4; //审核结果
        private JSONObject remark;
    }
}
