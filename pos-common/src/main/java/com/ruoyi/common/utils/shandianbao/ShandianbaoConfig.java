package com.ruoyi.common.utils.shandianbao;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author luobo
 * @title: ShandianbaoConfig
 * @projectName pos
 * @description: 汇付天下配置信息
 * @date 2022-04-07 11:02:40
 */
@Data
@Component
@ConfigurationProperties(prefix = "shandianbao")
public class ShandianbaoConfig {
    private String key; //请求秘钥
    private String rugrId; //渠道id
    private String queryRegInfoUrl; //查询商户注册信息
    private String queryAuthInfoUrl; //查询商户认证信息
    private String queryTransInfoUrl; //查询交易记录请求地址
}
