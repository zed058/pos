package com.ruoyi.common.utils.fulinmen;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.UUID;

/**
 * 私钥加解密认证工具
 * 
 * @author zhangzhenqiang 2015年12月23日
 *
 */
public class PrivateKeySecurityUtil {

	public static final Log log = LogFactory.getLog(PrivateKeySecurityUtil.class);

	/**
	 * 私钥加密
	 * @param sendData
	 * @param des3Key
	 * @param privateKey
	 * @param sha1Key
	 * @return
	 * @throws Exception
	 */
	public static String encript(String sendData,String des3Key,String privateKey,String sha1Key) throws Exception {
		// 第二部分数据加密
		// 随机数
		String random = UUID.randomUUID().toString().substring(0, 8);
		// 随机数+RSA公钥 一起加密，得到RSA key
		byte[] rsaData = RSACoder.encryptByPrivateKey(random + des3Key, privateKey);
		// RSA key使用base64加密 得到第一部分
		String one = new String(Base64.encodeBase64(rsaData));
		String two = ThreeDES.encryptDESCBC(sendData, random + des3Key);
		// String two = new String(msgkey);
		// 最终第一部分，第二部分，KEY一起SHA1加密得到第三部分
		String data = one + two + sha1Key;
		String three = SHA1.encryptSHA(data);
		JSONObject obj = new JSONObject();
		obj.put("one", one.replaceAll(des3Key, ""));
		obj.put("two", two);
		obj.put("three", three);
		log.debug("加密字符串=" + obj.toString());
		return obj.toString();

	}

	/**
	 * 私钥解密
	 * @param reqMsg
	 * @param des3Key
	 * @param privateKey
	 * @param sha1Key
	 * @return
	 * @throws Exception
	 */
	public static String decript(String reqMsg,String des3Key,String privateKey,String sha1Key) throws Exception {

		JSONObject obj = JSONObject.parseObject(reqMsg);
		String paramOne = obj.getString("one");
		String paramTwo = obj.getString("two");
		String paramThree = obj.getString("three").replaceAll("-", "");
		log.debug("paramOne:" + paramOne);
		log.debug("paramTwo:" + paramTwo);
		log.debug("paramThree:" + paramThree);
		// 第一步SHA1验证
		String s = paramOne + paramTwo + sha1Key;
		String sha1 = SHA1.encryptSHA(s);
		if (!sha1.equals(paramThree.toUpperCase()) && !sha1.equals(paramThree.toLowerCase())) {
			log.error("第三部分MD5签名失败");
			return null;
		}
		// 第一部分
		// 解密随机数
		byte[] desRsaKey = Base64.decodeBase64(paramOne.getBytes("UTF-8"));
		byte[] randomByte = RSACoder.decryptByPrivateKey(desRsaKey, privateKey);
		String random = new String(randomByte);

		// 第二部分
		String message = ThreeDES.decryptDESCBC(paramTwo, random + des3Key);
		return message;
	}

}
