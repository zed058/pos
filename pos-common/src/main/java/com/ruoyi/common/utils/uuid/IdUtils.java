package com.ruoyi.common.utils.uuid;

import com.ruoyi.common.utils.DateUtils;

/**
 * ID生成器工具类
 * 
 * @author ruoyi
 */
public class IdUtils
{
    /**
     * 获取随机UUID
     * 
     * @return 随机UUID
     */
    public static String randomUUID()
    {
        return UUID.randomUUID().toString();
    }

    /**
     * 简化的UUID，去掉了横线
     * 
     * @return 简化的UUID，去掉了横线
     */
    public static String simpleUUID()
    {
        return UUID.randomUUID().toString(true);
    }

    /**
     * 获取随机UUID，使用性能更好的ThreadLocalRandom生成UUID
     * 
     * @return 随机UUID
     */
    public static String fastUUID()
    {
        return UUID.fastUUID().toString();
    }

    /**
     * 简化的UUID，去掉了横线，使用性能更好的ThreadLocalRandom生成UUID
     * 
     * @return 简化的UUID，去掉了横线
     */
    public static String fastSimpleUUID()
    {
        return UUID.fastUUID().toString(true);
    }

    /**
     * 生成6位纯数字随机数
     * @return
     */
    public static String getRandomForSix() {
        return String.valueOf((int)((Math.random()*9+1)*100000));
    }

    /**
     * 生成一个订单号 yyyymmddhhddhhmmm+三位随机数
     * @return
     */
    public static String getOrderNo() {
        return DateUtils.getNowDateStr() + getRandom();
    }

    /**
     * 获取三维随机数
     * @return
     */
    public static int getRandom() {
        return (int)(Math.random()*900)+100;
    }
}
