package com.ruoyi.common.utils.shandianbao;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author luobo
 * @title: APIRegResultbean
 * @projectName pos
 * @description: TODO
 * @date 2022-04-06 09:53:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApiResultRegBean {
    private String saruLruid; //商户号
    private String recommLruid; //推荐人
    private String phoneNo; //手机号
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date registerDate; //注册时间
    private String passWord; //密码（md5加密）
    private String vCode; //注册码
}
