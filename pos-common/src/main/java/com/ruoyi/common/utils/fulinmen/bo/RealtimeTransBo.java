package com.ruoyi.common.utils.fulinmen.bo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author luobo
 * @title: RealtimeTransBo
 * @projectName pos
 * @description: 付临门实时交易结果实体封装类
 * @date 2022-06-13 10:13:26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RealtimeTransBo {
    @JsonProperty(value = "agentNo")
    private String agentId; //代理商账号
    @JsonProperty(value = "keyRsp")
    private String orderId; //平台唯一流水号
    private String cardNo; //交易卡号

    //1011、消费  2011、消费撤销  1171、实时收款  1191、银联反扫  1181、银联反扫  1681、银联正扫  1691、银联正扫  1391、支付宝反扫  1381、支付宝反扫  1581、支付宝正扫  1591、支付宝正扫  1291、微信反扫  1281、微信反扫  1481、微信正扫  1491、微信正扫
    private String transType; //交易类型
    @JsonProperty(value = "memNo")
    private String merId; //商户号
    @JsonProperty(value = "memName")
    private String merName; //商户姓名
    @JsonProperty(value = "snNo")
    private String snCode; //SN号
    private String retriRefNo; //系统参考号
    @JsonProperty(value = "cardClass")
    private String cardType; //卡类型
    @JsonProperty(value = "transAmt") //交易金额    元为单位
    private BigDecimal transAmount; //交易金额(单位：分)
    @JsonProperty(value = "fee") //手续费    元为单位
    private BigDecimal extractionFee; //提现手续费(单位：分)
    private String transDate; //交易日期
    private String transTime; //交易时间
    private String posEntry; //支付方式   02、刷卡   05、插卡   07、非接   98、非接   01、手工   03、正扫/反扫


    private String transName; //交易名称
    private String mcc; //行业类别
    private String freezeflag; //冻结标识    Y 冻结、N 非冻结
    private String choicenessFlag; //精选标识    Y、精选   N、非精选   S、综合
    private BigDecimal stlmFee; //服务费    分为单位
    private BigDecimal communicationFee; //通讯费   分为单位：1、按年   2、按月   5、按3个月   6、按6个月
    private String communicationFlag; //通讯费规则
    private String cardBankName; //发卡行

}
