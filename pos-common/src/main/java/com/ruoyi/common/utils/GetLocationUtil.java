package com.ruoyi.common.utils;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.builder.ToStringExclude;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author luobo
 * @Date 2021/8/27 10:23
 * @Describetion 根据经纬度获取城市信息工具类
 */
public class GetLocationUtil {
    public static void main(String[] args) {
        System.out.println(getLocatin("112.091832411025,32.04224609375"));
        //System.out.println(getLocatin("0,0"));
        System.out.println(getCoor("湖北省襄阳市襄轴家园"));
    }

    public static Map<String,Object> getLocatin(String coor){
        Map<String,Object> map = new HashMap<>();
        String json = getAdd(coor);
//        String json = add.substring(add.indexOf("(")+1,add.lastIndexOf(")"));
        JSONObject jsonObject = JSONObject.parseObject(json);
        if("1".equals(jsonObject.getString("status"))
                && "10000".equals(jsonObject.getString("infocode"))){
            JSONObject regeocode = jsonObject.getJSONObject("regeocode");
            if(null != regeocode){
                map.put("address",regeocode.getString("formatted_address"));
                JSONObject addressComponent = regeocode.getJSONObject("addressComponent");
                if(null != addressComponent){
                    String province = addressComponent.getString("province");
                    if(province.endsWith("省")){
                        province = province.substring(0,province.length() - 1);
                    }else if (province.endsWith("市")){
                        province = province.substring(0,province.length() - 1);
                    }
                    map.put("province",province);
                    String city = addressComponent.getString("city");
                    map.put("city",city);
                }
            }
        }
        return map;
    }

    /**
     * 根据坐标获取具体地址
     * @param coor 坐标字符串
     * @return
     */
    public static String getAdd(String coor){
        String urlString = "http://restapi.amap.com/v3/geocode/regeo?key=39fbebe922015cc17aaf687dcdf11a0c&location="+ coor +"&radius=1000&extensions=base&batch=false&roadlevel=0";
        String res = "";
        try {
            URL url = new URL(urlString);
            java.net.HttpURLConnection conn = (java.net.HttpURLConnection)url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(conn.getInputStream(),"UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                res += line+"\n";
            }
            in.close();
        } catch (Exception e) {
            System.out.println("error in wapaction,and e is " + e.getMessage());
        }
        // System.out.println(res);
        return res;
    }

    /**
     * 根据地名获取坐标
     * @param
     */
    public static String getCoor(String address){
        String urlString = "http://restapi.amap.com/v3/place/text?s=rsv3&children=&key=8325164e247e15eea68b59e89200988b&page=1&offset=10&city=610100&language=zh_cn&callback=jsonp_25126_&platform=JS&logversion=2.0&sdkversion=1.3&appname=http%3A%2F%2Flbs.amap.com%2Fconsole%2Fshow%2Fpicker&csid=19FA0D45-180F-4D45-BCB4-C6C265F55FF6&keywords="+address;
        String res = "";
        try {
            //http://restapi.amap.com/v3/geocode/regeo?key=8325164e247e15eea68b59e89200988b&s=rsv3&location=101.539737903028,36.79828256329313&radius=2800&callback=jsonp_452865_&platform=JS&logversion=2.0&sdkversion=1.3&appname=http%3A%2F%2Flbs.amap.com%2Fconsole%2Fshow%2Fpicker&csid=49851531-2AE3-4A3B-A8C8-675A69BCA316
            URL url = new URL(urlString);
            java.net.HttpURLConnection conn = (java.net.HttpURLConnection)url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(conn.getInputStream(),"UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                res += line+"\n";
            }
            in.close();
        } catch (Exception e) {
            System.out.println("error in wapaction,and e is " + e.getMessage());
        }
        System.out.println(res);
        return res;
    }

}