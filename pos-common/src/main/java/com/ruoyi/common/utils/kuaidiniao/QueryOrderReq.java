package com.ruoyi.common.utils.kuaidiniao;

import lombok.Builder;
import lombok.Data;

/**
 * @author luobo
 * @title: QueryOrderBean
 * @projectName pos
 * @description: 快递鸟订单查询接口请求参数
 * @date 2021-12-09 19:03:05
 */
@Data
@Builder
public class QueryOrderReq {
    private String customerName;
    private String orderCode;
    private String shipperCode;
    private String logisticCode;
}
