package com.ruoyi.common.utils.shandianbao;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.json.JsonUtils;
import com.ruoyi.common.utils.rest.RestTemplateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author luobo
 * @title: ShandianbaoUtils
 * @projectName pos
 * @description: TODO
 * @date 2022-04-06 09:47:22
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class ShandianbaoUtils {

    private final RestTemplateUtil restTemplateUtil;
    private final ShandianbaoConfig shandianbaoConfig;

    //测试汇付天下接口
    public Map<String, String> test() throws Exception {
        String url = "http://112.124.6.222:8080/sdbpl-mobile/channel/queryInfoChannel/queryRegisterInfo";
        String param = ThreeDES.test();
        System.out.println("加密后数据：" + param);
        Map<String, String> params = new LinkedHashMap<String, String>();
        params.put("params", param);
        HttpHeaders herader = new HttpHeaders();
        herader.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<>();
        for (String key : params.keySet()) {
            bodyMap.add(key, params.get(key));
        }
        HttpEntity<MultiValueMap<String, String>> multiValueMapHttpEntity = new HttpEntity<>(bodyMap, herader);
        ResponseEntity<String> temp = restTemplateUtil.post(url, multiValueMapHttpEntity, String.class);
        if (temp.getStatusCode().value() == HttpStatus.OK.value()) {
            String response = temp.getBody();
            Map<String, String> resultMap = JSON.parseObject(response, new TypeReference<Map<String, String>>() {});
            if (null != resultMap.get("resultCode") && "0000".equals(resultMap.get("resultCode"))) {
                System.out.println("汇付天下返回结果：" + resultMap);
                String dd = resultMap.get("resultMsg");
                String sss = ThreeDES.decryptThreeDESECB(dd, "7560654CD81AED8541B05F1245714123");
                JSONObject jo = JSONObject.parseObject(sss);
                List<ApiResultRegBean> list = JsonUtils.jsonToList(jo.get("saruRegisterDetailsList").toString(), ApiResultRegBean.class);
                System.out.println(list);
            } else {
                log.info("接口返回信息失败：{}", resultMap.get("resultMsg"));
            }
            return resultMap;
        } else {
            throw new RuntimeException("查询汇付天下状态结果失败");
        }
    }

    //查询商户注册信息
    public List<ApiResultRegBean> queryRegInfoList(ApiRequestBean request) throws Exception {
        log.info("查询商户注册信息请求原始参数：{}", request);
        String param = ThreeDES.encryptThreeDESECB(JsonUtils.pojoToJson(request), shandianbaoConfig.getKey());
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("params", param);
        ResponseEntity<String> temp = restTemplateUtil.postForm(shandianbaoConfig.getQueryRegInfoUrl(), params, String.class);
        if (temp.getStatusCode().value() == HttpStatus.OK.value()) {
            String response = temp.getBody();
            log.info("汇付天下返回body：" + response);
            ApiBaseResponseBean baseBean = JsonUtils.jsonToPojo(response, ApiBaseResponseBean.class);
            if (StringUtils.isNotEmpty(baseBean.getResultCode()) && "0000".equals(baseBean.getResultCode())) {
                String sss = ThreeDES.decryptThreeDESECB(baseBean.getResultMsg(), shandianbaoConfig.getKey());
                JSONObject jo = JSONObject.parseObject(sss);
                List<ApiResultRegBean> list = JsonUtils.jsonToList(jo.get("saruRegisterDetailsList").toString(), ApiResultRegBean.class);
                log.info("汇付天下返回结果集：{}", list);
                return list;
            } else {
                log.info("接口返回信息失败：{}", baseBean.getResultMsg());
            }
            return null;
        } else {
            throw new RuntimeException("查询汇付天下状态结果失败");
        }
    }

    //查询商户认证信息
    public List<ApiResultAuthBean> queryAuthInfoList(ApiRequestBean request) throws Exception {
        log.info("查询商户认证信息请求原始参数：{}", request);
        String param = ThreeDES.encryptThreeDESECB(JsonUtils.pojoToJson(request), shandianbaoConfig.getKey());
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("params", param);
        ResponseEntity<String> temp = restTemplateUtil.postForm(shandianbaoConfig.getQueryAuthInfoUrl(), params, String.class);
        if (temp.getStatusCode().value() == HttpStatus.OK.value()) {
            String response = temp.getBody();
            log.info("汇付天下返回body：" + response);
            ApiBaseResponseBean baseBean = JsonUtils.jsonToPojo(response, ApiBaseResponseBean.class);
            if (StringUtils.isNotEmpty(baseBean.getResultCode()) && "0000".equals(baseBean.getResultCode())) {
                String sss = ThreeDES.decryptThreeDESECB(baseBean.getResultMsg(), shandianbaoConfig.getKey());
                JSONObject jo = JSONObject.parseObject(sss);
                List<ApiResultAuthBean> list = JsonUtils.jsonToList(jo.get("saruRegisterDetailsList").toString(), ApiResultAuthBean.class);
                log.info("汇付天下返回结果集：{}", list);
                return list;
            } else {
                log.info("接口返回信息失败：{}", baseBean.getResultMsg());
            }
            return null;
        } else {
            throw new RuntimeException("查询汇付天下状态结果失败");
        }
    }

    //查询商户交易信息列表
    public List<ApiResultTransBean> queryTransInfoList(ApiRequestBean request) throws Exception {
        log.info("同步交易结果数据请求原始参数：{}", request);
        String param = ThreeDES.encryptThreeDESECB(JsonUtils.pojoToJson(request), shandianbaoConfig.getKey());
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("params", param);
        ResponseEntity<String> temp = restTemplateUtil.postForm(shandianbaoConfig.getQueryTransInfoUrl(), params, String.class);
        if (temp.getStatusCode().value() == HttpStatus.OK.value()) {
            String response = temp.getBody();
            log.info("汇付天下返回body：" + response);
            ApiBaseResponseBean baseBean = JsonUtils.jsonToPojo(response, ApiBaseResponseBean.class);
            if (StringUtils.isNotEmpty(baseBean.getResultCode()) && "0000".equals(baseBean.getResultCode())) {
                String sss = ThreeDES.decryptThreeDESECB(baseBean.getResultMsg(), shandianbaoConfig.getKey());
                JSONObject jo = JSONObject.parseObject(sss);
                List<ApiResultTransBean> list = JsonUtils.jsonToList(jo.get("transDetailsVOList").toString(), ApiResultTransBean.class);
                log.info("汇付天下返回结果集：{}", list);
                return list;
            } else {
                log.info("接口返回信息失败：{}", baseBean.getResultMsg());
            }
            return null;
        } else {
            throw new RuntimeException("查询商户交易信息列表失败");
        }
    }
}
