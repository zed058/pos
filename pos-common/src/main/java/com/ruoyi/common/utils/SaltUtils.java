package com.ruoyi.common.utils;

import org.apache.shiro.crypto.hash.SimpleHash;

import java.util.UUID;

/**
 * @author luobo
 * @title: SaltUtils
 * @projectName pos
 * @description: 密码加盐工具类
 * @date 2021/11/4 000413:38
 */
public class SaltUtils {
    /**
     * 生成32的随机盐值
     */
    public static String createSalt(){
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 加盐加密
     * @param srcPwd    原始密码
     * @param saltValue 盐值
     */
    public static String salt(Object srcPwd, String saltValue){
        return new SimpleHash("MD5", srcPwd, saltValue, 1024).toString();
    }


    public static void main(String[] args) {

        String pa= "123456";
        String salt = createSalt();
        String salt1 = SaltUtils.salt("123456", "51e35c70a9ff478c8c696a87a3668483");
//        String salt = "be8ae4688b0d3510895dc39be58ad30f";
        String salt2 = SaltUtils.salt(pa, salt);;
//        System.out.println(salt1);
        System.out.println("be8ae4688b0d3510895dc39be58ad30f");
        System.out.println(salt1);
    }
}
