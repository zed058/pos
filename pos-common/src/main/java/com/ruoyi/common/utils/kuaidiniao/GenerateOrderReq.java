package com.ruoyi.common.utils.kuaidiniao;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author luobo
 * @title: GenerateOrderBean
 * @projectName pos
 * @description: 快递鸟下单接口请求参数
 * @date 2021-12-09 19:08:04
 */
@Data
@Builder
public class GenerateOrderReq {
    private String orderCode;
    private String shipperCode;
    private String payType;
    private String expType;
    private Sender sender;
    private Receiver receiver;
    private List<Goods> commodity;
    @Data
    @Builder
    public static class Sender{
        private String company;
        private String name;
        private String mobile;
        private String provinceName;
        private String cityName;
        private String expAreaName;
        private String address;
    }
    @Data
    @Builder
    public static class Receiver{
        private String company;
        private String name;
        private String mobile;
        private String provinceName;
        private String cityName;
        private String expAreaName;
        private String address;
    }
    @Data
    @Builder
    public static class Goods{
        private String goodsName;
        private String goodsquantity;
        private String goodsWeight;
    }
    private String weight;
    private String quantity;
    private String volume;
    private String remark;
}



















