package com.ruoyi.common.utils.qugongbao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author luobo
 * @title: SignContractReq
 * @projectName pos
 * @description: 创客签约状态查询接⼝请求参数封装类
 * @date 2021-12-29 16:42:36
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SignContractStatusReq {
    private String memberNo; //创客编号
    private String spCode; //服务商代码
    private String contractNo; //合同编号
}
