package com.ruoyi.common.utils.qugongbao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author luobo
 * @title: memberRegReq
 * @projectName pos
 * @description: 趣工宝注册接口请求参数封装类
 * @date 2021-12-29 14:46:48
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MemberRegReq {
    private String spCode; //服务商代码
    private List<MemberRegReq.MemberRegDetail> dataList; //创客注册

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class MemberRegDetail {
        private String memberName; //创客姓名
        private String mobile; //创客⼿机号
        private String idCardNo; //证件编号(身份证号)
        private String accountType; //账户类型
        private String bankCardNo; //银行卡号
    }
}
