package com.ruoyi.common.utils.qugongbao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/***
 * @Description: API接口请求参数
 * @author gangwei.chen
 * @date 2019年6月24日上午10:35:36
 * @version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ApiBaseRequestBean implements Serializable {
    private static final long serialVersionUID = -1;

    //"1174590085794889728", required = true
    private String custNo;

    // "请求的时间戳，接入系统的时间误差不能超过10分钟，格式为：yyyy-MM-dd HH:mm:ss，如：2019-07-17 12:58:15", required = true)
    private String timestamp;

    // 请求流水号", required = true)
    private String reqNo;

    // "SHA256WithRSA",  required = true)
    @Builder.Default
    private String signType = "SHA256WithRSA";

    // "签名后字符串，通过签名机制，防止应用的请求参数被非法篡改，业务系统必须保证该值不被泄露",  required = true)
    private String signValue;

    //"接口版本 默认1.0", example = "1.0", required = true)
    @Builder.Default
    private String v = "1.0";

    // "服务请求的Json 字符串", name = "body", required = true)
    private String body;

}
