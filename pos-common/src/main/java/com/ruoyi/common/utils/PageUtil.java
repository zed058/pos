package com.ruoyi.common.utils;

import com.github.pagehelper.PageHelper;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.utils.sql.SqlUtil;
import org.springframework.beans.BeanUtils;

/**
 * @Author luobo
 * @Date 2021/8/27 10:23
 * @Describetion 分页辅助工具类
 */
public class PageUtil {
    /**
     * 设置请求分页数据
     */
    public static void startPage(Object obj) {
        if (null != obj) {
            PageDomain pageDomain = new PageDomain();
            BeanUtils.copyProperties(obj, pageDomain);
            Integer pageNum = pageDomain.getPageNum();
            Integer pageSize = pageDomain.getPageSize();
            if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
                String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
                Boolean reasonable = pageDomain.getReasonable();
                PageHelper.startPage(pageNum, pageSize, orderBy).setReasonable(reasonable);
            }
        }
    }
    /**
     * 设置请求分页数据
     */
    public static void startPage(Integer pageNum, Integer pageSize) {
            PageDomain pageDomain = new PageDomain();
            if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
                String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
                Boolean reasonable = pageDomain.getReasonable();
                PageHelper.startPage(pageNum, pageSize, orderBy).setReasonable(reasonable);
            }
    }
}