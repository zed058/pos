package com.ruoyi.common.utils;

import com.alibaba.fastjson.JSONObject;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.ruoyi.common.config.RuoYiConfig;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class QrCodeUtils {

    /**
     * 创建一个默认大小的二维码，并存放在默认路径下
     * @param fileName 文件名
     * @param content 二维码内容
     * @return
     * @throws IOException
     */
    public static String createQRCodeDefultForDefultPath(String fileName, String content) throws IOException {
        return createQRCodeDefult(RuoYiConfig.getQrPath(), fileName, content);
    }
    /**
     * 创建一个默认大小的二维码，存放在指定路径下
     * @param savePath 文件路径
     * @param fileName 文件名
     * @param content 二维码内容
     * @return
     * @throws IOException
     */
    public static String createQRCodeDefult(String savePath, String fileName, String content) throws IOException {
        return createQRCode(savePath, fileName, 300, 300, content);
    }
    /**
     * 生成一个指定大小的二维码图片，存放在指定目录下
     *
     * @param width
     * @param height
     * @param content
     * @return
     * @throws WriterException
     * @throws IOException
     */
    public static String createQRCode(String savePath, String fileName, int width, int height, String content) throws IOException {
        OutputStream op = null;
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        File desc = new File(savePath + File.separator + fileName);
        try {
            if (!desc.getParentFile().exists()) {
                desc.getParentFile().mkdirs();
            }
            if (!desc.exists()) {
                desc.createNewFile();
            }
            // 二维码基本参数设置
            Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");// 设置编码字符集utf-8
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);// 设置纠错等级L/M/Q/H,纠错等级越高越不易识别，当前设置等级为最高等级H
            hints.put(EncodeHintType.MARGIN, 2);// 可设置范围为0-10，但仅四个变化0 1(2) 3(4 5 6) 7(8 9 10)
            // 生成图片类型为QRCode
            BarcodeFormat format = BarcodeFormat.QR_CODE;
            // 创建位矩阵对象
            BitMatrix bitMatrix = new MultiFormatWriter().encode(content, format, width, height, hints);
            // 设置位矩阵转图片的参数
            //        MatrixToImageConfig config = new MatrixToImageConfig(Color.black.getRGB(), Color.white.getRGB());
            // 位矩阵对象转流对象
            MatrixToImageWriter.writeToStream(bitMatrix, "png", os);
            op = new FileOutputStream(savePath + File.separator + fileName);
            op.write(os.toByteArray());
            op.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != os) {
                os.close();
            }
            if (null != op) {
                op.close();
            }
        }
        return desc.getAbsolutePath();
    }


    public static void main(String[] args) throws WriterException, IOException {
        JSONObject jo = new JSONObject();
        jo.put("orderId", "orderId");
        jo.put("userUrl", "userUrl");
        String qrCodePath = QrCodeUtils.createQRCodeDefultForDefultPath("orderId" + ".jpg", jo.toString());
        System.out.println(qrCodePath);
           /*byte[] b = createQRCode( 100 ,  100 , "遇见最好的自己！");
          
           OutputStream os = new FileOutputStream("D:\\bestme.png");
          
           os.write(b);
          
           os.close();*/
        //createQRCode("D:\\", "0x95dDe2D33aEDF03Cb32d860048f5864194851e97.jpg", 300, 300, "https://fanmaiji.dev.hbbeisheng.com/user?deviceId=1&direction=l");
    }

}
