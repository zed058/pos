package com.ruoyi.common.exception.localAssert;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * @author luobo
 * @title: LocalAssert
 * @projectName pos
 * @description: 自定义Assert异常
 * @date 2022-02-25 17:37:36
 */
public abstract class LocalAssert extends Assert {
    public static void hasNotText(String str, String message) {
        if (!StringUtils.hasText(str)) {
            throw new IllegalStateException(message);
        }
    }
}
