package com.ruoyi.pos.api.dto.user;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

/**
 * @author luobo
 * @title: UserAddressDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-10 17:16:04
 */
@Data
public class UserAddressDto extends BaseDto {
    private String id; //收货地址id
    private String userName; //收货人姓名
    private String phone; //收货人手机号
    private String fixationPhone; //收货人固定电话
    private String district; //地区
    private String address; //详情地址
    private String isValid; //是否查询默认地址（0：是，1：否）
    private String districtCode; //城市code
}
