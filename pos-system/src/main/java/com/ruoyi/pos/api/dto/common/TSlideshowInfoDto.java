package com.ruoyi.pos.api.dto.common;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

/**
 * @author luobo
 * @title: TSlideshowInfoDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-11 14:00:09
 */
@Data
public class TSlideshowInfoDto extends BaseDto {
    private String imgType; //图片类型(1:首页弹窗,2:首页轮播图,3:广告展示,4:分享图片,5:腰部广告图)
}
