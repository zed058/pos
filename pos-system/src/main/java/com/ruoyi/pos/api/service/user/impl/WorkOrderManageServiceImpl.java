package com.ruoyi.pos.api.service.user.impl;

import com.ruoyi.common.enums.AuditStatusEnum;
import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.PageUtil;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.pos.api.dto.BaseDto;
import com.ruoyi.pos.api.dto.user.WorkOrderDto;
import com.ruoyi.pos.api.service.user.IWorkOrderManageService;
import com.ruoyi.pos.api.utils.CommonUtils;
import com.ruoyi.pos.api.vo.user.WorkOrderVo;
import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.domain.TUserWorkOrderInfo;
import com.ruoyi.pos.web.mapper.TUserInfoMapper;
import com.ruoyi.pos.web.mapper.TUserWorkOrderInfoMapper;
import com.ruoyi.pos.web.mapper.TWordOrderTypeInfoMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author luobo
 * @title: WorkOrderManageServiceImpl
 * @projectName pos
 * @description: TODO
 * @date 2021-11-12 08:41:59
 */
@Service
public class WorkOrderManageServiceImpl implements IWorkOrderManageService {
    @Autowired
    private CommonUtils commonUtils;
    @Autowired
    private TUserInfoMapper userInfoMapper;
    @Autowired
    private TUserWorkOrderInfoMapper userWorkOrderInfoMapper;
    @Autowired
    private TWordOrderTypeInfoMapper wordOrderTypeInfoMapper;
    //发布工单
    @Override
    @Transactional
    public void saveWorkOrderInfo(WorkOrderDto workOrderDto) {
        String userId = commonUtils.getUserId();
        TUserInfo tUserInfo = userInfoMapper.selectTUserInfoByUserId(userId);
        workOrderDto.setCreateTime(DateUtils.getNowDate());
        workOrderDto.setUserName(tUserInfo.getUserName());
        workOrderDto.setUserId(userId);
        workOrderDto.setAudit(YesOrNoEnums.YES.getCode());//默认待审核
        TUserWorkOrderInfo userWorkOrderInfo = new TUserWorkOrderInfo();
        BeanUtils.copyProperties(workOrderDto,userWorkOrderInfo);
        userWorkOrderInfo.setId(IdUtils.simpleUUID());
        userWorkOrderInfoMapper.insertTUserWorkOrderInfo(userWorkOrderInfo);
    }

    //查询工单详情
    @Override
    public WorkOrderVo queryWorkOrderInfoDeatil(WorkOrderDto workOrderDto) {
        TUserWorkOrderInfo userWorkOrderInfo = new TUserWorkOrderInfo();
        userWorkOrderInfo.setId(workOrderDto.getId());
        WorkOrderVo workOrderVo = new WorkOrderVo();
        List<WorkOrderVo> workOrderVos = userWorkOrderInfoMapper.queryTUserWorkOrderListApi(userWorkOrderInfo);
        BeanUtils.copyProperties(workOrderVos.get(0),workOrderVo);
        workOrderVo.setAuditShow(Optional.ofNullable(AuditStatusEnum.getEnumsByCode(workOrderVo.getAudit())).map(item-> item.getValue()).orElse(workOrderVo.getAudit()));
        return workOrderVo;
    }

    //查询工单列表
    @Override
    public List<WorkOrderVo> queryWorkOrderInfoList(WorkOrderDto workOrderDto) {
        String userId = commonUtils.getUserId();
        TUserWorkOrderInfo userWorkOrderInfo = new TUserWorkOrderInfo();
        userWorkOrderInfo.setUserId(userId);
        PageUtil.startPage(workOrderDto);
        List<WorkOrderVo> tUserWorkOrderInfos = userWorkOrderInfoMapper.queryTUserWorkOrderListApi(userWorkOrderInfo);//工单
        List<WorkOrderVo> workOrderVos = new ArrayList<>();
        Optional.ofNullable(tUserWorkOrderInfos).ifPresent(list ->{
            list.stream().forEach(bean->{
                WorkOrderVo workOrderVo = new WorkOrderVo();
                BeanUtils.copyProperties(bean,workOrderVo);
                workOrderVo.setAuditShow(Optional.ofNullable(AuditStatusEnum.getEnumsByCode(workOrderVo.getAudit())).map(item-> item.getValue()).orElse(workOrderVo.getAudit()));
                workOrderVos.add(workOrderVo);
            });
        });

        return workOrderVos;
    }
}
