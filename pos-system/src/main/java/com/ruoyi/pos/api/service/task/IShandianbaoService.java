package com.ruoyi.pos.api.service.task;

import com.ruoyi.pos.web.domain.TTerminalBrandInfo;
import com.ruoyi.pos.web.domain.TUserInfo;

import java.util.List;

/**
 * @author luobo
 * @title: ISandianbaoService
 * @projectName pos
 * @description: TODO
 * @date 2022-04-08 14:12:39
 */
public interface IShandianbaoService {
    TTerminalBrandInfo queryTTerminalBrandInfo();
    List<TUserInfo> queryUserList();
    void syncRegInfo(TUserInfo user, TTerminalBrandInfo brand) throws Exception;
    void syncAuthInfo(TUserInfo user, TTerminalBrandInfo brand) throws Exception;
    void syncTransInfo(TTerminalBrandInfo brand) throws Exception;
}
