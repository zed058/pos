package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author luobo
 * @title: UserOpinionVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-11 08:47:05
 */
@Data
public class UserOpinionVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id; //意见id
    private String opinion; //意见描述
    private Date createTime; //发布时间
    private String contactWay; //联系方式
    private String userId; //用户id
    private String userName; //用户姓名
    private String reply; //意见回复信息
    private List<OpinionImgBean> imgList; //意见图片集合

    @Data
    public static class OpinionImgBean{
        private String opinionImg; //意见图片
    }
}
