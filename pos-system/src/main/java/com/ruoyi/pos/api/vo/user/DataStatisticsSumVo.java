package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Administrator
 * @title: DataStatisticsSumVo
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class DataStatisticsSumVo {
    private String dateTime; //时间
    private BigDecimal teamTransMoney;//今日(本月)团队交易额
    private BigDecimal myTransMoney;//今日(本月)我的交易额
    private BigDecimal zhiTransMoney;//今日(本月)直营交易额

    private BigDecimal teamTransNum;//今日(本月)团队交易笔数
    private BigDecimal myTransNum;//今日(本月)我的交易笔数
    private BigDecimal zhiTransNum;//今日(本月)直营交易笔数

    private BigDecimal teamTerminalNum;//今日(本月)团队累计终端
    private BigDecimal myTerminalNum;//今日(本月)我的新增终端
    private BigDecimal zhiTerminalNum;//今日(本月)新增直营终端

    private BigDecimal teamPartnerNum;//今日(本月)团队新增伙伴
    private BigDecimal myPartnerNum;//今日(本月)我的新增伙伴
    private BigDecimal zhiPartnerNum;//今日(本月)新增直营伙伴

}
