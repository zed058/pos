package com.ruoyi.pos.api.service.common;


import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.leshua.LeshuaApiBaseRequestBean;
import com.ruoyi.pos.api.dto.BaseDto;
import com.ruoyi.pos.api.dto.common.*;
import com.ruoyi.pos.api.vo.common.*;
import com.ruoyi.pos.api.vo.user.BankVo;
import com.ruoyi.pos.api.vo.user.TGoldRankingInfoVo;
import com.ruoyi.pos.api.vo.user.VipExplainVo;
import com.ruoyi.pos.web.domain.TOrderInfo;
import com.ruoyi.pos.web.domain.TRebateSetInfo;
import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.domain.TWordOrderTypeInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @Author luobo
 * @Date 2021/7/17 15:17
 */
public interface ICommonService {

    //查询系统基础配置信息
    TRebateSetInfo queryTRebateSetInfoVo() throws Exception;
    //查询会员等级列表
    Map<String, Object> queryGradeInfoList(BaseDto baseDto) throws Exception;
    //查询关于我们信息
    TAboutUsInfoVo queryTAboutUsInfoInfo() throws Exception;
    //查询终端品牌列表
    List<TerminalBrandVo> queryTerminalBrandList() throws Exception;
    //查询终端商品列表
    Map<String, Object> queryTerminalGoodsList(TerminalGoodsDto terminalGoodsDto) throws Exception;
    //查询终端商品详情信息
    TerminalGoodsBuyVo queryTerminalGoodsDetail(TerminalGoodsDto terminalGoodsDto) throws Exception;
    //查询金刚导航栏列表
    List<NavigationInfoVo> queryNavigationInfoList() throws Exception;
    //查询轮播图列表
    Map<String, Object> queryTSlideshowInfoList(TSlideshowInfoDto slideshowInfoDto) throws Exception;
    //查询平台政策/商学院列表
    Map<String, Object> queryPolicyInfoList(TPolicyInfoDto policyInfoDto) throws Exception;
    //查询自提点列表
    List<TSelfPickupPointVo> querySelfPickupPointList(CommonDto commonDto) throws Exception;
    //根据微信回调信息，更新订单状态
    void weCatPayNotify(Map map) throws Exception;
    //处理兑换商品的兑换逻辑
    void handleExchangeOrder(TOrderInfo order, TUserInfo user) throws Exception;
    //终端交易结果通知，终端产生交易时，第三方平台调用
    JSONObject terminalTransResultNotify(String bodyStr, String sign) throws Exception;
    //新增/变更商户结果通知，新增/变更商户时，第三方平台调用
    JSONObject newMerchantNotify(String bodyStr, String sign) throws Exception;
    //智强科技预制码交易结果通知，终端产生交易时，第三方平台调用
    String preCodeTransResultNotify(HttpServletRequest request) throws Exception;
    //乐刷交易结果通知，终端产生交易时，第三方平台调用
    String leshuaTransResultNotify(LeshuaApiBaseRequestBean bean) throws Exception;
    //乐刷激活交易结果通知，终端产生交易时，第三方平台调用
    String leshuaActiveResultNotify(LeshuaApiBaseRequestBean bean) throws Exception;
    //趣工宝注册接口
    JSONObject memberRegUrl(String userName, String userPhone, String idCardNo, String bankCardNo) throws Exception;
    //趣工宝签约接口
    JSONObject signContract(String memberNo, String serverCallbackUrl, String pageCallbackUrl) throws Exception;
    boolean updateBankCard(String memberNo, String bankCardNo) throws Exception;
    JSONObject getMemberInfo(String memberNo) throws Exception;
    //趣工宝签约接口
    JSONObject queryContractStatus(String memberNo, String spCode) throws Exception;
    //趣工宝提现接口
    Map<String, String> withdrawalForPay() throws Exception;
    //趣工宝提现结果查询
    Map<String, String> withdrawalResultQuery() throws Exception;
    //趣工宝提现结果通知接口
    JSONObject withdrawalResultNotify(WithdrawalResultNotifyDto dto) throws Exception;
    //趣工宝提现退票查询接口
    JSONObject withdrawalResultForRefund(WithdrawalRefundResultNotifyDto dto) throws Exception;
    //趣工宝签约结果通知接口
    JSONObject signContractResultNotify(WithdrawalResultNotifyDto dto) throws Exception;
    //查询工单类型列表
    List<TWordOrderTypeInfo> queryTWordOrderTypeInfoList() throws Exception;

    List<TGoldRankingInfoVo> queryTGoldRankingInfo(String dealAwardType) throws Exception;

    List<VipExplainVo> queryVipExplain() throws Exception;

    /*银行维护*/
    List<BankVo> queryBankList() throws Exception;

    /*微信授权空白页域名*/
    String getAuthorizationUrl(String url)throws Exception;


    /*微信公众号签名授权*/
    Map<String, String> getPermissionsToSign(String url) throws Exception;

    /*code换openId*/
    JSONObject queryWxOpenId(String code) throws Exception;
}
