package com.ruoyi.pos.api.dto.user;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author luobo
 * @title: WorkOrderDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-12 08:42:45
 */
@Data
public class WorkOrderDto extends BaseDto {
    private String id; //工单id
    private String userId; //用户id
    private String userName; //用户名称
    private String title; //工单管理标题
    private String workOrderId; //工单管理类别id
    private String workOrderName; //工单管理类别名称
    private String reason; //申请理由
    private String terminalName; //产品名称
    private String terminalImg; //上传机器SN图片
    private String workOrderDescribe; //工单描述
    private String describeImg; //工单描述图片
    private String audit; //工单审核（0：待审核，1：审核通过，2：审核驳回）
    private String rejectReason; //驳回理由
    private Date createTime; //创建时间
    private Date auditTime; //审核时间

    private List<WorkOrderDto.WorkOrderImgBean> imgList; //工单图片集合
    @Data
    public static class WorkOrderImgBean{
        private String workOrderImg; //工单图片
    }
}
