package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.util.Date;

/**
 * @author luobo
 * @title: TerminalTransRecordVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-18 11:28:43
 */
@Data
public class TerminalTransRecordVo {
    private String terminalTransId; //划拨记录id
    private String snCode; //终端SN号
    private String transOutUserId; //划出人
    private String transOutUserName; //划出人姓名
    private String transInUserId; //接收人
    private String transInUserName; //接收人姓名
    private Date transOutTime; //划出时间
    private Date transInTime; //接收时间
    private String transStatus; //划拨状态（0、待确认，1、已确认，2、拒绝)
    private String transStatusShow; //划拨状态（0、待确认，1、已确认，2、拒绝)
    private String transType; //划拨方式（0、划拨，1、划回）
}
