package com.ruoyi.pos.api.vo.user;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * @author Administrator
 * @title: BankVo
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class BankVo {
    /** 数据主键 */
    private String bankId;
    /** 银行名称 */
    private String bankName;
}
