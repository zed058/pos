package com.ruoyi.pos.api.dto.common;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

/**
 * @author luobo
 * @title: ExchangeDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-18 08:44:14
 */
@Data
public class ExchangeDto extends BaseDto {
    private String transType;//(1:交易金序列,2:号奖励金序列号)必填
    private Integer pageNumw;//奖励金分页
    private Integer pageSizew;//奖励金分页
}
