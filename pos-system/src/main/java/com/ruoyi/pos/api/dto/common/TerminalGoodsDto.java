package com.ruoyi.pos.api.dto.common;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

/**
 * @author luobo
 * @title: TerminalModelDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-12 10:15:18
 */
@Data
public class TerminalGoodsDto extends BaseDto {
    private String brandId; //品牌id（当传0时，默认查询的是终端购买的热门推荐）
    private String isPopular; //是否热门推荐（0：是，1：不是）
    private String goodsName; //商品名称
    private String goodsType; //商品类型（0：终端商品，1：兑换商品）
    private String goodsStatus; //商品交易类型（10:采购,20:积分兑换，21:交易金兑换，22:奖励金兑换，23:金币兑换）
    private String goodsId; //商品id
}
