package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luobo
 * @title: MainVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-17 09:39:05
 */
@Data
public class MainVo {

    //平台实时总交易量
    private BigDecimal totalTrans; //平台实时总交易量
    private BigDecimal commandAwardFund; //司令部奖励基金

    //我的交易
    private BigDecimal myTrans; //我的当月交易量
    private BigDecimal myDirectlyTrans; //我的直营交易量
    private BigDecimal myTeamTrans; //我的团队交易量

    //我的商户
    private BigDecimal myTerminal; //我的终端数
    private BigDecimal myDirectlyTerminal; //我的直营终端数
    private BigDecimal myTeamTerminal; //我的团队终端数

    //我的团队
    private BigDecimal myNewPartner; //本月新增伙伴
    private BigDecimal myDirectlyPartner; //我的直营伙伴
    private BigDecimal myTeamPartner; //我的团队伙伴
}
