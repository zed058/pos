package com.ruoyi.pos.api.vo.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Administrator
 * @title: GoldserialVo
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class GoldserialVo {

    private Long id;//序列号

    /** 是否已兑换(0:是，1：否) */
    private String exchange;
    private String exchangeShow; //是否已兑换 中文名
    /** 兑换时间 */
    private Date exchangeTime;
    /** 兑换奖励金（金额） */
    private BigDecimal amount;
    /*用户名称*/
    private String userName;
    /*序列号获取时间*/
    private Date createTime;
    private Long serialNo;
}
