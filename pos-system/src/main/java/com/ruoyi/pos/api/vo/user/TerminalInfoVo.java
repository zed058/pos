package com.ruoyi.pos.api.vo.user;

import lombok.Data;

/**
 * @author luobo
 * @title: TerminalInfoVo
 * @projectName pos
 * @description: 终端信息
 * @date 2021-11-16 08:36:39
 */
@Data
public class TerminalInfoVo {
    private String userId; //用户id
    private String userName; //用户名称
    private String userImg;//用户头像
    private String phone; //手机号
    private String snCode; //终端sn码
    private String activateStatus; //激活状态(0：未激活，1：已激活)
    private String activateStatusShow; //激活状态(0：未激活，1：已激活)
    private String bindingStatus; //绑定状态(0：未绑定，1：已绑定)
    private String bindingStatusShow; //绑定状态(0：未绑定，1：已绑定)
    private String transStatus; //划拨状态（10、未划拨，20、待确认，30、已确认，40、拒绝)
    private String transStatusShow; //划拨状态（10、未划拨，20、待确认，30、已确认，40、拒绝)
    private String terminalType; //终端获取方式（10：线上购买，20：终端划拨，30：兑换终端）
    private String terminalTypeShow; //终端获取方式（10：线上购买，20：终端划拨，30：兑换终端）
    private String transType; //queryTerminalList接口自定义状态（0:显示按钮）
}
