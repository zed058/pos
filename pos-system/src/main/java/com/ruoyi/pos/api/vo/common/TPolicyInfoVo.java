package com.ruoyi.pos.api.vo.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @author luobo
 * @title: TPolicyInfoVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-11 13:51:44
 */
@Data
public class TPolicyInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String type; //类型(0=：平台政策，1：商学院)
    private String title; //平台政策/商学院标题
    private String describeText; //平台政策/商学院(富文本)
}
