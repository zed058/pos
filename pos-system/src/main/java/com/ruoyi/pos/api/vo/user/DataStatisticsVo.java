package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Administrator
 * @title: DataStatisticsVo
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class DataStatisticsVo {
    private String dateTime; //统计图时间
    private BigDecimal myTransMoney; //我的交易总额
    private BigDecimal myTransNum; //我的交易笔数
    private BigDecimal myTerminalNum; //我的终端数
    private BigDecimal myPartnerNum; //我的伙伴数
    private Integer userCount;//新增用户
//    private BigDecimal myBuyTerminalNum; //我的购买终端数
//    private BigDecimal myExchangelTerminalNum; //我的兑换终端数
//    private BigDecimal myOutTerminalNum; //我的划出终端数
//    private BigDecimal myInTerminalNum; //我的划入终端数
}
