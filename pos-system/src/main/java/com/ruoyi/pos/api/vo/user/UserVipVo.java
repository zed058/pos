package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Administrator
 * @title: UserVipVo
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class UserVipVo {
    private String userImg;//用户头像
    private String userName;//用户名称
    private String userPhone;//用户手机号
    private String gradeCode;//vip等级
    private String gradeName;//vip等级名称
    private BigDecimal myTransMoney;//我的交易金额
    private BigDecimal myTerminalNum;//我的终端数
}
