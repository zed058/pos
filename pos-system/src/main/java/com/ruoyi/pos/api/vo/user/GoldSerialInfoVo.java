package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author luobo
 * @title: GoldSerialInfoVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-24 08:37:37
 */
@Data
public class GoldSerialInfoVo {
    private String type;
    private Long id;
    private String userId; //用户id
    private String exchange; //是否已兑换(0:是，1：否)
    private Date exchangeTime; //兑换时间
    private BigDecimal amount;
}
