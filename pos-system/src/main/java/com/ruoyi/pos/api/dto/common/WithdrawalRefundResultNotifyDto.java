package com.ruoyi.pos.api.dto.common;

import lombok.*;

import java.io.Serializable;

/**
 * @author luobo
 * @title: WithdrawalRefundResultNotifyDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-19 11:16:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WithdrawalRefundResultNotifyDto implements Serializable {
    @NonNull
    private String code;
    @NonNull
    private String msg;
    @NonNull
    private String signType;
    @NonNull
    private String signValue;
    private String body;

}
