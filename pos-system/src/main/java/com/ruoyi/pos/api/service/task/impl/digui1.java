package com.ruoyi.pos.api.service.task.impl;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.pos.api.vo.user.UserInfoVo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author luobo
 * @title: digui1
 * @projectName pos
 * @description: TODO
 * @date 2021/11/6 000614:02
 */
public class digui1 {
    public static void main(String[] args) {
        List<UserInfoVo> zhiying = new ArrayList<UserInfoVo>();
        List<UserInfoVo> tuandui = new ArrayList<UserInfoVo>();
        UserInfoVo user = new UserInfoVo();
        System.out.println("-----------"+System.currentTimeMillis());
        //digui(user, true, zhiying, tuandui);
    }

    public static void digui (UserInfoVo user, boolean flag, List<UserInfoVo> zhiying, List<UserInfoVo> tuandui) {
        //判断是否有邀请人
        if (StringUtils.isNotEmpty(user.getInviteCode())) { //邀请码不为空
            UserInfoVo user1 = new UserInfoVo(); //根据邀请码，查询邀请人
            if (flag) { //是否需要计算直营团队
                //添加邀请人的直营团队表
                System.out.println("插入直营团队");
                zhiying.add(user1);
            }
            //方案2添加邀请人的团队表
            System.out.println("插入团队表");
            tuandui.add(user1);
            //方案1、查询团队表中，团队成员包含当前邀请人的所有记录，
            //循环insert
            //方案2、递归
            digui(user1, false, zhiying, tuandui);
        } else {
            return;
        }
    }
}
