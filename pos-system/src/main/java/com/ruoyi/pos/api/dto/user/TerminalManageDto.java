package com.ruoyi.pos.api.dto.user;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

import java.io.Serializable;

/**
 * @author luobo
 * @title: TerminalManageDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-15 16:35:32
 */
@Data
public class TerminalManageDto extends BaseDto implements Serializable {

    private String snCode; //终端sn码
    private String transInUserId; //接收人
    private String terminalTransId; //划拨记录id
    private String activateStatus; //激活状态(0：未激活，1：已激活)
    private String brandId; //品牌id
    private String goodsId; //商品id
    private String transType; //商品id
}
