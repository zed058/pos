package com.ruoyi.pos.api.service.user;

import com.ruoyi.pos.api.dto.user.UserDto;
import com.ruoyi.pos.api.vo.user.UserInfoVo;

/**
 * @Author luobo
 * @Date 2021/9/17 11:27
 */
public interface ILoginService {
    /**
     * 用户注册
     * @return
     */
    UserInfoVo userRegister(UserDto userDto) throws Exception;
    /**
     * openId登录
     * @return
     */
    UserInfoVo loginByOpenId(UserDto userDto) throws Exception;
    /**
     * 手机验证码登录
     * @return
     */
    UserInfoVo loginByPhone(UserDto userDto) throws Exception;
    /**
     * 账号密码登录
     * @return
     */
    UserInfoVo loginByAccount(UserDto userDto) throws Exception;
    /**
     * 账号密码登录
     * @return
     */
    void loginOut() throws Exception;
}
