package com.ruoyi.pos.api.vo.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @author luobo
 * @title: TerminalBrandVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-12 10:12:28
 */
@Data
public class TerminalBrandVo implements Serializable {
    private String brandId; //主键id
    private String brand; //终端型号品牌名称
    private String sort; //排序

}
