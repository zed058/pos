package com.ruoyi.pos.api.vo.common;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * @author Administrator
 * @title: TUserAddress
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class TUserAddressVo {
    /** 数据主键 */
    private String id;
    /** 用户id */
    private String userId;
    /** 用户名称 */
    private String userName;
    /** 收货人手机号 */
    private String phone;
    /** 收货人固定电话 */
    private String fixationPhone;
    /** 地区 */
    private String district;
    /** 详情地址 */
    private String address;
    /** 是否默认（0：默认，1：不默认，2：删除） */
    private String isValid;
    /** 城市code */
    private String districtCode;
}
