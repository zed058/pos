package com.ruoyi.pos.api.dto.user;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

import java.util.List;

/**
 * @author luobo
 * @title: UserOpinionDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-11 08:44:45
 */
@Data
public class UserOpinionDto extends BaseDto {
    private String id;//意见反馈id
    private String contactWay; //联系方式
    private String opinion; //意见或建议
    private String opinionImg;//意见图片
    private List<UserOpinionDto.OpinionImgBean> imgList; //意见图片集合
    @Data
    public static class OpinionImgBean{
        private String opinionImg; //意见图片
    }

}
