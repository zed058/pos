package com.ruoyi.pos.api.service.task.impl;

import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.pos.api.service.common.ICommonService;
import com.ruoyi.pos.api.service.task.IGoldCoinIssuanceService;
import com.ruoyi.pos.web.domain.*;
import com.ruoyi.pos.web.mapper.TGoldAwardSerialInfoMapper;
import com.ruoyi.pos.web.mapper.TGoldDealSerialInfoMapper;
import com.ruoyi.pos.web.mapper.TGoldInfoMapper;
import com.ruoyi.pos.web.mapper.TStatisticInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author luobo
 * @title: GoldCoinIssuanceServiceImpl
 * @projectName pos
 * @description: TODO
 * @date 2021-11-24 10:48:57
 */
@Service
public class GoldCoinIssuanceServiceImpl implements IGoldCoinIssuanceService {
    private static final Logger log = LoggerFactory.getLogger(GoldCoinIssuanceServiceImpl.class);
    @Autowired
    private TStatisticInfoMapper statisticInfoMapper;
    @Autowired
    private TGoldAwardSerialInfoMapper goldAwardSerialInfoMapper;
    @Autowired
    private TGoldDealSerialInfoMapper goldDealSerialInfoMapper;
    @Autowired
    private TGoldInfoMapper goldInfoMapper;
    @Autowired
    private ICommonService commonService;

    /*@Override
    @Transactional
    public void transGoldCoinIssuance() throws Exception {
        TRebateSetInfo set = commonService.queryTRebateSetInfoVo();
        TStatisticInfo statistic = statisticInfoMapper.selectTStatisticInfoForTotal(); //查询平台累计统计数据
        if (null != statistic) {
            Date date = DateUtils.getNowDate();
            BigDecimal totalTransMoney = statistic.getMyTransMoney();
            TGoldInfo gold = null;
            if (totalTransMoney.compareTo(set.getPlatformAward()) > 0) { //平台累计交易额，达到配置信息，释放奖励金
                List<TGoldAwardSerialInfo> awardList = goldAwardSerialInfoMapper.selectTGoldAwardSerialInfoList(new TGoldAwardSerialInfo());
                TGoldAwardSerialInfo award = awardList.get(0);
                award.setExchangeTime(date);
                award.setExchange(YesOrNoEnums.YES.getCode());
                award.setAmount(set.getAwardGold());
                goldAwardSerialInfoMapper.updateTGoldAwardSerialInfo(award);
                for (int i = 0; i < set.getAwardGold().intValue(); i++) {
                    gold = new TGoldInfo();
                    gold.setSerialId(award.getId());
                    gold.setSerialType("2");
                    gold.setUserId(award.getUserId());
                    gold.setCreateTime(date);
                    goldInfoMapper.insertTGoldInfo(gold);
                }
            }
            if (totalTransMoney.compareTo(set.getPlatformDeal()) > 0) { //平台累计交易额，达到配置信息，释放交易金
                List<TGoldDealSerialInfo> dealList = goldDealSerialInfoMapper.selectTGoldDealSerialInfoList(new TGoldDealSerialInfo());
                TGoldDealSerialInfo deal = dealList.get(0);
                deal.setExchangeTime(date);
                deal.setExchange(YesOrNoEnums.YES.getCode());
                deal.setAmount(set.getAwardGold());
                goldDealSerialInfoMapper.updateTGoldDealSerialInfo(deal);
                for (int i = 0; i < set.getAwardGold().intValue(); i++) {
                    gold = new TGoldInfo();
                    gold.setSerialId(deal.getId());
                    gold.setSerialType("1");
                    gold.setUserId(deal.getUserId());
                    gold.setCreateTime(date);
                    goldInfoMapper.insertTGoldInfo(gold);
                }
            }
        }
    }*/
}
