package com.ruoyi.pos.api.dto.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author luobo
 * @title: PreCodeTransResult
 * @projectName pos
 * @description: 智强科技支付结果调用封装类
 * @date 2021-12-23 08:35:11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class PreCodeTransResult implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "tradeOrderId")
    private String tradeOrderId; //商户订单号
    @JsonProperty(value = "payOrderId")
    private String orderId; //支付系统订单号
    @JsonProperty(value = "mchId")
    private String merId; //商户ID
    @JsonProperty(value = "appId")
    private String appId; //商户应用ID
    /*@JsonProperty(value = "orderAmount")
    private BigDecimal orderAmount; //订单金额*/
    @JsonProperty(value = "orderAmount")
    private BigDecimal transAmount; //本地处理交易金额
    @JsonProperty(value = "discountAmount")
    private BigDecimal discountAmount; //优惠金额
    @JsonProperty(value = "amount")
    private BigDecimal amount; //支付金额
    @JsonProperty(value = "channelUserId")
    private String channelUserId; //渠道用户ID
    @JsonProperty(value = "status")
    private String status; //状态   2-支付成功
    @JsonProperty(value = "productId")
    private String productId; //支付产品ID
    @JsonProperty(value = "productType")
    private String productType; //支付产品类型   1-现金收款, 2-会员卡支付, 3-微信支付, 4-支付宝支付，5-云闪付
    @JsonProperty(value = "storeId")
    private String storeId; //门店ID
    @JsonProperty(value = "tradeSuccTime")
    private String tradeSuccTime; //交易成功时间
    @JsonProperty(value = "preQrcodeId")
    private String snCode; //预制码ID
    @JsonProperty(value = "sign")
    private String sign; //签名
}
