package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.io.Serializable;

/**
 * @author luobo
 * @title: UserAddressVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-11 08:47:42
 */
@Data
public class UserAddressVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;//地址id
    private String userName; //收货人名称
    private String phone; //收货人手机号
    private String fixationPhone; //收货人固定电话
    private String district; //地区
    private String address; //详情地址
    private String districtCode; //详情地址
    private String isValid; //是否查询默认地址（0：是，1：否）
}
