package com.ruoyi.pos.api.dto.common;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

/**
 * @author luobo
 * @title: CommonDto
 * @projectName pos
 * @description: TODO
 * @date 2021/11/4 000414:48
 */
@Data
public class CommonDto extends BaseDto {
    private String userPhone; //手机号
    private String code; //验证码
    private String lng; //经度
    private String lat; //维度
    private String type; //查询类别（0、国家，1、省份，2、城市，3、区县）
    private String title; //城市名称
    private String shortEng; //首字母简称

    private String bannerType; //轮播图类型

    //快递物流查询条件
    private String customerName; //ShipperCode 为 SF 时必填，对应寄件人/收件人手机号后四位； ShipperCode 为其他快递时，可不填或保 留字段，不可传值
    private String orderCode; //订单编号
    private String sort; //轨迹排序，0-升序，1-降序，默认 0-升序
    private String shipperCode; //快递公司编码
    private String logisticCode; //物流单号
}
