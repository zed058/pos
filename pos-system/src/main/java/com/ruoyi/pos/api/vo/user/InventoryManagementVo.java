package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.io.Serializable;

/**
 * @author luobo
 * @title: InventoryManagementVo
 * @projectName pos
 * @description: 查询库存管理vo
 * @date 2021-11-15 16:34:05
 */
@Data
public class InventoryManagementVo implements Serializable {
    private String totalTerminal; //终端总数
    private String totalTerminalActivated; //已激活终端总数
    private String totalTerminalInactivated; //未激活终端总数
}
