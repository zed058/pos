package com.ruoyi.pos.api.dto.user;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

/**
 * @author luobo
 * @title: UserNoticeDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-11 14:15:50
 */
@Data
public class UserNoticeDto extends BaseDto {
    private String userNoticeId; //用户公告消息id

    private String noticeId;//消息id
}
