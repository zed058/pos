package com.ruoyi.pos.api.service.user.impl;

import com.github.pagehelper.PageInfo;
import com.ruoyi.common.enums.TeamTypeEnum;
import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.pos.api.dto.user.DataStatisticsDto;
import com.ruoyi.pos.api.service.user.DataStatisticsService;
import com.ruoyi.pos.api.utils.CommonUtils;
import com.ruoyi.pos.api.vo.user.DataStatisticsSumVo;
import com.ruoyi.pos.api.vo.user.DataStatisticsVo;
import com.ruoyi.pos.web.domain.TStatisticInfo;
import com.ruoyi.pos.web.domain.TUserTeamInfo;
import com.ruoyi.pos.web.mapper.TStatisticInfoMapper;
import com.ruoyi.pos.web.mapper.TUserTeamInfoMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Administrator
 * @title: DataStatisticsService
 * @projectName ruoyi
 * @description: TODO
 */
@Service
public class DataStatisticsServiceImpl implements DataStatisticsService {
    @Autowired
    private TStatisticInfoMapper tStatisticInfoMapper;
    @Autowired
    private CommonUtils commonUtils;
    @Autowired
    private TUserTeamInfoMapper userTeamInfoMapper;
    @Autowired
    private TUserTeamInfoMapper TUserTeamInfoMapper;

    public Map<String, Object> mainStatistic(DataStatisticsDto tStatisticInfo) {
        Map<String, Object> returnMap = new HashMap<>();
        Map<String, Object> StatisticsMap = new HashMap<>();//交易类型
        List<String> categories = new ArrayList<>();//交易时间
        List<Object> lists = new ArrayList<>();//交易数据
        String userId = commonUtils.getUserId();
        tStatisticInfo.setUserId(userId);


        if ("3".equals(tStatisticInfo.getStatisticsTpye())) {
//            StatisticsMap.put("name", "伙伴数");
//            lists.add(Optional.ofNullable(bean.getMyPartnerNum()).orElse(BigDecimal.ZERO));//伙伴数
            tStatisticInfo.setTeamType(YesOrNoEnums.YES.getCode());
            List<DataStatisticsVo> statisticsVos = TUserTeamInfoMapper.queryChart(tStatisticInfo);
            Optional.ofNullable(statisticsVos).ifPresent(map -> {
                map.stream().forEach(bean -> {
                    categories.add(bean.getDateTime().substring(5));
                    lists.add(bean.getUserCount());//伙伴数
                });
            });
            returnMap.put("categories", categories);
            returnMap.put("StatisticsMap", StatisticsMap);
            returnMap.put("lists", lists);
        } else {
            List<DataStatisticsVo> tStatisticInfos = tStatisticInfoMapper.queryTStatistic(tStatisticInfo);
            Optional.ofNullable(tStatisticInfos).ifPresent(map -> {
                map.stream().forEach(bean -> {
                    categories.add(bean.getDateTime().substring(5));
                    if ("0".equals(tStatisticInfo.getStatisticsTpye())) {
                        StatisticsMap.put("name", "交易额");
                        lists.add(Optional.ofNullable(bean.getMyTransMoney()).orElse(BigDecimal.ZERO));//交易额
                    } else if ("1".equals(tStatisticInfo.getStatisticsTpye())) {
                        StatisticsMap.put("name", "交易笔数");
                        lists.add(Optional.ofNullable(bean.getMyTransNum()).orElse(BigDecimal.ZERO));//交易笔数
                    } else if ("2".equals(tStatisticInfo.getStatisticsTpye())) {
                        StatisticsMap.put("name", "终端数");
                        lists.add(Optional.ofNullable(bean.getMyTerminalNum()).orElse(BigDecimal.ZERO));//终端数
                    }
                });
            });
            returnMap.put("categories", categories);
            returnMap.put("StatisticsMap", StatisticsMap);
            returnMap.put("lists", lists);
        }
        return returnMap;
    }

    @Override
    public DataStatisticsSumVo queryTStatisticSum(DataStatisticsDto tStatisticInfo) {
        String userId = commonUtils.getUserId();
        tStatisticInfo.setUserId(userId);
        //查询我的（交易量，交易笔数，新增终端）
        DataStatisticsSumVo statistics = new DataStatisticsSumVo();//new 一个返回的对象
        DataStatisticsVo StatisticsVo = tStatisticInfoMapper.queryTStatisticSum(tStatisticInfo);
        statistics.setMyTerminalNum(StatisticsVo == null ? BigDecimal.ZERO : StatisticsVo.getMyTerminalNum());//今日(本月)我的新增终端
        statistics.setMyTransMoney(StatisticsVo == null ? BigDecimal.ZERO : StatisticsVo.getMyTransMoney());//今日(本月)我的交易额
        statistics.setMyTransNum(StatisticsVo == null ? BigDecimal.ZERO : StatisticsVo.getMyTransNum());//今日(本月)我的交易笔数

        TUserTeamInfo teamInfo = new TUserTeamInfo();
        teamInfo.setUserId(userId);
        List<TUserTeamInfo> tUserTeamInfos = userTeamInfoMapper.selectTUserTeamInfoList(teamInfo);
        StringBuffer campUserId = new StringBuffer();//直营用户
        StringBuffer teamUserId = new StringBuffer();//团队用户
        if (StringUtils.isNotNull(tUserTeamInfos) && tUserTeamInfos.size() > 0) {
            for (TUserTeamInfo tUserTeamInfo : tUserTeamInfos) {
                if (tUserTeamInfo.getTeamType().equals(TeamTypeEnum.DIRECTLY.getCode())) {//直营
                    campUserId.append(tUserTeamInfo.getSubUserId() + ",");
                } else {//团队
                    teamUserId.append(tUserTeamInfo.getSubUserId() + ",");
                }
            }
        }
        tStatisticInfo.setUserId(campUserId.toString());//直营查询
        DataStatisticsVo camp = tStatisticInfoMapper.queryTStatisticSum(tStatisticInfo);
        statistics.setZhiTransMoney(camp == null ? BigDecimal.ZERO : camp.getMyTransMoney());//今日(本月)直营交易额
        statistics.setZhiTransNum(camp == null ? BigDecimal.ZERO : camp.getMyTransNum());//今日(本月)直营交易笔数
        statistics.setZhiTerminalNum(camp == null ? BigDecimal.ZERO : camp.getMyTerminalNum());//今日(本月)新增直营终端

        tStatisticInfo.setUserId(teamUserId.toString());//团队查询
        DataStatisticsVo team = tStatisticInfoMapper.queryTStatisticSum(tStatisticInfo);
        statistics.setTeamTransMoney(team == null ? BigDecimal.ZERO : team.getMyTransMoney());//今日(本月)团队交易额
        statistics.setTeamTransNum(team == null ? BigDecimal.ZERO : team.getMyTransNum());//今日(本月)团队交易笔数
        statistics.setTeamTerminalNum(team == null ? BigDecimal.ZERO : team.getMyTerminalNum());//今日(本月)团队累计终端

        //直营
        tStatisticInfo.setTeamType(YesOrNoEnums.YES.getCode());//直营
        tStatisticInfo.setUserId(userId);
        int campUser = TUserTeamInfoMapper.queryCount(tStatisticInfo);
        statistics.setZhiPartnerNum(BigDecimal.valueOf(campUser));//今日(本月)新增直营伙伴

        tStatisticInfo.setTeamType(YesOrNoEnums.NO.getCode());//团队
        int teamUser = TUserTeamInfoMapper.queryCount(tStatisticInfo);
        statistics.setTeamPartnerNum(BigDecimal.valueOf(teamUser));//今日(本月)团队新增伙伴

        return statistics;
    }


    @Override
    public Map<String, Object> queryTStatisticMonthAndDay(DataStatisticsDto tStatisticInfo) {
        String userId = commonUtils.getUserId();
        Map<String, Object> returnMap = new HashMap<>();//返回的map
        List<String> dateTime = new ArrayList<>();//时间集合
        tStatisticInfo.setUserId(userId);

        //查询我的（交易量，交易笔数，新增终端）
        List<DataStatisticsSumVo> dataStatisticsSumVos = tStatisticInfoMapper.queryTStatisticMonthAndDay(tStatisticInfo);
        returnMap.put("total", new PageInfo(dataStatisticsSumVos).getTotal());
        dataStatisticsSumVos.stream().forEach(bean -> {
            dateTime.add(bean.getDateTime().substring(5));
        });
        returnMap.put("dateTime", dateTime);
        returnMap.put("lists", groupDirectSales(userId, tStatisticInfo, dataStatisticsSumVos, 0));

        return returnMap;
    }

    /*数据统计更多*/
//    @Override
/*
    public Map<String, Object> queryTStatisticMonthAndDayMore(DataStatisticsDto tStatisticInfo) {
        String userId = commonUtils.getUserId();
        Map<String, Object> returnMap = new HashMap<>();//返回的map
        List<Map<String, Object>> listsMap = new ArrayList<>();
        tStatisticInfo.setUserId(userId);
        //查询我的（交易量，交易笔数，新增终端）
        List<DataStatisticsSumVo> dataStatisticsSumVos = tStatisticInfoMapper.queryTStatisticMonthAndDayMore(tStatisticInfo);
        returnMap.put("total", 12);
        //时间数组
        String[] dateTime = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"}; //12个月份
        List<DataStatisticsSumVo> list = groupDirectSales(userId, tStatisticInfo, dataStatisticsSumVos, 1);
        Arrays.stream(dateTime).forEach(date -> {
            List<Map> arrayList = new ArrayList<>();
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("dateTime", date);
            AtomicBoolean flag = new AtomicBoolean(true);
            Map<String, Object> map = new HashMap<>();
            list.stream().forEach(bean -> {
                if (list.stream().anyMatch(bean1 -> date.equals(bean1.getDateTime().substring(5)))) {
                    if ("0".equals(tStatisticInfo.getStatisticsTpye())) {
                        map.put("group", Optional.ofNullable(bean.getMyTransMoney()).orElse(BigDecimal.ZERO));//团队交易额
                        map.put("my", Optional.ofNullable(bean.getMyTransMoney()).orElse(BigDecimal.ZERO));//我的交易额
                        map.put("directSales", Optional.ofNullable(bean.getMyTransMoney()).orElse(BigDecimal.ZERO));//直营交易额
                    } else if ("1".equals(tStatisticInfo.getStatisticsTpye())) {
                        map.put("group", Optional.ofNullable(bean.getMyTransNum()).orElse(BigDecimal.ZERO));//团队交易笔数
                        map.put("my", Optional.ofNullable(bean.getMyTransNum()).orElse(BigDecimal.ZERO));//我的交易笔数
                        map.put("directSales", Optional.ofNullable(bean.getMyTransNum()).orElse(BigDecimal.ZERO));//直营交易笔数
                    } else if ("2".equals(tStatisticInfo.getStatisticsTpye())) {
                        map.put("group", Optional.ofNullable(bean.getMyTerminalNum()).orElse(BigDecimal.ZERO));//团队终端数
                        map.put("my", Optional.ofNullable(bean.getMyTerminalNum()).orElse(BigDecimal.ZERO));//我的终端数
                        map.put("directSales", Optional.ofNullable(bean.getMyTerminalNum()).orElse(BigDecimal.ZERO));//直营终端数
                    } else if ("3".equals(tStatisticInfo.getStatisticsTpye())) {
                        map.put("group", Optional.ofNullable(bean.getZhiPartnerNum()).orElse(BigDecimal.ZERO));//团队新增伙伴
                        map.put("directSales", Optional.ofNullable(bean.getTeamPartnerNum()).orElse(BigDecimal.ZERO));//新增直营伙伴
                    }
                    flag.set(false);
                }
            });
            if (flag.get()) {
                if ("3".equals(tStatisticInfo.getStatisticsTpye())) {
                    map.put("group", BigDecimal.ZERO);//团队交易额
                    map.put("directSales", BigDecimal.ZERO);//直营交易额
                } else {
                    map.put("group", BigDecimal.ZERO);//团队交易额
                    map.put("my", BigDecimal.ZERO);//我的交易额
                    map.put("directSales", BigDecimal.ZERO);//直营交易额
                }
            }
            arrayList.add(map);
            resultMap.put("list", arrayList);
            listsMap.add(resultMap);
        });
        returnMap.put("lists", listsMap);

        return returnMap;
    }
*/


    /*数据统计更多*/
    @Override
    public Map<String, Object> queryTStatisticMonthAndDayMore(DataStatisticsDto tStatisticInfo) {
        String userId = commonUtils.getUserId();
        Map<String, Object> returnMap = new HashMap<>();//返回的map
        List<Map<String, Object>> listsMap = new ArrayList<>();
        //查询我的（交易量，交易笔数，新增终端）
        tStatisticInfo.setUserId(userId);
//        List<DataStatisticsSumVo> dataStatisticsSumVos = tStatisticInfoMapper.queryTStatisticMonthAndDay(tStatisticInfo);
        List<DataStatisticsSumVo> dataStatisticsSumVos = tStatisticInfoMapper.queryTStatisticMonthAndDayMore(tStatisticInfo);
        String[] dateTime = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"}; //12个月份
            if (dataStatisticsSumVos.size() == 0){
                Arrays.stream(dateTime).forEach(date->{
                    DataStatisticsSumVo vo = new DataStatisticsSumVo();
                    vo.setDateTime(tStatisticInfo.getCreateTime()+"-"+date);
                    dataStatisticsSumVos.add(vo);
                });
            }
        returnMap.put("total", 12);
        List<DataStatisticsSumVo> list = groupDirectSales(userId, tStatisticInfo, dataStatisticsSumVos, 1);

        List<DataStatisticsSumVo> voList = new ArrayList<>();
        if (!"3".equals(tStatisticInfo.getStatisticsTpye())) {
            LocalDate today = LocalDate.now();//查询当前时间
            for (long i = 0L; i <= 11L; i++) {
                LocalDate localDate = today.minusMonths(i);
                String month = localDate.toString().substring(0, 7);
                DataStatisticsSumVo vo = new DataStatisticsSumVo();
                vo.setDateTime(month);
                voList.add(vo);
            }
            for (DataStatisticsSumVo sumVo : voList) {
                System.out.println(sumVo.getDateTime());
                for (DataStatisticsSumVo campVo : list) {
                    if (sumVo.getDateTime().equals(campVo.getDateTime())) {
                        sumVo.setZhiTransMoney(campVo.getZhiTransMoney());//今日(本月)直营交易额
                        sumVo.setMyTransMoney(campVo.getZhiTransNum());////今日(本月)我的交易额
                        sumVo.setZhiTransNum(campVo.getZhiTransNum());//今日(本月)直营交易笔数

                        sumVo.setZhiTerminalNum(campVo.getZhiTerminalNum());//今日(本月)新增直营终端
                        sumVo.setMyTransNum(campVo.getMyTransNum());//今日(本月)我的交易笔数
                        sumVo.setTeamTransMoney(campVo.getTeamTransMoney());//今日(本月)团队交易额

                        sumVo.setTeamTransNum(campVo.getTeamTransNum());//今日(本月)团队交易笔数
                        sumVo.setMyTerminalNum(campVo.getMyTerminalNum());//今日(本月)团队交易笔数
                        sumVo.setTeamTerminalNum(campVo.getTeamTerminalNum());//今日(本月)团队累计终端
                        break;
                    }else {
                        continue;
                    }
                }

            }
            Arrays.stream(dateTime).forEach(date -> {
                Map<String, Object> beanMap = new HashMap<>();//返回的map
                beanMap.put("dateTime", date);
                voList.stream().forEach(bean -> {
                    Map<String, Object> map = new HashMap<>();//返回的map
                    if (date.equals(bean.getDateTime().substring(5))){
                        if ("0".equals(tStatisticInfo.getStatisticsTpye())) {
                            map.put("group", Optional.ofNullable(bean.getTeamTransMoney()).orElse(BigDecimal.ZERO));//团队交易额
                            map.put("my", Optional.ofNullable(bean.getMyTransMoney()).orElse(BigDecimal.ZERO));//我的交易额
                            map.put("directSales", Optional.ofNullable(bean.getZhiTransMoney()).orElse(BigDecimal.ZERO));//直营交易额
                        } else if ("1".equals(tStatisticInfo.getStatisticsTpye())) {
                            map.put("group", Optional.ofNullable(bean.getTeamTransNum()).orElse(BigDecimal.ZERO));//团队交易笔数
                            map.put("my", Optional.ofNullable(bean.getMyTransNum()).orElse(BigDecimal.ZERO));//我的交易笔数
                            map.put("directSales", Optional.ofNullable(bean.getZhiTransNum()).orElse(BigDecimal.ZERO));//直营交易笔数
                        } else if ("2".equals(tStatisticInfo.getStatisticsTpye())) {
                            map.put("group", Optional.ofNullable(bean.getTeamTerminalNum()).orElse(BigDecimal.ZERO));//团队终端数
                            map.put("my", Optional.ofNullable(bean.getMyTerminalNum()).orElse(BigDecimal.ZERO));//我的终端数
                            map.put("directSales", Optional.ofNullable(bean.getZhiTerminalNum()).orElse(BigDecimal.ZERO));//直营终端数
                        }
                        beanMap.put("list",map);
                    }
                });
                listsMap.add(beanMap);
            });
        }else {
            Arrays.stream(dateTime).forEach(date -> {
                Map<String, Object> beanMap = new HashMap<>();//返回的map
                beanMap.put("dateTime", date);
                list.stream().forEach(bean -> {
                    Map<String, Object> map = new HashMap<>();//返回的map
                    if (date.equals(bean.getDateTime().substring(5))){
                            map.put("group", Optional.ofNullable(bean.getZhiPartnerNum()).orElse(BigDecimal.ZERO));//团队新增伙伴
                            map.put("directSales", Optional.ofNullable(bean.getTeamPartnerNum()).orElse(BigDecimal.ZERO));//新增直营伙伴
                        beanMap.put("list",map);
                    }
                });
                listsMap.add(beanMap);
            });
        }

        returnMap.put("lists", listsMap);
        return returnMap;
    }


    public List<DataStatisticsSumVo> groupDirectSales(String userId, DataStatisticsDto tStatisticInfo, List<DataStatisticsSumVo> dataStatisticsSumVos, Integer type) {

        TUserTeamInfo teamInfo = new TUserTeamInfo();
        teamInfo.setUserId(userId);
        List<TUserTeamInfo> tUserTeamInfos = userTeamInfoMapper.selectTUserTeamInfoList(teamInfo);
        StringBuffer campUserId = new StringBuffer();//直营用户
        StringBuffer teamUserId = new StringBuffer();//团队用户
        if (StringUtils.isNotNull(tUserTeamInfos) && tUserTeamInfos.size() > 0) {
            for (TUserTeamInfo tUserTeamInfo : tUserTeamInfos) {
                if (tUserTeamInfo.getTeamType().equals(TeamTypeEnum.DIRECTLY.getCode())) {//直营
                    campUserId.append(tUserTeamInfo.getSubUserId() + ",");
                } else {//团队
                    teamUserId.append(tUserTeamInfo.getSubUserId() + ",");
                }
            }

            List<DataStatisticsSumVo> camp = null;
            List<DataStatisticsSumVo> team = null;
            List<DataStatisticsVo> campUser = null;
            List<DataStatisticsVo> teamUser = null;
            DataStatisticsDto campDto = new DataStatisticsDto();
            BeanUtils.copyProperties(tStatisticInfo,campDto);
            campDto.setUserId(String.valueOf(campUserId));//直营
            DataStatisticsDto teamDto = new DataStatisticsDto();
            BeanUtils.copyProperties(tStatisticInfo,teamDto);
            teamDto.setUserId(String.valueOf(teamUserId));//直营


            if (type == 0) {
                camp = tStatisticInfoMapper.queryTStatisticMonthAndDay(campDto);//直营查询(查询本月和前六个月)
                team = tStatisticInfoMapper.queryTStatisticMonthAndDay(teamDto);//团队查询(查询本月和前六个月)
                campDto.setTeamType(YesOrNoEnums.YES.getCode()); //自定义字段（0：直营，1团队）
                campUser = TUserTeamInfoMapper.queryChart(campDto);
                teamDto.setTeamType(YesOrNoEnums.NO.getCode()); //自定义字段（0：直营，1团队）
                teamUser = TUserTeamInfoMapper.queryChart(teamDto);

            } else {
                camp = tStatisticInfoMapper.queryTStatisticMonthAndDayMore(campDto);//直营查询(查询一年)
                team = tStatisticInfoMapper.queryTStatisticMonthAndDayMore(teamDto);//团队查询(查询一年)
                campDto.setTeamType(YesOrNoEnums.YES.getCode()); //自定义字段（0：直营，1团队）
                campUser = TUserTeamInfoMapper.queryYear(campDto);
                teamDto.setTeamType(YesOrNoEnums.NO.getCode()); //自定义字段（0：直营，1团队）
                teamUser = TUserTeamInfoMapper.queryYear(teamDto);
            }
            if (!"3".equals(tStatisticInfo.getStatisticsTpye())) {
                for (DataStatisticsSumVo sumVo : dataStatisticsSumVos) {
                    for (DataStatisticsVo campVo : campUser) {
                        if (sumVo.getDateTime().equals(campVo.getDateTime())) {
                            sumVo.setZhiPartnerNum(BigDecimal.valueOf(campVo.getUserCount() == null ? 0L : campVo.getUserCount()));//今日(本月)新增直营伙伴
                            break;
                        }
                    }
                }

                for (DataStatisticsSumVo sumVo : dataStatisticsSumVos) {
                    for (DataStatisticsVo campVo : teamUser) {
                        if (sumVo.getDateTime().equals(campVo.getDateTime())) {
                            sumVo.setTeamPartnerNum(BigDecimal.valueOf(campVo.getUserCount() == null ? 0L : campVo.getUserCount()));//今日(本月)团队新增伙伴
                            break;
                        }
                    }
                }

                for (DataStatisticsSumVo sumVo : dataStatisticsSumVos) {
                    for (DataStatisticsSumVo campVo : camp) {
                        if (sumVo.getDateTime().equals(campVo.getDateTime())) {
                            sumVo.setZhiTransMoney(campVo.getMyTransMoney() == null ? BigDecimal.ZERO : campVo.getMyTransMoney());//今日(本月)直营交易额
                            sumVo.setZhiTransNum(campVo.getMyTransNum() == null ? BigDecimal.ZERO : campVo.getMyTransNum());//今日(本月)直营交易笔数
                            sumVo.setZhiTerminalNum(campVo.getMyTerminalNum() == null ? BigDecimal.ZERO : campVo.getMyTerminalNum());//今日(本月)新增直营终端
//                        sumVo.setZhiPartnerNum(campVo.getMyPartnerNum() == null ? BigDecimal.ZERO : campVo.getMyPartnerNum());//今日(本月)新增直营伙伴
                            break;
                        }

                    }
                }

                for (DataStatisticsSumVo sumVo : dataStatisticsSumVos) {
                    for (DataStatisticsSumVo teamVo : team) {
                        if (sumVo.getDateTime().equals(teamVo.getDateTime())) {
                            sumVo.setTeamTransMoney(teamVo.getMyTransMoney() == null ? BigDecimal.ZERO : teamVo.getMyTransMoney());//今日(本月)团队交易额
                            sumVo.setTeamTransNum(teamVo.getMyTransNum() == null ? BigDecimal.ZERO : teamVo.getMyTransNum());//今日(本月)团队交易笔数
                            sumVo.setTeamTerminalNum(teamVo.getMyTerminalNum() == null ? BigDecimal.ZERO : teamVo.getMyTerminalNum());//今日(本月)团队累计终端
//                        sumVo.setTeamPartnerNum(teamVo.getMyPartnerNum() == null ? BigDecimal.ZERO : teamVo.getMyPartnerNum());//今日(本月)团队新增伙伴
                            break;
                        }
                    }
                }

            } else {
                List<DataStatisticsSumVo> voList = new ArrayList<>();
                LocalDate today = LocalDate.now();
                for (long i = 0L; i <= 11L; i++) {
                    LocalDate localDate = today.minusMonths(i);
                    String month = localDate.toString().substring(0, 7);
                    DataStatisticsSumVo vo = new DataStatisticsSumVo();
                    vo.setDateTime(month);
                    voList.add(vo);
                }
                for (DataStatisticsSumVo sumVo : voList) {
                    for (DataStatisticsVo campVo : campUser) {
                        if (sumVo.getDateTime().equals(campVo.getDateTime())) {
                            sumVo.setZhiPartnerNum(BigDecimal.valueOf(campVo.getUserCount() == null ? 0L : campVo.getUserCount()));//今日(本月)新增直营伙伴
                            break;
                        }
                    }
                    for (DataStatisticsVo campVo : teamUser) {
                        if (sumVo.getDateTime().equals(campVo.getDateTime())) {
                            sumVo.setTeamPartnerNum(BigDecimal.valueOf(campVo.getUserCount() == null ? 0L : campVo.getUserCount()));//今日(本月)团队新增伙伴
                            break;
                        }
                    }
                }
                return voList;
            }
        }
        return dataStatisticsSumVos;
    }
}
