package com.ruoyi.pos.api.vo.user;

import lombok.Data;

/**
 * @author luobo
 * @title: InventoryRecordGoodsVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-16 16:39:06
 */
@Data
public class InventoryRecordGoodsVo {
    private String goodsId; //商品id
    private String goodsName; //商品名称
    private String goodsImg; //商品图片
    private String totalTerminal; //终端总数
    private String totalTerminalActivated; //已激活终端总数
    private String totalTerminalInactivated; //未激活终端总数
}
