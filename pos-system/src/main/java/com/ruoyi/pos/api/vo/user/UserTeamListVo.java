package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author luobo
 * @title: UserTeamVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-11 08:58:19
 */
@Data
public class UserTeamListVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String userId; //用户id
    private String userName; //用户名
    private String userImg; //用户头像
    private String userPhone; //用户手机号
    private BigDecimal balanceSum; //总余额
    private String gradeId; //用户会员等级
    private int sort; //用户会员等级排序
}
