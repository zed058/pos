package com.ruoyi.pos.api.vo.user;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Administrator
 * @title: vip基础说明
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class VipExplainVo {
    private String gradeCode;/** 等级（v1-v2-v3。。。。。。v-10） */
    private String gradeName;/** 等级名称(骑士。。。。。。。司令) */
    private BigDecimal terminalSum;/** 绑定终端数量 */
    private BigDecimal badSum; /** 交易金额 */
    private int sort;/** 排序 */
}
