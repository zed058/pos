package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.io.Serializable;

/**
 * @author luobo
 * @title: InventoryRecordVo
 * @projectName pos
 * @description: 查询库存记录vo
 * @date 2021-11-15 16:34:31
 */
@Data
public class InventoryRecordVo implements Serializable {
    private String totalTerminal; //终端总数
    private String totalTerminalActivated; //已激活终端总数
    private String totalTerminalInactivated; //未激活终端总数
}
