package com.ruoyi.pos.api.dto.user;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Administrator
 * @title: UserDefaultDto
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class UserDefaultDto extends BaseDto {
    private String createTime;//金币、积分、交易金、奖励金详情传时间，，，收益明细传（1：日纬度，2：月维度）
    private String userId;
    private String brandId;
    private String transType;
    private String transOperType;//收益明细状态(0:商户收益,1:分润收益,2:导师收益)【目前收益明细接口用queryEarningsDetail】
    private BigDecimal transAmount; //提现金额
    private String releaseType;//0:待释放1：已转化2 ：已兑换
}
