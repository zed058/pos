package com.ruoyi.pos.api.vo.user;

import com.ruoyi.pos.api.vo.common.TUserAddressVo;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author luobo
 * @title: OrderDetailVo
 * @projectName pos
 * @description: 订单详情vo
 * @date 2021-11-17 13:35:20
 */
@Data
public class OrderDetailVo {
    private static final long serialVersionUID = 1L;
    private String orderId; //定单id
    private String orderNo; //订单编号
    private BigDecimal goodsNum; //商品数量
    private String orderStatus; //订单状态
    private String orderStatusShow; //订单状态（10：待支付（待兑换），20：待发货，30：已发货（已兑换），40：已完成，50：已取消/已关闭）
    private BigDecimal payAmount; //支付金额
    private String deliveryType; //配送方式
    private String deliveryTypeShow; //配送方式（0：自提，1：物流）
    private BigDecimal deliveryAmount; //配送费
    private String address; //物流地点/自提地点
    private String contactPerson; //提货人/收货人姓名
    private String contactPhone; //提货人/收货人电话
    private String remark; //备注
    private Date createTime; //下单时间
    private String goodsId; //商品id
    private String goodsName; //商品名称
    private String goodsImg; //商品图片
    private BigDecimal amount; //商品单价
    private String exchangeUnit; //兑换单位
    private String unit; //兑换单位
    private BigDecimal exchangeAmount; //兑换金额
    private String orderType; //订单类型（1、采购订单，2、兑换订单）
    private String orderTypeShow; //订单类型
    private String payType; //支付方式(0:微信支付，1：余额支付)
    private String payTypeShow; //支付方式(0:微信支付，1：余额支付)
    private String servicePhone; //客服电话

    private String shipperName;//快递公司名称
    private String oddNo;//快递单号

    private String selfPhone;//自提点联系电话
    private String selfName;//自提点名称
}
