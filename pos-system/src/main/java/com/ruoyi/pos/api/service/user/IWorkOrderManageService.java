package com.ruoyi.pos.api.service.user;

import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.pos.api.dto.BaseDto;
import com.ruoyi.pos.api.dto.user.WorkOrderDto;
import com.ruoyi.pos.api.vo.user.WorkOrderVo;

import java.util.List;

/**
 * @author luobo
 * @title: IWorkOrderManageService
 * @projectName pos
 * @description: TODO
 * @date 2021-11-12 08:41:44
 */
public interface IWorkOrderManageService {
    //发布工单
    void saveWorkOrderInfo(WorkOrderDto workOrderDto);
    //查询工单详情
    WorkOrderVo queryWorkOrderInfoDeatil(WorkOrderDto workOrderDto);
    //查询工单列表
    List<WorkOrderVo> queryWorkOrderInfoList(WorkOrderDto workOrderDto);
}
