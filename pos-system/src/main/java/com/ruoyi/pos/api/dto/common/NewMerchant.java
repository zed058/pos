package com.ruoyi.pos.api.dto.common;

import lombok.Data;

/**
 * @author luobo
 * @title: NewMerchant
 * @projectName pos
 * @description: 新增商户回调封装
 * @date 2021-11-08 16:26:57
 */
@Data
public class NewMerchant {
    private String agentId; //代理商编号(唯一)
    private String merName; //商户名称
    private String merId; //商户号
    private String merUsername; //商户姓名
    private String merPhone; //商户手机号(脱敏)
    private String screenNum; //商户结算卡(脱敏)
    private String createTime; //终端绑定状态(0.未绑定，1.已绑定)
    private String merchantType; //Sn 编号(终端绑定状态为 1 时返回)
    private String bindStatus; //入网时间(yyyyMMddHHmmss)
    private String snCode; //商户类型(1.小微商户，2.实体商户)
}
