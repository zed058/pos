package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author luobo
 * @title: OrderListVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-08 08:38:17
 */
@Data
public class OrderListVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String orderId; //定单id
    private String orderNo; //订单编号
    private BigDecimal goodsNum; //商品数量
    private String orderStatus; //订单状态
    private String orderStatusShow; //订单状态（10：待支付（待兑换），20：待发货，30：已发货（已兑换），40：已完成，50：已取消/已关闭）
    private String payAmount; //支付金额
    private String deliveryType; //配送方式
    private String deliveryTypeShow; //配送方式（0：自提，1：物流）
    private String deliveryAmount; //配送费
    private String goodsName; //商品名称
    private String goodsImg; //商品图片
    private BigDecimal amount; //商品单价
    private String unit; //商品单位（台，盒，个）
    private String exchangeUnit; //商品价格单位（元，积分，金币）
    private String exchangeAmount; //兑换金额
    private String orderType; //订单类型（10：现金，20：金币，21：积分，22：交易，23：奖励金）
    private String orderTypeShow; //订单类型
    private String payType; //支付方式(0:微信支付，1：余额支付)
    private String payTypeShow; //支付方式(0:微信支付，1：余额支付)
}
