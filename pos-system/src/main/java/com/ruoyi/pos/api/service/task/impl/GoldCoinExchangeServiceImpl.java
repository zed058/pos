package com.ruoyi.pos.api.service.task.impl;

import com.ruoyi.common.enums.TransOperTypeEnum;
import com.ruoyi.common.enums.TransTypeEnum;
import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.pos.api.service.common.ICommonService;
import com.ruoyi.pos.api.service.task.IGoldCoinExchangeService;
import com.ruoyi.pos.api.utils.CommonUtils;
import com.ruoyi.pos.web.domain.TGoldInfo;
import com.ruoyi.pos.web.domain.TUserDefaultInfo;
import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.mapper.TGoldInfoMapper;
import com.ruoyi.pos.web.mapper.TUserDefaultInfoMapper;
import com.ruoyi.pos.web.mapper.TUserInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author luobo
 * @title: GoldCoinExchangeServiceImpl
 * @projectName pos
 * @description: TODO
 * @date 2021-11-24 10:10:34
 */
@Service
public class GoldCoinExchangeServiceImpl implements IGoldCoinExchangeService {
    private static final Logger log = LoggerFactory.getLogger(GoldCoinExchangeServiceImpl.class);
    @Autowired
    private TUserInfoMapper userInfoMapper;
    @Autowired
    private TUserDefaultInfoMapper userDefaultInfoMapper;
    @Autowired
    private TGoldInfoMapper goldInfoMapper;
    @Autowired
    private ICommonService commonService;
    @Autowired
    private CommonUtils commonUtils;
    //查询需要转化为余额的金币列表
    @Override
    public List<TGoldInfo> queryGoldWaitConvertedList() throws Exception {
        return goldInfoMapper.selectTGoldInfoListForExchange(commonService.queryTRebateSetInfoVo().getGoldTime());
    }

    //金币转化为余额
    @Override
    @Transactional
    public void goldCoinExchange(TGoldInfo gold) throws Exception {
        TGoldInfo newGold = goldInfoMapper.selectTGoldInfoById(gold.getId());
        if (YesOrNoEnums.YES.getCode().equals(newGold.getExchange())) {
            log.info("金币已使用或转化，不能再次转化为余额，金币id：" + gold.getId());
            return;
        }
        log.info("金币转化开始-----------------------------------");
        TUserInfo user = userInfoMapper.selectTUserInfoByUserId(gold.getUserId());
        user.setGoldSum(user.getGoldSum().subtract(BigDecimal.ONE)); //更新金币总数量
        user.setBalanceSum(user.getBalanceSum().add(BigDecimal.ONE)); //更新余额总数量
        userInfoMapper.updateTUserInfo(user);
        gold.setExchange(YesOrNoEnums.YES.getCode());
        goldInfoMapper.updateTGoldInfo(gold);
        log.info("金币转化为余额成功，金币id：" + gold.getId());
        //插入余额交易流水记录表
        TUserDefaultInfo defaultInfo = commonUtils.generateTUserDefaultInfo(
                user.getUserId(),
                "",
                TransTypeEnum.YUE.getCode(),
                TransOperTypeEnum.YUE_1010.getCode(),
                BigDecimal.ONE,
                "",
                "",
                null,
                "",
                null);
        userDefaultInfoMapper.insertTUserDefaultInfo(defaultInfo);
        //插入金币交易流水记录表
        defaultInfo = commonUtils.generateTUserDefaultInfo(
                user.getUserId(),
                "",
                TransTypeEnum.JINBI.getCode(),
                TransOperTypeEnum.JINBI_3004.getCode(),
                BigDecimal.ZERO.subtract(BigDecimal.ONE),
                "",
                "",
                null,
                "",
                null);
        userDefaultInfoMapper.insertTUserDefaultInfo(defaultInfo);
    }
}
