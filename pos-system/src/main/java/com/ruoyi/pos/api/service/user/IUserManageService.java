package com.ruoyi.pos.api.service.user;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.pos.api.dto.BaseDto;
import com.ruoyi.pos.api.dto.common.ExchangeDto;
import com.ruoyi.pos.api.dto.user.*;
import com.ruoyi.pos.api.vo.user.*;

import java.util.List;
import java.util.Map;

/**
 * @author luobo
 * @title: UserManageService
 * @projectName pos
 * @description: TODO
 * @date 2021-11-08 09:40:56
 */
public interface IUserManageService {

    //查询用户信息
    UserInfoVo queryTUserInfoVo() throws Exception;
    //修改用户信息
    void saveUserInfo(UserDto userDto) throws Exception;
    //保存收货地址信息
    void saveUserAddressInfo(UserAddressDto userAddressDto) throws Exception;
    //查询收货地址列表
    Map<String, Object> queryUserAddressList(UserAddressDto userAddressDto) throws Exception;
    //查询平台公告列表
    List<UserNoticeVo> queryUserNoticeList(UserNoticeDto userNoticeDto) throws Exception;
    //查询平台公告详情
    UserNoticeVo queryUserNoticeDetail(UserNoticeDto userNoticeDto) throws Exception;
    //发布意见
    void saveUserOpinionInfo(UserOpinionDto userOpinionDto) throws Exception;
    //查询意见列表
    List<UserOpinionVo> queryUserOpinionList(UserOpinionDto userOpinionDto) throws Exception;
    //查询用户团队列表
    UserTeamVo queryTeamUserList(UserTeamDto userTeamDto) throws Exception;
    //查询兑换商城列表
    List<ExchangeOrderVo> queryExchangeRecordList(OrderDto orderDto) throws Exception;
    //查询交易金/奖励金序列号列表
    Map<String, Object> queryGoldSerialNumberList(ExchangeDto exchangeDto) throws Exception;
    //校验支付密码
    JSONObject checkPayPassword(UserDto userDto) throws Exception;
    //查询划拨记录列表
    Map<String, Object> queryTerminalTransRecordList(TerminalTransRecordDto terminalTransRecordDto) throws Exception;
    //划拨（确认）
    void terminalTransferConfirm(TerminalManageDto terminalManageDto) throws Exception;
    //划拨（拒绝）
    void terminalTransferRefuse(TerminalManageDto terminalManageDto) throws Exception;
    //兑换商城用户余额
    Map<String, Object> userBalance() throws Exception;
    //(我的收益)收益明细
    Map<String, Object> queryMyIncome(UserDefaultDto userDefaultDto) throws Exception;

    //我的收益（我的金币详情）
    Map<String,Object> queryMyGoldDetails(UserDefaultDto userDefaultDto) throws Exception;

    //我的收益（我的积分详情）
    Map<String,Object> queryMyIntegralDetails(UserDefaultDto userDefaultDto) throws Exception;

    //我的收益（我的交易金序列号）
    Map<String,Object> queryMyDealAndGoldDetails(UserDefaultDto userDefaultDto) throws Exception;

    //我的收益（我的奖励金序列号）
    Map<String,Object> queryMyAwardAndGoldDetails(UserDefaultDto userDefaultDto) throws Exception;

    //我的收益（总收益分析）
    Map<String,Object> queryMyTotalRevenueDetails(UserDefaultDto userDefaultDto) throws Exception;

    //我的收益（收益明细）
    Map<String,Object> queryEarningsDetail(UserDefaultDto userDefaultDto) throws Exception;

    public UserVipVo queryUserVip() throws Exception;
    //提现
    public void userWithdrawDeposit(UserDefaultDto dto) throws Exception;
    //提现明细
    public List<UserDefaultVo> queryUserWithdrawDeposit(UserDefaultDto dto) throws Exception;
    //校验支付密码
    JSONObject userSignContract(UserDto userDto) throws Exception;
}
