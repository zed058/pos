package com.ruoyi.pos.api.service.user;

import com.ruoyi.pos.api.dto.user.OrderDto;
import com.ruoyi.pos.api.vo.user.OrderDetailVo;
import com.ruoyi.pos.api.vo.user.OrderListVo;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author luobo
 * @title: OrderService
 * @projectName pos
 * @description: TODO
 * @date 2021-11-08 08:36:13
 */
public interface IOrderService {
    //查询订单列表
    List<OrderListVo> queryOrderList(OrderDto orderDto) throws Exception;
    //保存订单详情
    OrderDetailVo saveOrderDetail(OrderDto orderDto) throws Exception;
    //查询订单详情
    OrderDetailVo queryOrderDetail(OrderDto orderDto) throws Exception;
    //订单支付
    Map<String, Object> orderPay(OrderDto orderDto) throws Exception;
    //取消订单
    void cancelOrder(OrderDto orderDto) throws Exception;
    //确认收货
    void confirmReceipt(OrderDto orderDto) throws Exception;
    //终端申领，兑换商品数据回调
    Map<String , Object> queryConfirmOrder(OrderDto orderDto) throws Exception;
}
