package com.ruoyi.pos.api.dto.user;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

/**
 * @author luobo
 * @title: UserTeamDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-11 08:56:33
 */
@Data
public class UserTeamDto extends BaseDto {
    private String userId; //用户id
    private String teamType; //查询团队类型（0、查询我的商户人员，1、查询团队人员）
    private String userPhone; //用户手机号（模糊匹配查询）
    private String sort;// desc降序
    private String isContainOwn;// 是否包含自己（0：是，1否）
}
