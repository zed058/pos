package com.ruoyi.pos.api.vo.common;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author luobo
 * @title: GradeInfoVo
 * @projectName pos
 * @description: 会员等级vo
 * @date 2021-11-18 11:11:22
 */
@Data
public class GradeInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String gradeCode; //等级
    private String gradeName; //等级名称(骑士。。。。。。。司令)
    private BigDecimal badRatio; //等额差总和
    private BigDecimal shareProfit; //直属分润
    private BigDecimal shareProfitBalance; //分润差额
    private BigDecimal terminalSum; //绑定终端数量
    private Long badSum; //交易金额
    private String sort; //排序
}
