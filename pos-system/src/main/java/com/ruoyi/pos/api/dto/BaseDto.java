package com.ruoyi.pos.api.dto;

import com.ruoyi.common.utils.StringUtils;
import lombok.Data;

import java.io.Serializable;

/**
 * @author luobo
 * @title: BaseDto
 * @projectName pos
 * @description: 请求参数的基础封装类
 * @date 2021/10/21 00219:49
 */
@Data
public class BaseDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer pageNum; //分页，第几页
    private Integer pageSize; //分页，每页展示的条数
    private String orderByColumn; //分页，排序列
    private String isAsc; //分页，排序的方向 "desc" 或者 "asc"
    private Boolean reasonable = true; //分页，分页参数合理化，默认为true(假如发得页码已将超过了数据库分页,则会自动给你把最后一页给你)
    private String startTimeStr; //查询条件开始时间，默认年月日
    private String startTimeStrDeTail; //查询条件开始时间，添加时分秒
    private String endTimeStr; //查询条件结束时间，默认年月日
    private String endTimeStrDeTail; //查询条件结束时间，添加时分秒

    //将日期转为当天的开始时间
    public void setStartTimeStr(String startTimeStr) {
        if (StringUtils.isNotEmpty(startTimeStr)) {
            this.startTimeStrDeTail = startTimeStr + " 00:00:00";
        }
    }
    //将日期转为当天的结束时间
    public void setEndTimeStr(String endTimeStr) {
        if (StringUtils.isNotEmpty(endTimeStr)) {
            this.endTimeStrDeTail = endTimeStr + " 23:59:59";
        }
    }
}
