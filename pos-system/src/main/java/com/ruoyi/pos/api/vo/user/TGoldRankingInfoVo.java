package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Administrator
 * @title: TGoldRankingInfoVo
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class TGoldRankingInfoVo {
    private String userImg;
    private String userName;
    private BigDecimal dealAwardGold;
}
