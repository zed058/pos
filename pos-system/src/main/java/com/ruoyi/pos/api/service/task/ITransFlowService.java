package com.ruoyi.pos.api.service.task;

import com.ruoyi.pos.web.domain.TTerminalTransFlowInfo;

import java.util.List;

/**
 * @author luobo
 * @title: ITransFlowService
 * @projectName pos
 * @description: TODO
 * @date 2021-11-09 10:51:16
 */
public interface ITransFlowService {
    //查询未处理的临时表数据
    List<TTerminalTransFlowInfo> queryTransFlow();
    //根据交易流水处理本地业务逻辑
    void handleLocalTransLogic(TTerminalTransFlowInfo flow) throws Exception;
}
