package com.ruoyi.pos.api.service.common;

import com.ruoyi.common.utils.fulinmen.ReturnData;

/**
 * @author luobo
 * @title: FulinmenService
 * @projectName pos
 * @description: 付临门业务接口类
 * @date 2022-06-13 08:47:37
 */
public interface IFulinmenService {
    //处理对方推送的实时交易数据
    ReturnData realtimeTrans(String body) throws Exception;
    //处理对方推送的实时交易数据
    ReturnData check(String body) throws Exception;
}
