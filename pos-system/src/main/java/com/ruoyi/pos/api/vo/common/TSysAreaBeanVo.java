package com.ruoyi.pos.api.vo.common;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author luobo
 * @title: TSysAreaBeanVo
 * @projectName pos
 * @description: TODO
 * @date 2021/11/4 000417:35
 */
@Data
public class TSysAreaBeanVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /*城市首字母*/
    private  String shortEng;
    private List<CityName> cityNameList;

    @Data
    public static class CityName{
        private String id;
        private String title; //城市名称
        private String shortName; //省/市/区
        private String type; //级别
        private String cityCode; //区号
        private String zipCode; //邮编
        private String mergerName; //省/市/区
        private String lng; //经度
        private String lat; //纬度
    }
}
