package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author luobo
 * @title: UserDto
 * @projectName pos
 * @description: 用户莫模块请求参数
 * @date 2021/11/4 00048:39
 */
@Data
public class UserInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String userId; //用户id
    private String wechatId; //微信号
    private String openId; //微信id
    private String userName; //用户名
    private String userImg; //用户头像
    private String userPhone; //用户手机号
    private String userStatus; //用户状态
    private String userStatusShow; //用户状态
    private BigDecimal balanceSum; //总余额
    private BigDecimal balanceWithdraw; //已提现余额
    private BigDecimal balanceUse; //已使用余额
    private BigDecimal goldSum; //总金币
    private BigDecimal goldWithdraw; //已提现金币
    private BigDecimal goldExchange; //已兑换金币
    private BigDecimal integralSum; //总积分
    private BigDecimal integralExchange; //已兑换积分
    private BigDecimal dealGoldMark; //总交易金
    private BigDecimal dealGoldExchangeMark; //已兑换交易金
    private BigDecimal dealGold; //已兑现奖励金
    private BigDecimal awardGoldMark; //总奖励金
    private BigDecimal awardGoldExchangeMark; //已兑换奖励金
    private BigDecimal awardGold; //已兑现交易金
    private String inviteImg; //邀请二维码
    private String inviteCode; //邀请码
    private String superiorUserId; //上级用户id
    private String superiorUserName; //上级用户名称
    private String autonymType; //是否实名认证
    private String identityMark; //身份证号
    private String bankCardMark; //银行卡号
    private String bankAddress; //开户行地址
    private String subBranch; //支行名称
    private String identityZhengImg; //身份证照片正面
    private String identityFanImg; //身份证照片反面
    private String bankCardZhengImg; //银行卡号正面
    private String gradeId; //用户等级id
    private String gradeCode; //用户等级名称
    private String gradeName; //用户等级图片
    private Date autonymTime; //实名认证时间
    private Date registreTime;//注册时间
    private String auditStatus; //平台审核状态
    private String auditStatusShow;//平台审核状态中文名
    private String token;//用户token
    private BigDecimal balance;//用户当前金额
    private String userNoticeType;//公告是否已读(1:未读，0：已读)

    //我的服务商
    private String myServiceProviderName;//我的服务商用户名称
    private String myServiceProviderPhone;//我的服务商用户电话

    //当前等级
    private BigDecimal myTransMoney;//我的交易金额
    private BigDecimal myTerminalNum;//我的终端数

    private String signStatus;//趣工宝签约状态（0：待签约，1：签约成功，2：签约失败）

    //闪电宝相关信息
    private String shandianAuthFlag; //闪电宝注册标识（10：未注册，11：待同步，12：已同步）
    private Date shandianAuthTime; //闪电宝注册时间
    private String shandianBankName; //闪电宝注册时间
    private String shandianBankNo; //闪电宝注册时间
    private String shandianCertNo; //闪电宝注册时间
    private String shandianName; //闪电宝注册时间
}
