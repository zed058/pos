package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.io.Serializable;

/**
 * @author luobo
 * @title: UserNoticeVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-11 14:17:33
 */
@Data
public class UserNoticeVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String noticeId; //公告消息id
    private String userNoticeId; //用户公告消息id
    private String title; //公告消息标题
    private String noticeContent; //公告消息内容
    private String unreadNum;//是否已读  >0:未读；=0已读
}
