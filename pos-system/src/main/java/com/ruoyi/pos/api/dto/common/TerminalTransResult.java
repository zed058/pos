package com.ruoyi.pos.api.dto.common;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luobo
 * @title: TerminalTransResult
 * @projectName pos
 * @description: 用户交易记录回调封装
 * @date 2021-11-08 16:26:45
 */
@Data
public class TerminalTransResult {

    private String agentId; //代理商编号(唯一)
    private String machineType; //机具类型(2.TPOS 传统机，4.EPOS 电签)
    private String orderId; //交易订单流水号(唯一)
    private String sysno; //POS 小票参考号(唯一)
    private String merName; //商户名称
    private String merId; //商户号
    private String snCode; //终端 sn 编号
    private BigDecimal transAmount; //交易金额(单位：分)
    private BigDecimal merSigeFee; //交易费率
    private BigDecimal extractionFee; //提现手续费(单位：分)
    private String settlementCycle; //结算方式(0.DO 结算，1.T1 结算)
    private String payType; //交易类型(1.刷卡，2.扫码，3.闪付)
    private String cardType; //卡类型(1.贷记卡，2.借记卡，3.支付宝，4. 银联二维码，6.云闪付)
    private String transCategory; //交易类别(1.普通，2.VIP) 刷卡区分，扫码闪付默认 1
    private String cardNo; //交易卡号
    private String transTime; //交易时间(yyyyMMddHHmmss)
    private String proceedsTemplateId; //收益模板 ID(政策标识)
    private String agentCost; //代理成本

}
