package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author luobo
 * @title: UserTeamVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-11 08:58:19
 */
@Data
public class UserTeamVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String parentUserName; //上级用户名
    private String parentUserImg; //上级用户头像
    private String parentUserPhone; //上级用户手机号
    private Long total; //团队列表数量
    private List<UserTeamListVo> teamList; //团队列表
}
