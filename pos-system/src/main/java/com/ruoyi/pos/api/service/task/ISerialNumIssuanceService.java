package com.ruoyi.pos.api.service.task;

import com.ruoyi.pos.web.domain.TRebateSetInfo;
import com.ruoyi.pos.web.domain.TTerminalBrandInfo;
import com.ruoyi.pos.web.domain.TTerminalTransFlowInfo;
import com.ruoyi.pos.web.domain.TUserInfo;

import java.util.List;

/**
 * @author luobo
 * @title: IISerialNumIssuanceService
 * @projectName pos
 * @description: TODO
 * @date 2021-11-23 15:19:55
 */
public interface ISerialNumIssuanceService {
    //查询用户信息列表
    List<TUserInfo> queryTransFlowTemp();
    //根据交易流水处理本地业务逻辑
    void handleLocalTransLogic(TTerminalTransFlowInfo temp, TRebateSetInfo setInfo, TTerminalBrandInfo brand);
}
