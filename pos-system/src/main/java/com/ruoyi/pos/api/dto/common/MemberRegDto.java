package com.ruoyi.pos.api.dto.common;

import lombok.*;

import java.io.Serializable;

/**
 * @author luobo
 * @title: WithdrawalResultNotifyDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-19 11:16:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MemberRegDto implements Serializable {
    private String code;
    private String msg;
    private String memberNo;
    private String memberName;
    private String mobile;
    private String idCardNo;
    private String bankCardNo;
}
