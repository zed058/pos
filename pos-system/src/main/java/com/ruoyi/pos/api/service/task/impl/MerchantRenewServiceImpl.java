package com.ruoyi.pos.api.service.task.impl;

import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.pos.api.service.task.IMerchantRenewService;
import com.ruoyi.pos.web.domain.TTerminalBindInfo;
import com.ruoyi.pos.web.domain.TTerminalInfo;
import com.ruoyi.pos.web.domain.TGradeInfo;
import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.mapper.TTerminalBindInfoMapper;
import com.ruoyi.pos.web.mapper.TTerminalInfoMapper;
import com.ruoyi.pos.web.mapper.TGradeInfoMapper;
import com.ruoyi.pos.web.mapper.TUserInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author luobo
 * @title: MerchantRenewServiceImpl
 * @projectName pos
 * @description: TODO
 * @date 2021-11-19 08:57:44
 */
@Service
public class MerchantRenewServiceImpl implements IMerchantRenewService {
    private static final Logger log = LoggerFactory.getLogger(MerchantRenewServiceImpl.class);

    @Autowired
    private TUserInfoMapper userInfoMapper;
    @Autowired
    private TGradeInfoMapper gradeInfoMapper;
    @Autowired
    private TTerminalInfoMapper terminalInfoMapper;
    @Autowired
    private TTerminalBindInfoMapper terminalBindInfoMapper;

    //查询未处理的临时表数据
    @Override
    public List<TTerminalBindInfo> queryTerminalBind() {
        TTerminalBindInfo mapper = new TTerminalBindInfo();
        mapper.setIsValid(YesOrNoEnums.YES.getCode());
        return terminalBindInfoMapper.selectTTerminalBindInfoList(mapper);
    }

    //更新本地终端表商户号信息
    @Override
    @Transactional
    public void renewTerminalBindStatus(TTerminalBindInfo bind) {
        //根据sn号查询用户信息
        TTerminalInfo terminal = terminalInfoMapper.selectTTerminalInfoBySnCode(bind.getSnCode());
        if (null == terminal || StringUtils.isEmpty(terminal.getId())) {
            log.info("renewTerminalBindStatus终端信息出错，snCode：" + bind.getSnCode());
            return;
        }
        log.info("更新本地终端表商户号信息开始-----------------------------------");
        terminal.setMerchant(bind.getMerId());
        terminal.setBindingStatus(YesOrNoEnums.NO.getCode());
        terminal.setBindingTime(DateUtils.getNowDate());
        terminalInfoMapper.updateTTerminalInfo(terminal);
        bind.setIsValid(YesOrNoEnums.NO.getCode());
        terminalBindInfoMapper.updateTTerminalBindInfo(bind);

        //达到配置的终端数，进行用户升级--购买确认收货时进行计算
        TTerminalInfo mapper = new TTerminalInfo();
        mapper.setUserId(terminal.getUserId());
        mapper.setBindingStatus(YesOrNoEnums.YES.getCode());
        List<TTerminalInfo> terminalList = terminalInfoMapper.selectTTerminalInfoList(mapper);
        TUserInfo user = userInfoMapper.selectTUserInfoByUserId(terminal.getUserId());
        TGradeInfo userGrade = gradeInfoMapper.selectTGradeInfoById(user.getGradeId());
        List<TGradeInfo> gradeList = gradeInfoMapper.selectTGradeInfoList(new TGradeInfo()); //已按照sort进行排序
        log.info("更新本地终端表商户号信息开始---------------------绑定终端升级--------------当前会员等级：" + userGrade.getSort());
        Optional.ofNullable(gradeList).ifPresent(list ->{
            list.stream().forEach(grade ->{
                if (Integer.valueOf(grade.getSort()) > Integer.valueOf(userGrade.getSort())) {
                    log.info("更新本地终端表商户号信息开始---------------------绑定终端升级--------------当前终端数：" + terminalList.size());
                    log.info("更新本地终端表商户号信息开始---------------------绑定终端升级--------------会员升级终端数：" + grade.getTerminalSum().intValue());
                    if (terminalList.size() > grade.getTerminalSum().intValue()) { //说明终端数量满足了升级条件
                        log.info("更新本地终端表商户号信息开始---------------------绑定终端升级--------------进行升级");
                        user.setGradeId(grade.getId());
                        userInfoMapper.updateTUserInfo(user);
                    }
                }
            });
        });
    }
}
