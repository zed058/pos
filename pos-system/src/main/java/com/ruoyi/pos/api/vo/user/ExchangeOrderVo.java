package com.ruoyi.pos.api.vo.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Administrator
 * @title: 兑换记录
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class ExchangeOrderVo {
    private String id;//订单id
    private String orderNo;//订单编号
    private String goodsId;//兑换记录商品id
    private String goodsName;//兑换记录商品名称
    private BigDecimal goodsNum;//商品数量
    private String orderType;//订单类型（10：现金，20：金币，21：积分，22：交易，23：奖励金）
    private String orderTypeShow;//订单类型（10：现金，20：金币，21：积分，22：交易，23：奖励金）
    private String orderStatus;//订单状态（10：待支付（待兑换），20：待发货，30：已发货（已兑换），40：已完成，50：已取消/已关闭）
    private String orderStatusShow;//订单状态（10：待支付（待兑换），20：待发货，30：已发货（已兑换），40：已完成，50：已取消/已关闭）
    private BigDecimal payAmount;//支付金额
    private BigDecimal exchangeAmount;//兑换金额
//    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;//订单创建时间
}
