package com.ruoyi.pos.api.dto.user;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author luobo
 * @title: UserDto
 * @projectName pos
 * @description: 用户莫模块请求参数
 * @date 2021/11/4 00048:39
 */
@Data
public class UserDto extends BaseDto {
    private static final long serialVersionUID = 1L;

    private String userName; //用户名
    private String userImg; //用户头像
    private String userPhone; //用户手机号
    private String payPassword; //支付密码
    private String inviteCode; //邀请码
    private String autonymType; //是否实名认证
    private String identityMark; //身份证号
    private String bankCardMark; //银行卡号
    private String bankAddress; //开户行地址
    private String bankId;//银行id
    private String subBranch; //支行名称
    private String identityZhengImg; //身份证照片正面
    private String identityFanImg; //身份证照片反面
    private String bankCardZhengImg; //银行卡号正面
    private String verifyCode;
    private String password;//登录密码
    private String code; //微信code，用于获取openId
    private String openId; //微信openId
    private String userUrl;//二维码回调url地址;

    private String pageCallbackUrl;//趣工宝签约回调地址
}
