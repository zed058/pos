package com.ruoyi.pos.api.vo.user;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author luobo
 * @title: InventoryRecordStatisticVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-16 16:39:06
 */
@Data
public class InventoryRecordStatisticVo {
    private BigDecimal teamTotalBuy; //团队累计采购
    private BigDecimal directlyTotalBuy; //直营累计采购
    private BigDecimal myTotalBuy; //我的累计采购
    private BigDecimal teamTotalExchange; //团队累计兑换
    private BigDecimal directlyTotalExchange; //直营累计兑换
    private BigDecimal myTotalExchange; //我的累计兑换
}
