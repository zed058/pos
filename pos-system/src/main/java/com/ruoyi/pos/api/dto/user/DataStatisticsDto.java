package com.ruoyi.pos.api.dto.user;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

/**
 * @author luobo
 * @title: DataStatisticsDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-10 08:53:06
 */
@Data
public class DataStatisticsDto extends BaseDto {
    private static final long serialVersionUID = 1L;

    private String userId; //用户id
    private String time; //时间(1=本周,2=本月)
    private String createTime;//时间
    private String brandId; //终端品牌id
    private String statisticsTpye;// 0:交易额 1:交易笔数 2:终端数 3:伙伴数
    private String teamType; //自定义字段（0：直营，1团队）

}
