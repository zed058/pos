package com.ruoyi.pos.api.service.user;

import com.ruoyi.pos.api.dto.user.DataStatisticsDto;
import com.ruoyi.pos.api.vo.user.DataStatisticsSumVo;
import com.ruoyi.pos.api.vo.user.DataStatisticsVo;
import com.ruoyi.pos.web.domain.TStatisticInfo;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * @title: DataStatisticsService
 * @projectName ruoyi
 * @description: TODO
 */
public interface DataStatisticsService {

    Map<String,Object> mainStatistic(DataStatisticsDto tStatisticInfo);
    /*数据统计*/
    public DataStatisticsSumVo queryTStatisticSum(DataStatisticsDto tStatisticInfo);
    /*数据统计日纬度月维度*/
    Map<String, Object> queryTStatisticMonthAndDay(DataStatisticsDto tStatisticInfo);

    /*数据统计更多*/
    Map<String, Object> queryTStatisticMonthAndDayMore(DataStatisticsDto tStatisticInfo);
}
