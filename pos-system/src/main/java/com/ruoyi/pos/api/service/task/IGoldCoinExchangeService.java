package com.ruoyi.pos.api.service.task;

import com.ruoyi.pos.web.domain.TGoldInfo;

import java.util.List;

/**
 * @author luobo
 * @title: IGoldCoinExchangeService
 * @projectName pos
 * @description: TODO
 * @date 2021-11-24 10:10:24
 */
public interface IGoldCoinExchangeService {
    //查询需要转化为余额的金币列表
    List<TGoldInfo> queryGoldWaitConvertedList() throws Exception;
    //金币转化为余额
    void goldCoinExchange(TGoldInfo gold) throws Exception;
}
