package com.ruoyi.pos.api.vo.common;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author luobo
 * @title: TerminalModelVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-12 10:15:07
 */
@Data
public class TerminalGoodsVo implements Serializable {
    private String goodsId; //商品id
    private String brandId; //品牌id
    private String goodsType; //商品类型（0：终端商品，1：兑换商品）
    private String goodsTypeShow; //商品类型（0：终端商品，1：兑换商品）
    private String goodsStatus; //商品交易类型（10:采购,20:积分兑换，21:交易金兑换，22:奖励金兑换，23:金币兑换）
    private String goodsStatusShow; //商品交易类型（10:采购,20:积分兑换，21:交易金兑换，22:奖励金兑换，23:金币兑换）
    private String goodsName; //商品名称
    private String goodsImg; //商品主图
    private BigDecimal amount; //兑换/购买金额
    private String unit; //商品单位
    private String exchangeUnit; //商品价格单位
    private String goodsDescribe; //文字介绍
    private String sort; //商品排序

}
