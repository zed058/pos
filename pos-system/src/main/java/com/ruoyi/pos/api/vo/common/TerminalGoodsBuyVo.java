package com.ruoyi.pos.api.vo.common;

import com.ruoyi.pos.web.domain.TSelfPickupPointInfo;
import com.ruoyi.pos.web.domain.TUserAddressInfo;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author luobo
 * @title: TerminalGoodsVo
 * @projectName pos
 * @description: 终端商品详细信息(购买)
 * @date 2021-11-12 10:59:22
 */
@Data
public class TerminalGoodsBuyVo implements Serializable {
    private String goodsId; //主键id
    private String brandId; //终端购买品牌id（兑换商城商品id)
    private String goodsType; //商品类型（0：终端商品，1：兑换商品）
    private String goodsTypeShow; //商品类型（0：终端商品，1：兑换商品）
    private String goodsStatus; //商品交易类型（10:采购,20:积分兑换，21:交易金兑换，22:奖励金兑换，23:金币兑换）
    private String goodsStatusShow; //商品交易类型（10:采购,20:积分兑换，21:交易金兑换，22:奖励金兑换，23:金币兑换）
    private String goodsName; //商品名称
    private String goodsUserId; //商品所有人
    private BigDecimal amount; //兑换/购买金额
    private BigDecimal deliveryFee; //兑换商城商品 (配=送费)
    private String unit; //商品单位
    private String sort; //商品排序
    private String isValid; //商品状态
    private String exchangeUnit; //商品价格单位
    private String goodsDescribe; //文字介绍
    private String conversionExplain; //产品兑换说明(兑换商城)
    private String goodsExplain; //产品说明

    private List<ImgList> imgList;
    @Data
    public static class ImgList{
        private String goodsImg; //商品图片
        private String sort; //轮播图排序
        private String imgType; //图片类型
    }

    private List<SpeList> speList;
    @Data
    public static class SpeList{
        private String speId; //规格id
        private String specification; //规格名称
        private BigDecimal amount; //规格金额
        private BigDecimal deliveryFee; //配送费
    }
    private String buyHint;//终端购买提示




}
