package com.ruoyi.pos.api.vo.common;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * @author luobo
 * @title: TSelfPickupPointVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-12 16:33:24
 */
@Data
public class TSelfPickupPointVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id; //主键id
    private String shopName; //自提点店名
    private String phone; //联系电话
    private String region; //地区
    private String address; //详细地址
    private String logo;//商户logo
}
