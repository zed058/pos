package com.ruoyi.pos.api.vo.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @author luobo
 * @title: TSlideshowInfoVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-11 14:02:20
 */
@Data
public class TSlideshowInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private String imgType; //图片类型(1:首页弹窗,2:首页轮播图,3:广告展示,4:分享图片,5:腰部广告图)
    private String imgUrl; //图片路径
    private String img; //图片
    private String sort; //图片路径
    private String linkType; //0：内链 1外链
}
