package com.ruoyi.pos.api.dto.user;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author luobo
 * @title: OrderDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-08 08:42:05
 */
@Data
public class OrderDto extends BaseDto {
    //查询条件
    private String userId; //用户id
    private String orderId; //订单id
    private String orderNo; //订单编号
    private String orderStatus; //订单状态（10：待支付（待兑换），20：待发货，30：已发货（已兑换），40：已完成，50：已取消/已关闭）
    private String orderType; //订单类型（1、采购订单，2、兑换订单）
    //保存订单
//    private String orderType; //订单类型（10、终端购买，20、积分兑换，21、交易金兑换，22、奖励金兑换，23、金币兑换）
    private String brandId; //品牌id
    private String goodsId; //商品表id
    private String sepId; //规格id
    private BigDecimal goodsNum; //商品数量
    private String deliveryType; //配送方式（0：自提，1：物流）
    private BigDecimal deliveryAmount; //配送费
    private String selfId; //自提表id
    private String address; //物流地点/自提地点
    private String contactPerson; //提货人/收货人姓名
    private String contactPhone; //提货人/收货人电话
    private String remark; //备注
    //微信支付时获取openId
    private String payType; //支付方式(0:微信支付，1：余额支付)
    private String goodsUserId; //商品所有人
    private BigDecimal payAmount; //支付金额
    private BigDecimal exchangeAmount; //兑换金额
    private String passWord;//订单支付密码校验
}
