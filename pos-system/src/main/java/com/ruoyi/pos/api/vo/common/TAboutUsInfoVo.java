package com.ruoyi.pos.api.vo.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @author luobo
 * @title: TAboutUsInfoVo
 * @projectName pos
 * @description: 关于我们
 * @date 2021/11/4 000416:25
 */
@Data
public class TAboutUsInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 数据主键 */
    private String id;
    /** 终端购买提示 */
    private String buyHint;
    /** 终端划回提示 */
    private String transferHint;
    /** 终端划拨提示 */
    private String returnHint;
    /** 商户奖励规则（富文本） */
    private String awardExplain;
    /** 平台公约（富文本） */
    private String platformPact;
    /** 平台logo图片 */
    private String logoImg;
    /** 平台名称 */
    private String name;
    /** 客服电话 */
    private String phone;
    /** 官网地址 */
    private String domainName;
    /** 联系邮箱 */
    private String postbox;
    /** 平台简介 */
    private String platformExplain;
    /*用户协议（富文本）*/
    private String userAgreement;
    /*隐私协议  (富文本)*/
    private String userPrivacy;


}
