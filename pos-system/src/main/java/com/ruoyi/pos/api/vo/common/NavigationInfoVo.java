package com.ruoyi.pos.api.vo.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @author luobo
 * @title: NavigationInfoVo
 * @projectName pos
 * @description: TODO
 * @date 2021-11-12 09:57:33
 */
@Data
public class NavigationInfoVo implements Serializable {
    private String logo; //金刚导航栏logo
    private String navigationName; //金刚导航栏名称
    private String navigationUrl; //金刚导航栏url地址路径
    private String sort; //排序
    private String linkType; //链接类型
}
