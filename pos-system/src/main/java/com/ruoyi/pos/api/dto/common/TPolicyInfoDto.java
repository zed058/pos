package com.ruoyi.pos.api.dto.common;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

/**
 * @author luobo
 * @title: TPolicyInfoDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-11 13:55:02
 */
@Data
public class TPolicyInfoDto extends BaseDto {
    private String type; //类型(0=：平台政策，1：商学院)
}
