package com.ruoyi.pos.api.service.task;

import com.ruoyi.pos.web.domain.TTerminalBindInfo;

import java.util.List;

/**
 * @author luobo
 * @title: IMerchantRenewService
 * @projectName pos
 * @description: TODO
 * @date 2021-11-19 08:58:36
 */
public interface IMerchantRenewService {
    //查询未处理的临时表数据
    List<TTerminalBindInfo> queryTerminalBind();
    //更新本地终端表商户号信息
    void renewTerminalBindStatus(TTerminalBindInfo bind);
}
