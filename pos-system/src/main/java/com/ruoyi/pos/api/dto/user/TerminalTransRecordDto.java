package com.ruoyi.pos.api.dto.user;

import com.ruoyi.pos.api.dto.BaseDto;
import lombok.Data;

/**
 * @author luobo
 * @title: TerminalTransRecordDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-18 11:29:02
 */
@Data
public class TerminalTransRecordDto extends BaseDto {
    private String transType; //划拨类型（0：划拨，1：划回）
}
