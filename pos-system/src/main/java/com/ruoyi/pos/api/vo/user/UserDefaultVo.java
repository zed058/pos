package com.ruoyi.pos.api.vo.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Administrator
 * @title: UserDefaultVo
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class UserDefaultVo {

    /** 出入账类型（10：余额，20：积分，30：金币，40：交易金序列号，50：奖励金序列号） */
    @Excel(name = "出入账类型", readConverterExp = " =10：余额，20：积分，30：金币，40：交易金序列号，50：奖励金序列号")
    private String transType;
    private String transTypeShow;
    /** 出入账数量 */
    private BigDecimal transAmount;
    /**
     * 出入账操作类型
     * 余额                      积分                 金币：        交易金序列号：    奖励金序列号：
     * 1001、终端购买             2001、激活奖励   3001、序列号兑换   4001、系统发放    5001、激活奖励
     * 1002、商品兑换             2002、出售商品   3002、出售商品     4002、出售商品    5002、出售商品
     * 1003、提现                 2003、商品兑换   3003、商品兑换     4003、商品兑换    5003、商品兑换
     * 1004、商户收益（激活奖励）                  3004、兑换现金     4004、兑换金币    5004、兑换金币
     * 1005、直接受益（分润收益）
     * 1006、下级收益（分润收益）
     * 1007、采购收益（导师收益）
     * 1008、提现收益（导师收益）
     * 1009、办公补贴（导师收益）
     * 1010、金币兑换
     * */
    private String transOperType;
    private String transOperTypeShow;

    /** 金币类型（1、交易金金币，2、奖励金金币） */
    private String goldType;
    private String goldTypeShow;

    /*提现审核*/
    private String checkStatus;
    private String checkStatusShow;


    /*创建时间*/
    private Date createTime;
    /*序列号*/
    private Long serialId;
    /*终端号*/
    private String snCode;
    /*下级交易额【收益明细（第三方交易额）】*/
    private BigDecimal otherTransAmount;
    /*我下级用户名称*/
    private String userName;


}
