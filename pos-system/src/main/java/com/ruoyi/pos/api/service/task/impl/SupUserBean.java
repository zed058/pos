package com.ruoyi.pos.api.service.task.impl;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author luobo
 * @title: SupUserBean
 * @projectName pos
 * @description: TODO
 * @date 2021-12-27 10:23:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SupUserBean {
    private String userId;
    private BigDecimal proportion = BigDecimal.ZERO;
}
