package com.ruoyi.pos.api.service.user.impl;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.enums.ShandianFlagEnum;
import com.ruoyi.common.enums.TeamTypeEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.pos.api.service.common.ICommonService;
import com.ruoyi.pos.api.service.user.IMainManageService;
import com.ruoyi.pos.api.utils.CommonUtils;
import com.ruoyi.pos.api.vo.user.MainVo;
import com.ruoyi.pos.web.domain.TRebateSetInfo;
import com.ruoyi.pos.web.domain.TStatisticInfo;
import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.domain.TUserTeamInfo;
import com.ruoyi.pos.web.mapper.TStatisticInfoMapper;
import com.ruoyi.pos.web.mapper.TTerminalInfoMapper;
import com.ruoyi.pos.web.mapper.TUserInfoMapper;
import com.ruoyi.pos.web.mapper.TUserTeamInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author luobo
 * @title: MainManageServiceImpl
 * @projectName pos
 * @description: TODO
 * @date 2021-11-17 09:39:44
 */
@Service
public class MainManageServiceImpl implements IMainManageService {
    private static final Logger log = LoggerFactory.getLogger(MainManageServiceImpl.class);
    @Autowired
    private CommonUtils commonUtils; //公共业务处理类

    @Autowired
    private TUserInfoMapper userInfoMapper;
    @Autowired
    private TUserTeamInfoMapper userTeamInfoMapper;
    @Autowired
    private TStatisticInfoMapper statisticInfoMapper;
    @Autowired
    private TTerminalInfoMapper terminalInfoMapper;
    @Autowired
    private ICommonService commonService;
    @Autowired
    private Environment env;


    //查询首页统计数据
    @Override
    public MainVo queryMainInfo() throws Exception {
        MainVo vo = new MainVo();
        //取平台实时总交易量
        TStatisticInfo info = statisticInfoMapper.selectTStatisticInfoForTotal();
        vo.setTotalTrans(info == null ? BigDecimal.ZERO : info.getMyTransMoney()); //我的当月交易量
        //取网站配置信息
        TRebateSetInfo setInfo = commonService.queryTRebateSetInfoVo();
        if (StringUtils.isNotNull(setInfo) && StringUtils.isNotNull(setInfo.getHundredThan())){
            vo.setCommandAwardFund((vo.getTotalTrans().multiply(setInfo.getHundredThan())).setScale(2,BigDecimal.ROUND_DOWN));
        }

        Integer terminals = 0;
        //取自己
        String userId = commonUtils.getUserId();
        List<String> userIds = new ArrayList<String>();
        userIds.add(userId);
        info = statisticInfoMapper.selectTStatisticInfoByUserIds(userIds, "", DateUtils.getCurrMonthDirstDateStr() + " 00:00:00", DateUtils.getTime());
        vo.setMyTrans(info == null ? BigDecimal.ZERO : info.getMyTransMoney()); //我的当月交易量
        terminals = terminalInfoMapper.selectTTerminalInfoListByUserIds(userIds, "", DateUtils.getCurrMonthDirstDateStr() + " 00:00:00", DateUtils.getTime());
        vo.setMyTerminal(new BigDecimal(terminals)); //我的终端数
        //取直营
        TUserTeamInfo mapper = new TUserTeamInfo();
        mapper.setUserId(userId);
        mapper.setTeamType(TeamTypeEnum.DIRECTLY.getCode());
        List<TUserTeamInfo> teams = userTeamInfoMapper.selectTUserTeamInfoList(mapper);
        userIds = teams.stream().map(TUserTeamInfo::getSubUserId).collect(Collectors.toList());
        info = statisticInfoMapper.selectTStatisticInfoByUserIds(userIds, "", DateUtils.getCurrMonthDirstDateStr() + " 00:00:00", DateUtils.getTime());
        vo.setMyDirectlyTrans(info == null ? BigDecimal.ZERO : info.getMyTransMoney()); //我的直营交易量
        vo.setMyDirectlyPartner(new BigDecimal(teams.size())); //我的直营伙伴
        terminals = terminalInfoMapper.selectTTerminalInfoListByUserIds(userIds, "", DateUtils.getCurrMonthDirstDateStr() + " 00:00:00", DateUtils.getTime());
        vo.setMyDirectlyTerminal(new BigDecimal(terminals)); //我的直营终端数
        //取团队
        mapper.setTeamType(TeamTypeEnum.TEAM.getCode());
        teams = userTeamInfoMapper.selectTUserTeamInfoList(mapper);
        userIds = teams.stream().map(TUserTeamInfo::getSubUserId).collect(Collectors.toList());
        info = statisticInfoMapper.selectTStatisticInfoByUserIds(userIds, "", DateUtils.getCurrMonthDirstDateStr() + " 00:00:00", DateUtils.getTime());
        vo.setMyTeamTrans(info == null ? BigDecimal.ZERO : info.getMyTransMoney()); //我的团队交易量
        vo.setMyTeamPartner(new BigDecimal(teams.size()));//我的团队伙伴
        terminals = terminalInfoMapper.selectTTerminalInfoListByUserIds(userIds, "", DateUtils.getCurrMonthDirstDateStr() + " 00:00:00", DateUtils.getTime());
        vo.setMyTeamTerminal(new BigDecimal(terminals)); //我的团队终端数
        Date firstDate = DateUtils.dateTime(DateUtils.YYYY_MM_DD_HH_MM_SS, DateUtils.getCurrMonthDirstDateStr() + " 00:00:00");
        List<TUserTeamInfo> currTeam = teams.stream().filter(team ->team.getCreateTime().after(firstDate)).collect(Collectors.toList());
        vo.setMyNewPartner(new BigDecimal(currTeam.size())); //本月新增伙伴
        return vo;
    }

    @Override
    public JSONObject toShandianbaoReg() throws Exception {
        JSONObject jo = new JSONObject();
        String userId = commonUtils.getUserId();
        TUserInfo user = userInfoMapper.selectTUserInfoByUserId(userId);
        user.setShandianAuthFlag(ShandianFlagEnum.WAIT_SNGY.getCode());
        user.setShandianAuthTime(DateUtils.getNowDate());
        userInfoMapper.updateTUserInfo(user);
        jo.put("regUri", env.getProperty("shandianbao.regUrl"));
        jo.put("shandianAuthFlag", user.getShandianAuthFlag());
        return jo;
    }
}
