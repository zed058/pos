package com.ruoyi.pos.api.service.user;

import com.ruoyi.pos.api.dto.user.TerminalManageDto;
import com.ruoyi.pos.api.vo.user.InventoryRecordGoodsVo;
import com.ruoyi.pos.api.vo.user.InventoryRecordStatisticVo;
import com.ruoyi.pos.api.vo.user.InventoryRecordVo;

import java.util.List;
import java.util.Map;

/**
 * @author luobo
 * @title: ITerminalManageService
 * @projectName pos
 * @description: TODO
 * @date 2021-11-15 16:30:56
 */
public interface ITerminalManageService {
    //查询库存管理/库存记录-头信息
    InventoryRecordVo queryInventoryRecord(TerminalManageDto terminalManageDto) throws Exception;
    //查询库存管理-根据品牌查询
    List<InventoryRecordGoodsVo> queryInventoryManagementByBrand(TerminalManageDto terminalManageDto) throws Exception;
    //查询库存记录-根据品牌查询
    InventoryRecordStatisticVo queryInventoryRecordByBrand(TerminalManageDto terminalManageDto) throws Exception;
    //查询终端列表
    Map<String, Object> queryTerminalList(TerminalManageDto terminalManageDto) throws Exception;
    //划拨（划出）
    void terminalTransferOut(TerminalManageDto terminalManageDto) throws Exception;
    //划拨（划回）
    void terminalTransferBack(TerminalManageDto terminalManageDto) throws Exception;
}
