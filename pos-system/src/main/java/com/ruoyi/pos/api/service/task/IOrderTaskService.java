package com.ruoyi.pos.api.service.task;

import com.ruoyi.pos.api.vo.user.OrderListVo;
import com.ruoyi.pos.web.domain.TOrderInfo;

import java.util.List;

/**
 * @author luobo
 * @title: OrderService
 * @projectName pos
 * @description: TODO
 * @date 2021-11-08 11:42:27
 */
public interface IOrderTaskService {
    //查询超时订单
    List<TOrderInfo> queryCloseTimeoutOrders() throws Exception;
    //关闭超时订单
    void closeTimeoutOrders(String orderId) throws Exception;
}
