package com.ruoyi.pos.api.dto.common;

import lombok.*;

import java.io.Serializable;

/**
 * @author luobo
 * @title: WithdrawalResultNotifyDto
 * @projectName pos
 * @description: TODO
 * @date 2021-11-19 11:16:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WithdrawalResultNotifyDto implements Serializable {
    @NonNull
    private String code;
    @NonNull
    private String msg;
    @NonNull
    private String signType;
    @NonNull
    private String signValue;
    private String body;

    //custOrderNo   客户订单批次号
    //orderNo   平台订单批次号
    //status   付款明细状态     2：处理中 ；3：成功；4：部分成功；5:失败
}
