package com.ruoyi.pos.api.vo.common;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Administrator
 * @title: 确定订单Vo
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class ConfirmOrderVo {
    /** 主键id */
    private String id;
    /** 终端购买品牌id（兑换商城商品id) */
    private String brandId;
    /** 商品类型（0：终端商品，1：兑换商品） */
    private String goodsType;
    /** 商品交易类型（10:采购,20:积分兑换，21:交易金兑换，22:奖励金兑换，23:金币兑换） */
    private String goodsStatus;
    /** 商品名称 */
    private String goodsName;
    /** 商品图片 */
    private String goodsImg;
    /** 商品所有人 */
    private String goodsUserId;
    /** 兑换/购买金额 */
    private BigDecimal amount;
    /** 兑换商城商品（配送费） */
    private BigDecimal deliveryFee;
    /** 商品单位 */
    private String unit;
    /** 兑换单位（购买时默认为人民币） */
    private String exchangeUnit;
    /** 商品排序 */
    private String sort;
    /*规格id*/
    private String sepId;
    /*终端购买数量*/
    private BigDecimal goodsNum;
    /*终端金额小计*/
    private BigDecimal payAmount;
}
