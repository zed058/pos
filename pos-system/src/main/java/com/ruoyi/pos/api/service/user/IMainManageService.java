package com.ruoyi.pos.api.service.user;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.pos.api.vo.user.MainVo;

/**
 * @author luobo
 * @title: IMainManageService
 * @projectName pos
 * @description: TODO
 * @date 2021-11-17 09:39:35
 */
public interface IMainManageService {
    //查询首页统计数据
    MainVo queryMainInfo() throws Exception;
    //查询首页统计数据
    JSONObject toShandianbaoReg() throws Exception;
}
