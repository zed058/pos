package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 平台设置（关于我们）对象 t_about_us_info
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public class TAboutUsInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 终端购买提示 */
    private String buyHint;

    /** 终端划回提示 */
    private String transferHint;

    /** 终端划拨提示 */
    private String returnHint;

    /** 商户奖励规则（富文本） */
    private String awardExplain;

    /** 平台公约（富文本） */
    private String platformPact;

    /** 平台logo图片 */
    private String logoImg;

    /** 平台名称 */
    private String name;

    /** 客服电话 */
    private String phone;

    /** 官网地址 */
    private String domainName;

    /** 联系邮箱 */
    private String postbox;

    /** 平台简介 */
    private String platformExplain;

    /*用户协议*/
    private String userAgreement;

    /*隐私政策*/
    private String userPrivacy;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setBuyHint(String buyHint) 
    {
        this.buyHint = buyHint;
    }

    public String getBuyHint() 
    {
        return buyHint;
    }
    public void setTransferHint(String transferHint) 
    {
        this.transferHint = transferHint;
    }

    public String getTransferHint() 
    {
        return transferHint;
    }
    public void setReturnHint(String returnHint) 
    {
        this.returnHint = returnHint;
    }

    public String getReturnHint() 
    {
        return returnHint;
    }
    public void setAwardExplain(String awardExplain) 
    {
        this.awardExplain = awardExplain;
    }

    public String getAwardExplain() 
    {
        return awardExplain;
    }
    public void setPlatformPact(String platformPact) 
    {
        this.platformPact = platformPact;
    }

    public String getPlatformPact() 
    {
        return platformPact;
    }
    public void setLogoImg(String logoImg) 
    {
        this.logoImg = logoImg;
    }

    public String getLogoImg() 
    {
        return logoImg;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setDomainName(String domainName) 
    {
        this.domainName = domainName;
    }

    public String getDomainName() 
    {
        return domainName;
    }
    public void setPostbox(String postbox) 
    {
        this.postbox = postbox;
    }

    public String getPostbox() 
    {
        return postbox;
    }
    public void setPlatformExplain(String platformExplain) 
    {
        this.platformExplain = platformExplain;
    }

    public String getPlatformExplain() 
    {
        return platformExplain;
    }

    public String getUserAgreement() {
        return userAgreement;
    }

    public void setUserAgreement(String userAgreement) {
        this.userAgreement = userAgreement;
    }

    public String getUserPrivacy() {
        return userPrivacy;
    }

    public void setUserPrivacy(String userPrivacy) {
        this.userPrivacy = userPrivacy;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("buyHint", getBuyHint())
            .append("transferHint", getTransferHint())
            .append("returnHint", getReturnHint())
            .append("awardExplain", getAwardExplain())
            .append("platformPact", getPlatformPact())
            .append("logoImg", getLogoImg())
            .append("name", getName())
            .append("phone", getPhone())
            .append("domainName", getDomainName())
            .append("postbox", getPostbox())
            .append("platformExplain", getPlatformExplain())
            .toString();
    }
}
