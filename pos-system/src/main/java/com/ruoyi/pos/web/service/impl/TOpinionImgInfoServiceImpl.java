package com.ruoyi.pos.web.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TOpinionImgInfoMapper;
import com.ruoyi.pos.web.domain.TOpinionImgInfo;
import com.ruoyi.pos.web.service.ITOpinionImgInfoService;

/**
 * 意见反馈图片Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@Service
public class TOpinionImgInfoServiceImpl implements ITOpinionImgInfoService 
{
    @Autowired
    private TOpinionImgInfoMapper tOpinionImgInfoMapper;

    /**
     * 查询意见反馈图片
     * 
     * @param id 意见反馈图片主键
     * @return 意见反馈图片
     */
    @Override
    public TOpinionImgInfo selectTOpinionImgInfoById(String id)
    {
        return tOpinionImgInfoMapper.selectTOpinionImgInfoById(id);
    }

    /**
     * 查询意见反馈图片列表
     * 
     * @param tOpinionImgInfo 意见反馈图片
     * @return 意见反馈图片
     */
    @Override
    public List<TOpinionImgInfo> selectTOpinionImgInfoList(TOpinionImgInfo tOpinionImgInfo)
    {
        return tOpinionImgInfoMapper.selectTOpinionImgInfoList(tOpinionImgInfo);
    }

    /**
     * 新增意见反馈图片
     * 
     * @param tOpinionImgInfo 意见反馈图片
     * @return 结果
     */
    @Override
    public int insertTOpinionImgInfo(TOpinionImgInfo tOpinionImgInfo)
    {
        return tOpinionImgInfoMapper.insertTOpinionImgInfo(tOpinionImgInfo);
    }

    /**
     * 修改意见反馈图片
     * 
     * @param tOpinionImgInfo 意见反馈图片
     * @return 结果
     */
    @Override
    public int updateTOpinionImgInfo(TOpinionImgInfo tOpinionImgInfo)
    {
        return tOpinionImgInfoMapper.updateTOpinionImgInfo(tOpinionImgInfo);
    }

    /**
     * 批量删除意见反馈图片
     * 
     * @param ids 需要删除的意见反馈图片主键
     * @return 结果
     */
    @Override
    public int deleteTOpinionImgInfoByIds(String[] ids)
    {
        return tOpinionImgInfoMapper.deleteTOpinionImgInfoByIds(ids);
    }

    /**
     * 删除意见反馈图片信息
     * 
     * @param id 意见反馈图片主键
     * @return 结果
     */
    @Override
    public int deleteTOpinionImgInfoById(String id)
    {
        return tOpinionImgInfoMapper.deleteTOpinionImgInfoById(id);
    }
}
