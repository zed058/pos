package com.ruoyi.pos.web.mapper;

import com.ruoyi.pos.web.domain.TTerminalInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户商户终端关系Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface TTerminalInfoMapper 
{
    /**
     * 查询用户商户终端关系
     * 
     * @param id 用户商户终端关系主键
     * @return 用户商户终端关系
     */
    public TTerminalInfo selectTTerminalInfoById(String id);
    public TTerminalInfo selectTTerminalInfoByIdForTrans(String id);

    /**
     * 查询用户商户终端关系列表
     * 
     * @param tTerminalInfo 用户商户终端关系
     * @return 用户商户终端关系集合
     */
    public List<TTerminalInfo> selectTTerminalInfoList(TTerminalInfo tTerminalInfo);
    public List<TTerminalInfo> selectTTerminalInfoListForWeb(TTerminalInfo tTerminalInfo);
    public List<TTerminalInfo> selectTTerminalInfoListForAdd(TTerminalInfo tTerminalInfo);


    /**
     * 新增用户商户终端关系
     * 
     * @param tTerminalInfo 用户商户终端关系
     * @return 结果
     */
    public int insertTTerminalInfo(TTerminalInfo tTerminalInfo);

    /**
     * 修改用户商户终端关系
     * 
     * @param tTerminalInfo 用户商户终端关系
     * @return 结果
     */
    public int updateTTerminalInfo(TTerminalInfo tTerminalInfo);

    /**
     * 删除用户商户终端关系
     * 
     * @param id 用户商户终端关系主键
     * @return 结果
     */
    public int deleteTTerminalInfoById(String id);

    /**
     * 批量删除用户商户终端关系
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTerminalInfoByIds(String[] ids);
    //---------------------------------------------------------------------------------------------------------------------------
    /**
     * @return
     */
    TTerminalInfo selectTTerminalInfoBySnCode(String snCode);


    public List<TTerminalInfo> queryUserTerminalList(TTerminalInfo tTerminalInfo);


    /*批量添加*/
    public int insertList(@Param("tTerminalInfo") List<TTerminalInfo> tTerminalInfo);

    Integer selectTTerminalInfoListByUserIds(@Param("userIds") List<String> userIds, @Param("brandId") String brandId, @Param("startTimeStr") String startTimeStr, @Param("endTimeStr") String endTimeStr);
}
