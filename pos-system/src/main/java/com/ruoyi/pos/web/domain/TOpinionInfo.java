package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 意见反馈对象 t_opinion_info
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public class TOpinionInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 联系电话，或邮箱 */
    @Excel(name = "联系电话，或邮箱")
    private String contactWay;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 意见或建议 */
    @Excel(name = "意见或建议")
    private String opinion;

    /** 回复状态（0：回复，1：未回复） */
    @Excel(name = "回复状态", readConverterExp = "0=：回复，1：未回复")
    private String replyState;
    private String replyStateName;

    /** 回复信息 */
    @Excel(name = "回复信息")
    private String reply;

    /*用户名称*/
    private String userName;

    private String opinionImg;

    public String getOpinionImg() {
        return opinionImg;
    }

    public void setOpinionImg(String opinionImg) {
        this.opinionImg = opinionImg;
    }

    public String getReplyStateName() {
        return replyStateName;
    }

    public void setReplyStateName(String replyStateName) {
        this.replyStateName = replyStateName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setContactWay(String contactWay) 
    {
        this.contactWay = contactWay;
    }

    public String getContactWay() 
    {
        return contactWay;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setOpinion(String opinion) 
    {
        this.opinion = opinion;
    }

    public String getOpinion() 
    {
        return opinion;
    }
    public void setReplyState(String replyState) 
    {
        this.replyState = replyState;
    }

    public String getReplyState() 
    {
        return replyState;
    }
    public void setReply(String reply) 
    {
        this.reply = reply;
    }

    public String getReply() 
    {
        return reply;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("contactWay", getContactWay())
            .append("userId", getUserId())
            .append("opinion", getOpinion())
            .append("createTime", getCreateTime())
            .append("replyState", getReplyState())
            .append("reply", getReply())
            .toString();
    }
}
