package com.ruoyi.pos.web.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TGoldAwardSerialInfoMapper;
import com.ruoyi.pos.web.domain.TGoldAwardSerialInfo;
import com.ruoyi.pos.web.service.ITGoldAwardSerialInfoService;

/**
 * (金币序列号Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-20
 */
@Service
public class TGoldAwardSerialInfoServiceImpl implements ITGoldAwardSerialInfoService 
{
    @Autowired
    private TGoldAwardSerialInfoMapper tGoldAwardSerialInfoMapper;

    /**
     * 查询(金币序列号
     * 
     * @param id (金币序列号主键
     * @return (金币序列号
     */
    @Override
    public TGoldAwardSerialInfo selectTGoldAwardSerialInfoById(Long id)
    {
        return tGoldAwardSerialInfoMapper.selectTGoldAwardSerialInfoById(id);
    }

    /**
     * 查询(金币序列号列表
     * 
     * @param tGoldAwardSerialInfo (金币序列号
     * @return (金币序列号
     */
    @Override
    public List<TGoldAwardSerialInfo> selectTGoldAwardSerialInfoList(TGoldAwardSerialInfo tGoldAwardSerialInfo)
    {
        return tGoldAwardSerialInfoMapper.selectTGoldAwardSerialInfoList(tGoldAwardSerialInfo);
    }

    /**
     * 新增(金币序列号
     * 
     * @param tGoldAwardSerialInfo (金币序列号
     * @return 结果
     */
    @Override
    public int insertTGoldAwardSerialInfo(TGoldAwardSerialInfo tGoldAwardSerialInfo)
    {
        tGoldAwardSerialInfo.setCreateTime(DateUtils.getNowDate());
        return tGoldAwardSerialInfoMapper.insertTGoldAwardSerialInfo(tGoldAwardSerialInfo);
    }

    /**
     * 修改(金币序列号
     * 
     * @param tGoldAwardSerialInfo (金币序列号
     * @return 结果
     */
    @Override
    public int updateTGoldAwardSerialInfo(TGoldAwardSerialInfo tGoldAwardSerialInfo)
    {
        return tGoldAwardSerialInfoMapper.updateTGoldAwardSerialInfo(tGoldAwardSerialInfo);
    }

    /**
     * 批量删除(金币序列号
     * 
     * @param ids 需要删除的(金币序列号主键
     * @return 结果
     */
    @Override
    public int deleteTGoldAwardSerialInfoByIds(Long[] ids)
    {
        return tGoldAwardSerialInfoMapper.deleteTGoldAwardSerialInfoByIds(ids);
    }

    /**
     * 删除(金币序列号信息
     * 
     * @param id (金币序列号主键
     * @return 结果
     */
    @Override
    public int deleteTGoldAwardSerialInfoById(Long id)
    {
        return tGoldAwardSerialInfoMapper.deleteTGoldAwardSerialInfoById(id);
    }
}
