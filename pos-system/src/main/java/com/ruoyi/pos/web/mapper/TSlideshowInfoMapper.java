package com.ruoyi.pos.web.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.pos.web.domain.TSlideshowInfo;

/**
 * 引导页面Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface TSlideshowInfoMapper 
{
    /**
     * 查询引导页面
     * 
     * @param id 引导页面主键
     * @return 引导页面
     */
    public TSlideshowInfo selectTSlideshowInfoById(String id);

    /**
     * 查询引导页面列表
     * 
     * @param tSlideshowInfo 引导页面
     * @return 引导页面集合
     */
    public List<TSlideshowInfo> selectTSlideshowInfoList(TSlideshowInfo tSlideshowInfo);

    /**
     * 新增引导页面
     * 
     * @param tSlideshowInfo 引导页面
     * @return 结果
     */
    public int insertTSlideshowInfo(TSlideshowInfo tSlideshowInfo);

    /**
     * 修改引导页面
     * 
     * @param tSlideshowInfo 引导页面
     * @return 结果
     */
    public int updateTSlideshowInfo(TSlideshowInfo tSlideshowInfo);

    /**
     * 删除引导页面
     * 
     * @param id 引导页面主键
     * @return 结果
     */
    public int deleteTSlideshowInfoById(String id);

    /**
     * 批量删除引导页面
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTSlideshowInfoByIds(String[] ids);

    int selectSlideShowInfoByImgType(Map<String, Object> params);
}
