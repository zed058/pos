package com.ruoyi.pos.web.mapper;

import java.util.List;
import com.ruoyi.pos.web.domain.TTerminalTransInfo;

/**
 * 终端划拨记录Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public interface TTerminalTransInfoMapper 
{
    /**
     * 查询终端划拨记录
     * 
     * @param id 终端划拨记录主键
     * @return 终端划拨记录
     */
    public TTerminalTransInfo selectTTerminalTransInfoById(String id);

    /**
     * 查询终端划拨记录列表
     * 
     * @param tTerminalTransInfo 终端划拨记录
     * @return 终端划拨记录集合
     */
    public List<TTerminalTransInfo> selectTTerminalTransInfoList(TTerminalTransInfo tTerminalTransInfo);

    /**
     * 新增终端划拨记录
     * 
     * @param tTerminalTransInfo 终端划拨记录
     * @return 结果
     */
    public int insertTTerminalTransInfo(TTerminalTransInfo tTerminalTransInfo);

    /**
     * 修改终端划拨记录
     * 
     * @param tTerminalTransInfo 终端划拨记录
     * @return 结果
     */
    public int updateTTerminalTransInfo(TTerminalTransInfo tTerminalTransInfo);

    /**
     * 删除终端划拨记录
     * 
     * @param id 终端划拨记录主键
     * @return 结果
     */
    public int deleteTTerminalTransInfoById(String id);

    /**
     * 批量删除终端划拨记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTerminalTransInfoByIds(String[] ids);

}
