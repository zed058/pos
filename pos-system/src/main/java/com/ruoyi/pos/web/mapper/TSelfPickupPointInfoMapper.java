package com.ruoyi.pos.web.mapper;

import java.util.List;

import com.ruoyi.pos.api.dto.common.CommonDto;
import com.ruoyi.pos.api.vo.common.TSelfPickupPointVo;
import com.ruoyi.pos.web.domain.TSelfPickupPointInfo;

/**
 * 商品自提点Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface TSelfPickupPointInfoMapper 
{
    /**
     * 查询商品自提点
     * 
     * @param id 商品自提点主键
     * @return 商品自提点
     */
    public TSelfPickupPointInfo selectTSelfPickupPointInfoById(String id);

    /**
     * 查询商品自提点列表
     * 
     * @param tSelfPickupPointInfo 商品自提点
     * @return 商品自提点集合
     */
    public List<TSelfPickupPointInfo> selectTSelfPickupPointInfoList(TSelfPickupPointInfo tSelfPickupPointInfo);

    /**
     * 新增商品自提点
     * 
     * @param tSelfPickupPointInfo 商品自提点
     * @return 结果
     */
    public int insertTSelfPickupPointInfo(TSelfPickupPointInfo tSelfPickupPointInfo);

    /**
     * 修改商品自提点
     * 
     * @param tSelfPickupPointInfo 商品自提点
     * @return 结果
     */
    public int updateTSelfPickupPointInfo(TSelfPickupPointInfo tSelfPickupPointInfo);

    /**
     * 删除商品自提点
     * 
     * @param id 商品自提点主键
     * @return 结果
     */
    public int deleteTSelfPickupPointInfoById(String id);

    /**
     * 批量删除商品自提点
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTSelfPickupPointInfoByIds(String[] ids);

    //查询商品自提点列表
    public List<TSelfPickupPointVo> querySelfPickupPointList(CommonDto commonDto);
}
