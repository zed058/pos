package com.ruoyi.pos.web.mapper;

import com.ruoyi.pos.web.domain.TShipperCodeInfo;

import java.util.List;

/**
 * 物流公司编码Mapper接口
 * 
 * @author ruoyi
 * @date 2021-12-10
 */
public interface TShipperCodeInfoMapper 
{
    /**
     * 查询物流公司编码
     * 
     * @param id 物流公司编码主键
     * @return 物流公司编码
     */
    public TShipperCodeInfo selectTShipperCodeInfoById(String id);

    /**
     * 查询物流公司编码列表
     * 
     * @param tShipperCodeInfo 物流公司编码
     * @return 物流公司编码集合
     */
    public List<TShipperCodeInfo> selectTShipperCodeInfoList(TShipperCodeInfo tShipperCodeInfo);

    /**
     * 新增物流公司编码
     * 
     * @param tShipperCodeInfo 物流公司编码
     * @return 结果
     */
    public int insertTShipperCodeInfo(TShipperCodeInfo tShipperCodeInfo);

    /**
     * 修改物流公司编码
     * 
     * @param tShipperCodeInfo 物流公司编码
     * @return 结果
     */
    public int updateTShipperCodeInfo(TShipperCodeInfo tShipperCodeInfo);

    /**
     * 删除物流公司编码
     * 
     * @param id 物流公司编码主键
     * @return 结果
     */
    public int deleteTShipperCodeInfoById(String id);

    /**
     * 批量删除物流公司编码
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTShipperCodeInfoByIds(String[] ids);

    /*物流公司编码*/
    public TShipperCodeInfo selectTShipperCodeInfoCode(String shipperCode);
}
