package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TNavigationInfo;

/**
 * 金刚导航栏Service接口
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public interface ITNavigationInfoService 
{
    /**
     * 查询金刚导航栏
     * 
     * @param id 金刚导航栏主键
     * @return 金刚导航栏
     */
    public TNavigationInfo selectTNavigationInfoById(String id);

    /**
     * 查询金刚导航栏列表
     * 
     * @param tNavigationInfo 金刚导航栏
     * @return 金刚导航栏集合
     */
    public List<TNavigationInfo> selectTNavigationInfoList(TNavigationInfo tNavigationInfo);

    /**
     * 新增金刚导航栏
     * 
     * @param tNavigationInfo 金刚导航栏
     * @return 结果
     */
    public int insertTNavigationInfo(TNavigationInfo tNavigationInfo);

    /**
     * 修改金刚导航栏
     * 
     * @param tNavigationInfo 金刚导航栏
     * @return 结果
     */
    public int updateTNavigationInfo(TNavigationInfo tNavigationInfo);

    /**
     * 批量删除金刚导航栏
     * 
     * @param ids 需要删除的金刚导航栏主键集合
     * @return 结果
     */
    public int deleteTNavigationInfoByIds(String[] ids);

    /**
     * 删除金刚导航栏信息
     * 
     * @param id 金刚导航栏主键
     * @return 结果
     */
    public int deleteTNavigationInfoById(String id);
}
