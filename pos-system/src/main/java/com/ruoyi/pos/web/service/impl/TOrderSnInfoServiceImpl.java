package com.ruoyi.pos.web.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TOrderSnInfoMapper;
import com.ruoyi.pos.web.domain.TOrderSnInfo;
import com.ruoyi.pos.web.service.ITOrderSnInfoService;

/**
 * 订单终端SN码（字）Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
@Service
public class TOrderSnInfoServiceImpl implements ITOrderSnInfoService 
{
    @Autowired
    private TOrderSnInfoMapper tOrderSnInfoMapper;

    /**
     * 查询订单终端SN码（字）
     * 
     * @param id 订单终端SN码（字）主键
     * @return 订单终端SN码（字）
     */
    @Override
    public TOrderSnInfo selectTOrderSnInfoById(String id)
    {
        return tOrderSnInfoMapper.selectTOrderSnInfoById(id);
    }

    /**
     * 查询订单终端SN码（字）列表
     * 
     * @param tOrderSnInfo 订单终端SN码（字）
     * @return 订单终端SN码（字）
     */
    @Override
    public List<TOrderSnInfo> selectTOrderSnInfoList(TOrderSnInfo tOrderSnInfo)
    {
        return tOrderSnInfoMapper.selectTOrderSnInfoList(tOrderSnInfo);
    }

    /**
     * 新增订单终端SN码（字）
     * 
     * @param tOrderSnInfo 订单终端SN码（字）
     * @return 结果
     */
    @Override
    public int insertTOrderSnInfo(TOrderSnInfo tOrderSnInfo)
    {
        return tOrderSnInfoMapper.insertTOrderSnInfo(tOrderSnInfo);
    }

    /**
     * 修改订单终端SN码（字）
     * 
     * @param tOrderSnInfo 订单终端SN码（字）
     * @return 结果
     */
    @Override
    public int updateTOrderSnInfo(TOrderSnInfo tOrderSnInfo)
    {
        return tOrderSnInfoMapper.updateTOrderSnInfo(tOrderSnInfo);
    }

    /**
     * 批量删除订单终端SN码（字）
     * 
     * @param ids 需要删除的订单终端SN码（字）主键
     * @return 结果
     */
    @Override
    public int deleteTOrderSnInfoByIds(String[] ids)
    {
        return tOrderSnInfoMapper.deleteTOrderSnInfoByIds(ids);
    }

    /**
     * 删除订单终端SN码（字）信息
     * 
     * @param id 订单终端SN码（字）主键
     * @return 结果
     */
    @Override
    public int deleteTOrderSnInfoById(String id)
    {
        return tOrderSnInfoMapper.deleteTOrderSnInfoById(id);
    }

    @Override
    public void insert(String orderId,String[] snCodeArray) {
        tOrderSnInfoMapper.insert(orderId,snCodeArray);
    }

    @Override
    public void delete(TOrderSnInfo snInfo) {
        tOrderSnInfoMapper.delete(snInfo);
    }

}
