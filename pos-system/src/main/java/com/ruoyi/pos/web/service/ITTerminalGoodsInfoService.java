package com.ruoyi.pos.web.service;

import java.util.List;

import com.ruoyi.pos.api.vo.user.UserInfoVo;
import com.ruoyi.pos.web.domain.TTerminalBrandInfo;
import com.ruoyi.pos.web.domain.TTerminalGoodsInfo;
import com.ruoyi.pos.web.webDto.TTerminalInfoWebDto;

/**
 * 商品基础Service接口
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public interface ITTerminalGoodsInfoService 
{
    /**
     * 查询商品基础
     * 
     * @param id 商品基础主键
     * @return 商品基础
     */
    public TTerminalGoodsInfo selectTTerminalGoodsInfoById(String id);

    /**
     * 查询商品基础列表
     * 
     * @param tTerminalGoodsInfo 商品基础
     * @return 商品基础集合
     */
    public List<TTerminalGoodsInfo> selectTTerminalGoodsInfoList(TTerminalGoodsInfo tTerminalGoodsInfo);

    /**
     * 新增商品基础
     * 
     * @param tTerminalGoodsInfo 商品基础
     * @return 结果
     */
    public int insertTTerminalGoodsInfo(TTerminalGoodsInfo tTerminalGoodsInfo);

    /**
     * 修改商品基础
     * 
     * @param tTerminalGoodsInfo 商品基础
     * @return 结果
     */
    public int updateTTerminalGoodsInfo(TTerminalGoodsInfo tTerminalGoodsInfo);

    /**
     * 批量删除商品基础
     * 
     * @param ids 需要删除的商品基础主键集合
     * @return 结果
     */
    public int deleteTTerminalGoodsInfoByIds(String[] ids);

    /**
     * 删除商品基础信息
     * 
     * @param id 商品基础主键
     * @return 结果
     */
    public int deleteTTerminalGoodsInfoById(String id);


    /**
     * 查询终端||兑换商品基础列表
     *
     * @param tTerminalGoodsInfo 终端||兑换商品基础
     * @return 终端||兑换商品基础集合
     */
    public List<TTerminalGoodsInfo> queryTerminalAndExchange(TTerminalGoodsInfo tTerminalGoodsInfo);

    //查询商品品牌
    List<TTerminalBrandInfo> queryBrand();

    //添加终端SN码
    int insertList (TTerminalInfoWebDto dto);

    //已实名认证的用户
    List<UserInfoVo> auditUserList();
}
