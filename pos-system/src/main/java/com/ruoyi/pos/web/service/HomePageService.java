package com.ruoyi.pos.web.service;

import java.util.Map;

/**
 * @author Administrator
 * @title: HomePageService
 * @projectName ruoyi
 * @description: TODO
 */
public interface HomePageService {

    /*首页头部数据统计*/
    Map<String,Object> quantitySum() throws Exception;

    /*用户统计图*/
    Map<String, Object> queryUserChart() throws Exception;

    /*订单统计图*/
    Map<String, Object> queryOrderChart() throws Exception;

    /*采购商统计图*/
    Map<String, Object> queryOrderTerminalChart();

    /*兑换商统计图*/
    Map<String, Object> queryOrderExchangeChart();
}
