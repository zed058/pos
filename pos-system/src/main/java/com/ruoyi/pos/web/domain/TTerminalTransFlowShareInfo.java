package com.ruoyi.pos.web.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 第三方交易流水分润详情对象 t_terminal_trans_flow_share_info
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TTerminalTransFlowShareInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 交易流水id */
    @Excel(name = "交易流水", sort = 1)
    private String flowId;

    /** 分润人user_id */
    private String shareUserId;

    /** 分润等额差比例 */
    @Excel(name = "分润等额差比例", sort = 3)
    private BigDecimal shareDiff;

    /** 分润金额 */
    @Excel(name = "分润金额", sort = 4)
    private BigDecimal shareAmount;
    /*用户姓名*/
    @Excel(name = "分润人", sort = 2)
    private String userName;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setFlowId(String flowId) 
    {
        this.flowId = flowId;
    }

    public String getFlowId() 
    {
        return flowId;
    }
    public void setShareUserId(String shareUserId) 
    {
        this.shareUserId = shareUserId;
    }

    public String getShareUserId() 
    {
        return shareUserId;
    }
    public void setShareDiff(BigDecimal shareDiff)
    {
        this.shareDiff = shareDiff;
    }

    public BigDecimal getShareDiff()
    {
        return shareDiff;
    }
    public void setShareAmount(BigDecimal shareAmount)
    {
        this.shareAmount = shareAmount;
    }

    public BigDecimal getShareAmount()
    {
        return shareAmount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("flowId", getFlowId())
            .append("shareUserId", getShareUserId())
            .append("shareDiff", getShareDiff())
            .append("shareAmount", getShareAmount())
            .append("createTime", getCreateTime())
            .append("userName", getUserName())
            .toString();
    }
}
