package com.ruoyi.pos.web.service.impl;

import java.util.List;
import java.util.UUID;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TWordOrderTypeInfoMapper;
import com.ruoyi.pos.web.domain.TWordOrderTypeInfo;
import com.ruoyi.pos.web.service.ITWordOrderTypeInfoService;

/**
 * 工单管理类型Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@Service
public class TWordOrderTypeInfoServiceImpl implements ITWordOrderTypeInfoService 
{
    @Autowired
    private TWordOrderTypeInfoMapper tWordOrderTypeInfoMapper;

    /**
     * 查询工单管理类型
     * 
     * @param id 工单管理类型主键
     * @return 工单管理类型
     */
    @Override
    public TWordOrderTypeInfo selectTWordOrderTypeInfoById(String id)
    {
        return tWordOrderTypeInfoMapper.selectTWordOrderTypeInfoById(id);
    }

    /**
     * 查询工单管理类型列表
     * 
     * @param tWordOrderTypeInfo 工单管理类型
     * @return 工单管理类型
     */
    @Override
    public List<TWordOrderTypeInfo> selectTWordOrderTypeInfoList(TWordOrderTypeInfo tWordOrderTypeInfo)
    {
        return tWordOrderTypeInfoMapper.selectTWordOrderTypeInfoList(tWordOrderTypeInfo);
    }

    /**
     * 新增工单管理类型
     * 
     * @param tWordOrderTypeInfo 工单管理类型
     * @return 结果
     */
    @Override
    public int insertTWordOrderTypeInfo(TWordOrderTypeInfo tWordOrderTypeInfo)
    {
        tWordOrderTypeInfo.setCreateTime(DateUtils.getNowDate());
        tWordOrderTypeInfo.setId(IdUtils.simpleUUID());
        return tWordOrderTypeInfoMapper.insertTWordOrderTypeInfo(tWordOrderTypeInfo);
    }

    /**
     * 修改工单管理类型
     * 
     * @param tWordOrderTypeInfo 工单管理类型
     * @return 结果
     */
    @Override
    public int updateTWordOrderTypeInfo(TWordOrderTypeInfo tWordOrderTypeInfo)
    {
        return tWordOrderTypeInfoMapper.updateTWordOrderTypeInfo(tWordOrderTypeInfo);
    }

    /**
     * 批量删除工单管理类型
     * 
     * @param ids 需要删除的工单管理类型主键
     * @return 结果
     */
    @Override
    public int deleteTWordOrderTypeInfoByIds(String[] ids)
    {
        return tWordOrderTypeInfoMapper.deleteTWordOrderTypeInfoByIds(ids);
    }

    /**
     * 删除工单管理类型信息
     * 
     * @param id 工单管理类型主键
     * @return 结果
     */
    @Override
    public int deleteTWordOrderTypeInfoById(String id)
    {
        return tWordOrderTypeInfoMapper.deleteTWordOrderTypeInfoById(id);
    }
}
