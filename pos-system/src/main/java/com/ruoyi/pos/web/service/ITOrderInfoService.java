package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TOrderInfo;

/**
 * 订单Service接口
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public interface ITOrderInfoService 
{
    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    public TOrderInfo selectTOrderInfoById(String id);

    /**
     * 查询订单列表
     * 
     * @param tOrderInfo 订单
     * @return 订单集合
     */
    public List<TOrderInfo> selectTOrderInfoList(TOrderInfo tOrderInfo);

    /**
     * 新增订单
     * 
     * @param tOrderInfo 订单
     * @return 结果
     */
    public int insertTOrderInfo(TOrderInfo tOrderInfo);

    /**
     * 修改订单
     * 
     * @param tOrderInfo 订单
     * @return 结果
     */
    public int updateTOrderInfo(TOrderInfo tOrderInfo);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的订单主键集合
     * @return 结果
     */
    public int deleteTOrderInfoByIds(String[] ids);

    /**
     * 删除订单信息
     * 
     * @param id 订单主键
     * @return 结果
     */
    public int deleteTOrderInfoById(String id);

    /*发货*/
    int ship(TOrderInfo tOrderInfo);

    /*查询订单兑换记录*/
    List<TOrderInfo> exchangeOrderList(TOrderInfo tOrderInfo);
}
