package com.ruoyi.pos.web.mapper;

import java.util.List;
import com.ruoyi.pos.web.domain.TTerminalGoodsSpeInfo;

/**
 * 商品规格Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public interface TTerminalGoodsSpeInfoMapper 
{
    /**
     * 查询商品规格
     * 
     * @param id 商品规格主键
     * @return 商品规格
     */
    public TTerminalGoodsSpeInfo selectTTerminalGoodsSpeInfoById(String id);

    /**
     * 查询商品规格列表
     * 
     * @param tTerminalGoodsSpeInfo 商品规格
     * @return 商品规格集合
     */
    public List<TTerminalGoodsSpeInfo> selectTTerminalGoodsSpeInfoList(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo);

    /**
     * 新增商品规格
     * 
     * @param tTerminalGoodsSpeInfo 商品规格
     * @return 结果
     */
    public int insertTTerminalGoodsSpeInfo(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo);

    /**
     * 修改商品规格
     * 
     * @param tTerminalGoodsSpeInfo 商品规格
     * @return 结果
     */
    public int updateTTerminalGoodsSpeInfo(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo);

    /**
     * 删除商品规格
     * 
     * @param id 商品规格主键
     * @return 结果
     */
    public int deleteTTerminalGoodsSpeInfoById(String id);

    /**
     * 批量删除商品规格
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTerminalGoodsSpeInfoByIds(String[] ids);

    public List<TTerminalGoodsSpeInfo> querySpeInfoList(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo);

}
