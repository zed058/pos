package com.ruoyi.pos.web.service.impl;

import java.util.List;
import java.util.UUID;

import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TAboutUsInfoMapper;
import com.ruoyi.pos.web.domain.TAboutUsInfo;
import com.ruoyi.pos.web.service.ITAboutUsInfoService;

/**
 * 平台设置（关于我们）Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@Service
public class TAboutUsInfoServiceImpl implements ITAboutUsInfoService 
{
    @Autowired
    private TAboutUsInfoMapper tAboutUsInfoMapper;

    /**
     * 查询平台设置（关于我们）
     * 
     * @param id 平台设置（关于我们）主键
     * @return 平台设置（关于我们）
     */
    @Override
    public TAboutUsInfo selectTAboutUsInfoById(String id)
    {
        return tAboutUsInfoMapper.selectTAboutUsInfoById(id);
    }

    /**
     * 查询平台设置（关于我们）列表
     * 
     * @param tAboutUsInfo 平台设置（关于我们）
     * @return 平台设置（关于我们）
     */
    @Override
    public List<TAboutUsInfo> selectTAboutUsInfoList(TAboutUsInfo tAboutUsInfo)
    {
        return tAboutUsInfoMapper.selectTAboutUsInfoList(tAboutUsInfo);
    }

    /**
     * 新增平台设置（关于我们）
     * 
     * @param tAboutUsInfo 平台设置（关于我们）
     * @return 结果
     */
    @Override
    public int insertTAboutUsInfo(TAboutUsInfo tAboutUsInfo)
    {
        tAboutUsInfo.setId(IdUtils.simpleUUID());
        return tAboutUsInfoMapper.insertTAboutUsInfo(tAboutUsInfo);
    }

    /**
     * 修改平台设置（关于我们）
     * 
     * @param tAboutUsInfo 平台设置（关于我们）
     * @return 结果
     */
    @Override
    public int updateTAboutUsInfo(TAboutUsInfo tAboutUsInfo)
    {
        return tAboutUsInfoMapper.updateTAboutUsInfo(tAboutUsInfo);
    }

    /**
     * 批量删除平台设置（关于我们）
     * 
     * @param ids 需要删除的平台设置（关于我们）主键
     * @return 结果
     */
    @Override
    public int deleteTAboutUsInfoByIds(String[] ids)
    {
        return tAboutUsInfoMapper.deleteTAboutUsInfoByIds(ids);
    }

    /**
     * 删除平台设置（关于我们）信息
     * 
     * @param id 平台设置（关于我们）主键
     * @return 结果
     */
    @Override
    public int deleteTAboutUsInfoById(String id)
    {
        return tAboutUsInfoMapper.deleteTAboutUsInfoById(id);
    }
}
