package com.ruoyi.pos.web.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TStatisticInfoMapper;
import com.ruoyi.pos.web.domain.TStatisticInfo;
import com.ruoyi.pos.web.service.ITStatisticInfoService;

/**
 * 统计信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
@Service
public class TStatisticInfoServiceImpl implements ITStatisticInfoService 
{
    @Autowired
    private TStatisticInfoMapper tStatisticInfoMapper;

    /**
     * 查询统计信息
     * 
     * @param id 统计信息主键
     * @return 统计信息
     */
    @Override
    public TStatisticInfo selectTStatisticInfoById(String id)
    {
        return tStatisticInfoMapper.selectTStatisticInfoById(id);
    }

    /**
     * 查询统计信息列表
     * 
     * @param tStatisticInfo 统计信息
     * @return 统计信息
     */
    @Override
    public List<TStatisticInfo> selectTStatisticInfoList(TStatisticInfo tStatisticInfo)
    {
        return tStatisticInfoMapper.selectTStatisticInfoList(tStatisticInfo);
    }

    /**
     * 新增统计信息
     * 
     * @param tStatisticInfo 统计信息
     * @return 结果
     */
    @Override
    public int insertTStatisticInfo(TStatisticInfo tStatisticInfo)
    {
        tStatisticInfo.setCreateTime(DateUtils.getNowDate());
        return tStatisticInfoMapper.insertTStatisticInfo(tStatisticInfo);
    }

    /**
     * 修改统计信息
     * 
     * @param tStatisticInfo 统计信息
     * @return 结果
     */
    @Override
    public int updateTStatisticInfo(TStatisticInfo tStatisticInfo)
    {
        return tStatisticInfoMapper.updateTStatisticInfo(tStatisticInfo);
    }

    /**
     * 批量删除统计信息
     * 
     * @param ids 需要删除的统计信息主键
     * @return 结果
     */
    @Override
    public int deleteTStatisticInfoByIds(String[] ids)
    {
        return tStatisticInfoMapper.deleteTStatisticInfoByIds(ids);
    }

    /**
     * 删除统计信息信息
     * 
     * @param id 统计信息主键
     * @return 结果
     */
    @Override
    public int deleteTStatisticInfoById(String id)
    {
        return tStatisticInfoMapper.deleteTStatisticInfoById(id);
    }
}
