package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 终端激活配置对象 t_terminal_activate_set_info
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public class TTerminalActivateSetInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 品牌id */
    @Excel(name = "品牌id")
    private String brandId;
    private String brandName;

    /** 终端激活有效期（小时制） */
    @Excel(name = "终端激活有效期")
    private String activeTime;

    /** 终端激活金额 */
    @Excel(name = "终端激活金额")
    private BigDecimal activeAmount;

    /** 终端激活奖励积分（奖励上级） */
    @Excel(name = "终端激活奖励积分")
    private BigDecimal awardIntegral;

    /** 终端激活奖励现金（奖励上级） */
    @Excel(name = "终端激活奖励现金")
    private BigDecimal awardCash;
    @Excel(name = "终端激活办公补贴")
    private BigDecimal workSubsidy;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setBrandId(String brandId) 
    {
        this.brandId = brandId;
    }

    public String getBrandId() 
    {
        return brandId;
    }
    public void setActiveTime(String activeTime) 
    {
        this.activeTime = activeTime;
    }

    public String getActiveTime() 
    {
        return activeTime;
    }
    public void setActiveAmount(BigDecimal activeAmount) 
    {
        this.activeAmount = activeAmount;
    }

    public BigDecimal getActiveAmount() 
    {
        return activeAmount;
    }
    public void setAwardIntegral(BigDecimal awardIntegral) 
    {
        this.awardIntegral = awardIntegral;
    }

    public BigDecimal getAwardIntegral() 
    {
        return awardIntegral;
    }
    public void setAwardCash(BigDecimal awardCash) 
    {
        this.awardCash = awardCash;
    }

    public BigDecimal getAwardCash() 
    {
        return awardCash;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public BigDecimal getWorkSubsidy() {
        return workSubsidy;
    }

    public void setWorkSubsidy(BigDecimal workSubsidy) {
        this.workSubsidy = workSubsidy;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("brandId", getBrandId())
            .append("activeTime", getActiveTime())
            .append("activeAmount", getActiveAmount())
            .append("awardIntegral", getAwardIntegral())
            .append("awardCash", getAwardCash())
            .toString();
    }
}
