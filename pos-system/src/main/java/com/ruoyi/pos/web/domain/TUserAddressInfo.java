package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户收货地址对象 t_user_address_info
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public class TUserAddressInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;

    /** 收货人手机号 */
    @Excel(name = "收货人手机号")
    private String phone;

    /** 收货人固定电话 */
    @Excel(name = "收货人固定电话")
    private String fixationPhone;

    /** 地区 */
    @Excel(name = "地区")
    private String district;

    /** 详情地址 */
    @Excel(name = "详情地址")
    private String address;

    /** 是否默认（0：默认，1：不默认，2：删除） */
    @Excel(name = "是否默认", readConverterExp = "0=：默认，1：不默认，2：删除")
    private String isValid;
    private String isValidShow;

    /** 城市code */
    private String districtCode;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setFixationPhone(String fixationPhone) 
    {
        this.fixationPhone = fixationPhone;
    }

    public String getFixationPhone() 
    {
        return fixationPhone;
    }
    public void setDistrict(String district) 
    {
        this.district = district;
    }

    public String getDistrict() 
    {
        return district;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setIsValid(String isValid) 
    {
        this.isValid = isValid;
    }

    public String getIsValid() 
    {
        return isValid;
    }

    public String getIsValidShow() {
        return isValidShow;
    }

    public void setIsValidShow(String isValidShow) {
        this.isValidShow = isValidShow;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("phone", getPhone())
            .append("fixationPhone", getFixationPhone())
            .append("district", getDistrict())
            .append("address", getAddress())
            .append("createTime", getCreateTime())
            .append("isValid", getIsValid())
            .append("districtCode", getDistrictCode())
            .toString();
    }
}
