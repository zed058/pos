package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TOrderSnInfo;

/**
 * 订单终端SN码（字）Service接口
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public interface ITOrderSnInfoService 
{
    /**
     * 查询订单终端SN码（字）
     * 
     * @param id 订单终端SN码（字）主键
     * @return 订单终端SN码（字）
     */
    public TOrderSnInfo selectTOrderSnInfoById(String id);

    /**
     * 查询订单终端SN码（字）列表
     * 
     * @param tOrderSnInfo 订单终端SN码（字）
     * @return 订单终端SN码（字）集合
     */
    public List<TOrderSnInfo> selectTOrderSnInfoList(TOrderSnInfo tOrderSnInfo);

    /**
     * 新增订单终端SN码（字）
     * 
     * @param tOrderSnInfo 订单终端SN码（字）
     * @return 结果
     */
    public int insertTOrderSnInfo(TOrderSnInfo tOrderSnInfo);

    /**
     * 修改订单终端SN码（字）
     * 
     * @param tOrderSnInfo 订单终端SN码（字）
     * @return 结果
     */
    public int updateTOrderSnInfo(TOrderSnInfo tOrderSnInfo);

    /**
     * 批量删除订单终端SN码（字）
     * 
     * @param ids 需要删除的订单终端SN码（字）主键集合
     * @return 结果
     */
    public int deleteTOrderSnInfoByIds(String[] ids);

    /**
     * 删除订单终端SN码（字）信息
     * 
     * @param id 订单终端SN码（字）主键
     * @return 结果
     */
    public int deleteTOrderSnInfoById(String id);

    void insert(String orderId,String[] snCodeArray);

    void delete(TOrderSnInfo snInfo);
}
