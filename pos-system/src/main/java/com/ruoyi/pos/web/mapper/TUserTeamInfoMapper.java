package com.ruoyi.pos.web.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.pos.api.dto.user.DataStatisticsDto;
import com.ruoyi.pos.api.dto.user.UserTeamDto;
import com.ruoyi.pos.api.vo.user.DataStatisticsVo;
import com.ruoyi.pos.api.vo.user.UserTeamListVo;
import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.domain.TUserTeamInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 用户团队关系Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface TUserTeamInfoMapper 
{
    /**
     * 查询用户团队关系
     * 
     * @param id 用户团队关系主键
     * @return 用户团队关系
     */
    public TUserTeamInfo selectTUserTeamInfoById(String id);

    /**
     * 查询用户团队关系列表
     * 
     * @param tUserTeamInfo 用户团队关系
     * @return 用户团队关系集合
     */
    public List<TUserTeamInfo> selectTUserTeamInfoList(TUserTeamInfo tUserTeamInfo);

    /**
     * 新增用户团队关系
     * 
     * @param tUserTeamInfo 用户团队关系
     * @return 结果
     */
    public int insertTUserTeamInfo(TUserTeamInfo tUserTeamInfo);

    /**
     * 修改用户团队关系
     * 
     * @param tUserTeamInfo 用户团队关系
     * @return 结果
     */
    public int updateTUserTeamInfo(TUserTeamInfo tUserTeamInfo);

    /**
     * 删除用户团队关系
     * 
     * @param id 用户团队关系主键
     * @return 结果
     */
    public int deleteTUserTeamInfoById(String id);

    /**
     * 批量删除用户团队关系
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTUserTeamInfoByIds(String[] ids);
    //-----------------------------------------自定义--------------------------------------------
    //根据查询团队列表
    List<UserTeamListVo> selectUserTeamList(UserTeamDto userTeamDto);
    //根据查询团队列表
    List<UserTeamListVo> selectUserTeamListForParent(UserTeamDto userTeamDto);
    //根据上下级和团对类型删除团队关系
    int deleteTUserTeamInfoByUsers(@Param("userId") String userId, @Param("subUserId") String subUserId, @Param("teamType") String teamType);

    public int queryCount(DataStatisticsDto dto);

    public List<DataStatisticsVo> queryChart(DataStatisticsDto tStatisticInfo);

    public List<DataStatisticsVo> queryYear(DataStatisticsDto tStatisticInfo);
}
