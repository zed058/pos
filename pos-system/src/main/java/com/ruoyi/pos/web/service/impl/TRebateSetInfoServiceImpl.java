package com.ruoyi.pos.web.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TRebateSetInfoMapper;
import com.ruoyi.pos.web.domain.TRebateSetInfo;
import com.ruoyi.pos.web.service.ITRebateSetInfoService;

/**
 * 基础配置Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
@Service
public class TRebateSetInfoServiceImpl implements ITRebateSetInfoService 
{
    @Autowired
    private TRebateSetInfoMapper tRebateSetInfoMapper;

    /**
     * 查询基础配置
     * 
     * @param id 基础配置主键
     * @return 基础配置
     */
    @Override
    public TRebateSetInfo selectTRebateSetInfoById(String id)
    {
        return tRebateSetInfoMapper.selectTRebateSetInfoById(id);
    }

    /**
     * 查询基础配置列表
     * 
     * @param tRebateSetInfo 基础配置
     * @return 基础配置
     */
    @Override
    public List<TRebateSetInfo> selectTRebateSetInfoList(TRebateSetInfo tRebateSetInfo)
    {
        return tRebateSetInfoMapper.selectTRebateSetInfoList(tRebateSetInfo);
    }

    /**
     * 新增基础配置
     * 
     * @param tRebateSetInfo 基础配置
     * @return 结果
     */
    @Override
    public int insertTRebateSetInfo(TRebateSetInfo tRebateSetInfo)
    {
        return tRebateSetInfoMapper.insertTRebateSetInfo(tRebateSetInfo);
    }

    /**
     * 修改基础配置
     * 
     * @param tRebateSetInfo 基础配置
     * @return 结果
     */
    @Override
    public int updateTRebateSetInfo(TRebateSetInfo tRebateSetInfo)
    {
        return tRebateSetInfoMapper.updateTRebateSetInfo(tRebateSetInfo);
    }

    /**
     * 批量删除基础配置
     * 
     * @param ids 需要删除的基础配置主键
     * @return 结果
     */
    @Override
    public int deleteTRebateSetInfoByIds(String[] ids)
    {
        return tRebateSetInfoMapper.deleteTRebateSetInfoByIds(ids);
    }

    /**
     * 删除基础配置信息
     * 
     * @param id 基础配置主键
     * @return 结果
     */
    @Override
    public int deleteTRebateSetInfoById(String id)
    {
        return tRebateSetInfoMapper.deleteTRebateSetInfoById(id);
    }
}
