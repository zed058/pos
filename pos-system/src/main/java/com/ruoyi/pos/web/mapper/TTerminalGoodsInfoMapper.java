package com.ruoyi.pos.web.mapper;

import java.util.List;
import com.ruoyi.pos.web.domain.TTerminalGoodsInfo;

/**
 * 商品基础Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public interface TTerminalGoodsInfoMapper 
{
    /**
     * 查询商品基础
     * 
     * @param id 商品基础主键
     * @return 商品基础
     */
    public TTerminalGoodsInfo selectTTerminalGoodsInfoById(String id);

    /**
     * 查询商品基础列表
     * 
     * @param tTerminalGoodsInfo 商品基础
     * @return 商品基础集合
     */
    public List<TTerminalGoodsInfo> selectTTerminalGoodsInfoList(TTerminalGoodsInfo tTerminalGoodsInfo);

    /**
     * 新增商品基础
     * 
     * @param tTerminalGoodsInfo 商品基础
     * @return 结果
     */
    public int insertTTerminalGoodsInfo(TTerminalGoodsInfo tTerminalGoodsInfo);

    /**
     * 修改商品基础
     * 
     * @param tTerminalGoodsInfo 商品基础
     * @return 结果
     */
    public int updateTTerminalGoodsInfo(TTerminalGoodsInfo tTerminalGoodsInfo);

    /**
     * 删除商品基础
     * 
     * @param id 商品基础主键
     * @return 结果
     */
    public int deleteTTerminalGoodsInfoById(String id);

    /**
     * 批量删除商品基础
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTerminalGoodsInfoByIds(String[] ids);


    /**
     * 查询终端||兑换商品基础列表
     *
     * @param tTerminalGoodsInfo 终端||兑换商品基础
     * @return 终端||兑换商品基础集合
     */
    public List<TTerminalGoodsInfo> queryTerminalAndExchange(TTerminalGoodsInfo tTerminalGoodsInfo);

    public TTerminalGoodsInfo selectTTerminalGoodsInfoBySnCode(String snCode);

}
