package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户出入账信息对象 t_user_default_info
 *
 * @author ruoyi
 * @date 2021-11-24
 */
public class TUserDefaultInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 用户id */
    private String userId;

    /** 出入账类型（
     10：余额，
     20：积分，
     30：金币，
     40：交易金序列号，
     50：奖励金序列号
     ） */
    private String transType;

    /** 出入账数量 */
    @Excel(name = "申请金额")
    private BigDecimal transAmount;

    /** 出入账操作类型*/
    private String transOperType;

    /** 金币类型（） */
    private String goldType;

    /** 品牌id(收益那里要区分品牌展示) */
    private String brandId;

    /** 订单id */
    private String orderId;

    /** 更新人 */
    private String updateUserId;

    /** 审核状态(0：待审核，1：审核通过，2：审核不通过) */
    private String checkStatus;
    @Excel(name = "审核状态(0：待审核，1：审核通过，2：审核不通过)")
    private String checkStatusShow;

    /** 审核人userId */
    private String checkStatusUserId;

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date checkStatusTime;

    /** 审核状态描述 */
    private String checkStatusDescribe;

    /** 提现手续费 */
    private BigDecimal serviceChargeAmount;
    @Excel(name = "趣工宝状态")
    private String withdrawStatus; //提现状态（10：提现处理中 ；20：提现成功；30：提现失败）
    private String withdrawStatusShow; //提现状态（10：提现处理中 ；20：提现成功；30：提现失败）
    @Excel(name = "支付时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date withdrawStatusTime; //提现状态时间
    private String withdrawOrderNo; //趣工宝平台单号

    @Excel(name = "用户姓名")
    private String userName; //用户姓名
    @Excel(name = "手机号码")
    private String userPhone; //手机号码
    private String identityMark; //收款人身份证号
    private String bankCardMark; //银行卡号
    private String bankName; //银行名称
    private String subBranch; //支行名称

    //以下为web端查询条件特有字段
    private String[] queryCreateTimeStr; //申请时间查询条件
    private String[] queryCheckTimeStr; //审核时间查询条件
    private String[] queryPayTimeStr; //支付时间查询条件
    private String createTimeStart;
    private String createTimeEnd;
    private String checkTimeStart;
    private String checkTimeEnd;
    private String payTimeStart;
    private String payTimeEnd;

    /*所属用户*/
    private String belongingUser;
    /*订单编号*/
    private String orderNo;
    /*兑换时间*/
    private String exchangeTime;
    /*兑换用户*/
    private String exchangeUser;

    public String getBelongingUser() {
        return belongingUser;
    }

    public void setBelongingUser(String belongingUser) {
        this.belongingUser = belongingUser;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getExchangeTime() {
        return exchangeTime;
    }

    public void setExchangeTime(String exchangeTime) {
        this.exchangeTime = exchangeTime;
    }

    public String getExchangeUser() {
        return exchangeUser;
    }

    public void setExchangeUser(String exchangeUser) {
        this.exchangeUser = exchangeUser;
    }

    public String getCheckStatusShow() {
        return checkStatusShow;
    }

    public void setCheckStatusShow(String checkStatusShow) {
        this.checkStatusShow = checkStatusShow;
    }

    public String[] getQueryCreateTimeStr() {
        return queryCreateTimeStr;
    }

    public void setQueryCreateTimeStr(String[] queryCreateTimeStr) {
        this.queryCreateTimeStr = queryCreateTimeStr;
    }

    public String[] getQueryCheckTimeStr() {
        return queryCheckTimeStr;
    }

    public void setQueryCheckTimeStr(String[] queryCheckTimeStr) {
        this.queryCheckTimeStr = queryCheckTimeStr;
    }

    public String[] getQueryPayTimeStr() {
        return queryPayTimeStr;
    }

    public void setQueryPayTimeStr(String[] queryPayTimeStr) {
        this.queryPayTimeStr = queryPayTimeStr;
    }

    public String getCreateTimeStart() {
        return createTimeStart;
    }

    public void setCreateTimeStart(String createTimeStart) {
        this.createTimeStart = createTimeStart;
    }

    public String getCreateTimeEnd() {
        return createTimeEnd;
    }

    public void setCreateTimeEnd(String createTimeEnd) {
        this.createTimeEnd = createTimeEnd;
    }

    public String getCheckTimeStart() {
        return checkTimeStart;
    }

    public void setCheckTimeStart(String checkTimeStart) {
        this.checkTimeStart = checkTimeStart;
    }

    public String getCheckTimeEnd() {
        return checkTimeEnd;
    }

    public void setCheckTimeEnd(String checkTimeEnd) {
        this.checkTimeEnd = checkTimeEnd;
    }

    public String getPayTimeStart() {
        return payTimeStart;
    }

    public void setPayTimeStart(String payTimeStart) {
        this.payTimeStart = payTimeStart;
    }

    public String getPayTimeEnd() {
        return payTimeEnd;
    }

    public void setPayTimeEnd(String payTimeEnd) {
        this.payTimeEnd = payTimeEnd;
    }

    /** 实际提现金额 */
    @Excel(name = "到账金额")
    private BigDecimal withdrawAmount;

    /** 推荐人userId */
    private String refereesUserId;

    /** 我的下级用户id */
    private String subUserId;

    /** 序列号 */
    private Long serialId;

    /** 序列号 */
    private String snCode;

    /** 序列号 */
    private BigDecimal otherTransAmount;

    public String getWithdrawStatusShow() {
        return withdrawStatusShow;
    }

    public void setWithdrawStatusShow(String withdrawStatusShow) {
        this.withdrawStatusShow = withdrawStatusShow;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getIdentityMark() {
        return identityMark;
    }

    public void setIdentityMark(String identityMark) {
        this.identityMark = identityMark;
    }

    public String getBankCardMark() {
        return bankCardMark;
    }

    public void setBankCardMark(String bankCardMark) {
        this.bankCardMark = bankCardMark;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getSubBranch() {
        return subBranch;
    }

    public void setSubBranch(String subBranch) {
        this.subBranch = subBranch;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId()
    {
        return userId;
    }
    public void setTransType(String transType)
    {
        this.transType = transType;
    }

    public String getTransType()
    {
        return transType;
    }
    public void setTransAmount(BigDecimal transAmount)
    {
        this.transAmount = transAmount;
    }

    public BigDecimal getTransAmount()
    {
        return transAmount;
    }
    public void setTransOperType(String transOperType)
    {
        this.transOperType = transOperType;
    }

    public String getTransOperType()
    {
        return transOperType;
    }
    public void setGoldType(String goldType)
    {
        this.goldType = goldType;
    }

    public String getGoldType()
    {
        return goldType;
    }
    public void setBrandId(String brandId)
    {
        this.brandId = brandId;
    }

    public String getBrandId()
    {
        return brandId;
    }
    public void setOrderId(String orderId)
    {
        this.orderId = orderId;
    }

    public String getOrderId()
    {
        return orderId;
    }
    public void setUpdateUserId(String updateUserId)
    {
        this.updateUserId = updateUserId;
    }

    public String getUpdateUserId()
    {
        return updateUserId;
    }
    public void setCheckStatus(String checkStatus)
    {
        this.checkStatus = checkStatus;
    }

    public String getCheckStatus()
    {
        return checkStatus;
    }
    public void setCheckStatusUserId(String checkStatusUserId)
    {
        this.checkStatusUserId = checkStatusUserId;
    }

    public String getCheckStatusUserId()
    {
        return checkStatusUserId;
    }
    public void setCheckStatusTime(Date checkStatusTime)
    {
        this.checkStatusTime = checkStatusTime;
    }

    public Date getCheckStatusTime()
    {
        return checkStatusTime;
    }
    public void setCheckStatusDescribe(String checkStatusDescribe)
    {
        this.checkStatusDescribe = checkStatusDescribe;
    }

    public String getCheckStatusDescribe()
    {
        return checkStatusDescribe;
    }
    public void setServiceChargeAmount(BigDecimal serviceChargeAmount)
    {
        this.serviceChargeAmount = serviceChargeAmount;
    }

    public BigDecimal getServiceChargeAmount()
    {
        return serviceChargeAmount;
    }

    public String getWithdrawStatus() {
        return withdrawStatus;
    }

    public void setWithdrawStatus(String withdrawStatus) {
        this.withdrawStatus = withdrawStatus;
    }

    public Date getWithdrawStatusTime() {
        return withdrawStatusTime;
    }

    public void setWithdrawStatusTime(Date withdrawStatusTime) {
        this.withdrawStatusTime = withdrawStatusTime;
    }

    public String getWithdrawOrderNo() {
        return withdrawOrderNo;
    }

    public void setWithdrawOrderNo(String withdrawOrderNo) {
        this.withdrawOrderNo = withdrawOrderNo;
    }

    public void setWithdrawAmount(BigDecimal withdrawAmount)
    {
        this.withdrawAmount = withdrawAmount;
    }

    public BigDecimal getWithdrawAmount()
    {
        return withdrawAmount;
    }
    public void setRefereesUserId(String refereesUserId)
    {
        this.refereesUserId = refereesUserId;
    }

    public String getRefereesUserId()
    {
        return refereesUserId;
    }

    public String getSubUserId() {
        return subUserId;
    }

    public void setSubUserId(String subUserId) {
        this.subUserId = subUserId;
    }

    public Long getSerialId() {
        return serialId;
    }

    public void setSerialId(Long serialId) {
        this.serialId = serialId;
    }

    public String getSnCode() {
        return snCode;
    }

    public void setSnCode(String snCode) {
        this.snCode = snCode;
    }

    public BigDecimal getOtherTransAmount() {
        return otherTransAmount;
    }

    public void setOtherTransAmount(BigDecimal otherTransAmount) {
        this.otherTransAmount = otherTransAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("transType", getTransType())
                .append("transAmount", getTransAmount())
                .append("transOperType", getTransOperType())
                .append("goldType", getGoldType())
                .append("brandId", getBrandId())
                .append("orderId", getOrderId())
                .append("createTime", getCreateTime())
                .append("updateUserId", getUpdateUserId())
                .append("updateTime", getUpdateTime())
                .append("checkStatus", getCheckStatus())
                .append("checkStatusUserId", getCheckStatusUserId())
                .append("checkStatusTime", getCheckStatusTime())
                .append("checkStatusDescribe", getCheckStatusDescribe())
                .append("serviceChargeAmount", getServiceChargeAmount())
                .append("withdrawAmount", getWithdrawAmount())
                .append("refereesUserId", getRefereesUserId())
                .append("subUserId", getSubUserId())
                .append("serialId", getSerialId())
                .append("snCode", getSnCode())
                .append("otherTransAmount", getOtherTransAmount())
                .toString();
    }

    public static final class Builder {
        private Date createTime;
        private String id;
        private String userId;
        private String transType;
        private BigDecimal transAmount;
        private String transOperType;
        private String goldType;
        private String brandId;
        private String orderId;
        private String updateUserId;
        private String checkStatus;
        private String checkStatusUserId;
        private Date checkStatusTime;
        private String checkStatusDescribe;
        private BigDecimal serviceChargeAmount;
        private BigDecimal withdrawAmount;
        private String refereesUserId;
        private String subUserId;
        private Long serialId;
        private String snCode;
        private BigDecimal otherTransAmount;

        private Builder() {
        }

        public static Builder getInstance() {
            return new Builder();
        }

        public Builder withCreateTime(Date createTime) {
            this.createTime = createTime;
            return this;
        }

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder withTransType(String transType) {
            this.transType = transType;
            return this;
        }

        public Builder withTransAmount(BigDecimal transAmount) {
            this.transAmount = transAmount;
            return this;
        }

        public Builder withTransOperType(String transOperType) {
            this.transOperType = transOperType;
            return this;
        }

        public Builder withGoldType(String goldType) {
            this.goldType = goldType;
            return this;
        }

        public Builder withBrandId(String brandId) {
            this.brandId = brandId;
            return this;
        }

        public Builder withOrderId(String orderId) {
            this.orderId = orderId;
            return this;
        }

        public Builder withUpdateUserId(String updateUserId) {
            this.updateUserId = updateUserId;
            return this;
        }

        public Builder withCheckStatus(String checkStatus) {
            this.checkStatus = checkStatus;
            return this;
        }

        public Builder withCheckStatusUserId(String checkStatusUserId) {
            this.checkStatusUserId = checkStatusUserId;
            return this;
        }

        public Builder withCheckStatusTime(Date checkStatusTime) {
            this.checkStatusTime = checkStatusTime;
            return this;
        }

        public Builder withCheckStatusDescribe(String checkStatusDescribe) {
            this.checkStatusDescribe = checkStatusDescribe;
            return this;
        }

        public Builder withServiceChargeAmount(BigDecimal serviceChargeAmount) {
            this.serviceChargeAmount = serviceChargeAmount;
            return this;
        }

        public Builder withWithdrawAmount(BigDecimal withdrawAmount) {
            this.withdrawAmount = withdrawAmount;
            return this;
        }

        public Builder withRefereesUserId(String refereesUserId) {
            this.refereesUserId = refereesUserId;
            return this;
        }

        public Builder withSubUserId(String subUserId) {
            this.subUserId = subUserId;
            return this;
        }

        public Builder withSerialId(Long serialId) {
            this.serialId = serialId;
            return this;
        }

        public Builder withSnCode(String snCode) {
            this.snCode = snCode;
            return this;
        }

        public Builder withOtherTransAmount(BigDecimal otherTransAmount) {
            this.otherTransAmount = otherTransAmount;
            return this;
        }

        public TUserDefaultInfo build() {
            TUserDefaultInfo tUserDefaultInfo = new TUserDefaultInfo();
            tUserDefaultInfo.setCreateTime(createTime);
            tUserDefaultInfo.setId(id);
            tUserDefaultInfo.setUserId(userId);
            tUserDefaultInfo.setTransType(transType);
            tUserDefaultInfo.setTransAmount(transAmount);
            tUserDefaultInfo.setTransOperType(transOperType);
            tUserDefaultInfo.setGoldType(goldType);
            tUserDefaultInfo.setBrandId(brandId);
            tUserDefaultInfo.setOrderId(orderId);
            tUserDefaultInfo.setUpdateUserId(updateUserId);
            tUserDefaultInfo.setCheckStatus(checkStatus);
            tUserDefaultInfo.setCheckStatusUserId(checkStatusUserId);
            tUserDefaultInfo.setCheckStatusTime(checkStatusTime);
            tUserDefaultInfo.setCheckStatusDescribe(checkStatusDescribe);
            tUserDefaultInfo.setServiceChargeAmount(serviceChargeAmount);
            tUserDefaultInfo.setWithdrawAmount(withdrawAmount);
            tUserDefaultInfo.setRefereesUserId(refereesUserId);
            tUserDefaultInfo.setSubUserId(subUserId);
            tUserDefaultInfo.setSerialId(serialId);
            tUserDefaultInfo.setSnCode(snCode);
            tUserDefaultInfo.setOtherTransAmount(otherTransAmount);
            return tUserDefaultInfo;
        }
    }
}