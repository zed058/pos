package com.ruoyi.pos.web.service.impl;

import java.util.List;

import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TUserAddressInfoMapper;
import com.ruoyi.pos.web.domain.TUserAddressInfo;
import com.ruoyi.pos.web.service.ITUserAddressInfoService;

/**
 * 用户收货地址Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@Service
public class TUserAddressInfoServiceImpl implements ITUserAddressInfoService 
{
    @Autowired
    private TUserAddressInfoMapper tUserAddressInfoMapper;

    /**
     * 查询用户收货地址
     * 
     * @param id 用户收货地址主键
     * @return 用户收货地址
     */
    @Override
    public TUserAddressInfo selectTUserAddressInfoById(String id)
    {
        return tUserAddressInfoMapper.selectTUserAddressInfoById(id);
    }

    /**
     * 查询用户收货地址列表
     * 
     * @param tUserAddressInfo 用户收货地址
     * @return 用户收货地址
     */
    @Override
    public List<TUserAddressInfo> selectTUserAddressInfoList(TUserAddressInfo tUserAddressInfo)
    {
        List<TUserAddressInfo> list = tUserAddressInfoMapper.selectTUserAddressInfoList(tUserAddressInfo);
        list.stream().forEach(bean ->{
            if (YesOrNoEnums.YES.getCode().equals(bean.getIsValid())) {
                bean.setIsValidShow(YesOrNoEnums.NO.getValue());
            } else {
                bean.setIsValidShow(YesOrNoEnums.YES.getValue());
            }
        });
        return list;
    }

    /**
     * 新增用户收货地址
     * 
     * @param tUserAddressInfo 用户收货地址
     * @return 结果
     */
    @Override
    public int insertTUserAddressInfo(TUserAddressInfo tUserAddressInfo)
    {
        tUserAddressInfo.setCreateTime(DateUtils.getNowDate());
        return tUserAddressInfoMapper.insertTUserAddressInfo(tUserAddressInfo);
    }

    /**
     * 修改用户收货地址
     * 
     * @param tUserAddressInfo 用户收货地址
     * @return 结果
     */
    @Override
    public int updateTUserAddressInfo(TUserAddressInfo tUserAddressInfo)
    {
        return tUserAddressInfoMapper.updateTUserAddressInfo(tUserAddressInfo);
    }

    /**
     * 批量删除用户收货地址
     * 
     * @param ids 需要删除的用户收货地址主键
     * @return 结果
     */
    @Override
    public int deleteTUserAddressInfoByIds(String[] ids)
    {
        return tUserAddressInfoMapper.deleteTUserAddressInfoByIds(ids);
    }

    /**
     * 删除用户收货地址信息
     * 
     * @param id 用户收货地址主键
     * @return 结果
     */
    @Override
    public int deleteTUserAddressInfoById(String id)
    {
        return tUserAddressInfoMapper.deleteTUserAddressInfoById(id);
    }
}
