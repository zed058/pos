package com.ruoyi.pos.web.mapper;

import java.util.Date;
import java.util.List;
import com.ruoyi.pos.web.domain.TTerminalTransFlowInfo;

/**
 * 终端交易流水Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public interface TTerminalTransFlowInfoMapper 
{
    /**
     * 查询终端交易流水
     * 
     * @param id 终端交易流水主键
     * @return 终端交易流水
     */
    public TTerminalTransFlowInfo selectTTerminalTransFlowInfoById(String id);

    /**
     * 查询终端交易流水列表
     * 
     * @param tTerminalTransFlowInfo 终端交易流水
     * @return 终端交易流水集合
     */
    public List<TTerminalTransFlowInfo> selectTTerminalTransFlowInfoList(TTerminalTransFlowInfo tTerminalTransFlowInfo);

    /**
     * 新增终端交易流水
     * 
     * @param tTerminalTransFlowInfo 终端交易流水
     * @return 结果
     */
    public int insertTTerminalTransFlowInfo(TTerminalTransFlowInfo tTerminalTransFlowInfo);

    /**
     * 修改终端交易流水
     * 
     * @param tTerminalTransFlowInfo 终端交易流水
     * @return 结果
     */
    public int updateTTerminalTransFlowInfo(TTerminalTransFlowInfo tTerminalTransFlowInfo);

    /**
     * 删除终端交易流水
     * 
     * @param id 终端交易流水主键
     * @return 结果
     */
    public int deleteTTerminalTransFlowInfoById(String id);

    /**
     * 批量删除终端交易流水
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTerminalTransFlowInfoByIds(String[] ids);
    //web端列表查询
    public List<TTerminalTransFlowInfo> selectTTerminalTransFlowInfoListForWeb(TTerminalTransFlowInfo tTerminalTransFlowInfo);
    //查询最早的一笔交易成功的数据时间
    Date selectTTerminalTransFlowInfoForMinTime(String snCode);
}
