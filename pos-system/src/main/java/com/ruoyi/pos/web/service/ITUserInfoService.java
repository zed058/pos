package com.ruoyi.pos.web.service;

import java.util.List;

import com.ruoyi.pos.web.domain.TGradeInfo;
import com.ruoyi.pos.web.domain.TUserDefaultInfo;
import com.ruoyi.pos.web.domain.TUserInfo;

/**
 * 用户基础数据Service接口
 * 
 * @author ruoyi
 * @date 2021-11-10
 */
public interface ITUserInfoService 
{
    /**
     * 查询用户基础数据
     * 
     * @param userId 用户基础数据主键
     * @return 用户基础数据
     */
    public TUserInfo selectTUserInfoByUserId(String userId);

    /**
     * 查询用户基础数据列表
     * 
     * @param tUserInfo 用户基础数据
     * @return 用户基础数据集合
     */
    public List<TUserInfo> selectTUserInfoList(TUserInfo tUserInfo);
    public List<TUserInfo> selectTUserInfoListForTrans(TUserInfo tUserInfo);

    /**
     * 新增用户基础数据
     * 
     * @param tUserInfo 用户基础数据
     * @return 结果
     */
    public int insertTUserInfo(TUserInfo tUserInfo);

    /**
     * 修改用户基础数据
     * 
     * @param tUserInfo 用户基础数据
     * @return 结果
     */
    public int updateTUserInfo(TUserInfo tUserInfo) throws Exception;

    /**
     * 批量删除用户基础数据
     * 
     * @param userIds 需要删除的用户基础数据主键集合
     * @return 结果
     */
    public int deleteTUserInfoByUserIds(String[] userIds);

    /**
     * 删除用户基础数据信息
     * 
     * @param userId 用户基础数据主键
     * @return 结果
     */
    public int deleteTUserInfoByUserId(String userId);

    /**
     * 修改用户状态
     *
     * @param job 调度信息
     * @return 结果
     */
    int changeUserStatus(TUserInfo tUserInfo) throws Exception;
    //变更用户团队
    int changeUserTeam(TUserInfo tUserInfo) throws Exception;
    List<TGradeInfo> queryGradeInfoList() throws Exception;
    //查询实名认证审核列表
    List<TUserInfo> selectTUserInfoCheckList(TUserInfo tUserInfo);
    //实名认证审核通过
    int checkPass(TUserInfo userInfo) throws Exception;
    //实名认证审核不通过
    int checkRefuse(TUserInfo userInfo);

    List<TUserInfo> merchantList(TUserInfo userInfo);
    //变更趣工宝银行卡
    boolean updateBankCard(TUserInfo userInfo) throws Exception;
}
