package com.ruoyi.pos.web.service.impl;

import java.util.List;

import com.ruoyi.common.enums.GoodsStatusEnum;
import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.pos.web.domain.TTerminalGoodsInfo;
import com.ruoyi.pos.web.mapper.TTerminalGoodsInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TTerminalGoodsSpeInfoMapper;
import com.ruoyi.pos.web.domain.TTerminalGoodsSpeInfo;
import com.ruoyi.pos.web.service.ITTerminalGoodsSpeInfoService;

/**
 * 商品规格Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@Service
public class TTerminalGoodsSpeInfoServiceImpl implements ITTerminalGoodsSpeInfoService 
{
    @Autowired
    private TTerminalGoodsSpeInfoMapper tTerminalGoodsSpeInfoMapper;
    @Autowired
    private TTerminalGoodsInfoMapper terminalGoodsInfoMapper;

    /**
     * 查询商品规格
     * 
     * @param id 商品规格主键
     * @return 商品规格
     */
    @Override
    public TTerminalGoodsSpeInfo selectTTerminalGoodsSpeInfoById(String id)
    {
        return tTerminalGoodsSpeInfoMapper.selectTTerminalGoodsSpeInfoById(id);
    }

    /**
     * 查询商品规格列表
     * 
     * @param tTerminalGoodsSpeInfo 商品规格
     * @return 商品规格
     */
    @Override
    public List<TTerminalGoodsSpeInfo> selectTTerminalGoodsSpeInfoList(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo)
    {
        return tTerminalGoodsSpeInfoMapper.selectTTerminalGoodsSpeInfoList(tTerminalGoodsSpeInfo);
    }



    /**
     * 新增商品规格
     * 
     * @param tTerminalGoodsSpeInfo 商品规格
     * @return 结果
     */
    @Override
    public int insertTTerminalGoodsSpeInfo(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo)
    {
        tTerminalGoodsSpeInfo.setId(IdUtils.simpleUUID());
        tTerminalGoodsSpeInfo.setCreateTime(DateUtils.getNowDate());
        return tTerminalGoodsSpeInfoMapper.insertTTerminalGoodsSpeInfo(tTerminalGoodsSpeInfo);
    }

    /**
     * 修改商品规格
     * 
     * @param tTerminalGoodsSpeInfo 商品规格
     * @return 结果
     */
    @Override
    public int updateTTerminalGoodsSpeInfo(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo)
    {
        return tTerminalGoodsSpeInfoMapper.updateTTerminalGoodsSpeInfo(tTerminalGoodsSpeInfo);
    }

    /**
     * 批量删除商品规格
     * 
     * @param ids 需要删除的商品规格主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalGoodsSpeInfoByIds(String[] ids)
    {
        return tTerminalGoodsSpeInfoMapper.deleteTTerminalGoodsSpeInfoByIds(ids);
    }

    /**
     * 删除商品规格信息
     * 
     * @param id 商品规格主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalGoodsSpeInfoById(String id)
    {
        return tTerminalGoodsSpeInfoMapper.deleteTTerminalGoodsSpeInfoById(id);
    }


    @Override
    public List<TTerminalGoodsSpeInfo> querySpeInfoList(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo)
    {
        return tTerminalGoodsSpeInfoMapper.querySpeInfoList(tTerminalGoodsSpeInfo);
    }

    /*查询商品名称*/
    @Override
    public List<TTerminalGoodsInfo> queryTerminalGoods()
    {
        TTerminalGoodsInfo info = new TTerminalGoodsInfo();
        info.setGoodsType(YesOrNoEnums.YES.getCode());//终端商品
        info.setGoodsStatus(GoodsStatusEnum.TERMINALPURCHASE.getCode());//余额
        return terminalGoodsInfoMapper.selectTTerminalGoodsInfoList(info);
    }
}
