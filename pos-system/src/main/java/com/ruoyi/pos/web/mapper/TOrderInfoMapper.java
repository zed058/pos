package com.ruoyi.pos.web.mapper;

import java.util.List;

import com.ruoyi.pos.api.dto.user.OrderDto;
import com.ruoyi.pos.api.vo.user.ExchangeOrderVo;
import com.ruoyi.pos.api.vo.user.OrderListVo;
import com.ruoyi.pos.web.domain.TOrderInfo;
import com.ruoyi.pos.web.weVo.HomePageVo;
import org.apache.ibatis.annotations.Param;

/**
 * 订单Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public interface TOrderInfoMapper 
{
    /**
     * 查询订单
     * 
     * @param id 订单主键
     * @return 订单
     */
    public TOrderInfo selectTOrderInfoById(String id);

    /**
     * 查询订单列表
     * 
     * @param tOrderInfo 订单
     * @return 订单集合
     */
    public List<TOrderInfo> selectTOrderInfoList(TOrderInfo tOrderInfo);

    /**
     * 新增订单
     * 
     * @param tOrderInfo 订单
     * @return 结果
     */
    public int insertTOrderInfo(TOrderInfo tOrderInfo);

    /**
     * 修改订单
     * 
     * @param tOrderInfo 订单
     * @return 结果
     */
    public int updateTOrderInfo(TOrderInfo tOrderInfo);

    /**
     * 删除订单
     * 
     * @param id 订单主键
     * @return 结果
     */
    public int deleteTOrderInfoById(String id);

    /**
     * 批量删除订单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTOrderInfoByIds(String[] ids);

//----------------------------------------------------------------------------------------------------------
    //查询我的订单列表
    List<OrderListVo> selectTOrderInfoListForMy(OrderDto orderDto);
    //查询需要自动取消的订单
    public List<TOrderInfo> queryCloseTimeoutOrders(@Param("waitCancelTime") String waitCancelTime, @Param("waitConfirmTime") String waitConfirmTime);

    public List<ExchangeOrderVo> queryExchangeRecordList(OrderDto orderDto);


    List<HomePageVo> queryHomePageCount(@Param("orderType")String orderType, @Param("orderStatus") List<String> orderStatus);

    public List<TOrderInfo> exchangeOrderList(TOrderInfo tOrderInfo);

    public List<TOrderInfo> querySum(TOrderInfo tOrderInfo);

}
