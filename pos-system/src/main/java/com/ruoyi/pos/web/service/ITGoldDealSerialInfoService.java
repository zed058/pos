package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TGoldDealSerialInfo;

/**
 * (金币序列号Service接口
 * 
 * @author ruoyi
 * @date 2021-11-20
 */
public interface ITGoldDealSerialInfoService 
{
    /**
     * 查询(金币序列号
     * 
     * @param id (金币序列号主键
     * @return (金币序列号
     */
    public TGoldDealSerialInfo selectTGoldDealSerialInfoById(Long id);

    /**
     * 查询(金币序列号列表
     * 
     * @param tGoldDealSerialInfo (金币序列号
     * @return (金币序列号集合
     */
    public List<TGoldDealSerialInfo> selectTGoldDealSerialInfoList(TGoldDealSerialInfo tGoldDealSerialInfo);

    /**
     * 新增(金币序列号
     * 
     * @param tGoldDealSerialInfo (金币序列号
     * @return 结果
     */
    public int insertTGoldDealSerialInfo(TGoldDealSerialInfo tGoldDealSerialInfo);

    /**
     * 修改(金币序列号
     * 
     * @param tGoldDealSerialInfo (金币序列号
     * @return 结果
     */
    public int updateTGoldDealSerialInfo(TGoldDealSerialInfo tGoldDealSerialInfo);

    /**
     * 批量删除(金币序列号
     * 
     * @param ids 需要删除的(金币序列号主键集合
     * @return 结果
     */
    public int deleteTGoldDealSerialInfoByIds(Long[] ids);

    /**
     * 删除(金币序列号信息
     * 
     * @param id (金币序列号主键
     * @return 结果
     */
    public int deleteTGoldDealSerialInfoById(Long id);
}
