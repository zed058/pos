package com.ruoyi.pos.web.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TSelfPickupPointInfoMapper;
import com.ruoyi.pos.web.domain.TSelfPickupPointInfo;
import com.ruoyi.pos.web.service.ITSelfPickupPointInfoService;

/**
 * 商品自提点Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@Service
public class TSelfPickupPointInfoServiceImpl implements ITSelfPickupPointInfoService 
{
    @Autowired
    private TSelfPickupPointInfoMapper tSelfPickupPointInfoMapper;

    /**
     * 查询商品自提点
     * 
     * @param id 商品自提点主键
     * @return 商品自提点
     */
    @Override
    public TSelfPickupPointInfo selectTSelfPickupPointInfoById(String id)
    {
        return tSelfPickupPointInfoMapper.selectTSelfPickupPointInfoById(id);
    }

    /**
     * 查询商品自提点列表
     * 
     * @param tSelfPickupPointInfo 商品自提点
     * @return 商品自提点
     */
    @Override
    public List<TSelfPickupPointInfo> selectTSelfPickupPointInfoList(TSelfPickupPointInfo tSelfPickupPointInfo)
    {
        return tSelfPickupPointInfoMapper.selectTSelfPickupPointInfoList(tSelfPickupPointInfo);
    }

    /**
     * 新增商品自提点
     * 
     * @param tSelfPickupPointInfo 商品自提点
     * @return 结果
     */
    @Override
    public int insertTSelfPickupPointInfo(TSelfPickupPointInfo tSelfPickupPointInfo)
    {
        tSelfPickupPointInfo.setId(IdUtils.simpleUUID());
        tSelfPickupPointInfo.setCreateTime(DateUtils.getNowDate());
        return tSelfPickupPointInfoMapper.insertTSelfPickupPointInfo(tSelfPickupPointInfo);
    }

    /**
     * 修改商品自提点
     * 
     * @param tSelfPickupPointInfo 商品自提点
     * @return 结果
     */
    @Override
    public int updateTSelfPickupPointInfo(TSelfPickupPointInfo tSelfPickupPointInfo)
    {
        return tSelfPickupPointInfoMapper.updateTSelfPickupPointInfo(tSelfPickupPointInfo);
    }

    /**
     * 批量删除商品自提点
     * 
     * @param ids 需要删除的商品自提点主键
     * @return 结果
     */
    @Override
    public int deleteTSelfPickupPointInfoByIds(String[] ids)
    {
        return tSelfPickupPointInfoMapper.deleteTSelfPickupPointInfoByIds(ids);
    }

    /**
     * 删除商品自提点信息
     * 
     * @param id 商品自提点主键
     * @return 结果
     */
    @Override
    public int deleteTSelfPickupPointInfoById(String id)
    {
        return tSelfPickupPointInfoMapper.deleteTSelfPickupPointInfoById(id);
    }
}
