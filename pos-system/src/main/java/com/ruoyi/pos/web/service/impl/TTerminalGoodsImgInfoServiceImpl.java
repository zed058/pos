package com.ruoyi.pos.web.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TTerminalGoodsImgInfoMapper;
import com.ruoyi.pos.web.domain.TTerminalGoodsImgInfo;
import com.ruoyi.pos.web.service.ITTerminalGoodsImgInfoService;

/**
 * 商品轮播图，产品说明Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@Service
public class TTerminalGoodsImgInfoServiceImpl implements ITTerminalGoodsImgInfoService 
{
    @Autowired
    private TTerminalGoodsImgInfoMapper tTerminalGoodsImgInfoMapper;

    /**
     * 查询商品轮播图，产品说明
     * 
     * @param id 商品轮播图，产品说明主键
     * @return 商品轮播图，产品说明
     */
    @Override
    public TTerminalGoodsImgInfo selectTTerminalGoodsImgInfoById(String id)
    {
        return tTerminalGoodsImgInfoMapper.selectTTerminalGoodsImgInfoById(id);
    }

    /**
     * 查询商品轮播图，产品说明列表
     * 
     * @param tTerminalGoodsImgInfo 商品轮播图，产品说明
     * @return 商品轮播图，产品说明
     */
    @Override
    public List<TTerminalGoodsImgInfo> selectTTerminalGoodsImgInfoList(TTerminalGoodsImgInfo tTerminalGoodsImgInfo)
    {
        return tTerminalGoodsImgInfoMapper.selectTTerminalGoodsImgInfoList(tTerminalGoodsImgInfo);
    }

    /**
     * 新增商品轮播图，产品说明
     * 
     * @param tTerminalGoodsImgInfo 商品轮播图，产品说明
     * @return 结果
     */
    @Override
    public int insertTTerminalGoodsImgInfo(TTerminalGoodsImgInfo tTerminalGoodsImgInfo)
    {
        tTerminalGoodsImgInfo.setCreateTime(DateUtils.getNowDate());
        return tTerminalGoodsImgInfoMapper.insertTTerminalGoodsImgInfo(tTerminalGoodsImgInfo);
    }

    /**
     * 修改商品轮播图，产品说明
     * 
     * @param tTerminalGoodsImgInfo 商品轮播图，产品说明
     * @return 结果
     */
    @Override
    public int updateTTerminalGoodsImgInfo(TTerminalGoodsImgInfo tTerminalGoodsImgInfo)
    {
        return tTerminalGoodsImgInfoMapper.updateTTerminalGoodsImgInfo(tTerminalGoodsImgInfo);
    }

    /**
     * 批量删除商品轮播图，产品说明
     * 
     * @param ids 需要删除的商品轮播图，产品说明主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalGoodsImgInfoByIds(String[] ids)
    {
        return tTerminalGoodsImgInfoMapper.deleteTTerminalGoodsImgInfoByIds(ids);
    }

    /**
     * 删除商品轮播图，产品说明信息
     * 
     * @param id 商品轮播图，产品说明主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalGoodsImgInfoById(String id)
    {
        return tTerminalGoodsImgInfoMapper.deleteTTerminalGoodsImgInfoById(id);
    }
}
