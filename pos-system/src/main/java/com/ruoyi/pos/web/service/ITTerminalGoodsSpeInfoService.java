package com.ruoyi.pos.web.service;

import java.util.List;

import com.ruoyi.pos.web.domain.TTerminalGoodsInfo;
import com.ruoyi.pos.web.domain.TTerminalGoodsSpeInfo;

/**
 * 商品规格Service接口
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public interface ITTerminalGoodsSpeInfoService 
{
    /**
     * 查询商品规格
     * 
     * @param id 商品规格主键
     * @return 商品规格
     */
    public TTerminalGoodsSpeInfo selectTTerminalGoodsSpeInfoById(String id);

    /**
     * 查询商品规格列表
     * 
     * @param tTerminalGoodsSpeInfo 商品规格
     * @return 商品规格集合
     */
    public List<TTerminalGoodsSpeInfo> selectTTerminalGoodsSpeInfoList(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo);

    /**
     * 新增商品规格
     * 
     * @param tTerminalGoodsSpeInfo 商品规格
     * @return 结果
     */
    public int insertTTerminalGoodsSpeInfo(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo);

    /**
     * 修改商品规格
     * 
     * @param tTerminalGoodsSpeInfo 商品规格
     * @return 结果
     */
    public int updateTTerminalGoodsSpeInfo(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo);

    /**
     * 批量删除商品规格
     * 
     * @param ids 需要删除的商品规格主键集合
     * @return 结果
     */
    public int deleteTTerminalGoodsSpeInfoByIds(String[] ids);

    /**
     * 删除商品规格信息
     * 
     * @param id 商品规格主键
     * @return 结果
     */
    public int deleteTTerminalGoodsSpeInfoById(String id);


    List<TTerminalGoodsSpeInfo> querySpeInfoList(TTerminalGoodsSpeInfo tTerminalGoodsSpeInfo);

    /*查询商品名称*/
    List<TTerminalGoodsInfo> queryTerminalGoods();
}
