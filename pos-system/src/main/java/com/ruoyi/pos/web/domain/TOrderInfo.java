package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单对象 t_order_info
 *
 * @author ruoyi
 * @date 2021-11-17
 */
public class TOrderInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderNo;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /*用户名称*/
    @Excel(name = "用户名称")
    private String userName;

    /** 品牌id */
    @Excel(name = "品牌id")
    private String brandId;

    /** 品牌名称 */
    @Excel(name = "品牌名称")
    private String brand;

    /** 商品表id */
    @Excel(name = "商品表id")
    private String goodsId;

    /** 商品名称 */
    @Excel(name = "商品表id")
    private String goodsName;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private BigDecimal goodsNum;

    /** 规格id */
    @Excel(name = "规格id")
    private String sepId;

    /** 规格名称 */
    @Excel(name = "规格名称")
    private String specification;

    /** 订单类型（10：现金，20：金币，21：积分，22：交易，23：奖励金） */
    @Excel(name = "订单类型", readConverterExp = "1=0：现金，20：金币，21：积分，22：交易，23：奖励金")
    private String orderType;

    /** 订单状态（10：待支付（待兑换），20：待发货，30：已发货（已兑换），40：已完成，50：已取消/已关闭） */
    @Excel(name = "订单状态", readConverterExp = "1=0：待支付（待兑换")
    private String orderStatus;

    /** 配送方式（0：自提，1：物流） */
    @Excel(name = "配送方式", readConverterExp = "0=：自提，1：物流")
    private String deliveryType;

    /** 配送费 */
    @Excel(name = "配送费")
    private BigDecimal deliveryAmount;

    /** 自提表id（用户收货地址表id） */
    @Excel(name = "自提表id", readConverterExp = "用=户收货地址表id")
    private String selfId;

    /** 自提名称 */
    @Excel(name = "自提名称")
    private String shopName;

    /** 物流地点/自提地点 */
    @Excel(name = "物流地点/自提地点")
    private String address;

    /** 提货人/收货人姓名 */
    @Excel(name = "提货人/收货人姓名")
    private String contactPerson;

    /** 提货人/收货人电话 */
    @Excel(name = "提货人/收货人电话")
    private String contactPhone;

    /** 订单发货时间（订单取消时间） */
    @Excel(name = "订单发货时间", readConverterExp = "订=单取消时间")
    private Date deliverGoodsTime;

    /** 支付方式(0:微信支付，1：微信支付) */
    @Excel(name = "支付方式(0:微信支付，1：微信支付)")
    private String payType;

    /** 支付金额 */
    @Excel(name = "支付金额")
    private BigDecimal payAmount;

    /** 兑换金额 */
    @Excel(name = "兑换金额")
    private BigDecimal exchangeAmount;

    /** 微信预支付时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Excel(name = "微信预支付时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date prepayTime;

    /** 微信预支付返回信息 */
    @Excel(name = "微信预支付返回信息")
    private String prepayInfo;

    /** 支付时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Excel(name = "支付时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date payTime;

    /** 快递公司 */
    @Excel(name = "快递公司")
    private String deliveryFirm;

    /*快递公司名称*/
    @Excel(name = "快递公司名称")
    private String shipperName;

    /** 快递单号 */
    @Excel(name = "快递单号")
    private String oddNo;

    @Excel(name = "sn码列表")
    private String snCodes;

    @Excel(name = "sn码列表")
    private List<String> snCodeList;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setOrderNo(String orderNo)
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo()
    {
        return orderNo;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId()
    {
        return userId;
    }
    public void setBrandId(String brandId)
    {
        this.brandId = brandId;
    }

    public String getBrandId()
    {
        return brandId;
    }
    public void setGoodsId(String goodsId)
    {
        this.goodsId = goodsId;
    }

    public String getGoodsId()
    {
        return goodsId;
    }
    public void setGoodsNum(BigDecimal goodsNum)
    {
        this.goodsNum = goodsNum;
    }

    public BigDecimal getGoodsNum()
    {
        return goodsNum;
    }
    public void setSepId(String sepId)
    {
        this.sepId = sepId;
    }

    public String getSepId()
    {
        return sepId;
    }
    public void setOrderType(String orderType)
    {
        this.orderType = orderType;
    }

    public String getOrderType()
    {
        return orderType;
    }
    public void setOrderStatus(String orderStatus)
    {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatus()
    {
        return orderStatus;
    }
    public void setDeliveryType(String deliveryType)
    {
        this.deliveryType = deliveryType;
    }

    public String getDeliveryType()
    {
        return deliveryType;
    }
    public void setDeliveryAmount(BigDecimal deliveryAmount)
    {
        this.deliveryAmount = deliveryAmount;
    }

    public BigDecimal getDeliveryAmount()
    {
        return deliveryAmount;
    }
    public void setSelfId(String selfId)
    {
        this.selfId = selfId;
    }

    public String getSelfId()
    {
        return selfId;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getAddress()
    {
        return address;
    }
    public void setContactPerson(String contactPerson)
    {
        this.contactPerson = contactPerson;
    }

    public String getContactPerson()
    {
        return contactPerson;
    }
    public void setContactPhone(String contactPhone)
    {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone()
    {
        return contactPhone;
    }
    public void setDeliverGoodsTime(Date deliverGoodsTime)
    {
        this.deliverGoodsTime = deliverGoodsTime;
    }

    public Date getDeliverGoodsTime()
    {
        return deliverGoodsTime;
    }
    public void setPayType(String payType)
    {
        this.payType = payType;
    }

    public String getPayType()
    {
        return payType;
    }
    public void setPayAmount(BigDecimal payAmount)
    {
        this.payAmount = payAmount;
    }

    public BigDecimal getPayAmount()
    {
        return payAmount;
    }
    public void setExchangeAmount(BigDecimal exchangeAmount)
    {
        this.exchangeAmount = exchangeAmount;
    }

    public BigDecimal getExchangeAmount()
    {
        return exchangeAmount;
    }
    public void setPrepayTime(Date prepayTime)
    {
        this.prepayTime = prepayTime;
    }

    public Date getPrepayTime()
    {
        return prepayTime;
    }
    public void setPrepayInfo(String prepayInfo)
    {
        this.prepayInfo = prepayInfo;
    }

    public String getPrepayInfo()
    {
        return prepayInfo;
    }
    public void setPayTime(Date payTime)
    {
        this.payTime = payTime;
    }

    public Date getPayTime()
    {
        return payTime;
    }
    public void setDeliveryFirm(String deliveryFirm)
    {
        this.deliveryFirm = deliveryFirm;
    }

    public String getDeliveryFirm()
    {
        return deliveryFirm;
    }
    public void setOddNo(String oddNo)
    {
        this.oddNo = oddNo;
    }

    public String getOddNo()
    {
        return oddNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public String getSnCodes() {
        return snCodes;
    }

    public void setSnCodes(String snCodes) {
        this.snCodes = snCodes;
    }

    public List<String> getSnCodeList() {
        return snCodeList;
    }

    public void setSnCodeList(List<String> snCodeList) {
        this.snCodeList = snCodeList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("orderNo", getOrderNo())
                .append("userId", getUserId())
                .append("brandId", getBrandId())
                .append("goodsId", getGoodsId())
                .append("goodsNum", getGoodsNum())
                .append("sepId", getSepId())
                .append("orderType", getOrderType())
                .append("orderStatus", getOrderStatus())
                .append("deliveryType", getDeliveryType())
                .append("deliveryAmount", getDeliveryAmount())
                .append("selfId", getSelfId())
                .append("address", getAddress())
                .append("contactPerson", getContactPerson())
                .append("contactPhone", getContactPhone())
                .append("createTime", getCreateTime())
                .append("deliverGoodsTime", getDeliverGoodsTime())
                .append("payType", getPayType())
                .append("payAmount", getPayAmount())
                .append("exchangeAmount", getExchangeAmount())
                .append("prepayTime", getPrepayTime())
                .append("prepayInfo", getPrepayInfo())
                .append("payTime", getPayTime())
                .append("deliveryFirm", getDeliveryFirm())
                .append("oddNo", getOddNo())
                .append("remark", getRemark())
                .toString();
    }
}