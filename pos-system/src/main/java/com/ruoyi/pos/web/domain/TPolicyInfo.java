package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 平台政策/商学院对象 t_policy_info
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public class TPolicyInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 类型（0：平台政策，1：商学院） */
    @Excel(name = "类型", readConverterExp = "0=：平台政策，1：商学院")
    private String type;

    /** 平台政策/商学院标题 */
    @Excel(name = "平台政策/商学院标题")
    private String title;

    /** 平台政策/商学院(富文本) */
    @Excel(name = "平台政策/商学院(富文本)")
    private String describeText;

    /** 状态（0：正常，1：禁用） */
    @Excel(name = "状态", readConverterExp = "0=：正常，1：禁用")
    private String isValid;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }

    public String getDescribeText() {
        return describeText;
    }

    public void setDescribeText(String describeText) {
        this.describeText = describeText;
    }

    public void setIsValid(String isValid)
    {
        this.isValid = isValid;
    }

    public String getIsValid() 
    {
        return isValid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("type", getType())
            .append("title", getTitle())
            .append("describeText", getDescribeText())
            .append("isValid", getIsValid())
            .append("createTime", getCreateTime())
            .toString();
    }
}
