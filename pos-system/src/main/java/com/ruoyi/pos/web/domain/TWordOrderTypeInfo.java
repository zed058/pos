package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 工单管理类型对象 t_word_order_type_info
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public class TWordOrderTypeInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 状态（0：正常，1：禁用，2：删除） */
    @Excel(name = "状态", readConverterExp = "0=：正常，1：禁用，2：删除")
    private String isValid;

    /** 工单管理类别名称 */
    @Excel(name = "工单管理类别名称")
    private String wordOrderName;

    /** 工单管理logo */
    @Excel(name = "工单管理logo")
    private String wordOrderImg;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setIsValid(String isValid) 
    {
        this.isValid = isValid;
    }

    public String getIsValid() 
    {
        return isValid;
    }
    public void setWordOrderName(String wordOrderName) 
    {
        this.wordOrderName = wordOrderName;
    }

    public String getWordOrderName() 
    {
        return wordOrderName;
    }
    public void setWordOrderImg(String wordOrderImg) 
    {
        this.wordOrderImg = wordOrderImg;
    }

    public String getWordOrderImg() 
    {
        return wordOrderImg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("isValid", getIsValid())
            .append("wordOrderName", getWordOrderName())
            .append("wordOrderImg", getWordOrderImg())
            .append("createTime", getCreateTime())
            .toString();
    }
}
