package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.springframework.data.domain.PageRequest;

/**
 * 用户商户终端关系对象 t_terminal_info
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public class TTerminalInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 用户id */
    private String userId;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** 商户号 */
    private String merchant;

    /** 终端累计交易额 */
    private BigDecimal dealSum;

    /** 终端获取方式（0：线上购买，1：终端划拨，1：兑换终端） */
    private String terminalType;

    /** 商品品牌id */
    private String brandId;

    /** 商品id */
    private String goodsId;

    /** 终端机型 */
    private String terminalModel;

    /** 终端SN码 */
    @Excel(name = "终端SN码")
    private String snCode;

    private String snCodes;

    /** 是否出售（0：未出售，1：已出售） */
    private String sellStatus;

    /** 是否激活（0：未激活，1：已激活） */
    private String activateStatus;
    @Excel(name = "激活状态")
    private String activateStatusShow;

    /** 是否绑定用户（0：未绑定，1：已绑定） */
    private String bindingStatus;
    @Excel(name = "绑定状态")
    private String bindingStatusShow;

    /** 终端出售时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date sellTime;

    /** 终端激活时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date activateTime;

    /** 终端绑定时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date bindingTime;


    private BigDecimal terminalNum;
    /** 终端交易金额 */
    private BigDecimal terminalGmv;

    /** 划拨状态（0、待确认，1、已确认，2、拒绝) */
    private String transStatus;

    /*用户头像*/
    private String userImg;

    /*品牌*/
    private String brand;

    /*商品名称*/
    private String goodsName;

    /*直属上级*/
    @Excel(name = "直属上级")
    private String superiorUserName;

    /*奖励记录列表*/
    private List<TUserDefaultInfo> userDefaultInfoList;

    /*交易笔数*/
    @Excel(name = "总交易笔数")
    private String tradeTotal;

    /*交易金额统计*/
    @Excel(name = "总交易金额")
    private String transAmount;
    @Excel(name = "终端品牌")
    private String brandName;
    private String[] ids;

    private String[] queryAddTimeStr; //创建时间查询条件
    private String addTimeStart;
    private String addTimeEnd;

    /** 终端添加时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", sort = 10)
    private Date addTime;
    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setMerchant(String merchant) 
    {
        this.merchant = merchant;
    }

    public String getMerchant() 
    {
        return merchant;
    }
    public void setDealSum(BigDecimal dealSum) 
    {
        this.dealSum = dealSum;
    }

    public BigDecimal getDealSum() 
    {
        return dealSum;
    }
    public void setTerminalType(String terminalType) 
    {
        this.terminalType = terminalType;
    }

    public String getTerminalType() 
    {
        return terminalType;
    }
    public void setBrandId(String brandId) 
    {
        this.brandId = brandId;
    }

    public String getBrandId() 
    {
        return brandId;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public void setTerminalModel(String terminalModel)
    {
        this.terminalModel = terminalModel;
    }

    public String getTerminalModel() 
    {
        return terminalModel;
    }

    public String getSnCode() {
        return snCode;
    }

    public void setSnCode(String snCode) {
        this.snCode = snCode;
    }

    public String getSnCodes() {
        return snCodes;
    }

    public void setSnCodes(String snCodes) {
        this.snCodes = snCodes;
    }

    public void setSellStatus(String sellStatus)
    {
        this.sellStatus = sellStatus;
    }

    public String getSellStatus() 
    {
        return sellStatus;
    }
    public void setActivateStatus(String activateStatus) 
    {
        this.activateStatus = activateStatus;
    }

    public String getActivateStatus() 
    {
        return activateStatus;
    }
    public void setBindingStatus(String bindingStatus) 
    {
        this.bindingStatus = bindingStatus;
    }

    public String getBindingStatus() 
    {
        return bindingStatus;
    }
    public void setAddTime(Date addTime) 
    {
        this.addTime = addTime;
    }

    public Date getAddTime() 
    {
        return addTime;
    }
    public void setSellTime(Date sellTime) 
    {
        this.sellTime = sellTime;
    }

    public Date getSellTime() 
    {
        return sellTime;
    }
    public void setActivateTime(Date activateTime) 
    {
        this.activateTime = activateTime;
    }

    public Date getActivateTime() 
    {
        return activateTime;
    }
    public void setBindingTime(Date bindingTime) 
    {
        this.bindingTime = bindingTime;
    }

    public Date getBindingTime() 
    {
        return bindingTime;
    }
    public void setTerminalGmv(BigDecimal terminalGmv) 
    {
        this.terminalGmv = terminalGmv;
    }

    public BigDecimal getTerminalGmv() 
    {
        return terminalGmv;
    }

    public BigDecimal getTerminalNum() {
        return terminalNum;
    }

    public void setTerminalNum(BigDecimal terminalNum) {
        this.terminalNum = terminalNum;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSuperiorUserName() {
        return superiorUserName;
    }

    public void setSuperiorUserName(String superiorUserName) {
        this.superiorUserName = superiorUserName;
    }

    public List<TUserDefaultInfo> getUserDefaultInfoList() {
        return userDefaultInfoList;
    }

    public void setUserDefaultInfoList(List<TUserDefaultInfo> userDefaultInfoList) {
        this.userDefaultInfoList = userDefaultInfoList;
    }

    public String getTradeTotal() {
        return tradeTotal;
    }

    public void setTradeTotal(String tradeTotal) {
        this.tradeTotal = tradeTotal;
    }

    public String getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(String transAmount) {
        this.transAmount = transAmount;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String[] getIds() {
        return ids;
    }

    public void setIds(String[] ids) {
        this.ids = ids;
    }

    public String getActivateStatusShow() {
        return activateStatusShow;
    }

    public void setActivateStatusShow(String activateStatusShow) {
        this.activateStatusShow = activateStatusShow;
    }

    public String getBindingStatusShow() {
        return bindingStatusShow;
    }

    public void setBindingStatusShow(String bindingStatusShow) {
        this.bindingStatusShow = bindingStatusShow;
    }

    public String[] getQueryAddTimeStr() {
        return queryAddTimeStr;
    }

    public void setQueryAddTimeStr(String[] queryAddTimeStr) {
        this.queryAddTimeStr = queryAddTimeStr;
    }

    public String getAddTimeStart() {
        return addTimeStart;
    }

    public void setAddTimeStart(String addTimeStart) {
        this.addTimeStart = addTimeStart;
    }

    public String getAddTimeEnd() {
        return addTimeEnd;
    }

    public void setAddTimeEnd(String addTimeEnd) {
        this.addTimeEnd = addTimeEnd;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("phone", getPhone())
            .append("merchant", getMerchant())
            .append("dealSum", getDealSum())
            .append("terminalType", getTerminalType())
            .append("brandId", getBrandId())
            .append("goodsId", getGoodsId())
            .append("terminalModel", getTerminalModel())
            .append("snCode", getSnCode())
            .append("sellStatus", getSellStatus())
            .append("activateStatus", getActivateStatus())
            .append("bindingStatus", getBindingStatus())
            .append("addTime", getAddTime())
            .append("sellTime", getSellTime())
            .append("activateTime", getActivateTime())
            .append("bindingTime", getBindingTime())
            .append("terminalGmv", getTerminalGmv())
            .append("transStatus", getTransStatus())
            .append("userImg", getUserImg())
            .toString();
    }
}
