package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 用户团队关系对象 t_user_team_info
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public class TUserTeamInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;
    private String userName;

    /** 用户上级id */
    @Excel(name = "下级用户id")
    private String subUserId;
    private String superiorUserName;
    /** 团队类型（0：直营，1：团队） */
    @Excel(name = "团队类型", readConverterExp = "0=：直营，1：团队")
    private String teamType;
    private String teamTypeName;

    public String getTeamTypeName() {
        return teamTypeName;
    }

    public void setTeamTypeName(String teamTypeName) {
        this.teamTypeName = teamTypeName;
    }

    public String getSuperiorUserName() {
        return superiorUserName;
    }

    public void setSuperiorUserName(String superiorUserName) {
        this.superiorUserName = superiorUserName;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }

    public String getSubUserId() {
        return subUserId;
    }

    public void setSubUserId(String subUserId) {
        this.subUserId = subUserId;
    }

    public void setTeamType(String teamType)
    {
        this.teamType = teamType;
    }

    public String getTeamType() 
    {
        return teamType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("subUserId", getSubUserId())
            .append("teamType", getTeamType())
            .append("createTime", getCreateTime())
            .toString();
    }

    public static final class Builder {
        private String id;
        private Date createTime;
        private String userId;
        private String subUserId;
        private String teamType;

        private Builder() {
        }

        public static Builder getInstance() {
            return new Builder();
        }

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withCreateTime(Date createTime) {
            this.createTime = createTime;
            return this;
        }

        public Builder withUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder withSubUserId(String subUserId) {
            this.subUserId = subUserId;
            return this;
        }

        public Builder withTeamType(String teamType) {
            this.teamType = teamType;
            return this;
        }

        public TUserTeamInfo build() {
            TUserTeamInfo tUserTeamInfo = new TUserTeamInfo();
            tUserTeamInfo.setId(id);
            tUserTeamInfo.setCreateTime(createTime);
            tUserTeamInfo.setUserId(userId);
            tUserTeamInfo.setSubUserId(subUserId);
            tUserTeamInfo.setTeamType(teamType);
            return tUserTeamInfo;
        }
    }
}
