package com.ruoyi.pos.web.service.impl;

import java.util.List;

import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TTerminalActivateSetInfoMapper;
import com.ruoyi.pos.web.domain.TTerminalActivateSetInfo;
import com.ruoyi.pos.web.service.ITTerminalActivateSetInfoService;

/**
 * 终端激活配置Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@Service
public class TTerminalActivateSetInfoServiceImpl implements ITTerminalActivateSetInfoService 
{
    @Autowired
    private TTerminalActivateSetInfoMapper tTerminalActivateSetInfoMapper;

    /**
     * 查询终端激活配置
     * 
     * @param id 终端激活配置主键
     * @return 终端激活配置
     */
    @Override
    public TTerminalActivateSetInfo selectTTerminalActivateSetInfoById(String id)
    {
        return tTerminalActivateSetInfoMapper.selectTTerminalActivateSetInfoById(id);
    }

    /**
     * 查询终端激活配置列表
     * 
     * @param tTerminalActivateSetInfo 终端激活配置
     * @return 终端激活配置
     */
    @Override
    public List<TTerminalActivateSetInfo> selectTTerminalActivateSetInfoList(TTerminalActivateSetInfo tTerminalActivateSetInfo)
    {
        return tTerminalActivateSetInfoMapper.selectTTerminalActivateSetInfoList(tTerminalActivateSetInfo);
    }

    /**
     * 新增终端激活配置
     * 
     * @param tTerminalActivateSetInfo 终端激活配置
     * @return 结果
     */
    @Override
    public int insertTTerminalActivateSetInfo(TTerminalActivateSetInfo tTerminalActivateSetInfo)
    {
        tTerminalActivateSetInfo.setId(IdUtils.simpleUUID());
        return tTerminalActivateSetInfoMapper.insertTTerminalActivateSetInfo(tTerminalActivateSetInfo);
    }

    /**
     * 修改终端激活配置
     * 
     * @param tTerminalActivateSetInfo 终端激活配置
     * @return 结果
     */
    @Override
    public int updateTTerminalActivateSetInfo(TTerminalActivateSetInfo tTerminalActivateSetInfo)
    {
        return tTerminalActivateSetInfoMapper.updateTTerminalActivateSetInfo(tTerminalActivateSetInfo);
    }

    /**
     * 批量删除终端激活配置
     * 
     * @param ids 需要删除的终端激活配置主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalActivateSetInfoByIds(String[] ids)
    {
        return tTerminalActivateSetInfoMapper.deleteTTerminalActivateSetInfoByIds(ids);
    }

    /**
     * 删除终端激活配置信息
     * 
     * @param id 终端激活配置主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalActivateSetInfoById(String id)
    {
        return tTerminalActivateSetInfoMapper.deleteTTerminalActivateSetInfoById(id);
    }
}
