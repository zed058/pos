package com.ruoyi.pos.web.service.impl;

import java.util.List;

import com.ruoyi.common.enums.TeamTypeEnum;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TUserTeamInfoMapper;
import com.ruoyi.pos.web.domain.TUserTeamInfo;
import com.ruoyi.pos.web.service.ITUserTeamInfoService;

/**
 * 用户团队关系Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@Service
public class TUserTeamInfoServiceImpl implements ITUserTeamInfoService 
{
    @Autowired
    private TUserTeamInfoMapper tUserTeamInfoMapper;

    /**
     * 查询用户团队关系
     * 
     * @param id 用户团队关系主键
     * @return 用户团队关系
     */
    @Override
    public TUserTeamInfo selectTUserTeamInfoById(String id)
    {
        return tUserTeamInfoMapper.selectTUserTeamInfoById(id);
    }

    /**
     * 查询用户团队关系列表
     * 
     * @param tUserTeamInfo 用户团队关系
     * @return 用户团队关系
     */
    @Override
    public List<TUserTeamInfo> selectTUserTeamInfoList(TUserTeamInfo tUserTeamInfo)
    {
        List<TUserTeamInfo> tUserTeamInfos = tUserTeamInfoMapper.selectTUserTeamInfoList(tUserTeamInfo);
        tUserTeamInfos.stream().forEach(bean -> {
            bean.setTeamTypeName(TeamTypeEnum.getEnumsByCode(bean.getTeamType()).getValue());
        });
        return tUserTeamInfos;
    }

    /**
     * 新增用户团队关系
     * 
     * @param tUserTeamInfo 用户团队关系
     * @return 结果
     */
    @Override
    public int insertTUserTeamInfo(TUserTeamInfo tUserTeamInfo)
    {
        tUserTeamInfo.setCreateTime(DateUtils.getNowDate());
        return tUserTeamInfoMapper.insertTUserTeamInfo(tUserTeamInfo);
    }

    /**
     * 修改用户团队关系
     * 
     * @param tUserTeamInfo 用户团队关系
     * @return 结果
     */
    @Override
    public int updateTUserTeamInfo(TUserTeamInfo tUserTeamInfo)
    {
        return tUserTeamInfoMapper.updateTUserTeamInfo(tUserTeamInfo);
    }

    /**
     * 批量删除用户团队关系
     * 
     * @param ids 需要删除的用户团队关系主键
     * @return 结果
     */
    @Override
    public int deleteTUserTeamInfoByIds(String[] ids)
    {
        return tUserTeamInfoMapper.deleteTUserTeamInfoByIds(ids);
    }

    /**
     * 删除用户团队关系信息
     * 
     * @param id 用户团队关系主键
     * @return 结果
     */
    @Override
    public int deleteTUserTeamInfoById(String id)
    {
        return tUserTeamInfoMapper.deleteTUserTeamInfoById(id);
    }
}
