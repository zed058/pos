package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品轮播图，产品说明对象 t_terminal_goods_img_info
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public class TTerminalGoodsImgInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 商品id */
    @Excel(name = "商品id")
    private String goodsId;

    /** 商品图片 */
    @Excel(name = "商品图片")
    private String goodsImg;

    /** 排序 */
    @Excel(name = "排序")
    private String sort;

    /** 图片类型（0：轮播图） */
    @Excel(name = "图片类型", readConverterExp = "0=：轮播图")
    private String imgType;


    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setGoodsId(String goodsId) 
    {
        this.goodsId = goodsId;
    }

    public String getGoodsId() 
    {
        return goodsId;
    }
    public void setGoodsImg(String goodsImg) 
    {
        this.goodsImg = goodsImg;
    }

    public String getGoodsImg() 
    {
        return goodsImg;
    }
    public void setSort(String sort) 
    {
        this.sort = sort;
    }

    public String getSort() 
    {
        return sort;
    }
    public void setImgType(String imgType) 
    {
        this.imgType = imgType;
    }

    public String getImgType() 
    {
        return imgType;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsId", getGoodsId())
            .append("goodsImg", getGoodsImg())
            .append("sort", getSort())
            .append("imgType", getImgType())
            .append("createTime", getCreateTime())
            .toString();
    }
}
