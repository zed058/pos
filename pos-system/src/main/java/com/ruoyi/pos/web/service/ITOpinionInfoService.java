package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TOpinionInfo;

/**
 * 意见反馈Service接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface ITOpinionInfoService 
{
    /**
     * 查询意见反馈
     * 
     * @param id 意见反馈主键
     * @return 意见反馈
     */
    public TOpinionInfo selectTOpinionInfoById(String id);

    /**
     * 查询意见反馈列表
     * 
     * @param tOpinionInfo 意见反馈
     * @return 意见反馈集合
     */
    public List<TOpinionInfo> selectTOpinionInfoList(TOpinionInfo tOpinionInfo);

    /**
     * 新增意见反馈
     * 
     * @param tOpinionInfo 意见反馈
     * @return 结果
     */
    public int insertTOpinionInfo(TOpinionInfo tOpinionInfo);

    /**
     * 修改意见反馈
     * 
     * @param tOpinionInfo 意见反馈
     * @return 结果
     */
    public int updateTOpinionInfo(TOpinionInfo tOpinionInfo);

    /**
     * 批量删除意见反馈
     * 
     * @param ids 需要删除的意见反馈主键集合
     * @return 结果
     */
    public int deleteTOpinionInfoByIds(String[] ids);

    /**
     * 删除意见反馈信息
     * 
     * @param id 意见反馈主键
     * @return 结果
     */
    public int deleteTOpinionInfoById(String id);
}
