package com.ruoyi.pos.web.mapper;

import java.util.List;
import com.ruoyi.pos.web.domain.TWordOrderTypeInfo;

/**
 * 工单管理类型Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public interface TWordOrderTypeInfoMapper 
{
    /**
     * 查询工单管理类型
     * 
     * @param id 工单管理类型主键
     * @return 工单管理类型
     */
    public TWordOrderTypeInfo selectTWordOrderTypeInfoById(String id);

    /**
     * 查询工单管理类型列表
     * 
     * @param tWordOrderTypeInfo 工单管理类型
     * @return 工单管理类型集合
     */
    public List<TWordOrderTypeInfo> selectTWordOrderTypeInfoList(TWordOrderTypeInfo tWordOrderTypeInfo);

    /**
     * 新增工单管理类型
     * 
     * @param tWordOrderTypeInfo 工单管理类型
     * @return 结果
     */
    public int insertTWordOrderTypeInfo(TWordOrderTypeInfo tWordOrderTypeInfo);

    /**
     * 修改工单管理类型
     * 
     * @param tWordOrderTypeInfo 工单管理类型
     * @return 结果
     */
    public int updateTWordOrderTypeInfo(TWordOrderTypeInfo tWordOrderTypeInfo);

    /**
     * 删除工单管理类型
     * 
     * @param id 工单管理类型主键
     * @return 结果
     */
    public int deleteTWordOrderTypeInfoById(String id);

    /**
     * 批量删除工单管理类型
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTWordOrderTypeInfoByIds(String[] ids);
}
