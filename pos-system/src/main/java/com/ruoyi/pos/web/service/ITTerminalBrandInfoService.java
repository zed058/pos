package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TTerminalBrandInfo;

/**
 * 商品品牌Service接口
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public interface ITTerminalBrandInfoService 
{
    /**
     * 查询商品品牌
     * 
     * @param id 商品品牌主键
     * @return 商品品牌
     */
    public TTerminalBrandInfo selectTTerminalBrandInfoById(String id);

    /**
     * 查询商品品牌列表
     * 
     * @param tTerminalBrandInfo 商品品牌
     * @return 商品品牌集合
     */
    public List<TTerminalBrandInfo> selectTTerminalBrandInfoList(TTerminalBrandInfo tTerminalBrandInfo);

    /**
     * 新增商品品牌
     * 
     * @param tTerminalBrandInfo 商品品牌
     * @return 结果
     */
    public int insertTTerminalBrandInfo(TTerminalBrandInfo tTerminalBrandInfo);

    /**
     * 修改商品品牌
     * 
     * @param tTerminalBrandInfo 商品品牌
     * @return 结果
     */
    public int updateTTerminalBrandInfo(TTerminalBrandInfo tTerminalBrandInfo);

    /**
     * 批量删除商品品牌
     * 
     * @param ids 需要删除的商品品牌主键集合
     * @return 结果
     */
    public int deleteTTerminalBrandInfoByIds(String[] ids);

    /**
     * 删除商品品牌信息
     * 
     * @param id 商品品牌主键
     * @return 结果
     */
    public int deleteTTerminalBrandInfoById(String id);
}
