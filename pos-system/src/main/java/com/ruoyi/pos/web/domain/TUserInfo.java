package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户基础数据对象 t_user_info
 * 
 * @author ruoyi
 * @date 2021-11-10
 */
public class TUserInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户id */
    private String userId;

    /** 微信号 */
    private String wechatId;

    /** 微信id */
    private String openId;

    /** 用户名 */
    @Excel(name = "用户名")
    private String userName;

    /** 用户头像 */
    private String userImg;

    /** 用户手机号 */
    @Excel(name = "用户手机号")
    private String userPhone;

    /** 用户密码 */
    private String password;

    /** 用户状态（0：正常，1：禁用） */
    private String userStatus;
    @Excel(name = "用户状态")
    private String userStatusShow;


    /** 总余额 */
    @Excel(name = "总余额")
    private BigDecimal balanceSum;

    /** 已提现余额 */
    @Excel(name = "已提现余额")
    private BigDecimal balanceWithdraw;

    /** 已使用余额 */
    @Excel(name = "已使用余额")
    private BigDecimal balanceUse;

    /** 总金币 */
    //@Excel(name = "总金币")
    private BigDecimal goldSum;

    /** 已提现金币（后台定时任务提现） */
    //@Excel(name = "已提现金币", readConverterExp = "后=台定时任务提现")
    private BigDecimal goldWithdraw;

    /** 已兑换金币 */
    //@Excel(name = "已兑换金币")
    private BigDecimal goldExchange;

    /** 总积分 */
    //@Excel(name = "总积分")
    private BigDecimal integralSum;

    /** 已兑换积分 */
    //@Excel(name = "已兑换积分")
    private BigDecimal integralExchange;

    /** 总交易金（序号个数） */
    //@Excel(name = "总交易金", readConverterExp = "序=号个数")
    private BigDecimal dealGoldMark;

    /** 已兑换交易金（序号个数） */
    //@Excel(name = "已兑换交易金", readConverterExp = "序=号个数")
    private BigDecimal dealGoldExchangeMark;

    /** 已兑现奖励金（平台发放金币时记录） */
    //@Excel(name = "已兑现奖励金", readConverterExp = "平=台发放金币时记录")
    private BigDecimal dealGold;

    /** 总奖励金（序号个数） */
    //@Excel(name = "总奖励金", readConverterExp = "序=号个数")
    private BigDecimal awardGoldMark;

    /** 已兑换奖励金（序号个数） */
    //@Excel(name = "已兑换奖励金", readConverterExp = "序=号个数")
    private BigDecimal awardGoldExchangeMark;

    /** 已兑现交易金（平台发放金币时记录） */
    //@Excel(name = "已兑现交易金", readConverterExp = "平=台发放金币时记录")
    private BigDecimal awardGold;

    /** 邀请二维码 */
    //@Excel(name = "邀请二维码")
    private String inviteImg;

    /** 邀请码 */
    //@Excel(name = "邀请码")
    private String inviteCode;

    /** 上级用户id */
    //@Excel(name = "上级用户id")
    private String superiorUserId;

    /** 上级用户名称 */
    @Excel(name = "上级用户名称")
    private String superiorUserName;

    /** 是否实名认证（0：否，1：是） */
    @Excel(name = "是否实名认证")
    private String autonymType;
    private String autonymTypeShow;

    /** 身份证号 */
    //@Excel(name = "身份证号")
    private String identityMark;

    /** 银行卡号 */
    //@Excel(name = "银行卡号")
    private String bankCardMark;

    /** 开户行地址 */
    //@Excel(name = "开户行地址")
    private String bankAddress;

    /** 银行名称 */
    //@Excel(name = "银行名称")
    private String bankId;
    private String bankName;

    /** 支行名称 */
    //@Excel(name = "支行名称")
    private String subBranch;

    /** 身份证照片正面 */
    //@Excel(name = "身份证照片正面")
    private String identityZhengImg;

    /** 身份证照片反面 */
    //@Excel(name = "身份证照片反面")
    private String identityFanImg;

    /** 银行卡号正面 */
    //@Excel(name = "银行卡号正面")
    private String bankCardZhengImg;

    /** 用户等级id */
    private String gradeId;

    /** 实名认证时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date autonymTime;

    /** token */
    private String token;

    /** token失效时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date registreTime;

    /** 实名认证（0:待审核，1：审核通过，2：审核驳回） */
    //@Excel(name = "实名认证", readConverterExp = "0=:待审核，1：审核通过，2：审核驳回")
    private String auditStatus;
    private String auditStatusDescribe;

    /** 密码盐 */
    private String salt;
    /*支付密码盐*/
    private String paySalt;

    /** 支付密码 */
    private String payPassword;
    /*经度*/
    private String lng;
    /*纬度*/
    private String lat;
    /*用户累计交易金额*/
    private BigDecimal totalTransAmount;


    private BigDecimal vipTerminalSum;
    private BigDecimal vipDealMoney;
    private BigDecimal dealAmountHasSend;
    private BigDecimal awardAmountHasSend;

    @Excel(name = "会员等级")
    private String gradeCode; //会员等级代码
    private String supUserPhone; //上级用户手机号
    private int teamSum; //团队总数
    private int directlyNum; //直营代理商数
    private BigDecimal ownTerminalNum; //自己终端数
    private BigDecimal directlyTerminalNum; //直营终端数

    //总交易笔数
    private String myTransNum;
    //公告是否已读(1:未读，0：已读)
    private String userNoticeType;

    //趣工宝相关业务项
    private String memberNo; //趣工宝会员编号
    private Date memberNoTime; //趣工宝会员编号生成时间
    private String signStatus; //趣工宝签约状态（10：待签约，20：签约审核中，30：签约成功，31：签约失败）
    private Date signTime; //趣工宝签约时间
    private String contractSignUrl; //趣工宝签约地址
    private String contractUrl; //趣工宝合同下载地址
    private String contractName; //趣工宝签约合同名称
    private String contractNo; //趣工宝签约合同编号

    private String shandianAuthFlag; //闪电宝注册标识（10：未注册，11：待同步，12：已同步）
    private Date shandianAuthTime; //闪电宝注册时间
    private String shandianBankName; //闪电宝注册时间
    private String shandianBankNo; //闪电宝注册时间
    private String shandianCertNo; //闪电宝注册时间
    private String shandianName; //闪电宝注册时间


    public String getUserStatusShow() {
        return userStatusShow;
    }

    public void setUserStatusShow(String userStatusShow) {
        this.userStatusShow = userStatusShow;
    }

    public String getAutonymTypeShow() {
        return autonymTypeShow;
    }

    public void setAutonymTypeShow(String autonymTypeShow) {
        this.autonymTypeShow = autonymTypeShow;
    }

    public String getMyTransNum() {
        return myTransNum;
    }

    public void setMyTransNum(String myTransNum) {
        this.myTransNum = myTransNum;
    }

    public String getGradeCode() {
        return gradeCode;
    }

    public void setGradeCode(String gradeCode) {
        this.gradeCode = gradeCode;
    }

    public String getSupUserPhone() {
        return supUserPhone;
    }

    public void setSupUserPhone(String supUserPhone) {
        this.supUserPhone = supUserPhone;
    }

    public int getTeamSum() {
        return teamSum;
    }

    public void setTeamSum(int teamSum) {
        this.teamSum = teamSum;
    }

    public int getDirectlyNum() {
        return directlyNum;
    }

    public void setDirectlyNum(int directlyNum) {
        this.directlyNum = directlyNum;
    }

    public BigDecimal getOwnTerminalNum() {
        return ownTerminalNum;
    }

    public void setOwnTerminalNum(BigDecimal ownTerminalNum) {
        this.ownTerminalNum = ownTerminalNum;
    }

    public BigDecimal getDirectlyTerminalNum() {
        return directlyTerminalNum;
    }

    public void setDirectlyTerminalNum(BigDecimal directlyTerminalNum) {
        this.directlyTerminalNum = directlyTerminalNum;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setWechatId(String wechatId) 
    {
        this.wechatId = wechatId;
    }

    public String getWechatId() 
    {
        return wechatId;
    }
    public void setOpenId(String openId) 
    {
        this.openId = openId;
    }

    public String getOpenId() 
    {
        return openId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setUserImg(String userImg) 
    {
        this.userImg = userImg;
    }

    public String getUserImg() 
    {
        return userImg;
    }
    public void setUserPhone(String userPhone) 
    {
        this.userPhone = userPhone;
    }

    public String getUserPhone() 
    {
        return userPhone;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setUserStatus(String userStatus) 
    {
        this.userStatus = userStatus;
    }

    public String getUserStatus() 
    {
        return userStatus;
    }
    public void setBalanceSum(BigDecimal balanceSum) 
    {
        this.balanceSum = balanceSum;
    }

    public BigDecimal getBalanceSum() 
    {
        return balanceSum;
    }
    public void setBalanceWithdraw(BigDecimal balanceWithdraw) 
    {
        this.balanceWithdraw = balanceWithdraw;
    }

    public BigDecimal getBalanceWithdraw() 
    {
        return balanceWithdraw;
    }
    public void setBalanceUse(BigDecimal balanceUse) 
    {
        this.balanceUse = balanceUse;
    }

    public BigDecimal getBalanceUse() 
    {
        return balanceUse;
    }
    public void setGoldSum(BigDecimal goldSum) 
    {
        this.goldSum = goldSum;
    }

    public BigDecimal getGoldSum() 
    {
        return goldSum;
    }
    public void setGoldWithdraw(BigDecimal goldWithdraw) 
    {
        this.goldWithdraw = goldWithdraw;
    }

    public BigDecimal getGoldWithdraw() 
    {
        return goldWithdraw;
    }
    public void setGoldExchange(BigDecimal goldExchange) 
    {
        this.goldExchange = goldExchange;
    }

    public BigDecimal getGoldExchange() 
    {
        return goldExchange;
    }
    public void setIntegralSum(BigDecimal integralSum) 
    {
        this.integralSum = integralSum;
    }

    public BigDecimal getIntegralSum() 
    {
        return integralSum;
    }
    public void setIntegralExchange(BigDecimal integralExchange) 
    {
        this.integralExchange = integralExchange;
    }

    public BigDecimal getIntegralExchange() 
    {
        return integralExchange;
    }
    public void setDealGoldMark(BigDecimal dealGoldMark) 
    {
        this.dealGoldMark = dealGoldMark;
    }

    public BigDecimal getDealGoldMark() 
    {
        return dealGoldMark;
    }
    public void setDealGoldExchangeMark(BigDecimal dealGoldExchangeMark) 
    {
        this.dealGoldExchangeMark = dealGoldExchangeMark;
    }

    public BigDecimal getDealGoldExchangeMark() 
    {
        return dealGoldExchangeMark;
    }
    public void setDealGold(BigDecimal dealGold) 
    {
        this.dealGold = dealGold;
    }

    public BigDecimal getDealGold() 
    {
        return dealGold;
    }
    public void setAwardGoldMark(BigDecimal awardGoldMark) 
    {
        this.awardGoldMark = awardGoldMark;
    }

    public BigDecimal getAwardGoldMark() 
    {
        return awardGoldMark;
    }
    public void setAwardGoldExchangeMark(BigDecimal awardGoldExchangeMark) 
    {
        this.awardGoldExchangeMark = awardGoldExchangeMark;
    }

    public BigDecimal getAwardGoldExchangeMark() 
    {
        return awardGoldExchangeMark;
    }
    public void setAwardGold(BigDecimal awardGold) 
    {
        this.awardGold = awardGold;
    }

    public BigDecimal getAwardGold() 
    {
        return awardGold;
    }
    public void setInviteImg(String inviteImg) 
    {
        this.inviteImg = inviteImg;
    }

    public String getInviteImg() 
    {
        return inviteImg;
    }
    public void setInviteCode(String inviteCode) 
    {
        this.inviteCode = inviteCode;
    }

    public String getInviteCode() 
    {
        return inviteCode;
    }
    public void setSuperiorUserId(String superiorUserId) 
    {
        this.superiorUserId = superiorUserId;
    }

    public String getSuperiorUserId() 
    {
        return superiorUserId;
    }
    public void setSuperiorUserName(String superiorUserName) 
    {
        this.superiorUserName = superiorUserName;
    }

    public String getSuperiorUserName() 
    {
        return superiorUserName;
    }
    public void setAutonymType(String autonymType) 
    {
        this.autonymType = autonymType;
    }

    public String getAutonymType() 
    {
        return autonymType;
    }
    public void setIdentityMark(String identityMark) 
    {
        this.identityMark = identityMark;
    }

    public String getIdentityMark() 
    {
        return identityMark;
    }
    public void setBankCardMark(String bankCardMark) 
    {
        this.bankCardMark = bankCardMark;
    }

    public String getBankCardMark() 
    {
        return bankCardMark;
    }
    public void setBankAddress(String bankAddress) 
    {
        this.bankAddress = bankAddress;
    }

    public String getBankAddress() 
    {
        return bankAddress;
    }
    public void setSubBranch(String subBranch) 
    {
        this.subBranch = subBranch;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getSubBranch()
    {
        return subBranch;
    }
    public void setIdentityZhengImg(String identityZhengImg) 
    {
        this.identityZhengImg = identityZhengImg;
    }

    public String getIdentityZhengImg() 
    {
        return identityZhengImg;
    }
    public void setIdentityFanImg(String identityFanImg) 
    {
        this.identityFanImg = identityFanImg;
    }

    public String getIdentityFanImg() 
    {
        return identityFanImg;
    }
    public void setBankCardZhengImg(String bankCardZhengImg) 
    {
        this.bankCardZhengImg = bankCardZhengImg;
    }

    public String getBankCardZhengImg() 
    {
        return bankCardZhengImg;
    }
    public void setGradeId(String gradeId) 
    {
        this.gradeId = gradeId;
    }

    public String getGradeId() 
    {
        return gradeId;
    }
    public void setAutonymTime(Date autonymTime) 
    {
        this.autonymTime = autonymTime;
    }

    public Date getAutonymTime() 
    {
        return autonymTime;
    }
    public void setToken(String token) 
    {
        this.token = token;
    }

    public String getToken() 
    {
        return token;
    }
    public void setRegistreTime(Date registreTime) 
    {
        this.registreTime = registreTime;
    }

    public Date getRegistreTime() 
    {
        return registreTime;
    }
    public void setAuditStatus(String auditStatus) 
    {
        this.auditStatus = auditStatus;
    }

    public String getAuditStatus() 
    {
        return auditStatus;
    }

    public String getAuditStatusDescribe() {
        return auditStatusDescribe;
    }

    public void setAuditStatusDescribe(String auditStatusDescribe) {
        this.auditStatusDescribe = auditStatusDescribe;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }

    public BigDecimal getVipTerminalSum() {
        return vipTerminalSum;
    }

    public void setVipTerminalSum(BigDecimal vipTerminalSum) {
        this.vipTerminalSum = vipTerminalSum;
    }

    public BigDecimal getVipDealMoney() {
        return vipDealMoney;
    }

    public void setVipDealMoney(BigDecimal vipDealMoney) {
        this.vipDealMoney = vipDealMoney;
    }

    public BigDecimal getDealAmountHasSend() {
        return dealAmountHasSend;
    }

    public void setDealAmountHasSend(BigDecimal dealAmountHasSend) {
        this.dealAmountHasSend = dealAmountHasSend;
    }

    public BigDecimal getAwardAmountHasSend() {
        return awardAmountHasSend;
    }

    public void setAwardAmountHasSend(BigDecimal awardAmountHasSend) {
        this.awardAmountHasSend = awardAmountHasSend;
    }

    public String getPaySalt() {
        return paySalt;
    }

    public void setPaySalt(String paySalt) {
        this.paySalt = paySalt;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public BigDecimal getTotalTransAmount() {
        return totalTransAmount;
    }

    public void setTotalTransAmount(BigDecimal totalTransAmount) {
        this.totalTransAmount = totalTransAmount;
    }

    public String getUserNoticeType() {
        return userNoticeType;
    }

    public void setUserNoticeType(String userNoticeType) {
        this.userNoticeType = userNoticeType;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public Date getMemberNoTime() {
        return memberNoTime;
    }

    public void setMemberNoTime(Date memberNoTime) {
        this.memberNoTime = memberNoTime;
    }

    public String getSignStatus() {
        return signStatus;
    }

    public void setSignStatus(String signStatus) {
        this.signStatus = signStatus;
    }

    public Date getSignTime() {
        return signTime;
    }

    public void setSignTime(Date signTime) {
        this.signTime = signTime;
    }

    public String getContractSignUrl() {
        return contractSignUrl;
    }

    public void setContractSignUrl(String contractSignUrl) {
        this.contractSignUrl = contractSignUrl;
    }

    public String getContractUrl() {
        return contractUrl;
    }

    public void setContractUrl(String contractUrl) {
        this.contractUrl = contractUrl;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getShandianAuthFlag() {
        return shandianAuthFlag;
    }

    public void setShandianAuthFlag(String shandianAuthFlag) {
        this.shandianAuthFlag = shandianAuthFlag;
    }

    public Date getShandianAuthTime() {
        return shandianAuthTime;
    }

    public void setShandianAuthTime(Date shandianAuthTime) {
        this.shandianAuthTime = shandianAuthTime;
    }

    public String getShandianBankName() {
        return shandianBankName;
    }

    public void setShandianBankName(String shandianBankName) {
        this.shandianBankName = shandianBankName;
    }

    public String getShandianBankNo() {
        return shandianBankNo;
    }

    public void setShandianBankNo(String shandianBankNo) {
        this.shandianBankNo = shandianBankNo;
    }

    public String getShandianCertNo() {
        return shandianCertNo;
    }

    public void setShandianCertNo(String shandianCertNo) {
        this.shandianCertNo = shandianCertNo;
    }

    public String getShandianName() {
        return shandianName;
    }

    public void setShandianName(String shandianName) {
        this.shandianName = shandianName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("wechatId", getWechatId())
            .append("openId", getOpenId())
            .append("userName", getUserName())
            .append("userImg", getUserImg())
            .append("userPhone", getUserPhone())
            .append("password", getPassword())
            .append("userStatus", getUserStatus())
            .append("balanceSum", getBalanceSum())
            .append("balanceWithdraw", getBalanceWithdraw())
            .append("balanceUse", getBalanceUse())
            .append("goldSum", getGoldSum())
            .append("goldWithdraw", getGoldWithdraw())
            .append("goldExchange", getGoldExchange())
            .append("integralSum", getIntegralSum())
            .append("integralExchange", getIntegralExchange())
            .append("dealGoldMark", getDealGoldMark())
            .append("dealGoldExchangeMark", getDealGoldExchangeMark())
            .append("dealGold", getDealGold())
            .append("awardGoldMark", getAwardGoldMark())
            .append("awardGoldExchangeMark", getAwardGoldExchangeMark())
            .append("awardGold", getAwardGold())
            .append("inviteImg", getInviteImg())
            .append("inviteCode", getInviteCode())
            .append("superiorUserId", getSuperiorUserId())
            .append("superiorUserName", getSuperiorUserName())
            .append("autonymType", getAutonymType())
            .append("identityMark", getIdentityMark())
            .append("bankCardMark", getBankCardMark())
            .append("bankAddress", getBankAddress())
            .append("subBranch", getSubBranch())
            .append("identityZhengImg", getIdentityZhengImg())
            .append("identityFanImg", getIdentityFanImg())
            .append("bankCardZhengImg", getBankCardZhengImg())
            .append("gradeId", getGradeId())
            .append("createTime", getCreateTime())
            .append("autonymTime", getAutonymTime())
            .append("token", getToken())
            .append("registreTime", getRegistreTime())
            .append("auditStatus", getAuditStatus())
            .append("vipTerminalSum", getVipTerminalSum())
            .append("vipDealMoney", getVipDealMoney())
            .append("dealAmountHasSend", getDealAmountHasSend())
            .append("awardAmountHasSend", getAwardAmountHasSend())
            .append("paySalt", getPaySalt())
            .append("lng", getLng())
            .append("lat", getLat())
            .append("totalTransAmount", getTotalTransAmount())
            .append("memberNo", getMemberNo())
            .append("memberNoTime", getMemberNoTime())
            .append("signStatus", getSignStatus())
            .append("signTime", getSignTime())
            .append("contractSignUrl", getContractSignUrl())
            .append("contractUrl", getContractUrl())
            .append("contractName", getContractName())
            .append("contractNo", getContractNo())
            .toString();
    }


    public static final class Builder {
        private Date createTime;
        private String userId;
        private String wechatId;
        private String openId;
        private Date updateTime;
        private String userName;
        private String userImg;
        private String userPhone;
        private String password;
        private String userStatus;
        private String userStatusShow;
        private BigDecimal balanceSum;
        private BigDecimal balanceWithdraw;
        private BigDecimal balanceUse;
        //@Excel(name = "总金币")
        private BigDecimal goldSum;
        //@Excel(name = "已提现金币", readConverterExp = "后=台定时任务提现")
        private BigDecimal goldWithdraw;
        //@Excel(name = "已兑换金币")
        private BigDecimal goldExchange;
        //@Excel(name = "总积分")
        private BigDecimal integralSum;
        //@Excel(name = "已兑换积分")
        private BigDecimal integralExchange;
        //@Excel(name = "总交易金", readConverterExp = "序=号个数")
        private BigDecimal dealGoldMark;
        //@Excel(name = "已兑换交易金", readConverterExp = "序=号个数")
        private BigDecimal dealGoldExchangeMark;
        //@Excel(name = "已兑现奖励金", readConverterExp = "平=台发放金币时记录")
        private BigDecimal dealGold;
        //@Excel(name = "总奖励金", readConverterExp = "序=号个数")
        private BigDecimal awardGoldMark;
        //@Excel(name = "已兑换奖励金", readConverterExp = "序=号个数")
        private BigDecimal awardGoldExchangeMark;
        //@Excel(name = "已兑现交易金", readConverterExp = "平=台发放金币时记录")
        private BigDecimal awardGold;
        //@Excel(name = "邀请二维码")
        private String inviteImg;
        //@Excel(name = "邀请码")
        private String inviteCode;
        //@Excel(name = "上级用户id")
        private String superiorUserId;
        private String superiorUserName;
        private String autonymType;
        private String autonymTypeShow;
        //@Excel(name = "身份证号")
        private String identityMark;
        //@Excel(name = "银行卡号")
        private String bankCardMark;
        //@Excel(name = "开户行地址")
        private String bankAddress;
        //@Excel(name = "银行名称")
        private String bankId;
        private String bankName;
        //@Excel(name = "支行名称")
        private String subBranch;
        //@Excel(name = "身份证照片正面")
        private String identityZhengImg;
        //@Excel(name = "身份证照片反面")
        private String identityFanImg;
        //@Excel(name = "银行卡号正面")
        private String bankCardZhengImg;
        private String gradeId;
        private Date autonymTime;
        private String token;
        private Date registreTime;
        //@Excel(name = "实名认证", readConverterExp = "0=:待审核，1：审核通过，2：审核驳回")
        private String auditStatus;
        private String auditStatusDescribe;
        private String salt;
        /*支付密码盐*/
        private String paySalt;
        private String payPassword;
        /*经度*/
        private String lng;
        /*纬度*/
        private String lat;
        /*用户累计交易金额*/
        private BigDecimal totalTransAmount;
        private BigDecimal vipTerminalSum;
        private BigDecimal vipDealMoney;
        private BigDecimal dealAmountHasSend;
        private BigDecimal awardAmountHasSend;
        private String gradeCode; //会员等级代码
        private String supUserPhone; //上级用户手机号
        private int teamSum; //团队总数
        private int directlyNum; //直营代理商数
        private BigDecimal ownTerminalNum; //自己终端数
        private BigDecimal directlyTerminalNum; //直营终端数
        //总交易笔数
        private String myTransNum;
        //公告是否已读(1:未读，0：已读)
        private String userNoticeType;
        //趣工宝相关业务项
        private String memberNo; //趣工宝会员编号
        private Date memberNoTime; //趣工宝会员编号生成时间
        private String signStatus; //趣工宝签约状态（10：待签约，20：签约审核中，30：签约成功，31：签约失败）
        private Date signTime; //趣工宝签约时间
        private String contractSignUrl; //趣工宝签约地址
        private String contractUrl; //趣工宝合同下载地址
        private String contractName; //趣工宝签约合同名称
        private String contractNo; //趣工宝签约合同编号

        private Builder() {
        }

        public static Builder getInstance() {
            return new Builder();
        }

        public Builder withCreateTime(Date createTime) {
            this.createTime = createTime;
            return this;
        }

        public Builder withUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder withWechatId(String wechatId) {
            this.wechatId = wechatId;
            return this;
        }

        public Builder withOpenId(String openId) {
            this.openId = openId;
            return this;
        }

        public Builder withUpdateTime(Date updateTime) {
            this.updateTime = updateTime;
            return this;
        }

        public Builder withUserName(String userName) {
            this.userName = userName;
            return this;
        }

        public Builder withUserImg(String userImg) {
            this.userImg = userImg;
            return this;
        }

        public Builder withUserPhone(String userPhone) {
            this.userPhone = userPhone;
            return this;
        }

        public Builder withPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder withUserStatus(String userStatus) {
            this.userStatus = userStatus;
            return this;
        }

        public Builder withUserStatusShow(String userStatusShow) {
            this.userStatusShow = userStatusShow;
            return this;
        }

        public Builder withBalanceSum(BigDecimal balanceSum) {
            this.balanceSum = balanceSum;
            return this;
        }

        public Builder withBalanceWithdraw(BigDecimal balanceWithdraw) {
            this.balanceWithdraw = balanceWithdraw;
            return this;
        }

        public Builder withBalanceUse(BigDecimal balanceUse) {
            this.balanceUse = balanceUse;
            return this;
        }

        public Builder withGoldSum(BigDecimal goldSum) {
            this.goldSum = goldSum;
            return this;
        }

        public Builder withGoldWithdraw(BigDecimal goldWithdraw) {
            this.goldWithdraw = goldWithdraw;
            return this;
        }

        public Builder withGoldExchange(BigDecimal goldExchange) {
            this.goldExchange = goldExchange;
            return this;
        }

        public Builder withIntegralSum(BigDecimal integralSum) {
            this.integralSum = integralSum;
            return this;
        }

        public Builder withIntegralExchange(BigDecimal integralExchange) {
            this.integralExchange = integralExchange;
            return this;
        }

        public Builder withDealGoldMark(BigDecimal dealGoldMark) {
            this.dealGoldMark = dealGoldMark;
            return this;
        }

        public Builder withDealGoldExchangeMark(BigDecimal dealGoldExchangeMark) {
            this.dealGoldExchangeMark = dealGoldExchangeMark;
            return this;
        }

        public Builder withDealGold(BigDecimal dealGold) {
            this.dealGold = dealGold;
            return this;
        }

        public Builder withAwardGoldMark(BigDecimal awardGoldMark) {
            this.awardGoldMark = awardGoldMark;
            return this;
        }

        public Builder withAwardGoldExchangeMark(BigDecimal awardGoldExchangeMark) {
            this.awardGoldExchangeMark = awardGoldExchangeMark;
            return this;
        }

        public Builder withAwardGold(BigDecimal awardGold) {
            this.awardGold = awardGold;
            return this;
        }

        public Builder withInviteImg(String inviteImg) {
            this.inviteImg = inviteImg;
            return this;
        }

        public Builder withInviteCode(String inviteCode) {
            this.inviteCode = inviteCode;
            return this;
        }

        public Builder withSuperiorUserId(String superiorUserId) {
            this.superiorUserId = superiorUserId;
            return this;
        }

        public Builder withSuperiorUserName(String superiorUserName) {
            this.superiorUserName = superiorUserName;
            return this;
        }

        public Builder withAutonymType(String autonymType) {
            this.autonymType = autonymType;
            return this;
        }

        public Builder withAutonymTypeShow(String autonymTypeShow) {
            this.autonymTypeShow = autonymTypeShow;
            return this;
        }

        public Builder withIdentityMark(String identityMark) {
            this.identityMark = identityMark;
            return this;
        }

        public Builder withBankCardMark(String bankCardMark) {
            this.bankCardMark = bankCardMark;
            return this;
        }

        public Builder withBankAddress(String bankAddress) {
            this.bankAddress = bankAddress;
            return this;
        }

        public Builder withBankId(String bankId) {
            this.bankId = bankId;
            return this;
        }

        public Builder withBankName(String bankName) {
            this.bankName = bankName;
            return this;
        }

        public Builder withSubBranch(String subBranch) {
            this.subBranch = subBranch;
            return this;
        }

        public Builder withIdentityZhengImg(String identityZhengImg) {
            this.identityZhengImg = identityZhengImg;
            return this;
        }

        public Builder withIdentityFanImg(String identityFanImg) {
            this.identityFanImg = identityFanImg;
            return this;
        }

        public Builder withBankCardZhengImg(String bankCardZhengImg) {
            this.bankCardZhengImg = bankCardZhengImg;
            return this;
        }

        public Builder withGradeId(String gradeId) {
            this.gradeId = gradeId;
            return this;
        }

        public Builder withAutonymTime(Date autonymTime) {
            this.autonymTime = autonymTime;
            return this;
        }

        public Builder withToken(String token) {
            this.token = token;
            return this;
        }

        public Builder withRegistreTime(Date registreTime) {
            this.registreTime = registreTime;
            return this;
        }

        public Builder withAuditStatus(String auditStatus) {
            this.auditStatus = auditStatus;
            return this;
        }

        public Builder withAuditStatusDescribe(String auditStatusDescribe) {
            this.auditStatusDescribe = auditStatusDescribe;
            return this;
        }

        public Builder withSalt(String salt) {
            this.salt = salt;
            return this;
        }

        public Builder withPaySalt(String paySalt) {
            this.paySalt = paySalt;
            return this;
        }

        public Builder withPayPassword(String payPassword) {
            this.payPassword = payPassword;
            return this;
        }

        public Builder withLng(String lng) {
            this.lng = lng;
            return this;
        }

        public Builder withLat(String lat) {
            this.lat = lat;
            return this;
        }

        public Builder withTotalTransAmount(BigDecimal totalTransAmount) {
            this.totalTransAmount = totalTransAmount;
            return this;
        }

        public Builder withVipTerminalSum(BigDecimal vipTerminalSum) {
            this.vipTerminalSum = vipTerminalSum;
            return this;
        }

        public Builder withVipDealMoney(BigDecimal vipDealMoney) {
            this.vipDealMoney = vipDealMoney;
            return this;
        }

        public Builder withDealAmountHasSend(BigDecimal dealAmountHasSend) {
            this.dealAmountHasSend = dealAmountHasSend;
            return this;
        }

        public Builder withAwardAmountHasSend(BigDecimal awardAmountHasSend) {
            this.awardAmountHasSend = awardAmountHasSend;
            return this;
        }

        public Builder withGradeCode(String gradeCode) {
            this.gradeCode = gradeCode;
            return this;
        }

        public Builder withSupUserPhone(String supUserPhone) {
            this.supUserPhone = supUserPhone;
            return this;
        }

        public Builder withTeamSum(int teamSum) {
            this.teamSum = teamSum;
            return this;
        }

        public Builder withDirectlyNum(int directlyNum) {
            this.directlyNum = directlyNum;
            return this;
        }

        public Builder withOwnTerminalNum(BigDecimal ownTerminalNum) {
            this.ownTerminalNum = ownTerminalNum;
            return this;
        }

        public Builder withDirectlyTerminalNum(BigDecimal directlyTerminalNum) {
            this.directlyTerminalNum = directlyTerminalNum;
            return this;
        }

        public Builder withMyTransNum(String myTransNum) {
            this.myTransNum = myTransNum;
            return this;
        }

        public Builder withUserNoticeType(String userNoticeType) {
            this.userNoticeType = userNoticeType;
            return this;
        }

        public Builder withMemberNo(String memberNo) {
            this.memberNo = memberNo;
            return this;
        }

        public Builder withMemberNoTime(Date memberNoTime) {
            this.memberNoTime = memberNoTime;
            return this;
        }

        public Builder withSignStatus(String signStatus) {
            this.signStatus = signStatus;
            return this;
        }

        public Builder withSignTime(Date signTime) {
            this.signTime = signTime;
            return this;
        }

        public Builder withContractSignUrl(String contractSignUrl) {
            this.contractSignUrl = contractSignUrl;
            return this;
        }

        public Builder withContractUrl(String contractUrl) {
            this.contractUrl = contractUrl;
            return this;
        }

        public Builder withContractName(String contractName) {
            this.contractName = contractName;
            return this;
        }

        public Builder withContractNo(String contractNo) {
            this.contractNo = contractNo;
            return this;
        }

        public TUserInfo build() {
            TUserInfo tUserInfo = new TUserInfo();
            tUserInfo.setCreateTime(createTime);
            tUserInfo.setUserId(userId);
            tUserInfo.setWechatId(wechatId);
            tUserInfo.setOpenId(openId);
            tUserInfo.setUpdateTime(updateTime);
            tUserInfo.setUserName(userName);
            tUserInfo.setUserImg(userImg);
            tUserInfo.setUserPhone(userPhone);
            tUserInfo.setPassword(password);
            tUserInfo.setUserStatus(userStatus);
            tUserInfo.setUserStatusShow(userStatusShow);
            tUserInfo.setBalanceSum(balanceSum);
            tUserInfo.setBalanceWithdraw(balanceWithdraw);
            tUserInfo.setBalanceUse(balanceUse);
            tUserInfo.setGoldSum(goldSum);
            tUserInfo.setGoldWithdraw(goldWithdraw);
            tUserInfo.setGoldExchange(goldExchange);
            tUserInfo.setIntegralSum(integralSum);
            tUserInfo.setIntegralExchange(integralExchange);
            tUserInfo.setDealGoldMark(dealGoldMark);
            tUserInfo.setDealGoldExchangeMark(dealGoldExchangeMark);
            tUserInfo.setDealGold(dealGold);
            tUserInfo.setAwardGoldMark(awardGoldMark);
            tUserInfo.setAwardGoldExchangeMark(awardGoldExchangeMark);
            tUserInfo.setAwardGold(awardGold);
            tUserInfo.setInviteImg(inviteImg);
            tUserInfo.setInviteCode(inviteCode);
            tUserInfo.setSuperiorUserId(superiorUserId);
            tUserInfo.setSuperiorUserName(superiorUserName);
            tUserInfo.setAutonymType(autonymType);
            tUserInfo.setAutonymTypeShow(autonymTypeShow);
            tUserInfo.setIdentityMark(identityMark);
            tUserInfo.setBankCardMark(bankCardMark);
            tUserInfo.setBankAddress(bankAddress);
            tUserInfo.setBankId(bankId);
            tUserInfo.setBankName(bankName);
            tUserInfo.setSubBranch(subBranch);
            tUserInfo.setIdentityZhengImg(identityZhengImg);
            tUserInfo.setIdentityFanImg(identityFanImg);
            tUserInfo.setBankCardZhengImg(bankCardZhengImg);
            tUserInfo.setGradeId(gradeId);
            tUserInfo.setAutonymTime(autonymTime);
            tUserInfo.setToken(token);
            tUserInfo.setRegistreTime(registreTime);
            tUserInfo.setAuditStatus(auditStatus);
            tUserInfo.setAuditStatusDescribe(auditStatusDescribe);
            tUserInfo.setSalt(salt);
            tUserInfo.setPaySalt(paySalt);
            tUserInfo.setPayPassword(payPassword);
            tUserInfo.setLng(lng);
            tUserInfo.setLat(lat);
            tUserInfo.setTotalTransAmount(totalTransAmount);
            tUserInfo.setVipTerminalSum(vipTerminalSum);
            tUserInfo.setVipDealMoney(vipDealMoney);
            tUserInfo.setDealAmountHasSend(dealAmountHasSend);
            tUserInfo.setAwardAmountHasSend(awardAmountHasSend);
            tUserInfo.setGradeCode(gradeCode);
            tUserInfo.setSupUserPhone(supUserPhone);
            tUserInfo.setTeamSum(teamSum);
            tUserInfo.setDirectlyNum(directlyNum);
            tUserInfo.setOwnTerminalNum(ownTerminalNum);
            tUserInfo.setDirectlyTerminalNum(directlyTerminalNum);
            tUserInfo.setMyTransNum(myTransNum);
            tUserInfo.setUserNoticeType(userNoticeType);
            tUserInfo.setMemberNo(memberNo);
            tUserInfo.setMemberNoTime(memberNoTime);
            tUserInfo.setSignStatus(signStatus);
            tUserInfo.setSignTime(signTime);
            tUserInfo.setContractSignUrl(contractSignUrl);
            tUserInfo.setContractUrl(contractUrl);
            tUserInfo.setContractName(contractName);
            tUserInfo.setContractNo(contractNo);
            return tUserInfo;
        }
    }
}
