package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 意见反馈图片对象 t_opinion_img_info
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public class TOpinionImgInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 意见反馈id */
    @Excel(name = "意见反馈id")
    private String opinionId;

    /** 意见图片 */
    @Excel(name = "意见图片")
    private String opinionImg;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setOpinionId(String opinionId) 
    {
        this.opinionId = opinionId;
    }

    public String getOpinionId() 
    {
        return opinionId;
    }
    public void setOpinionImg(String opinionImg) 
    {
        this.opinionImg = opinionImg;
    }

    public String getOpinionImg() 
    {
        return opinionImg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("opinionId", getOpinionId())
            .append("opinionImg", getOpinionImg())
            .toString();
    }
}
