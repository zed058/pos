package com.ruoyi.pos.web.mapper;

import java.util.List;

import com.ruoyi.pos.api.vo.user.UserOpinionVo;
import com.ruoyi.pos.web.domain.TOpinionInfo;

/**
 * 意见反馈Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface TOpinionInfoMapper 
{
    /**
     * 查询意见反馈
     * 
     * @param id 意见反馈主键
     * @return 意见反馈
     */
    public TOpinionInfo selectTOpinionInfoById(String id);

    /**
     * 查询意见反馈列表
     * 
     * @param tOpinionInfo 意见反馈
     * @return 意见反馈集合
     */
    public List<TOpinionInfo> selectTOpinionInfoList(TOpinionInfo tOpinionInfo);

    /**
     * 新增意见反馈
     * 
     * @param tOpinionInfo 意见反馈
     * @return 结果
     */
    public int insertTOpinionInfo(TOpinionInfo tOpinionInfo);

    /**
     * 修改意见反馈
     * 
     * @param tOpinionInfo 意见反馈
     * @return 结果
     */
    public int updateTOpinionInfo(TOpinionInfo tOpinionInfo);

    /**
     * 删除意见反馈
     * 
     * @param id 意见反馈主键
     * @return 结果
     */
    public int deleteTOpinionInfoById(String id);

    /**
     * 批量删除意见反馈
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTOpinionInfoByIds(String[] ids);

    public List<UserOpinionVo> selectTOpinionInfoListForVo(TOpinionInfo tOpinionInfo);
}
