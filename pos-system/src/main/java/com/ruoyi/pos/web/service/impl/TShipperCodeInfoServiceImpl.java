package com.ruoyi.pos.web.service.impl;

import java.util.List;

import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.pos.web.domain.TShipperCodeInfo;
import com.ruoyi.pos.web.mapper.TShipperCodeInfoMapper;
import com.ruoyi.pos.web.service.ITShipperCodeInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 物流公司编码Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-12-10
 */
@Service
public class TShipperCodeInfoServiceImpl implements ITShipperCodeInfoService
{
    @Autowired
    private TShipperCodeInfoMapper tShipperCodeInfoMapper;

    /**
     * 查询物流公司编码
     * 
     * @param id 物流公司编码主键
     * @return 物流公司编码
     */
    @Override
    public TShipperCodeInfo selectTShipperCodeInfoById(String id)
    {
        return tShipperCodeInfoMapper.selectTShipperCodeInfoById(id);
    }

    /**
     * 查询物流公司编码列表
     * 
     * @param tShipperCodeInfo 物流公司编码
     * @return 物流公司编码
     */
    @Override
    public List<TShipperCodeInfo> selectTShipperCodeInfoList(TShipperCodeInfo tShipperCodeInfo)
    {
        return tShipperCodeInfoMapper.selectTShipperCodeInfoList(tShipperCodeInfo);
    }

    /**
     * 新增物流公司编码
     * 
     * @param tShipperCodeInfo 物流公司编码
     * @return 结果
     */
    @Override
    public int insertTShipperCodeInfo(TShipperCodeInfo tShipperCodeInfo)
    {
        tShipperCodeInfo.setId(IdUtils.simpleUUID());
        return tShipperCodeInfoMapper.insertTShipperCodeInfo(tShipperCodeInfo);
    }

    /**
     * 修改物流公司编码
     * 
     * @param tShipperCodeInfo 物流公司编码
     * @return 结果
     */
    @Override
    public int updateTShipperCodeInfo(TShipperCodeInfo tShipperCodeInfo)
    {
        return tShipperCodeInfoMapper.updateTShipperCodeInfo(tShipperCodeInfo);
    }

    /**
     * 批量删除物流公司编码
     * 
     * @param ids 需要删除的物流公司编码主键
     * @return 结果
     */
    @Override
    public int deleteTShipperCodeInfoByIds(String[] ids)
    {
        return tShipperCodeInfoMapper.deleteTShipperCodeInfoByIds(ids);
    }

    /**
     * 删除物流公司编码信息
     * 
     * @param id 物流公司编码主键
     * @return 结果
     */
    @Override
    public int deleteTShipperCodeInfoById(String id)
    {
        return tShipperCodeInfoMapper.deleteTShipperCodeInfoById(id);
    }
}
