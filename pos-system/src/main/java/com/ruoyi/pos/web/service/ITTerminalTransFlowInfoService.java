package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TTerminalTransFlowInfo;

/**
 * 终端交易流水Service接口
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public interface ITTerminalTransFlowInfoService 
{
    /**
     * 查询终端交易流水
     * 
     * @param id 终端交易流水主键
     * @return 终端交易流水
     */
    public TTerminalTransFlowInfo selectTTerminalTransFlowInfoById(String id);

    /**
     * 查询终端交易流水列表
     * 
     * @param tTerminalTransFlowInfo 终端交易流水
     * @return 终端交易流水集合
     */
    public List<TTerminalTransFlowInfo> selectTTerminalTransFlowInfoList(TTerminalTransFlowInfo tTerminalTransFlowInfo);

    /**
     * 新增终端交易流水
     * 
     * @param tTerminalTransFlowInfo 终端交易流水
     * @return 结果
     */
    public int insertTTerminalTransFlowInfo(TTerminalTransFlowInfo tTerminalTransFlowInfo);

    /**
     * 修改终端交易流水
     * 
     * @param tTerminalTransFlowInfo 终端交易流水
     * @return 结果
     */
    public int updateTTerminalTransFlowInfo(TTerminalTransFlowInfo tTerminalTransFlowInfo);

    /**
     * 批量删除终端交易流水
     * 
     * @param ids 需要删除的终端交易流水主键集合
     * @return 结果
     */
    public int deleteTTerminalTransFlowInfoByIds(String[] ids);

    /**
     * 删除终端交易流水信息
     * 
     * @param id 终端交易流水主键
     * @return 结果
     */
    public int deleteTTerminalTransFlowInfoById(String id);
}
