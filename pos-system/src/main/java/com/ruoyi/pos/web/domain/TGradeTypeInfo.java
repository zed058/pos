package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;

import java.math.BigDecimal;
import java.util.List;

/**
 * vip跨级分润，直营分润，类型配置对象 t_grade_type_info
 *
 * @author ruoyi
 * @date 2021-11-24
 */
public class TGradeTypeInfo extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** vip表id */
    @Excel(name = "vip表id")
    private String gradeId;

    /** 类型（0:目录,1:vip配置） */
    @Excel(name = "类型", readConverterExp = "0=:目录,1:vip配置")
    private String gradeType;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private int sort;

    /** 名称(配置项) */
    @Excel(name = "名称(配置项)")
    private String gradeName;

    /** 状态（0：正常，1：禁用，2：删除） */
    @Excel(name = "状态", readConverterExp = "0=：正常，1：禁用，2：删除")
    private String isValid;

    /** 跨级分润百分百（分润差额） */
    @Excel(name = "跨级分润百分百", readConverterExp = "分=润差额")
    private BigDecimal shareProfitBalance;

    /** 直属分润百分百 */
    @Excel(name = "直属分润百分百")
    private BigDecimal shareProfit;

    /** 等额差之和 */
    @Excel(name = "等额差之和")
    private BigDecimal badRatio;

    @Excel(name = "vip名称")
    private String gradeVipName;

    private List<TGradeTypeInfo> childList;

    public List<TGradeTypeInfo> getChildList() {
        return childList;
    }

    public void setChildList(List<TGradeTypeInfo> childList) {
        this.childList = childList;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setGradeId(String gradeId)
    {
        this.gradeId = gradeId;
    }

    public String getGradeId()
    {
        return gradeId;
    }
    public void setGradeType(String gradeType)
    {
        this.gradeType = gradeType;
    }

    public String getGradeType()
    {
        return gradeType;
    }
    public void setSort(int sort)
    {
        this.sort = sort;
    }

    public int getSort()
    {
        return sort;
    }
    public void setGradeName(String gradeName)
    {
        this.gradeName = gradeName;
    }

    public String getGradeName()
    {
        return gradeName;
    }
    public void setIsValid(String isValid)
    {
        this.isValid = isValid;
    }

    public String getIsValid()
    {
        return isValid;
    }
    public void setShareProfitBalance(BigDecimal shareProfitBalance)
    {
        this.shareProfitBalance = shareProfitBalance;
    }

    public BigDecimal getShareProfitBalance()
    {
        return shareProfitBalance;
    }
    public void setShareProfit(BigDecimal shareProfit)
    {
        this.shareProfit = shareProfit;
    }

    public BigDecimal getShareProfit()
    {
        return shareProfit;
    }
    public void setBadRatio(BigDecimal badRatio)
    {
        this.badRatio = badRatio;
    }

    public BigDecimal getBadRatio()
    {
        return badRatio;
    }

    public String getGradeVipName() {
        return gradeVipName;
    }

    public void setGradeVipName(String gradeVipName) {
        this.gradeVipName = gradeVipName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("parentId", getParentId())
                .append("gradeId", getGradeId())
                .append("gradeType", getGradeType())
                .append("sort", getSort())
                .append("gradeName", getGradeName())
                .append("isValid", getIsValid())
                .append("shareProfitBalance", getShareProfitBalance())
                .append("shareProfit", getShareProfit())
                .append("badRatio", getBadRatio())
                .toString();
    }
}