package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TTerminalInfo;

/**
 * 用户商户终端关系Service接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface ITTerminalInfoService 
{
    /**
     * 查询用户商户终端关系
     * 
     * @param id 用户商户终端关系主键
     * @return 用户商户终端关系
     */
    public TTerminalInfo selectTTerminalInfoById(String id);

    /**
     * 查询用户商户终端关系列表
     * 
     * @param tTerminalInfo 用户商户终端关系
     * @return 用户商户终端关系集合
     */
    public List<TTerminalInfo> selectTTerminalInfoList(TTerminalInfo tTerminalInfo);

    /**
     * 新增用户商户终端关系
     * 
     * @param tTerminalInfo 用户商户终端关系
     * @return 结果
     */
    public int insertTTerminalInfo(TTerminalInfo tTerminalInfo);

    /**
     * 修改用户商户终端关系
     * 
     * @param tTerminalInfo 用户商户终端关系
     * @return 结果
     */
    public int updateTTerminalInfo(TTerminalInfo tTerminalInfo);

    /**
     * 批量删除用户商户终端关系
     * 
     * @param ids 需要删除的用户商户终端关系主键集合
     * @return 结果
     */
    public int deleteTTerminalInfoByIds(String[] ids);

    /**
     * 删除用户商户终端关系信息
     * 
     * @param id 用户商户终端关系主键
     * @return 结果
     */
    public int deleteTTerminalInfoById(String id);

    int transfer(TTerminalInfo tTerminalInfo);
}
