package com.ruoyi.pos.web.service.impl;

import java.util.List;

import com.ruoyi.common.enums.*;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.pos.web.domain.TTerminalTransInfo;
import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.mapper.TTerminalTransInfoMapper;
import com.ruoyi.pos.web.mapper.TUserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TTerminalInfoMapper;
import com.ruoyi.pos.web.domain.TTerminalInfo;
import com.ruoyi.pos.web.service.ITTerminalInfoService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户商户终端关系Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@Service
public class TTerminalInfoServiceImpl implements ITTerminalInfoService 
{
    @Autowired
    private TTerminalInfoMapper tTerminalInfoMapper;

    @Autowired
    private TTerminalTransInfoMapper transInfoMapper;
    @Autowired
    private TUserInfoMapper userInfoMapper;

    /**
     * 查询用户商户终端关系
     * 
     * @param id 用户商户终端关系主键
     * @return 用户商户终端关系
     */
    @Override
    public TTerminalInfo selectTTerminalInfoById(String id)
    {
        return tTerminalInfoMapper.selectTTerminalInfoById(id);
    }

    /**
     * 查询用户商户终端关系列表
     * 
     * @param tTerminalInfo 用户商户终端关系
     * @return 用户商户终端关系
     */
    @Override
    public List<TTerminalInfo> selectTTerminalInfoList(TTerminalInfo tTerminalInfo)
    {
        if (null != tTerminalInfo.getQueryAddTimeStr()) {
            tTerminalInfo.setAddTimeStart(tTerminalInfo.getQueryAddTimeStr()[0] + " 00:00:00");
            tTerminalInfo.setAddTimeEnd(tTerminalInfo.getQueryAddTimeStr()[1] + " 23:59:59");
        }
        List<TTerminalInfo> list = tTerminalInfoMapper.selectTTerminalInfoListForWeb(tTerminalInfo);
        list.stream().forEach(bean ->{
            if (StringUtils.isNotEmpty(bean.getBindingStatus())) { //翻译绑定状态
                bean.setBindingStatusShow(ActivateStatusEnum.getEnumsByCode(bean.getBindingStatus()).getBindValue());
            }
            if (StringUtils.isNotEmpty(bean.getActivateStatus())) { //翻译激活状态
                bean.setActivateStatusShow(ActivateStatusEnum.getEnumsByCode(bean.getActivateStatus()).getValue());
            }
        });
        return list;
    }

    /**
     * 新增用户商户终端关系
     * 
     * @param tTerminalInfo 用户商户终端关系
     * @return 结果
     */
    @Override
    public int insertTTerminalInfo(TTerminalInfo tTerminalInfo)
    {
        return tTerminalInfoMapper.insertTTerminalInfo(tTerminalInfo);
    }

    /**
     * 修改用户商户终端关系
     * 
     * @param tTerminalInfo 用户商户终端关系
     * @return 结果
     */
    @Override
    public int updateTTerminalInfo(TTerminalInfo tTerminalInfo)
    {
        return tTerminalInfoMapper.updateTTerminalInfo(tTerminalInfo);
    }

    /**
     * 批量删除用户商户终端关系
     * 
     * @param ids 需要删除的用户商户终端关系主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalInfoByIds(String[] ids)
    {
        return tTerminalInfoMapper.deleteTTerminalInfoByIds(ids);
    }

    /**
     * 删除用户商户终端关系信息
     * 
     * @param id 用户商户终端关系主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalInfoById(String id)
    {
        return tTerminalInfoMapper.deleteTTerminalInfoById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int transfer(TTerminalInfo tTerminalInfo) {
        if (StringUtils.isEmpty(tTerminalInfo.getUserId())) {
            throw new RuntimeException("请选择划拨接收人");
        }
        TTerminalInfo terminalInfo = null;
        TTerminalTransInfo transInfo = new TTerminalTransInfo();
        if (StringUtils.isNotEmpty(tTerminalInfo.getId())) {
            terminalInfo = tTerminalInfoMapper.selectTTerminalInfoByIdForTrans(tTerminalInfo.getId());
        } else if (null != tTerminalInfo.getIds()) {
            for (String id : tTerminalInfo.getIds()) {
                terminalInfo = tTerminalInfoMapper.selectTTerminalInfoByIdForTrans(id);
                transInfo = new TTerminalTransInfo();
                generateTransInfo(transInfo, tTerminalInfo, terminalInfo);
                transInfoMapper.insertTTerminalTransInfo(transInfo);
                terminalInfo.setTransStatus(TransStatusEnum.WAIT_CONFIIRM.getCode());
                tTerminalInfoMapper.updateTTerminalInfo(terminalInfo);
            }
            return tTerminalInfo.getIds().length;
        }
        generateTransInfo(transInfo, tTerminalInfo, terminalInfo);
        transInfoMapper.insertTTerminalTransInfo(transInfo);
        terminalInfo.setTransStatus(TransStatusEnum.WAIT_CONFIIRM.getCode());
        return tTerminalInfoMapper.updateTTerminalInfo(terminalInfo);
    }

    public void generateTransInfo(TTerminalTransInfo transInfo, TTerminalInfo tTerminalInfo, TTerminalInfo terminalInfo) {
        transInfo.setId(IdUtils.simpleUUID());
        transInfo.setSnCode(terminalInfo.getSnCode());
        transInfo.setTransOutUserId(terminalInfo.getUserId());
        if (StringUtils.isEmpty(terminalInfo.getUserId())) { //如果没有终端所有人，则默认取平台最初始的用户
            TUserInfo supUser = userInfoMapper.selectUserByInviteCode(userInfoMapper.selectInviteCodeForMin());
            transInfo.setTransOutUserId(supUser.getUserId());
        }
        transInfo.setTransInUserId(tTerminalInfo.getUserId());
        transInfo.setTransOutTime(DateUtils.getNowDate());
        transInfo.setTransInTime(DateUtils.getNowDate());
        transInfo.setTransStatus(TransStatusEnum.WAIT_CONFIIRM.getCode());
        transInfo.setTransType(YesOrNoEnums.YES.getCode());
        transInfo.setCreateTime(DateUtils.getNowDate());
        transInfo.setRemark(tTerminalInfo.getRemark());
    }
}
