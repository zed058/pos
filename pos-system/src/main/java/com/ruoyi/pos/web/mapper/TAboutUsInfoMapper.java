package com.ruoyi.pos.web.mapper;

import java.util.List;
import com.ruoyi.pos.web.domain.TAboutUsInfo;

/**
 * 平台设置（关于我们）Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public interface TAboutUsInfoMapper 
{
    /**
     * 查询平台设置（关于我们）
     * 
     * @param id 平台设置（关于我们）主键
     * @return 平台设置（关于我们）
     */
    public TAboutUsInfo selectTAboutUsInfoById(String id);

    /**
     * 查询平台设置（关于我们）列表
     * 
     * @param tAboutUsInfo 平台设置（关于我们）
     * @return 平台设置（关于我们）集合
     */
    public List<TAboutUsInfo> selectTAboutUsInfoList(TAboutUsInfo tAboutUsInfo);

    /**
     * 新增平台设置（关于我们）
     * 
     * @param tAboutUsInfo 平台设置（关于我们）
     * @return 结果
     */
    public int insertTAboutUsInfo(TAboutUsInfo tAboutUsInfo);

    /**
     * 修改平台设置（关于我们）
     * 
     * @param tAboutUsInfo 平台设置（关于我们）
     * @return 结果
     */
    public int updateTAboutUsInfo(TAboutUsInfo tAboutUsInfo);

    /**
     * 删除平台设置（关于我们）
     * 
     * @param id 平台设置（关于我们）主键
     * @return 结果
     */
    public int deleteTAboutUsInfoById(String id);

    /**
     * 批量删除平台设置（关于我们）
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTAboutUsInfoByIds(String[] ids);
}
