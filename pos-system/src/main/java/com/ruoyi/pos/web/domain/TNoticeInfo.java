package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 平台公告对象 t_notice_info
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public class TNoticeInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 消息标题 */
    @Excel(name = "消息标题")
    private String title;

    /** 消息内容 */
    @Excel(name = "消息内容")
    private String noticeContent;

    /** 创建人 */
    @Excel(name = "创建人")
    private Long createUserId;

    /** 状态（0：正常，1：禁用） */
    @Excel(name = "状态", readConverterExp = "0=：正常，1：禁用")
    private String isValid;

    /** 0未发布 1 已发布 */
    @Excel(name = "0未发布 1 已发布")
    private String issue;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setNoticeContent(String noticeContent) 
    {
        this.noticeContent = noticeContent;
    }

    public String getNoticeContent() 
    {
        return noticeContent;
    }
    public void setCreateUserId(Long createUserId) 
    {
        this.createUserId = createUserId;
    }

    public Long getCreateUserId() 
    {
        return createUserId;
    }
    public void setIsValid(String isValid) 
    {
        this.isValid = isValid;
    }

    public String getIsValid() 
    {
        return isValid;
    }
    public void setIssue(String issue) 
    {
        this.issue = issue;
    }

    public String getIssue() 
    {
        return issue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("noticeContent", getNoticeContent())
            .append("createUserId", getCreateUserId())
            .append("createTime", getCreateTime())
            .append("isValid", getIsValid())
            .append("issue", getIssue())
            .toString();
    }
}
