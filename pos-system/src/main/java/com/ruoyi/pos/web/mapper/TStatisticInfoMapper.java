package com.ruoyi.pos.web.mapper;

import java.util.List;

import com.ruoyi.pos.api.dto.user.DataStatisticsDto;
import com.ruoyi.pos.api.vo.user.DataStatisticsSumVo;
import com.ruoyi.pos.api.vo.user.DataStatisticsVo;
import com.ruoyi.pos.web.domain.TStatisticInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 统计信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public interface TStatisticInfoMapper 
{
    /**
     * 查询统计信息
     * 
     * @param id 统计信息主键
     * @return 统计信息
     */
    public TStatisticInfo selectTStatisticInfoById(String id);

    /**
     * 查询统计信息列表
     * 
     * @param tStatisticInfo 统计信息
     * @return 统计信息集合
     */
    public List<TStatisticInfo> selectTStatisticInfoList(TStatisticInfo tStatisticInfo);

    /**
     * 新增统计信息
     * 
     * @param tStatisticInfo 统计信息
     * @return 结果
     */
    public int insertTStatisticInfo(TStatisticInfo tStatisticInfo);

    /**
     * 修改统计信息
     * 
     * @param tStatisticInfo 统计信息
     * @return 结果
     */
    public int updateTStatisticInfo(TStatisticInfo tStatisticInfo);

    /**
     * 删除统计信息
     * 
     * @param id 统计信息主键
     * @return 结果
     */
    public int deleteTStatisticInfoById(String id);

    /**
     * 批量删除统计信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTStatisticInfoByIds(String[] ids);

    //---------------------------------------------------------------------------------------------------------------------------

    /**
     * 查询用户当天的统计数据
     * @param userId 用户id
     * @param brandId 品牌id
     * @return
     */
    TStatisticInfo selectTStatisticInfoForCurrentDay(@Param("userId") String userId, @Param("brandId") String brandId);
    TStatisticInfo selectTStatisticInfoByUserIds(@Param("userIds") List<String> userIds, @Param("brandId") String brandId, @Param("startTimeStr") String startTimeStr, @Param("endTimeStr") String endTimeStr);
    TStatisticInfo selectTStatisticInfoForTotal();
    /*app统计图*/
    public List<DataStatisticsVo> queryTStatistic(DataStatisticsDto tStatisticInfo);
    /*数据统计*/
    public DataStatisticsVo queryTStatisticSum(DataStatisticsDto tStatisticInfo);
    /*数据统计日纬度月维度*/
    public List<DataStatisticsSumVo> queryTStatisticMonthAndDay(DataStatisticsDto tStatisticInfo);

    public List<DataStatisticsSumVo> queryTStatisticMonthAndDayMore(DataStatisticsDto tStatisticInfo);
}
