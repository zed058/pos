package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TTerminalActivateSetInfo;

/**
 * 终端激活配置Service接口
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public interface ITTerminalActivateSetInfoService 
{
    /**
     * 查询终端激活配置
     * 
     * @param id 终端激活配置主键
     * @return 终端激活配置
     */
    public TTerminalActivateSetInfo selectTTerminalActivateSetInfoById(String id);

    /**
     * 查询终端激活配置列表
     * 
     * @param tTerminalActivateSetInfo 终端激活配置
     * @return 终端激活配置集合
     */
    public List<TTerminalActivateSetInfo> selectTTerminalActivateSetInfoList(TTerminalActivateSetInfo tTerminalActivateSetInfo);

    /**
     * 新增终端激活配置
     * 
     * @param tTerminalActivateSetInfo 终端激活配置
     * @return 结果
     */
    public int insertTTerminalActivateSetInfo(TTerminalActivateSetInfo tTerminalActivateSetInfo);

    /**
     * 修改终端激活配置
     * 
     * @param tTerminalActivateSetInfo 终端激活配置
     * @return 结果
     */
    public int updateTTerminalActivateSetInfo(TTerminalActivateSetInfo tTerminalActivateSetInfo);

    /**
     * 批量删除终端激活配置
     * 
     * @param ids 需要删除的终端激活配置主键集合
     * @return 结果
     */
    public int deleteTTerminalActivateSetInfoByIds(String[] ids);

    /**
     * 删除终端激活配置信息
     * 
     * @param id 终端激活配置主键
     * @return 结果
     */
    public int deleteTTerminalActivateSetInfoById(String id);
}
