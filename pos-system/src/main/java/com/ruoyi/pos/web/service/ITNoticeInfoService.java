package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TNoticeInfo;

/**
 * 平台公告Service接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface ITNoticeInfoService 
{
    /**
     * 查询平台公告
     * 
     * @param id 平台公告主键
     * @return 平台公告
     */
    public TNoticeInfo selectTNoticeInfoById(String id);

    /**
     * 查询平台公告列表
     * 
     * @param tNoticeInfo 平台公告
     * @return 平台公告集合
     */
    public List<TNoticeInfo> selectTNoticeInfoList(TNoticeInfo tNoticeInfo);

    /**
     * 新增平台公告
     * 
     * @param tNoticeInfo 平台公告
     * @return 结果
     */
    public int insertTNoticeInfo(TNoticeInfo tNoticeInfo);

    /**
     * 修改平台公告
     * 
     * @param tNoticeInfo 平台公告
     * @return 结果
     */
    public int updateTNoticeInfo(TNoticeInfo tNoticeInfo);

    /**
     * 批量删除平台公告
     * 
     * @param ids 需要删除的平台公告主键集合
     * @return 结果
     */
    public int deleteTNoticeInfoByIds(String[] ids);

    /**
     * 删除平台公告信息
     * 
     * @param id 平台公告主键
     * @return 结果
     */
    public int deleteTNoticeInfoById(String id);
}
