package com.ruoyi.pos.web.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 终端划拨记录对象 t_terminal_trans_info
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public class TTerminalTransInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 终端SN号 */
    @Excel(name = "终端SN号")
    private String snCode;

    /** 划出人 */
    @Excel(name = "划出人")
    private String transOutUserId;

    /** 接收人 */
    @Excel(name = "接收人")
    private String transInUserId;

    /** 划出时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "划出时间", width = 30, dateFormat = "yyyy-MM-dd hh:mm:ss")
    private Date transOutTime;

    /** 接收时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "接收时间", width = 30, dateFormat = "yyyy-MM-dd hh:mm:ss")
    private Date transInTime;

    /** 划拨状态（0、待确认，1、已确认，2、拒绝) */
    @Excel(name = "划拨状态", readConverterExp = "划拨状态（10、待确认，20、已确认，30、拒绝)")
    private String transStatus;
    private String transStatusShow;

    /*划拨方式（0、划拨，1、划回）*/
    private String transType;

    /*划出人*/
    private String outUserName;

    /*接收人*/
    private String inUserName;

    /*用户id*/
    private String userId;

    public String getTransStatusShow() {
        return transStatusShow;
    }

    public void setTransStatusShow(String transStatusShow) {
        this.transStatusShow = transStatusShow;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setSnCode(String snCode) 
    {
        this.snCode = snCode;
    }

    public String getSnCode() 
    {
        return snCode;
    }
    public void setTransOutUserId(String transOutUserId) 
    {
        this.transOutUserId = transOutUserId;
    }

    public String getTransOutUserId() 
    {
        return transOutUserId;
    }
    public void setTransInUserId(String transInUserId) 
    {
        this.transInUserId = transInUserId;
    }

    public String getTransInUserId() 
    {
        return transInUserId;
    }
    public void setTransOutTime(Date transOutTime) 
    {
        this.transOutTime = transOutTime;
    }

    public Date getTransOutTime() 
    {
        return transOutTime;
    }
    public void setTransInTime(Date transInTime) 
    {
        this.transInTime = transInTime;
    }

    public Date getTransInTime() 
    {
        return transInTime;
    }
    public void setTransStatus(String transStatus) 
    {
        this.transStatus = transStatus;
    }

    public String getTransStatus() 
    {
        return transStatus;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getOutUserName() {
        return outUserName;
    }

    public void setOutUserName(String outUserName) {
        this.outUserName = outUserName;
    }

    public String getInUserName() {
        return inUserName;
    }

    public void setInUserName(String inUserName) {
        this.inUserName = inUserName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("snCode", getSnCode())
            .append("transOutUserId", getTransOutUserId())
            .append("transInUserId", getTransInUserId())
            .append("transOutTime", getTransOutTime())
            .append("transInTime", getTransInTime())
            .append("transStatus", getTransStatus())
            .append("createTime", getCreateTime())
            .toString();
    }
}
