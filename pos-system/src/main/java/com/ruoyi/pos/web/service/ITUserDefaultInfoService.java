package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TUserDefaultInfo;

/**
 * 用户出入账信息Service接口
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public interface ITUserDefaultInfoService 
{
    /**
     * 查询用户出入账信息
     * 
     * @param id 用户出入账信息主键
     * @return 用户出入账信息
     */
    public TUserDefaultInfo selectTUserDefaultInfoById(String id);

    /**
     * 查询用户出入账信息列表
     * 
     * @param tUserDefaultInfo 用户出入账信息
     * @return 用户出入账信息集合
     */
    public List<TUserDefaultInfo> selectTUserDefaultInfoList(TUserDefaultInfo tUserDefaultInfo);

    /**
     * 新增用户出入账信息
     * 
     * @param tUserDefaultInfo 用户出入账信息
     * @return 结果
     */
    public int insertTUserDefaultInfo(TUserDefaultInfo tUserDefaultInfo);

    /**
     * 修改用户出入账信息
     * 
     * @param tUserDefaultInfo 用户出入账信息
     * @return 结果
     */
    public int updateTUserDefaultInfo(TUserDefaultInfo tUserDefaultInfo);

    /**
     * 批量删除用户出入账信息
     * 
     * @param ids 需要删除的用户出入账信息主键集合
     * @return 结果
     */
    public int deleteTUserDefaultInfoByIds(String[] ids);

    /**
     * 删除用户出入账信息信息
     * 
     * @param id 用户出入账信息主键
     * @return 结果
     */
    public int deleteTUserDefaultInfoById(String id);

    //提现审核通过
    int checkPass(TUserDefaultInfo tUserDefaultInfo) throws Exception;
    //提现审核不通过
    int checkRefuse(TUserDefaultInfo tUserDefaultInfo) throws Exception;

    List<TUserDefaultInfo> selectRecordList(TUserDefaultInfo userDefaultInfo);

    List<TUserDefaultInfo>  exchangeRecordList(TUserDefaultInfo userDefaultInfo);
}
