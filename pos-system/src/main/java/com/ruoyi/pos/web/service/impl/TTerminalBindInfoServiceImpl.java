package com.ruoyi.pos.web.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TTerminalBindInfoMapper;
import com.ruoyi.pos.web.domain.TTerminalBindInfo;
import com.ruoyi.pos.web.service.ITTerminalBindInfoService;

/**
 * 终端绑定临时（第三方杉德）Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@Service
public class TTerminalBindInfoServiceImpl implements ITTerminalBindInfoService 
{
    @Autowired
    private TTerminalBindInfoMapper tTerminalBindInfoMapper;

    /**
     * 查询终端绑定临时（第三方杉德）
     * 
     * @param id 终端绑定临时（第三方杉德）主键
     * @return 终端绑定临时（第三方杉德）
     */
    @Override
    public TTerminalBindInfo selectTTerminalBindInfoById(String id)
    {
        return tTerminalBindInfoMapper.selectTTerminalBindInfoById(id);
    }

    /**
     * 查询终端绑定临时（第三方杉德）列表
     * 
     * @param tTerminalBindInfo 终端绑定临时（第三方杉德）
     * @return 终端绑定临时（第三方杉德）
     */
    @Override
    public List<TTerminalBindInfo> selectTTerminalBindInfoList(TTerminalBindInfo tTerminalBindInfo)
    {
        return tTerminalBindInfoMapper.selectTTerminalBindInfoList(tTerminalBindInfo);
    }

    /**
     * 新增终端绑定临时（第三方杉德）
     * 
     * @param tTerminalBindInfo 终端绑定临时（第三方杉德）
     * @return 结果
     */
    @Override
    public int insertTTerminalBindInfo(TTerminalBindInfo tTerminalBindInfo)
    {
        tTerminalBindInfo.setCreateTime(DateUtils.getNowDate());
        return tTerminalBindInfoMapper.insertTTerminalBindInfo(tTerminalBindInfo);
    }

    /**
     * 修改终端绑定临时（第三方杉德）
     * 
     * @param tTerminalBindInfo 终端绑定临时（第三方杉德）
     * @return 结果
     */
    @Override
    public int updateTTerminalBindInfo(TTerminalBindInfo tTerminalBindInfo)
    {
        return tTerminalBindInfoMapper.updateTTerminalBindInfo(tTerminalBindInfo);
    }

    /**
     * 批量删除终端绑定临时（第三方杉德）
     * 
     * @param ids 需要删除的终端绑定临时（第三方杉德）主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalBindInfoByIds(String[] ids)
    {
        return tTerminalBindInfoMapper.deleteTTerminalBindInfoByIds(ids);
    }

    /**
     * 删除终端绑定临时（第三方杉德）信息
     * 
     * @param id 终端绑定临时（第三方杉德）主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalBindInfoById(String id)
    {
        return tTerminalBindInfoMapper.deleteTTerminalBindInfoById(id);
    }
}
