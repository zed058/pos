package com.ruoyi.pos.web;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author luobo
 * @title: TUserInfoBean
 * @projectName pos
 * @description: TODO
 * @date 2022-01-19 09:24:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TUserInfoBean extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 用户id */
    private String userId;

    /** 用户名 */
    @Excel(name = "用户名")
    private String userName;

    /** 用户头像 */
    private String userImg;

    /** 用户手机号 */
    @Excel(name = "用户手机号")
    private String userPhone;


    /** 身份证号 */
    @Excel(name = "身份证号")
    private String identityMark;

    /** 银行卡号 */
    @Excel(name = "银行卡号")
    private String bankCardMark;

    /** 银行名称 */
    private String bankId;
    @Excel(name = "银行名称")
    private String bankName;

    /** 支行名称 */
    //@Excel(name = "支行名称")
    private String subBranch;

    /** 身份证照片正面 */
    //@Excel(name = "身份证照片正面")
    private String identityZhengImg;

    /** 身份证照片反面 */
    //@Excel(name = "身份证照片反面")
    private String identityFanImg;

    /** 银行卡号正面 */
    //@Excel(name = "银行卡号正面")
    private String bankCardZhengImg;

    /** 用户等级id */
    private String gradeId;

    /** 实名认证时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date autonymTime;

    /** token */
    private String token;

    /** token失效时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date registreTime;

    /** 实名认证（0:待审核，1：审核通过，2：审核驳回） */
    private String auditStatus;
    //@Excel(name = "实名认证", readConverterExp = "0=:待审核，1：审核通过，2：审核驳回")
    private String auditStatusDescribe;
}
