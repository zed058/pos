package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 交易金/奖励金（排行榜）回台添加app显示的假数据对象 t_gold_ranking_info
 * 
 * @author ruoyi
 * @date 2021-11-20
 */
public class TGoldRankingInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 头像 */
    @Excel(name = "头像")
    private String userImg;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;

    /** (0:交易金,1:奖励金) */
    @Excel(name = "(0:交易金,1:奖励金)")
    private String dealAwardType;

    /** 交易金/奖励金（金额） */
    @Excel(name = "交易金/奖励金", readConverterExp = "金=额")
    private BigDecimal dealAwardGold;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUserImg(String userImg) 
    {
        this.userImg = userImg;
    }

    public String getUserImg() 
    {
        return userImg;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setDealAwardType(String dealAwardType) 
    {
        this.dealAwardType = dealAwardType;
    }

    public String getDealAwardType() 
    {
        return dealAwardType;
    }
    public void setDealAwardGold(BigDecimal dealAwardGold) 
    {
        this.dealAwardGold = dealAwardGold;
    }

    public BigDecimal getDealAwardGold() 
    {
        return dealAwardGold;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userImg", getUserImg())
            .append("userName", getUserName())
            .append("dealAwardType", getDealAwardType())
            .append("dealAwardGold", getDealAwardGold())
            .toString();
    }
}
