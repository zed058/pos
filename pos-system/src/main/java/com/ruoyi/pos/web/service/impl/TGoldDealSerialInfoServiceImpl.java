package com.ruoyi.pos.web.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TGoldDealSerialInfoMapper;
import com.ruoyi.pos.web.domain.TGoldDealSerialInfo;
import com.ruoyi.pos.web.service.ITGoldDealSerialInfoService;

/**
 * (金币序列号Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-20
 */
@Service
public class TGoldDealSerialInfoServiceImpl implements ITGoldDealSerialInfoService 
{
    @Autowired
    private TGoldDealSerialInfoMapper tGoldDealSerialInfoMapper;

    /**
     * 查询(金币序列号
     * 
     * @param id (金币序列号主键
     * @return (金币序列号
     */
    @Override
    public TGoldDealSerialInfo selectTGoldDealSerialInfoById(Long id)
    {
        return tGoldDealSerialInfoMapper.selectTGoldDealSerialInfoById(id);
    }

    /**
     * 查询(金币序列号列表
     * 
     * @param tGoldDealSerialInfo (金币序列号
     * @return (金币序列号
     */
    @Override
    public List<TGoldDealSerialInfo> selectTGoldDealSerialInfoList(TGoldDealSerialInfo tGoldDealSerialInfo)
    {
        return tGoldDealSerialInfoMapper.selectTGoldDealSerialInfoList(tGoldDealSerialInfo);
    }

    /**
     * 新增(金币序列号
     * 
     * @param tGoldDealSerialInfo (金币序列号
     * @return 结果
     */
    @Override
    public int insertTGoldDealSerialInfo(TGoldDealSerialInfo tGoldDealSerialInfo)
    {
        tGoldDealSerialInfo.setCreateTime(DateUtils.getNowDate());
        return tGoldDealSerialInfoMapper.insertTGoldDealSerialInfo(tGoldDealSerialInfo);
    }

    /**
     * 修改(金币序列号
     * 
     * @param tGoldDealSerialInfo (金币序列号
     * @return 结果
     */
    @Override
    public int updateTGoldDealSerialInfo(TGoldDealSerialInfo tGoldDealSerialInfo)
    {
        return tGoldDealSerialInfoMapper.updateTGoldDealSerialInfo(tGoldDealSerialInfo);
    }

    /**
     * 批量删除(金币序列号
     * 
     * @param ids 需要删除的(金币序列号主键
     * @return 结果
     */
    @Override
    public int deleteTGoldDealSerialInfoByIds(Long[] ids)
    {
        return tGoldDealSerialInfoMapper.deleteTGoldDealSerialInfoByIds(ids);
    }

    /**
     * 删除(金币序列号信息
     * 
     * @param id (金币序列号主键
     * @return 结果
     */
    @Override
    public int deleteTGoldDealSerialInfoById(Long id)
    {
        return tGoldDealSerialInfoMapper.deleteTGoldDealSerialInfoById(id);
    }
}
