package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品规格对象 t_terminal_goods_spe_info
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public class TTerminalGoodsSpeInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 商品id */
    @Excel(name = "商品id")
    private String goodsId;

    /** 商品数量 */
    @Excel(name = "商品数量")
    private BigDecimal goodsNum;

    /** 规格名称 */
    @Excel(name = "规格名称")
    private String specification;

    /** 规格金额 */
    @Excel(name = "规格金额")
    private BigDecimal amount;

    /** 配送费 */
    @Excel(name = "配送费")
    private BigDecimal deliveryFee;

    /*商品名称*/
    private String goodsName;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setGoodsId(String goodsId) 
    {
        this.goodsId = goodsId;
    }

    public String getGoodsId() 
    {
        return goodsId;
    }

    public BigDecimal getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(BigDecimal goodsNum) {
        this.goodsNum = goodsNum;
    }

    public void setSpecification(String specification)
    {
        this.specification = specification;
    }

    public String getSpecification() 
    {
        return specification;
    }
    public void setAmount(BigDecimal amount) 
    {
        this.amount = amount;
    }

    public BigDecimal getAmount() 
    {
        return amount;
    }
    public void setDeliveryFee(BigDecimal deliveryFee) 
    {
        this.deliveryFee = deliveryFee;
    }

    public BigDecimal getDeliveryFee() 
    {
        return deliveryFee;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsId", getGoodsId())
            .append("goodsNum", getGoodsNum())
            .append("specification", getSpecification())
            .append("amount", getAmount())
            .append("deliveryFee", getDeliveryFee())
            .append("createTime", getCreateTime())
            .append("goodsName", getGoodsName())
            .toString();
    }
}
