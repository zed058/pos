package com.ruoyi.pos.web.mapper;

import java.util.List;
import com.ruoyi.pos.web.domain.TTerminalTransFlowShareInfo;

/**
 * 第三方交易流水分润详情Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public interface TTerminalTransFlowShareInfoMapper 
{
    /**
     * 查询第三方交易流水分润详情
     * 
     * @param id 第三方交易流水分润详情主键
     * @return 第三方交易流水分润详情
     */
    public TTerminalTransFlowShareInfo selectTTerminalTransFlowShareInfoById(String id);

    /**
     * 查询第三方交易流水分润详情列表
     * 
     * @param tTerminalTransFlowShareInfo 第三方交易流水分润详情
     * @return 第三方交易流水分润详情集合
     */
    public List<TTerminalTransFlowShareInfo> selectTTerminalTransFlowShareInfoList(TTerminalTransFlowShareInfo tTerminalTransFlowShareInfo);

    /**
     * 新增第三方交易流水分润详情
     * 
     * @param tTerminalTransFlowShareInfo 第三方交易流水分润详情
     * @return 结果
     */
    public int insertTTerminalTransFlowShareInfo(TTerminalTransFlowShareInfo tTerminalTransFlowShareInfo);

    /**
     * 修改第三方交易流水分润详情
     * 
     * @param tTerminalTransFlowShareInfo 第三方交易流水分润详情
     * @return 结果
     */
    public int updateTTerminalTransFlowShareInfo(TTerminalTransFlowShareInfo tTerminalTransFlowShareInfo);

    /**
     * 删除第三方交易流水分润详情
     * 
     * @param id 第三方交易流水分润详情主键
     * @return 结果
     */
    public int deleteTTerminalTransFlowShareInfoById(String id);

    /**
     * 批量删除第三方交易流水分润详情
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTerminalTransFlowShareInfoByIds(String[] ids);



    public List<TTerminalTransFlowShareInfo> queryTransFlowUser(TTerminalTransFlowShareInfo tTerminalTransFlowShareInfo);

}
