package com.ruoyi.pos.web.weVo;

import lombok.Data;

/**
 * @author Administrator
 * @title: 统计图专用
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class HomePageVo {
    private String createTime;//时间
    private String userSum;//用户数量
    private String orderSum;//订单数量
    private String goodsNum;//商品数量
}
