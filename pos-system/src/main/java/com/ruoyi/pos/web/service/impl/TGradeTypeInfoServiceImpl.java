package com.ruoyi.pos.web.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.ruoyi.common.enums.YesOrNoEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TGradeTypeInfoMapper;
import com.ruoyi.pos.web.domain.TGradeTypeInfo;
import com.ruoyi.pos.web.service.ITGradeTypeInfoService;
import org.springframework.transaction.annotation.Transactional;

/**
 * vip跨级分润，直营分润，类型配置Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@Service
public class TGradeTypeInfoServiceImpl implements ITGradeTypeInfoService 
{
    @Autowired
    private TGradeTypeInfoMapper tGradeTypeInfoMapper;

    /**
     * 查询vip跨级分润，直营分润，类型配置
     * 
     * @param id vip跨级分润，直营分润，类型配置主键
     * @return vip跨级分润，直营分润，类型配置
     */
    @Override
    public TGradeTypeInfo selectTGradeTypeInfoById(Long id)
    {
        TGradeTypeInfo mapper = new TGradeTypeInfo();
        mapper.setParentId(id);
        List<TGradeTypeInfo> childList = tGradeTypeInfoMapper.selectTGradeTypeInfoList(mapper);
        TGradeTypeInfo type = tGradeTypeInfoMapper.selectTGradeTypeInfoById(id);
        type.setChildList(childList);
        return type;
    }

    /**
     * 查询vip跨级分润，直营分润，类型配置列表
     * 
     * @param tGradeTypeInfo vip跨级分润，直营分润，类型配置
     * @return vip跨级分润，直营分润，类型配置
     */
    @Override
    public List<TGradeTypeInfo> selectTGradeTypeInfoList(TGradeTypeInfo tGradeTypeInfo)
    {
        return tGradeTypeInfoMapper.selectTGradeTypeInfoList(tGradeTypeInfo);
    }

    /**
     * 新增vip跨级分润，直营分润，类型配置
     * 
     * @param tGradeTypeInfo vip跨级分润，直营分润，类型配置
     * @return 结果
     */
    @Override
    public int insertTGradeTypeInfo(TGradeTypeInfo tGradeTypeInfo)
    {
        return tGradeTypeInfoMapper.insertTGradeTypeInfo(tGradeTypeInfo);
    }

    /**
     * 修改vip跨级分润，直营分润，类型配置
     * 
     * @param tGradeTypeInfo vip跨级分润，直营分润，类型配置
     * @return 结果
     */
    @Override
    @Transactional
    public int updateTGradeTypeInfo(TGradeTypeInfo tGradeTypeInfo)
    {
        if (null == tGradeTypeInfo.getBadRatio()) {
            throw new RuntimeException("请设置分润等额差之和");
        }
        BigDecimal badRatio = tGradeTypeInfo.getBadRatio();
        Optional.ofNullable(tGradeTypeInfo.getChildList()).ifPresent(list ->{
            list.stream().forEach(child ->{
                if (null != child.getShareProfitBalance()) {
                    tGradeTypeInfo.setBadRatio(tGradeTypeInfo.getBadRatio().subtract(child.getShareProfitBalance()));
                } else {
                    child.setShareProfitBalance(BigDecimal.ZERO);
                }
            });
        });
        if (tGradeTypeInfo.getBadRatio().compareTo(BigDecimal.ZERO) != 0) {
            throw new RuntimeException("跨级分润百分比必须等于等额差之和");
        }
        Optional.ofNullable(tGradeTypeInfo.getChildList()).ifPresent(list ->{
            list.stream().forEach(child ->{
                tGradeTypeInfoMapper.updateTGradeTypeInfo(child);
            });
        });
        tGradeTypeInfo.setBadRatio(badRatio);
        return tGradeTypeInfoMapper.updateTGradeTypeInfo(tGradeTypeInfo);
    }

    /**
     * 批量删除vip跨级分润，直营分润，类型配置
     * 
     * @param ids 需要删除的vip跨级分润，直营分润，类型配置主键
     * @return 结果
     */
    @Override
    public int deleteTGradeTypeInfoByIds(Long[] ids)
    {
        return tGradeTypeInfoMapper.deleteTGradeTypeInfoByIds(ids);
    }

    /**
     * 删除vip跨级分润，直营分润，类型配置信息
     * 
     * @param id vip跨级分润，直营分润，类型配置主键
     * @return 结果
     */
    @Override
    public int deleteTGradeTypeInfoById(Long id)
    {
        return tGradeTypeInfoMapper.deleteTGradeTypeInfoById(id);
    }
}
