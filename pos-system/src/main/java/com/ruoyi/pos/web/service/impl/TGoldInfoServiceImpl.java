package com.ruoyi.pos.web.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TGoldInfoMapper;
import com.ruoyi.pos.web.domain.TGoldInfo;
import com.ruoyi.pos.web.service.ITGoldInfoService;

/**
 * 交易金序列号Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@Service
public class TGoldInfoServiceImpl implements ITGoldInfoService 
{
    @Autowired
    private TGoldInfoMapper tGoldInfoMapper;

    /**
     * 查询交易金序列号
     * 
     * @param id 交易金序列号主键
     * @return 交易金序列号
     */
    @Override
    public TGoldInfo selectTGoldInfoById(Long id)
    {
        return tGoldInfoMapper.selectTGoldInfoById(id);
    }

    /**
     * 查询交易金序列号列表
     * 
     * @param tGoldInfo 交易金序列号
     * @return 交易金序列号
     */
    @Override
    public List<TGoldInfo> selectTGoldInfoList(TGoldInfo tGoldInfo)
    {
        return tGoldInfoMapper.selectTGoldInfoList(tGoldInfo);
    }

    /**
     * 新增交易金序列号
     * 
     * @param tGoldInfo 交易金序列号
     * @return 结果
     */
    @Override
    public int insertTGoldInfo(TGoldInfo tGoldInfo)
    {
        tGoldInfo.setCreateTime(DateUtils.getNowDate());
        return tGoldInfoMapper.insertTGoldInfo(tGoldInfo);
    }

    /**
     * 修改交易金序列号
     * 
     * @param tGoldInfo 交易金序列号
     * @return 结果
     */
    @Override
    public int updateTGoldInfo(TGoldInfo tGoldInfo)
    {
        return tGoldInfoMapper.updateTGoldInfo(tGoldInfo);
    }

    /**
     * 批量删除交易金序列号
     * 
     * @param ids 需要删除的交易金序列号主键
     * @return 结果
     */
    @Override
    public int deleteTGoldInfoByIds(Long[] ids)
    {
        return tGoldInfoMapper.deleteTGoldInfoByIds(ids);
    }

    /**
     * 删除交易金序列号信息
     * 
     * @param id 交易金序列号主键
     * @return 结果
     */
    @Override
    public int deleteTGoldInfoById(Long id)
    {
        return tGoldInfoMapper.deleteTGoldInfoById(id);
    }
}
