package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 统计信息对象 t_statistic_info
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public class TStatisticInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;
    private String userName;

    /** 用户会员级别 */
    @Excel(name = "用户会员级别")
    private String userVip;

    /** 交易时间（以接口交易时间为准，要确定接口是否有返回） */
    @Excel(name = "交易时间", readConverterExp = "以=接口交易时间为准，要确定接口是否有返回")
    private Date transTime;

    /** 终端类别 */
    @Excel(name = "终端品牌id")
    private String brandId;

    /** 我的交易总额（拿到接口数据时写入，时间维度，需要花生米确定，是以接口时间为准还是我们接收时间为准） */
    @Excel(name = "我的交易总额", readConverterExp = "拿=到接口数据时写入，时间维度，需要花生米确定，是以接口时间为准还是我们接收时间为准")
    private BigDecimal myTransMoney;

    /** 我的交易笔数（拿到接口数据时写入，时间维度，需要花生米确定，是以接口时间为准还是我们接收时间为准） */
    @Excel(name = "我的交易笔数", readConverterExp = "拿=到接口数据时写入，时间维度，需要花生米确定，是以接口时间为准还是我们接收时间为准")
    private BigDecimal myTransNum;

    /** 我的终端数（购买付款、兑换付款、划拨确认为时间节点，写入数据） */
    @Excel(name = "我的终端数", readConverterExp = "购=买付款、兑换付款、划拨确认为时间节点，写入数据")
    private BigDecimal myTerminalNum;

    /** 我的伙伴数（直接统计用户团队表数据，此处不再重复记录、冗余） */
    @Excel(name = "我的伙伴数", readConverterExp = "直=接统计用户团队表数据，此处不再重复记录、冗余")
    private BigDecimal myPartnerNum;

    /** 我的购买终端数（购买付款时写入，终端管理，库存那里有显示，只统计，不写入详细） */
    @Excel(name = "我的购买终端数", readConverterExp = "购=买付款时写入，终端管理，库存那里有显示，只统计，不写入详细")
    private BigDecimal myBuyTerminalNum;

    /** 我的兑换终端数（兑换终端，付邮费时写入，终端管理，库存那里有显示，只统计，不写入详细） */
    @Excel(name = "我的兑换终端数", readConverterExp = "兑=换终端，付邮费时写入，终端管理，库存那里有显示，只统计，不写入详细")
    private BigDecimal myExchangelTerminalNum;

    /** 我的划出终端数（划入确认时写 */
    @Excel(name = "我的划出终端数", readConverterExp = "我的划出终端数（划入确认时写")
    private BigDecimal myOutTerminalNum;

    /** 我的划入终端数（划入确认时写） */
    @Excel(name = "我的划入终端数", readConverterExp = "划=入确认时写")
    private BigDecimal myInTerminalNum;

    /** 我的划入终端数（划入确认时写） */
    @Excel(name = "我的划入终端数", readConverterExp = "划=入确认时写")
    private String time;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setUserVip(String userVip) 
    {
        this.userVip = userVip;
    }

    public String getUserVip() 
    {
        return userVip;
    }
    public void setTransTime(Date transTime) 
    {
        this.transTime = transTime;
    }

    public Date getTransTime() 
    {
        return transTime;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public void setMyTransMoney(BigDecimal myTransMoney)
    {
        this.myTransMoney = myTransMoney;
    }

    public BigDecimal getMyTransMoney() 
    {
        return myTransMoney;
    }
    public void setMyTransNum(BigDecimal myTransNum) 
    {
        this.myTransNum = myTransNum;
    }

    public BigDecimal getMyTransNum() 
    {
        return myTransNum;
    }
    public void setMyTerminalNum(BigDecimal myTerminalNum) 
    {
        this.myTerminalNum = myTerminalNum;
    }

    public BigDecimal getMyTerminalNum() 
    {
        return myTerminalNum;
    }
    public void setMyPartnerNum(BigDecimal myPartnerNum) 
    {
        this.myPartnerNum = myPartnerNum;
    }

    public BigDecimal getMyPartnerNum() 
    {
        return myPartnerNum;
    }
    public void setMyBuyTerminalNum(BigDecimal myBuyTerminalNum) 
    {
        this.myBuyTerminalNum = myBuyTerminalNum;
    }

    public BigDecimal getMyBuyTerminalNum() 
    {
        return myBuyTerminalNum;
    }
    public void setMyExchangelTerminalNum(BigDecimal myExchangelTerminalNum) 
    {
        this.myExchangelTerminalNum = myExchangelTerminalNum;
    }

    public BigDecimal getMyExchangelTerminalNum() 
    {
        return myExchangelTerminalNum;
    }
    public void setMyOutTerminalNum(BigDecimal myOutTerminalNum) 
    {
        this.myOutTerminalNum = myOutTerminalNum;
    }

    public BigDecimal getMyOutTerminalNum() 
    {
        return myOutTerminalNum;
    }
    public void setMyInTerminalNum(BigDecimal myInTerminalNum) 
    {
        this.myInTerminalNum = myInTerminalNum;
    }

    public BigDecimal getMyInTerminalNum() 
    {
        return myInTerminalNum;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("userVip", getUserVip())
            .append("createTime", getCreateTime())
            .append("transTime", getTransTime())
            .append("brandId", getBrandId())
            .append("myTransMoney", getMyTransMoney())
            .append("myTransNum", getMyTransNum())
            .append("myTerminalNum", getMyTerminalNum())
            .append("myPartnerNum", getMyPartnerNum())
            .append("myBuyTerminalNum", getMyBuyTerminalNum())
            .append("myExchangelTerminalNum", getMyExchangelTerminalNum())
            .append("myOutTerminalNum", getMyOutTerminalNum())
            .append("myInTerminalNum", getMyInTerminalNum())
            .append("time", getTime())
            .toString();
    }

    public static final class Builder {
        private Date createTime;
        private String id;
        private String userId;
        private String userVip;
        private Date transTime;
        private String brandId;
        private BigDecimal myTransMoney;
        private BigDecimal myTransNum;
        private BigDecimal myTerminalNum;
        private BigDecimal myPartnerNum;
        private BigDecimal myBuyTerminalNum;
        private BigDecimal myExchangelTerminalNum;
        private BigDecimal myOutTerminalNum;
        private BigDecimal myInTerminalNum;
        private String time;

        private Builder() {
        }

        public static Builder getInstance() {
            return new Builder();
        }

        public Builder withCreateTime(Date createTime) {
            this.createTime = createTime;
            return this;
        }

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder withUserVip(String userVip) {
            this.userVip = userVip;
            return this;
        }

        public Builder withTransTime(Date transTime) {
            this.transTime = transTime;
            return this;
        }

        public Builder withBrandId(String brandId) {
            this.brandId = brandId;
            return this;
        }

        public Builder withMyTransMoney(BigDecimal myTransMoney) {
            this.myTransMoney = myTransMoney;
            return this;
        }

        public Builder withMyTransNum(BigDecimal myTransNum) {
            this.myTransNum = myTransNum;
            return this;
        }

        public Builder withMyTerminalNum(BigDecimal myTerminalNum) {
            this.myTerminalNum = myTerminalNum;
            return this;
        }

        public Builder withMyPartnerNum(BigDecimal myPartnerNum) {
            this.myPartnerNum = myPartnerNum;
            return this;
        }

        public Builder withMyBuyTerminalNum(BigDecimal myBuyTerminalNum) {
            this.myBuyTerminalNum = myBuyTerminalNum;
            return this;
        }

        public Builder withMyExchangelTerminalNum(BigDecimal myExchangelTerminalNum) {
            this.myExchangelTerminalNum = myExchangelTerminalNum;
            return this;
        }

        public Builder withMyOutTerminalNum(BigDecimal myOutTerminalNum) {
            this.myOutTerminalNum = myOutTerminalNum;
            return this;
        }

        public Builder withMyInTerminalNum(BigDecimal myInTerminalNum) {
            this.myInTerminalNum = myInTerminalNum;
            return this;
        }

        public Builder withTime(String time) {
            this.time = time;
            return this;
        }

        public TStatisticInfo build() {
            TStatisticInfo tStatisticInfo = new TStatisticInfo();
            tStatisticInfo.setCreateTime(createTime);
            tStatisticInfo.setId(id);
            tStatisticInfo.setUserId(userId);
            tStatisticInfo.setUserVip(userVip);
            tStatisticInfo.setTransTime(transTime);
            tStatisticInfo.setBrandId(brandId);
            tStatisticInfo.setMyTransMoney(myTransMoney);
            tStatisticInfo.setMyTransNum(myTransNum);
            tStatisticInfo.setMyTerminalNum(myTerminalNum);
            tStatisticInfo.setMyPartnerNum(myPartnerNum);
            tStatisticInfo.setMyBuyTerminalNum(myBuyTerminalNum);
            tStatisticInfo.setMyExchangelTerminalNum(myExchangelTerminalNum);
            tStatisticInfo.setMyOutTerminalNum(myOutTerminalNum);
            tStatisticInfo.setMyInTerminalNum(myInTerminalNum);
            tStatisticInfo.setTime(time);
            return tStatisticInfo;
        }
    }
}
