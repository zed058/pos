package com.ruoyi.pos.web.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ruoyi.common.enums.AuditStatusEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.pos.api.vo.user.WorkOrderVo;
import com.ruoyi.pos.web.domain.TWordOrderTypeInfo;
import com.ruoyi.pos.web.mapper.TWordOrderTypeInfoMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TUserWorkOrderInfoMapper;
import com.ruoyi.pos.web.domain.TUserWorkOrderInfo;
import com.ruoyi.pos.web.service.ITUserWorkOrderInfoService;

/**
 * 工单管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@Service
public class TUserWorkOrderInfoServiceImpl implements ITUserWorkOrderInfoService 
{
    @Autowired
    private TUserWorkOrderInfoMapper tUserWorkOrderInfoMapper;
    @Autowired
    private TWordOrderTypeInfoMapper wordOrderTypeInfoMapper;

    /**
     * 查询工单管理
     * 
     * @param id 工单管理主键
     * @return 工单管理
     */
    @Override
    public TUserWorkOrderInfo selectTUserWorkOrderInfoById(String id)
    {
        return tUserWorkOrderInfoMapper.selectTUserWorkOrderInfoById(id);
    }

    /**
     * 查询工单管理列表
     * 
     * @param tUserWorkOrderInfo 工单管理
     * @return 工单管理
     */
    @Override
    public List<TUserWorkOrderInfo> selectTUserWorkOrderInfoList(TUserWorkOrderInfo tUserWorkOrderInfo)
    {
        return tUserWorkOrderInfoMapper.selectTUserWorkOrderInfoList(tUserWorkOrderInfo);
    }

    /**
     * 新增工单管理
     * 
     * @param tUserWorkOrderInfo 工单管理
     * @return 结果
     */
    @Override
    public int insertTUserWorkOrderInfo(TUserWorkOrderInfo tUserWorkOrderInfo)
    {
        tUserWorkOrderInfo.setCreateTime(DateUtils.getNowDate());
        return tUserWorkOrderInfoMapper.insertTUserWorkOrderInfo(tUserWorkOrderInfo);
    }

    /**
     * 修改工单管理
     * 
     * @param tUserWorkOrderInfo 工单管理
     * @return 结果
     */
    @Override
    public int updateTUserWorkOrderInfo(TUserWorkOrderInfo tUserWorkOrderInfo)
    {
        tUserWorkOrderInfo.setAuditTime(DateUtils.getNowDate());
        return tUserWorkOrderInfoMapper.updateTUserWorkOrderInfo(tUserWorkOrderInfo);
    }

    /**
     * 批量删除工单管理
     * 
     * @param ids 需要删除的工单管理主键
     * @return 结果
     */
    @Override
    public int deleteTUserWorkOrderInfoByIds(String[] ids)
    {
        return tUserWorkOrderInfoMapper.deleteTUserWorkOrderInfoByIds(ids);
    }

    /**
     * 删除工单管理信息
     * 
     * @param id 工单管理主键
     * @return 结果
     */
    @Override
    public int deleteTUserWorkOrderInfoById(String id)
    {
        return tUserWorkOrderInfoMapper.deleteTUserWorkOrderInfoById(id);
    }

    @Override
    public List<WorkOrderVo> queryTUserWorkOrderListApi(TUserWorkOrderInfo tUserWorkOrderInfo) {
        List<WorkOrderVo> tUserWorkOrderInfos = tUserWorkOrderInfoMapper.queryTUserWorkOrderListApi(tUserWorkOrderInfo);
        List<WorkOrderVo> workOrderVos = new ArrayList<>();
        Optional.ofNullable(tUserWorkOrderInfos).ifPresent(list ->{
            list.stream().forEach(bean->{
                WorkOrderVo workOrderVo = new WorkOrderVo();
                BeanUtils.copyProperties(bean,workOrderVo);
                workOrderVo.setAuditShow(Optional.ofNullable(AuditStatusEnum.getEnumsByCode(workOrderVo.getAudit())).map(item-> item.getValue()).orElse(workOrderVo.getAudit()));
                workOrderVos.add(workOrderVo);
            });
        });
        return workOrderVos;
    }
    @Override
    public List<TWordOrderTypeInfo> queryWordorderTypeList(){
        return  wordOrderTypeInfoMapper.selectTWordOrderTypeInfoList(new TWordOrderTypeInfo());
    }
}
