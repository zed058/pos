package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 金刚导航栏对象 t_navigation_info
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public class TNavigationInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 金刚导航栏logo */
    @Excel(name = "金刚导航栏logo")
    private String logo;

    /** 金刚导航栏名称 */
    @Excel(name = "金刚导航栏名称")
    private String navigationName;

    /** 金刚导航栏url地址路径 */
    @Excel(name = "金刚导航栏url地址路径")
    private String navigationUrl;

    /** 排序 */
    @Excel(name = "排序")
    private String sort;

    /** 导航栏（0:显示，1：隐藏） */
    @Excel(name = "导航栏", readConverterExp = "0=:显示，1：隐藏")
    private String isValid;
    private String linkType;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setLogo(String logo) 
    {
        this.logo = logo;
    }

    public String getLogo() 
    {
        return logo;
    }
    public void setNavigationName(String navigationName) 
    {
        this.navigationName = navigationName;
    }

    public String getNavigationName() 
    {
        return navigationName;
    }
    public void setNavigationUrl(String navigationUrl) 
    {
        this.navigationUrl = navigationUrl;
    }

    public String getNavigationUrl() 
    {
        return navigationUrl;
    }
    public void setSort(String sort) 
    {
        this.sort = sort;
    }

    public String getSort() 
    {
        return sort;
    }
    public void setIsValid(String isValid) 
    {
        this.isValid = isValid;
    }

    public String getIsValid() 
    {
        return isValid;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("logo", getLogo())
            .append("navigationName", getNavigationName())
            .append("navigationUrl", getNavigationUrl())
            .append("sort", getSort())
            .append("isValid", getIsValid())
            .append("createTime", getCreateTime())
            .toString();
    }
}
