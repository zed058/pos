package com.ruoyi.pos.web.mapper;

import java.util.List;
import com.ruoyi.pos.web.domain.TGoldInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 交易金序列号Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public interface TGoldInfoMapper 
{
    /**
     * 查询交易金序列号
     * 
     * @param id 交易金序列号主键
     * @return 交易金序列号
     */
    public TGoldInfo selectTGoldInfoById(Long id);

    /**
     * 查询交易金序列号列表
     * 
     * @param tGoldInfo 交易金序列号
     * @return 交易金序列号集合
     */
    public List<TGoldInfo> selectTGoldInfoList(TGoldInfo tGoldInfo);

    /**
     * 新增交易金序列号
     * 
     * @param tGoldInfo 交易金序列号
     * @return 结果
     */
    public int insertTGoldInfo(TGoldInfo tGoldInfo);

    /**
     * 修改交易金序列号
     * 
     * @param tGoldInfo 交易金序列号
     * @return 结果
     */
    public int updateTGoldInfo(TGoldInfo tGoldInfo);

    /**
     * 删除交易金序列号
     * 
     * @param id 交易金序列号主键
     * @return 结果
     */
    public int deleteTGoldInfoById(Long id);

    /**
     * 批量删除交易金序列号
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTGoldInfoByIds(Long[] ids);

    //查询待转化的金币列表
    public List<TGoldInfo> selectTGoldInfoListForExchange(@Param("exchangeTime") String exchangeTime);
}
