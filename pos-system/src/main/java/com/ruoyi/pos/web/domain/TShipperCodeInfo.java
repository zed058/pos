package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物流公司编码对象 t_shipper_code_info
 * 
 * @author ruoyi
 * @date 2021-12-10
 */
public class TShipperCodeInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 物流公司编码 */
    @Excel(name = "物流公司编码")
    private String shipperCode;

    /** 物流公司名称 */
    @Excel(name = "物流公司名称")
    private String shipperName;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setShipperCode(String shipperCode) 
    {
        this.shipperCode = shipperCode;
    }

    public String getShipperCode() 
    {
        return shipperCode;
    }
    public void setShipperName(String shipperName) 
    {
        this.shipperName = shipperName;
    }

    public String getShipperName() 
    {
        return shipperName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("shipperCode", getShipperCode())
            .append("shipperName", getShipperName())
            .toString();
    }
}
