package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TPolicyInfo;

/**
 * 平台政策/商学院Service接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface ITPolicyInfoService 
{
    /**
     * 查询平台政策/商学院
     * 
     * @param id 平台政策/商学院主键
     * @return 平台政策/商学院
     */
    public TPolicyInfo selectTPolicyInfoById(String id);

    /**
     * 查询平台政策/商学院列表
     * 
     * @param tPolicyInfo 平台政策/商学院
     * @return 平台政策/商学院集合
     */
    public List<TPolicyInfo> selectTPolicyInfoList(TPolicyInfo tPolicyInfo);

    /**
     * 新增平台政策/商学院
     * 
     * @param tPolicyInfo 平台政策/商学院
     * @return 结果
     */
    public int insertTPolicyInfo(TPolicyInfo tPolicyInfo);

    /**
     * 修改平台政策/商学院
     * 
     * @param tPolicyInfo 平台政策/商学院
     * @return 结果
     */
    public int updateTPolicyInfo(TPolicyInfo tPolicyInfo);

    /**
     * 批量删除平台政策/商学院
     * 
     * @param ids 需要删除的平台政策/商学院主键集合
     * @return 结果
     */
    public int deleteTPolicyInfoByIds(String[] ids);

    /**
     * 删除平台政策/商学院信息
     * 
     * @param id 平台政策/商学院主键
     * @return 结果
     */
    public int deleteTPolicyInfoById(String id);
}
