package com.ruoyi.pos.web.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.ruoyi.common.enums.ImgTypeEnums;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TSlideshowInfoMapper;
import com.ruoyi.pos.web.domain.TSlideshowInfo;
import com.ruoyi.pos.web.service.ITSlideshowInfoService;

/**
 * 引导页面Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@Service
public class TSlideshowInfoServiceImpl implements ITSlideshowInfoService 
{
    @Autowired
    private TSlideshowInfoMapper tSlideshowInfoMapper;

    /**
     * 查询引导页面
     * 
     * @param id 引导页面主键
     * @return 引导页面
     */
    @Override
    public TSlideshowInfo selectTSlideshowInfoById(String id)
    {
        return tSlideshowInfoMapper.selectTSlideshowInfoById(id);
    }

    /**
     * 查询引导页面列表
     * 
     * @param tSlideshowInfo 引导页面
     * @return 引导页面
     */
    @Override
    public List<TSlideshowInfo> selectTSlideshowInfoList(TSlideshowInfo tSlideshowInfo)
    {
        List<TSlideshowInfo> slideshowInfoList = tSlideshowInfoMapper.selectTSlideshowInfoList(tSlideshowInfo);
        slideshowInfoList.stream().forEach(slid ->{
            slid.setImgTypeName(ImgTypeEnums.getEnumsByCode(slid.getImgType()).getValue());
        });
        return slideshowInfoList;
    }

    /**
     * 新增引导页面
     * 
     * @param tSlideshowInfo 引导页面
     * @return 结果
     */
    @Override
    public int insertTSlideshowInfo(TSlideshowInfo tSlideshowInfo)
    {
        tSlideshowInfo.setCreateTime(DateUtils.getNowDate());
        tSlideshowInfo.setId(IdUtils.simpleUUID());
        Map<String, Object> params = new HashMap<>();
        if (tSlideshowInfo.getImgType().equals(ImgTypeEnums.SLIDESHOW.getCode())){
            params.put("code",ImgTypeEnums.SLIDESHOW.getCode());
            int num = tSlideshowInfoMapper.selectSlideShowInfoByImgType(params);
            if (num>=5){
                throw new IllegalArgumentException("最多新增5张轮播图");
            }
        }else if (tSlideshowInfo.getImgType().equals(ImgTypeEnums.ACTIVATED.getCode())){
            params.put("code",ImgTypeEnums.ACTIVATED.getCode());
            int num = tSlideshowInfoMapper.selectSlideShowInfoByImgType(params);
            if (num>=1){
                throw new IllegalArgumentException("只能上传一张海报图片");
            }
        }
        return tSlideshowInfoMapper.insertTSlideshowInfo(tSlideshowInfo);
    }

    /**
     * 修改引导页面
     * 
     * @param tSlideshowInfo 引导页面
     * @return 结果
     */
    @Override
    public int updateTSlideshowInfo(TSlideshowInfo tSlideshowInfo)
    {
        Map<String, Object> params = new HashMap<>();
        params.put("id",tSlideshowInfo.getId());
        if (tSlideshowInfo.getImgType().equals(ImgTypeEnums.SLIDESHOW.getCode())){
            params.put("code",ImgTypeEnums.SLIDESHOW.getCode());
            int num = tSlideshowInfoMapper.selectSlideShowInfoByImgType(params);
            if (num>=5){
                throw new IllegalArgumentException("最多新增5张轮播图");
            }
        }else if (tSlideshowInfo.getImgType().equals(ImgTypeEnums.ACTIVATED.getCode())){
            params.put("code",ImgTypeEnums.ACTIVATED.getCode());
            int num = tSlideshowInfoMapper.selectSlideShowInfoByImgType(params);
            if (num>=1){
                throw new IllegalArgumentException("只能上传一张海报图片");
            }
        }
        return tSlideshowInfoMapper.updateTSlideshowInfo(tSlideshowInfo);
    }

    /**
     * 批量删除引导页面
     * 
     * @param ids 需要删除的引导页面主键
     * @return 结果
     */
    @Override
    public int deleteTSlideshowInfoByIds(String[] ids)
    {
        return tSlideshowInfoMapper.deleteTSlideshowInfoByIds(ids);
    }

    /**
     * 删除引导页面信息
     * 
     * @param id 引导页面主键
     * @return 结果
     */
    @Override
    public int deleteTSlideshowInfoById(String id)
    {
        return tSlideshowInfoMapper.deleteTSlideshowInfoById(id);
    }
}
