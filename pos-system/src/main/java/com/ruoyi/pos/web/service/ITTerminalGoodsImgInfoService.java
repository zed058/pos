package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TTerminalGoodsImgInfo;

/**
 * 商品轮播图，产品说明Service接口
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public interface ITTerminalGoodsImgInfoService 
{
    /**
     * 查询商品轮播图，产品说明
     * 
     * @param id 商品轮播图，产品说明主键
     * @return 商品轮播图，产品说明
     */
    public TTerminalGoodsImgInfo selectTTerminalGoodsImgInfoById(String id);

    /**
     * 查询商品轮播图，产品说明列表
     * 
     * @param tTerminalGoodsImgInfo 商品轮播图，产品说明
     * @return 商品轮播图，产品说明集合
     */
    public List<TTerminalGoodsImgInfo> selectTTerminalGoodsImgInfoList(TTerminalGoodsImgInfo tTerminalGoodsImgInfo);

    /**
     * 新增商品轮播图，产品说明
     * 
     * @param tTerminalGoodsImgInfo 商品轮播图，产品说明
     * @return 结果
     */
    public int insertTTerminalGoodsImgInfo(TTerminalGoodsImgInfo tTerminalGoodsImgInfo);

    /**
     * 修改商品轮播图，产品说明
     * 
     * @param tTerminalGoodsImgInfo 商品轮播图，产品说明
     * @return 结果
     */
    public int updateTTerminalGoodsImgInfo(TTerminalGoodsImgInfo tTerminalGoodsImgInfo);

    /**
     * 批量删除商品轮播图，产品说明
     * 
     * @param ids 需要删除的商品轮播图，产品说明主键集合
     * @return 结果
     */
    public int deleteTTerminalGoodsImgInfoByIds(String[] ids);

    /**
     * 删除商品轮播图，产品说明信息
     * 
     * @param id 商品轮播图，产品说明主键
     * @return 结果
     */
    public int deleteTTerminalGoodsImgInfoById(String id);
}
