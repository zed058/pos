package com.ruoyi.pos.web.service.impl;

import java.util.List;
import java.util.Optional;

import com.ruoyi.common.enums.OrderTypeEnum;
import com.ruoyi.common.enums.TransStatusEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TTerminalTransInfoMapper;
import com.ruoyi.pos.web.domain.TTerminalTransInfo;
import com.ruoyi.pos.web.service.ITTerminalTransInfoService;

/**
 * 终端划拨记录Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
@Service
public class TTerminalTransInfoServiceImpl implements ITTerminalTransInfoService 
{
    @Autowired
    private TTerminalTransInfoMapper tTerminalTransInfoMapper;

    /**
     * 查询终端划拨记录
     * 
     * @param id 终端划拨记录主键
     * @return 终端划拨记录
     */
    @Override
    public TTerminalTransInfo selectTTerminalTransInfoById(String id)
    {
        return tTerminalTransInfoMapper.selectTTerminalTransInfoById(id);
    }

    /**
     * 查询终端划拨记录列表
     * 
     * @param tTerminalTransInfo 终端划拨记录
     * @return 终端划拨记录
     */
    @Override
    public List<TTerminalTransInfo> selectTTerminalTransInfoList(TTerminalTransInfo tTerminalTransInfo)
    {
        List<TTerminalTransInfo> tTerminalTransInfos = tTerminalTransInfoMapper.selectTTerminalTransInfoList(tTerminalTransInfo);
        Optional.ofNullable(tTerminalTransInfos).ifPresent(map ->{
            map.stream().forEach(bean ->{
                bean.setTransStatusShow(Optional.ofNullable(TransStatusEnum.getEnumsByCode(bean.getTransStatus())).map(item -> item.getValue()).orElse(bean.getTransStatus()));
            });
        });
        return tTerminalTransInfos;
    }

    /**
     * 新增终端划拨记录
     * 
     * @param tTerminalTransInfo 终端划拨记录
     * @return 结果
     */
    @Override
    public int insertTTerminalTransInfo(TTerminalTransInfo tTerminalTransInfo)
    {
        tTerminalTransInfo.setCreateTime(DateUtils.getNowDate());
        tTerminalTransInfo.setId(IdUtils.simpleUUID());
        return tTerminalTransInfoMapper.insertTTerminalTransInfo(tTerminalTransInfo);
    }

    /**
     * 修改终端划拨记录
     * 
     * @param tTerminalTransInfo 终端划拨记录
     * @return 结果
     */
    @Override
    public int updateTTerminalTransInfo(TTerminalTransInfo tTerminalTransInfo)
    {
        return tTerminalTransInfoMapper.updateTTerminalTransInfo(tTerminalTransInfo);
    }

    /**
     * 批量删除终端划拨记录
     * 
     * @param ids 需要删除的终端划拨记录主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalTransInfoByIds(String[] ids)
    {
        return tTerminalTransInfoMapper.deleteTTerminalTransInfoByIds(ids);
    }

    /**
     * 删除终端划拨记录信息
     * 
     * @param id 终端划拨记录主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalTransInfoById(String id)
    {
        return tTerminalTransInfoMapper.deleteTTerminalTransInfoById(id);
    }
}
