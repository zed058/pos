package com.ruoyi.pos.web.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.weVo.HomePageVo;
import org.apache.ibatis.annotations.MapKey;
import org.springframework.beans.factory.annotation.Required;

/**
 * 用户基础数据Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-10
 */
public interface TUserInfoMapper 
{
    /**
     * 查询用户基础数据
     * 
     * @param userId 用户基础数据主键
     * @return 用户基础数据
     */
    public TUserInfo selectTUserInfoByUserId(String userId);

    /**
     * 查询用户基础数据列表
     * 
     * @param tUserInfo 用户基础数据
     * @return 用户基础数据集合
     */
    public List<TUserInfo> selectTUserInfoList(TUserInfo tUserInfo);

    /**
     * 新增用户基础数据
     * 
     * @param tUserInfo 用户基础数据
     * @return 结果
     */
    public int insertTUserInfo(TUserInfo tUserInfo);

    /**
     * 修改用户基础数据
     * 
     * @param tUserInfo 用户基础数据
     * @return 结果
     */
    public int updateTUserInfo(TUserInfo tUserInfo);

    /**
     * 删除用户基础数据
     * 
     * @param userId 用户基础数据主键
     * @return 结果
     */
    public int deleteTUserInfoByUserId(String userId);

    /**
     * 批量删除用户基础数据
     * 
     * @param userIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTUserInfoByUserIds(String[] userIds);

//-----------------------------------------自定义--------------------------------------------
    //根据openId查询用户信息
    TUserInfo selectUserByOpenId(String openId);
    //根据手机号查询用户信息
    TUserInfo selectUserByUserPhone(String userPhone);
    //根据手机号查询用户信息
    TUserInfo selectUserByUserMemberNo(String memberNo);
    //根据邀请码查询用户信息
    TUserInfo selectUserByInviteCode(String inviteCode);
    //根查询最小的邀请码
    String selectInviteCodeForMin();
    //查询web端用户管理列表
    List<TUserInfo> selectTUserInfoListForWeb(TUserInfo tUserInfo);
    //查询web端实名认证列表
    List<TUserInfo> selectTUserInfoListForCheck(TUserInfo tUserInfo);
    //用户数据统计
    Integer queryUserCount();

    List<HomePageVo> queryUserChart();

    List<TUserInfo> merchantList(TUserInfo userInfo);
    //状态修改
    public int updateTUserNoticeType(TUserInfo tUserInfo);
}
