package com.ruoyi.pos.web.mapper;

import com.ruoyi.pos.api.vo.user.GoldserialVo;
import com.ruoyi.pos.web.domain.TGoldAwardSerialInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (金币序列号Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-20
 */
public interface TGoldAwardSerialInfoMapper 
{
    /**
     * 查询(金币序列号
     * 
     * @param id (金币序列号主键
     * @return (金币序列号
     */
    public TGoldAwardSerialInfo selectTGoldAwardSerialInfoById(Long id);

    /**
     * 查询(金币序列号列表
     * 
     * @param tGoldAwardSerialInfo (金币序列号
     * @return (金币序列号集合
     */
    public List<TGoldAwardSerialInfo> selectTGoldAwardSerialInfoList(TGoldAwardSerialInfo tGoldAwardSerialInfo);
    public List<TGoldAwardSerialInfo> selectTGoldAwardSerialInfoList2(TGoldAwardSerialInfo tGoldAwardSerialInfo);

    /**
     * 新增(金币序列号
     * 
     * @param tGoldAwardSerialInfo (金币序列号
     * @return 结果
     */
    public int insertTGoldAwardSerialInfo(TGoldAwardSerialInfo tGoldAwardSerialInfo);

    /**
     * 修改(金币序列号
     * 
     * @param tGoldAwardSerialInfo (金币序列号
     * @return 结果
     */
    public int updateTGoldAwardSerialInfo(TGoldAwardSerialInfo tGoldAwardSerialInfo);

    /**
     * 删除(金币序列号
     * 
     * @param id (金币序列号主键
     * @return 结果
     */
    public int deleteTGoldAwardSerialInfoById(Long id);

    /**
     * 批量删除(金币序列号
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTGoldAwardSerialInfoByIds(Long[] ids);

//    public List<TGoldAwardSerialInfo> queryTGoldAwardSerialMaxList(TGoldAwardSerialInfo tGoldAwardSerialInfo);

    //平台最大奖励序列号
    public TGoldAwardSerialInfo queryAwardMax();
    //个人可消费奖励序列号
    public TGoldAwardSerialInfo queryAwardExchangeMax();
    //平台最大奖励金序列号
    public TGoldAwardSerialInfo queryAwardAllMax();

    //我个人可消费所有的奖励金序列号
    public int queryAwardCount(@Param("userId")String userId);

    //平台动态所有已兑奖的序列号
    public List<GoldserialVo> queryAwardRoll();

    public List<TGoldAwardSerialInfo> selectTGoldAwardExchangeTimeList(@Param("userId") String userId, @Param("exchange") String exchange, @Param("createTime") String createTime, @Param("exchangeTime") String exchangeTime);
    //已中奖序列号倒序排列
    public List<TGoldAwardSerialInfo> selectTGoldAwardSerialInfoDesc(TGoldAwardSerialInfo tGoldAwardSerialInfo);


}
