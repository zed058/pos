package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 终端绑定临时（第三方杉德）对象 t_terminal_bind_info
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public class TTerminalBindInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 代理商编号(唯一) */
    @Excel(name = "代理商编号(唯一)")
    private String agentId;

    /** 商户名称 */
    @Excel(name = "商户名称")
    private String merName;

    /** 商户号 */
    @Excel(name = "商户号")
    private String merId;

    /** 商户姓名 */
    @Excel(name = "商户姓名")
    private String merUsername;

    /** 商户手机号(脱敏) */
    @Excel(name = "商户手机号(脱敏)")
    private String merPhone;

    /** 商户结算卡(脱敏) */
    @Excel(name = "商户结算卡(脱敏)")
    private String screenNum;

    /** Sn 编号(终端绑定状态为 1 时返回) */
    @Excel(name = "Sn 编号(终端绑定状态为 1 时返回)")
    private String snCode;

    /** 商户类型(1.小微商户，2.实体商户) */
    @Excel(name = "商户类型(1.小微商户，2.实体商户)")
    private String merchantType;

    /** 终端绑定状态(0.未绑定，1.已绑定) */
    @Excel(name = "终端绑定状态(0.未绑定，1.已绑定)")
    private String bindStatus;

    /** 是否有效（0：是，1：否） */
    @Excel(name = "是否有效", readConverterExp = "0=：是，1：否")
    private String isValid;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setAgentId(String agentId) 
    {
        this.agentId = agentId;
    }

    public String getAgentId() 
    {
        return agentId;
    }
    public void setMerName(String merName) 
    {
        this.merName = merName;
    }

    public String getMerName() 
    {
        return merName;
    }
    public void setMerId(String merId) 
    {
        this.merId = merId;
    }

    public String getMerId() 
    {
        return merId;
    }
    public void setMerUsername(String merUsername) 
    {
        this.merUsername = merUsername;
    }

    public String getMerUsername() 
    {
        return merUsername;
    }
    public void setMerPhone(String merPhone) 
    {
        this.merPhone = merPhone;
    }

    public String getMerPhone() 
    {
        return merPhone;
    }
    public void setScreenNum(String screenNum) 
    {
        this.screenNum = screenNum;
    }

    public String getScreenNum() 
    {
        return screenNum;
    }
    public void setSnCode(String snCode) 
    {
        this.snCode = snCode;
    }

    public String getSnCode() 
    {
        return snCode;
    }
    public void setMerchantType(String merchantType) 
    {
        this.merchantType = merchantType;
    }

    public String getMerchantType() 
    {
        return merchantType;
    }
    public void setBindStatus(String bindStatus) 
    {
        this.bindStatus = bindStatus;
    }

    public String getBindStatus() 
    {
        return bindStatus;
    }
    public void setIsValid(String isValid) 
    {
        this.isValid = isValid;
    }

    public String getIsValid() 
    {
        return isValid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("agentId", getAgentId())
            .append("merName", getMerName())
            .append("merId", getMerId())
            .append("merUsername", getMerUsername())
            .append("merPhone", getMerPhone())
            .append("screenNum", getScreenNum())
            .append("snCode", getSnCode())
            .append("merchantType", getMerchantType())
            .append("bindStatus", getBindStatus())
            .append("createTime", getCreateTime())
            .append("isValid", getIsValid())
            .toString();
    }
}
