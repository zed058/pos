package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 基础配置对象 t_rebate_set_info
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public class TRebateSetInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 用户累计消费xx金额（发放交易金序列号） */
    @Excel(name = "用户累计消费xx金额", readConverterExp = "发=放交易金序列号")
    private BigDecimal userDeal;

    /** 用户累计消费xx金额（发放奖励金序列号） */
    @Excel(name = "用户累计消费xx金额", readConverterExp = "发=放奖励金序列号")
    private BigDecimal userAward;

    /** 平台累计达到xx金额（发放交易金） */
    @Excel(name = "平台累计达到xx金额", readConverterExp = "发=放交易金")
    private BigDecimal platformDeal;

    /** 平台累计达到xx金额（发放交易金） */
    @Excel(name = "平台累计达到xx金额（已发放额度）", readConverterExp = "发=放交易金")
    private BigDecimal platformDealHasSend;

    /** 平台累计达到xx金额（发放奖励金） */
    @Excel(name = "平台累计达到xx金额", readConverterExp = "发=放奖励金")
    private BigDecimal platformAward;

    /** 平台累计达到xx金额（发放奖励金） */
    @Excel(name = "平台累计达到xx金额（已发放额度）", readConverterExp = "发=放奖励金")
    private BigDecimal platformAwardHasSend;

    /** 交易金（金币） */
    @Excel(name = "交易金（金币）")
    private BigDecimal dealGold;

    /** 奖励金（金币） */
    @Excel(name = "奖励金（金币）")
    private BigDecimal awardGold;

    /** 金币转换现金时间（小时） */
    @Excel(name = "金币转换现金时间", readConverterExp = "小=时")
    private String goldTime;

    /** 金币转换现金比例 */
    @Excel(name = "金币转换现金比例")
    private BigDecimal goldRatio;

    /** 提现最低金额 */
    @Excel(name = "提现最低金额")
    private BigDecimal billAmount;

    /** 提现手续费 */
    @Excel(name = "提现手续费")
    private BigDecimal serviceCharge;

    /** 采购返利（导师收益【下级购买终端】） */
    @Excel(name = "采购返利", readConverterExp = "导=师收益【下级购买终端】")
    private BigDecimal purchasingRebate;

    /** 提现返利（导师收益【下级余额提现成功】） */
    @Excel(name = "提现返利", readConverterExp = "导=师收益【下级余额提现成功】")
    private BigDecimal carryRebate;

    /** 办公补贴（v10导师收益【团队购买终端余额】） */
    @Excel(name = "办公补贴", readConverterExp = "v=10导师收益【团队购买终端余额】")
    private BigDecimal workSubsidy;

    /** 平台累计交易金额 */
    @Excel(name = "平台累计交易金额")
    private BigDecimal platformDealSum;

    /** 终端累计交易额 */
    @Excel(name = "终端累计交易额")
    private BigDecimal terminalDealSum;

    /** 司令部百分百比 */
    @Excel(name = "司令部百分百比")
    private BigDecimal hundredThan;

    /** 司令部百分百比基数 */
    @Excel(name = "司令部百分百比基数")
    private BigDecimal hundredThanNumber;

    /** 司令部百分百比基数 */
    @Excel(name = "超时未支付订单取消时间")
    private String waitCancelTime;

    /** 超时未收货订单自动确认时间 */
    @Excel(name = "超时未收货订单自动确认时间")
    private String waitConfirmTime;

    /** 提现失败通知内容 */
    @Excel(name = "提现失败通知内容")
    private String withdrawFailResult;

    /** 提现失败通知内容 */
    @Excel(name = "提现失败通知内容")
    private BigDecimal distributionAmount;



    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setUserDeal(BigDecimal userDeal)
    {
        this.userDeal = userDeal;
    }

    public BigDecimal getUserDeal()
    {
        return userDeal;
    }
    public void setUserAward(BigDecimal userAward)
    {
        this.userAward = userAward;
    }

    public BigDecimal getUserAward()
    {
        return userAward;
    }
    public void setPlatformDeal(BigDecimal platformDeal)
    {
        this.platformDeal = platformDeal;
    }

    public BigDecimal getPlatformDeal()
    {
        return platformDeal;
    }
    public void setPlatformAward(BigDecimal platformAward)
    {
        this.platformAward = platformAward;
    }

    public BigDecimal getPlatformAward()
    {
        return platformAward;
    }
    public void setGoldTime(String goldTime)
    {
        this.goldTime = goldTime;
    }

    public String getGoldTime()
    {
        return goldTime;
    }
    public void setGoldRatio(BigDecimal goldRatio)
    {
        this.goldRatio = goldRatio;
    }

    public BigDecimal getGoldRatio()
    {
        return goldRatio;
    }
    public void setBillAmount(BigDecimal billAmount)
    {
        this.billAmount = billAmount;
    }

    public BigDecimal getBillAmount()
    {
        return billAmount;
    }
    public void setServiceCharge(BigDecimal serviceCharge)
    {
        this.serviceCharge = serviceCharge;
    }

    public BigDecimal getServiceCharge()
    {
        return serviceCharge;
    }
    public void setPurchasingRebate(BigDecimal purchasingRebate)
    {
        this.purchasingRebate = purchasingRebate;
    }

    public BigDecimal getPurchasingRebate()
    {
        return purchasingRebate;
    }
    public void setCarryRebate(BigDecimal carryRebate)
    {
        this.carryRebate = carryRebate;
    }

    public BigDecimal getCarryRebate()
    {
        return carryRebate;
    }
    public void setWorkSubsidy(BigDecimal workSubsidy)
    {
        this.workSubsidy = workSubsidy;
    }

    public BigDecimal getWorkSubsidy()
    {
        return workSubsidy;
    }
    public void setPlatformDealSum(BigDecimal platformDealSum)
    {
        this.platformDealSum = platformDealSum;
    }

    public BigDecimal getPlatformDealSum()
    {
        return platformDealSum;
    }
    public void setTerminalDealSum(BigDecimal terminalDealSum)
    {
        this.terminalDealSum = terminalDealSum;
    }

    public BigDecimal getTerminalDealSum()
    {
        return terminalDealSum;
    }
    public void setHundredThan(BigDecimal hundredThan)
    {
        this.hundredThan = hundredThan;
    }

    public BigDecimal getHundredThan()
    {
        return hundredThan;
    }
    public void setHundredThanNumber(BigDecimal hundredThanNumber)
    {
        this.hundredThanNumber = hundredThanNumber;
    }

    public BigDecimal getHundredThanNumber()
    {
        return hundredThanNumber;
    }

    public String getWaitCancelTime() {
        return waitCancelTime;
    }

    public void setWaitCancelTime(String waitCancelTime) {
        this.waitCancelTime = waitCancelTime;
    }

    public String getWaitConfirmTime() {
        return waitConfirmTime;
    }

    public void setWaitConfirmTime(String waitConfirmTime) {
        this.waitConfirmTime = waitConfirmTime;
    }

    public BigDecimal getDealGold() {
        return dealGold;
    }

    public void setDealGold(BigDecimal dealGold) {
        this.dealGold = dealGold;
    }

    public BigDecimal getAwardGold() {
        return awardGold;
    }

    public void setAwardGold(BigDecimal awardGold) {
        this.awardGold = awardGold;
    }

    public BigDecimal getPlatformDealHasSend() {
        return platformDealHasSend;
    }

    public void setPlatformDealHasSend(BigDecimal platformDealHasSend) {
        this.platformDealHasSend = platformDealHasSend;
    }

    public BigDecimal getPlatformAwardHasSend() {
        return platformAwardHasSend;
    }

    public void setPlatformAwardHasSend(BigDecimal platformAwardHasSend) {
        this.platformAwardHasSend = platformAwardHasSend;
    }

    public String getWithdrawFailResult() {
        return withdrawFailResult;
    }

    public void setWithdrawFailResult(String withdrawFailResult) {
        this.withdrawFailResult = withdrawFailResult;
    }

    public BigDecimal getDistributionAmount() {
        return distributionAmount;
    }

    public void setDistributionAmount(BigDecimal distributionAmount) {
        this.distributionAmount = distributionAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userDeal", getUserDeal())
                .append("userAward", getUserAward())
                .append("platformDeal", getPlatformDeal())
                .append("platformAward", getPlatformAward())
                .append("goldTime", getGoldTime())
                .append("goldRatio", getGoldRatio())
                .append("billAmount", getBillAmount())
                .append("serviceCharge", getServiceCharge())
                .append("purchasingRebate", getPurchasingRebate())
                .append("carryRebate", getCarryRebate())
                .append("workSubsidy", getWorkSubsidy())
                .append("platformDealSum", getPlatformDealSum())
                .append("terminalDealSum", getTerminalDealSum())
                .append("hundredThan", getHundredThan())
                .append("hundredThanNumber", getHundredThanNumber())
                .append("waitCancelTime", getWaitCancelTime())
                .append("waitConfirmTime", getWaitConfirmTime())
                .append("dealGold", getDealGold())
                .append("awardGold", getAwardGold())
                .append("distributionAmount", getDistributionAmount())
                .toString();
    }
}
