package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TGradeTypeInfo;

/**
 * vip跨级分润，直营分润，类型配置Service接口
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public interface ITGradeTypeInfoService 
{
    /**
     * 查询vip跨级分润，直营分润，类型配置
     * 
     * @param id vip跨级分润，直营分润，类型配置主键
     * @return vip跨级分润，直营分润，类型配置
     */
    public TGradeTypeInfo selectTGradeTypeInfoById(Long id);

    /**
     * 查询vip跨级分润，直营分润，类型配置列表
     * 
     * @param tGradeTypeInfo vip跨级分润，直营分润，类型配置
     * @return vip跨级分润，直营分润，类型配置集合
     */
    public List<TGradeTypeInfo> selectTGradeTypeInfoList(TGradeTypeInfo tGradeTypeInfo);

    /**
     * 新增vip跨级分润，直营分润，类型配置
     * 
     * @param tGradeTypeInfo vip跨级分润，直营分润，类型配置
     * @return 结果
     */
    public int insertTGradeTypeInfo(TGradeTypeInfo tGradeTypeInfo);

    /**
     * 修改vip跨级分润，直营分润，类型配置
     * 
     * @param tGradeTypeInfo vip跨级分润，直营分润，类型配置
     * @return 结果
     */
    public int updateTGradeTypeInfo(TGradeTypeInfo tGradeTypeInfo);

    /**
     * 批量删除vip跨级分润，直营分润，类型配置
     * 
     * @param ids 需要删除的vip跨级分润，直营分润，类型配置主键集合
     * @return 结果
     */
    public int deleteTGradeTypeInfoByIds(Long[] ids);

    /**
     * 删除vip跨级分润，直营分润，类型配置信息
     * 
     * @param id vip跨级分润，直营分润，类型配置主键
     * @return 结果
     */
    public int deleteTGradeTypeInfoById(Long id);
}
