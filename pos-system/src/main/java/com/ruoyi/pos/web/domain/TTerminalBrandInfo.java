package com.ruoyi.pos.web.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 商品品牌对象 t_terminal_brand_info
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TTerminalBrandInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 终端型号品牌名称 */
    @Excel(name = "终端型号品牌名称")
    private String brand;

    /** 排序 */
    @Excel(name = "排序")
    private String sort;

    /** 状态（0：正常，1：禁用，2:删除） */
    @Excel(name = "状态", readConverterExp = "0=：正常，1：禁用，2:删除")
    private String isValid;

    private Date synTime;

}
