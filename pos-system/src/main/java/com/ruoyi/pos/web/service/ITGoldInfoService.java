package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TGoldInfo;

/**
 * 交易金序列号Service接口
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public interface ITGoldInfoService 
{
    /**
     * 查询交易金序列号
     * 
     * @param id 交易金序列号主键
     * @return 交易金序列号
     */
    public TGoldInfo selectTGoldInfoById(Long id);

    /**
     * 查询交易金序列号列表
     * 
     * @param tGoldInfo 交易金序列号
     * @return 交易金序列号集合
     */
    public List<TGoldInfo> selectTGoldInfoList(TGoldInfo tGoldInfo);

    /**
     * 新增交易金序列号
     * 
     * @param tGoldInfo 交易金序列号
     * @return 结果
     */
    public int insertTGoldInfo(TGoldInfo tGoldInfo);

    /**
     * 修改交易金序列号
     * 
     * @param tGoldInfo 交易金序列号
     * @return 结果
     */
    public int updateTGoldInfo(TGoldInfo tGoldInfo);

    /**
     * 批量删除交易金序列号
     * 
     * @param ids 需要删除的交易金序列号主键集合
     * @return 结果
     */
    public int deleteTGoldInfoByIds(Long[] ids);

    /**
     * 删除交易金序列号信息
     * 
     * @param id 交易金序列号主键
     * @return 结果
     */
    public int deleteTGoldInfoById(Long id);
}
