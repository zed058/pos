package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 终端交易流水对象 t_terminal_trans_flow_info
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TTerminalTransFlowInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 代理商编号(唯一) */
    private String agentId;

    /** 机具类型(2.TPOS 传统机，4.EPOS 电签) */
    private String machineType;

    /** 交易订单流水号(唯一) */
    @Excel(name = "交易订单流水号(唯一)")
    private String orderId;

    /** POS 小票参考号(唯一) */
    private String sysno;

    /** 商户名称 */
    private String merName;

    /** 商户号 */
    private String merId;

    /** 终端 sn 编号 */
    @Excel(name = "终端SN号")
    private String snCode;

    /** 交易金额(单位：分) */
    @Excel(name = "交易金额")
    private BigDecimal transAmount;

    /** 交易费率 */
    private BigDecimal merSigeFee;

    /** 提现手续费(单位：分) */
    private BigDecimal extractionFee;

    /** 结算方式(0.DO 结算，1.T1 结算) */
    private String settlementCycle;

    /** 交易类型(1.刷卡，2.扫码，3.闪付) */
    @Excel(name = "交易类型")
    private String payType;

    /** 卡类型(1.贷记卡，2.借记卡，3.支付宝，4. 银联二维码，6.云闪付) */
    private String cardType;

    /** 交易类别(1.普通，2.VIP) 刷卡区分，扫码闪付默认 1 */
    private String transCategory;

    /** 交易卡号 */
    private String cardNo;

    /** 是否有效（0：是，1：否） */
    private String isValid;

    /** 处理状态 */
    @Excel(name = "处理状态")
    private String handleStaus;

    /** 处理结果 */
    @Excel(name = "处理结果")
    private String handleResult;

    /** 第三方交易成功时间 */
    @Excel(name = "第三方交易成功时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date transTime;

    @Excel(name = "用户名称")
    private String userName;
    @Excel(name = "用户手机号")
    private String userPhone;

    private List<TTerminalTransFlowShareInfo> shareList;
    private String[] queryCreateTimeStr; //申请时间查询条件
    private String createTimeStart;
    private String createTimeEnd;
    private String cpiNo;
    private String bcconMark;

}
