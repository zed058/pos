package com.ruoyi.pos.web.mapper;


import com.ruoyi.pos.web.domain.TBankInfo;

import java.util.List;

/**
 * 银行信息维护Mapper接口
 * 
 * @author ruoyi
 * @date 2021-12-06
 */
public interface TBankInfoMapper 
{
    /**
     * 查询银行信息维护
     * 
     * @param bankId 银行信息维护主键
     * @return 银行信息维护
     */
    public TBankInfo selectTBankInfoByBankId(String bankId);

    /**
     * 查询银行信息维护列表
     * 
     * @param tBankInfo 银行信息维护
     * @return 银行信息维护集合
     */
    public List<TBankInfo> selectTBankInfoList(TBankInfo tBankInfo);

    /**
     * 新增银行信息维护
     * 
     * @param tBankInfo 银行信息维护
     * @return 结果
     */
    public int insertTBankInfo(TBankInfo tBankInfo);

    /**
     * 修改银行信息维护
     * 
     * @param tBankInfo 银行信息维护
     * @return 结果
     */
    public int updateTBankInfo(TBankInfo tBankInfo);

    /**
     * 删除银行信息维护
     * 
     * @param bankId 银行信息维护主键
     * @return 结果
     */
    public int deleteTBankInfoByBankId(String bankId);

    /**
     * 批量删除银行信息维护
     * 
     * @param bankIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTBankInfoByBankIds(String[] bankIds);
}
