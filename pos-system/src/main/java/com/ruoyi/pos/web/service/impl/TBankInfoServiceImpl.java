package com.ruoyi.pos.web.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.pos.web.domain.TBankInfo;
import com.ruoyi.pos.web.mapper.TBankInfoMapper;
import com.ruoyi.pos.web.service.ITBankInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 银行信息维护Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-12-06
 */
@Service
public class TBankInfoServiceImpl implements ITBankInfoService
{
    @Autowired
    private TBankInfoMapper tBankInfoMapper;

    /**
     * 查询银行信息维护
     * 
     * @param bankId 银行信息维护主键
     * @return 银行信息维护
     */
    @Override
    public TBankInfo selectTBankInfoByBankId(String bankId)
    {
        return tBankInfoMapper.selectTBankInfoByBankId(bankId);
    }

    /**
     * 查询银行信息维护列表
     * 
     * @param tBankInfo 银行信息维护
     * @return 银行信息维护
     */
    @Override
    public List<TBankInfo> selectTBankInfoList(TBankInfo tBankInfo)
    {
        return tBankInfoMapper.selectTBankInfoList(tBankInfo);
    }

    /**
     * 新增银行信息维护
     * 
     * @param tBankInfo 银行信息维护
     * @return 结果
     */
    @Override
    public int insertTBankInfo(TBankInfo tBankInfo)
    {
        tBankInfo.setBankId(IdUtils.simpleUUID());
        tBankInfo.setUserId(SecurityUtils.getUserId() + "");
        tBankInfo.setCreateTime(DateUtils.getNowDate());
        return tBankInfoMapper.insertTBankInfo(tBankInfo);
    }

    /**
     * 修改银行信息维护
     * 
     * @param tBankInfo 银行信息维护
     * @return 结果
     */
    @Override
    public int updateTBankInfo(TBankInfo tBankInfo)
    {
        return tBankInfoMapper.updateTBankInfo(tBankInfo);
    }

    /**
     * 批量删除银行信息维护
     * 
     * @param bankIds 需要删除的银行信息维护主键
     * @return 结果
     */
    @Override
    public int deleteTBankInfoByBankIds(String[] bankIds)
    {
        return tBankInfoMapper.deleteTBankInfoByBankIds(bankIds);
    }

    /**
     * 删除银行信息维护信息
     * 
     * @param bankId 银行信息维护主键
     * @return 结果
     */
    @Override
    public int deleteTBankInfoByBankId(String bankId)
    {
        return tBankInfoMapper.deleteTBankInfoByBankId(bankId);
    }
}
