package com.ruoyi.pos.web.mapper;

import java.util.List;

import com.ruoyi.pos.api.vo.user.WorkOrderVo;
import com.ruoyi.pos.web.domain.TUserWorkOrderInfo;

/**
 * 工单管理Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public interface TUserWorkOrderInfoMapper 
{
    /**
     * 查询工单管理
     * 
     * @param id 工单管理主键
     * @return 工单管理
     */
    public TUserWorkOrderInfo selectTUserWorkOrderInfoById(String id);

    /**
     * 查询工单管理列表
     * 
     * @param tUserWorkOrderInfo 工单管理
     * @return 工单管理集合
     */
    public List<TUserWorkOrderInfo> selectTUserWorkOrderInfoList(TUserWorkOrderInfo tUserWorkOrderInfo);

    /**
     * 新增工单管理
     * 
     * @param tUserWorkOrderInfo 工单管理
     * @return 结果
     */
    public int insertTUserWorkOrderInfo(TUserWorkOrderInfo tUserWorkOrderInfo);

    /**
     * 修改工单管理
     * 
     * @param tUserWorkOrderInfo 工单管理
     * @return 结果
     */
    public int updateTUserWorkOrderInfo(TUserWorkOrderInfo tUserWorkOrderInfo);

    /**
     * 删除工单管理
     * 
     * @param id 工单管理主键
     * @return 结果
     */
    public int deleteTUserWorkOrderInfoById(String id);

    /**
     * 批量删除工单管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTUserWorkOrderInfoByIds(String[] ids);


    public List<WorkOrderVo> queryTUserWorkOrderListApi(TUserWorkOrderInfo tUserWorkOrderInfo);
}
