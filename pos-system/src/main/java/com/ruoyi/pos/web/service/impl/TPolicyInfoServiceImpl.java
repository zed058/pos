package com.ruoyi.pos.web.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TPolicyInfoMapper;
import com.ruoyi.pos.web.domain.TPolicyInfo;
import com.ruoyi.pos.web.service.ITPolicyInfoService;

/**
 * 平台政策/商学院Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@Service
public class TPolicyInfoServiceImpl implements ITPolicyInfoService 
{
    @Autowired
    private TPolicyInfoMapper tPolicyInfoMapper;

    /**
     * 查询平台政策/商学院
     * 
     * @param id 平台政策/商学院主键
     * @return 平台政策/商学院
     */
    @Override
    public TPolicyInfo selectTPolicyInfoById(String id)
    {
        return tPolicyInfoMapper.selectTPolicyInfoById(id);
    }

    /**
     * 查询平台政策/商学院列表
     * 
     * @param tPolicyInfo 平台政策/商学院
     * @return 平台政策/商学院
     */
    @Override
    public List<TPolicyInfo> selectTPolicyInfoList(TPolicyInfo tPolicyInfo)
    {
        return tPolicyInfoMapper.selectTPolicyInfoList(tPolicyInfo);
    }

    /**
     * 新增平台政策/商学院
     * 
     * @param tPolicyInfo 平台政策/商学院
     * @return 结果
     */
    @Override
    public int insertTPolicyInfo(TPolicyInfo tPolicyInfo)
    {
        tPolicyInfo.setCreateTime(DateUtils.getNowDate());
        tPolicyInfo.setId(IdUtils.simpleUUID());
        return tPolicyInfoMapper.insertTPolicyInfo(tPolicyInfo);
    }

    /**
     * 修改平台政策/商学院
     * 
     * @param tPolicyInfo 平台政策/商学院
     * @return 结果
     */
    @Override
    public int updateTPolicyInfo(TPolicyInfo tPolicyInfo)
    {
        return tPolicyInfoMapper.updateTPolicyInfo(tPolicyInfo);
    }

    /**
     * 批量删除平台政策/商学院
     * 
     * @param ids 需要删除的平台政策/商学院主键
     * @return 结果
     */
    @Override
    public int deleteTPolicyInfoByIds(String[] ids)
    {
        return tPolicyInfoMapper.deleteTPolicyInfoByIds(ids);
    }

    /**
     * 删除平台政策/商学院信息
     * 
     * @param id 平台政策/商学院主键
     * @return 结果
     */
    @Override
    public int deleteTPolicyInfoById(String id)
    {
        return tPolicyInfoMapper.deleteTPolicyInfoById(id);
    }
}
