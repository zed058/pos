package com.ruoyi.pos.web.mapper;

import java.util.List;
import com.ruoyi.pos.web.domain.TTerminalBindInfo;

/**
 * 终端绑定临时（第三方杉德）Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public interface TTerminalBindInfoMapper 
{
    /**
     * 查询终端绑定临时（第三方杉德）
     * 
     * @param id 终端绑定临时（第三方杉德）主键
     * @return 终端绑定临时（第三方杉德）
     */
    public TTerminalBindInfo selectTTerminalBindInfoById(String id);

    /**
     * 查询终端绑定临时（第三方杉德）列表
     * 
     * @param tTerminalBindInfo 终端绑定临时（第三方杉德）
     * @return 终端绑定临时（第三方杉德）集合
     */
    public List<TTerminalBindInfo> selectTTerminalBindInfoList(TTerminalBindInfo tTerminalBindInfo);

    /**
     * 新增终端绑定临时（第三方杉德）
     * 
     * @param tTerminalBindInfo 终端绑定临时（第三方杉德）
     * @return 结果
     */
    public int insertTTerminalBindInfo(TTerminalBindInfo tTerminalBindInfo);

    /**
     * 修改终端绑定临时（第三方杉德）
     * 
     * @param tTerminalBindInfo 终端绑定临时（第三方杉德）
     * @return 结果
     */
    public int updateTTerminalBindInfo(TTerminalBindInfo tTerminalBindInfo);

    /**
     * 删除终端绑定临时（第三方杉德）
     * 
     * @param id 终端绑定临时（第三方杉德）主键
     * @return 结果
     */
    public int deleteTTerminalBindInfoById(String id);

    /**
     * 批量删除终端绑定临时（第三方杉德）
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTerminalBindInfoByIds(String[] ids);
}
