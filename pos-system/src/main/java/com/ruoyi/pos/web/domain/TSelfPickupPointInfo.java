package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品自提点对象 t_self_pickup_point_info
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public class TSelfPickupPointInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;
    /*商品logo*/
    private String logo;
    /** 自提点店名 */
    @Excel(name = "自提点店名")
    private String shopName;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String phone;

    /** 地区 */
    @Excel(name = "地区")
    private String region;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String address;

    /** 经度 */
    @Excel(name = "经度")
    private String lng;

    /** 纬度 */
    @Excel(name = "纬度")
    private String lat;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setShopName(String shopName) 
    {
        this.shopName = shopName;
    }

    public String getShopName() 
    {
        return shopName;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setRegion(String region) 
    {
        this.region = region;
    }

    public String getRegion() 
    {
        return region;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setLng(String lng) 
    {
        this.lng = lng;
    }

    public String getLng() 
    {
        return lng;
    }
    public void setLat(String lat) 
    {
        this.lat = lat;
    }

    public String getLat() 
    {
        return lat;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("shopName", getShopName())
            .append("phone", getPhone())
            .append("region", getRegion())
            .append("address", getAddress())
            .append("lng", getLng())
            .append("lat", getLat())
            .append("createTime", getCreateTime())
            .append("logo", getLogo())
            .toString();
    }
}
