package com.ruoyi.pos.web.service.impl;

import java.util.List;

import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.mapper.TUserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TNoticeInfoMapper;
import com.ruoyi.pos.web.domain.TNoticeInfo;
import com.ruoyi.pos.web.service.ITNoticeInfoService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 平台公告Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@Service
public class TNoticeInfoServiceImpl implements ITNoticeInfoService 
{
    @Autowired
    private TNoticeInfoMapper tNoticeInfoMapper;
    @Autowired
    private TUserInfoMapper tUserInfoMapper;

    /**
     * 查询平台公告
     * 
     * @param id 平台公告主键
     * @return 平台公告
     */
    @Override
    public TNoticeInfo selectTNoticeInfoById(String id)
    {
        return tNoticeInfoMapper.selectTNoticeInfoById(id);
    }

    /**
     * 查询平台公告列表
     * 
     * @param tNoticeInfo 平台公告
     * @return 平台公告
     */
    @Override
    public List<TNoticeInfo> selectTNoticeInfoList(TNoticeInfo tNoticeInfo)
    {
        return tNoticeInfoMapper.selectTNoticeInfoList(tNoticeInfo);
    }

    /**
     * 新增平台公告
     * 
     * @param tNoticeInfo 平台公告
     * @return 结果
     */
    @Override
    public int insertTNoticeInfo(TNoticeInfo tNoticeInfo)
    {
        tNoticeInfo.setCreateUserId(SecurityUtils.getUserId());
        tNoticeInfo.setCreateTime(DateUtils.getNowDate());
        tNoticeInfo.setId(IdUtils.simpleUUID());
//        tNoticeInfo.setIssue(T);
        return tNoticeInfoMapper.insertTNoticeInfo(tNoticeInfo);
    }

    /**
     * 修改平台公告
     * 
     * @param tNoticeInfo 平台公告
     * @return 结果
     */
    @Override
    @Transactional
    public int updateTNoticeInfo(TNoticeInfo tNoticeInfo)
    {
        if (tNoticeInfo.getIssue().equals(YesOrNoEnums.NO.getCode())){//发布
            TUserInfo tUserInfo = new TUserInfo();
            tUserInfo.setUserNoticeType(YesOrNoEnums.YES.getCode());//0 未读
            tUserInfoMapper.updateTUserInfo(tUserInfo);
        }
        return tNoticeInfoMapper.updateTNoticeInfo(tNoticeInfo);
    }

    /**
     * 批量删除平台公告
     * 
     * @param ids 需要删除的平台公告主键
     * @return 结果
     */
    @Override
    public int deleteTNoticeInfoByIds(String[] ids)
    {
        return tNoticeInfoMapper.deleteTNoticeInfoByIds(ids);
    }

    /**
     * 删除平台公告信息
     * 
     * @param id 平台公告主键
     * @return 结果
     */
    @Override
    public int deleteTNoticeInfoById(String id)
    {
        return tNoticeInfoMapper.deleteTNoticeInfoById(id);
    }
}
