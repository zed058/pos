package com.ruoyi.pos.web.mapper;

import java.util.List;

import com.ruoyi.pos.api.vo.common.TUserAddressVo;
import com.ruoyi.pos.web.domain.TUserAddressInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

/**
 * 用户收货地址Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface TUserAddressInfoMapper 
{
    /**
     * 查询用户收货地址
     * 
     * @param id 用户收货地址主键
     * @return 用户收货地址
     */
    public TUserAddressInfo selectTUserAddressInfoById(String id);

    /**
     * 查询用户收货地址列表
     * 
     * @param tUserAddressInfo 用户收货地址
     * @return 用户收货地址集合
     */
    public List<TUserAddressInfo> selectTUserAddressInfoList(TUserAddressInfo tUserAddressInfo);

    /**
     * 新增用户收货地址
     * 
     * @param tUserAddressInfo 用户收货地址
     * @return 结果
     */
    public int insertTUserAddressInfo(TUserAddressInfo tUserAddressInfo);

    /**
     * 修改用户收货地址
     * 
     * @param tUserAddressInfo 用户收货地址
     * @return 结果
     */
    public int updateTUserAddressInfo(TUserAddressInfo tUserAddressInfo);

    /**
     * 删除用户收货地址
     * 
     * @param id 用户收货地址主键
     * @return 结果
     */
    public int deleteTUserAddressInfoById(String id);

    /**
     * 批量删除用户收货地址
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTUserAddressInfoByIds(String[] ids);

//    public TUserAddressInfo queryTUserAddressInfoById(@Param("id") String id, @P("userId") String userId);

    public List<TUserAddressInfo> selectTUserAddressInfoVo(TUserAddressVo tUserAddressVo);
}
