package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TGradeInfo;

/**
 * 用户vip等级Service接口
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public interface ITGradeInfoService
{
    /**
     * 查询用户vip等级
     * 
     * @param id 用户vip等级主键
     * @return 用户vip等级
     */
    public TGradeInfo selectTGradeInfoById(String id);

    /**
     * 查询用户vip等级列表
     * 
     * @param tGradeInfo 用户vip等级
     * @return 用户vip等级集合
     */
    public List<TGradeInfo> selectTGradeInfoList(TGradeInfo tGradeInfo);

    /**
     * 新增用户vip等级
     * 
     * @param tGradeInfo 用户vip等级
     * @return 结果
     */
    public int insertTGradeInfo(TGradeInfo tGradeInfo);

    /**
     * 修改用户vip等级
     * 
     * @param tGradeInfo 用户vip等级
     * @return 结果
     */
    public int updateTGradeInfo(TGradeInfo tGradeInfo);

    /**
     * 批量删除用户vip等级
     * 
     * @param ids 需要删除的用户vip等级主键集合
     * @return 结果
     */
    public int deleteTGradeInfoByIds(String[] ids);

    /**
     * 删除用户vip等级信息
     * 
     * @param id 用户vip等级主键
     * @return 结果
     */
    public int deleteTGradeInfoById(String id);
}
