package com.ruoyi.pos.web.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import com.ruoyi.common.enums.ReplyStateEnums;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.pos.web.domain.TOpinionImgInfo;
import com.ruoyi.pos.web.domain.TTerminalGoodsImgInfo;
import com.ruoyi.pos.web.mapper.TOpinionImgInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.ast.Operator;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TOpinionInfoMapper;
import com.ruoyi.pos.web.domain.TOpinionInfo;
import com.ruoyi.pos.web.service.ITOpinionInfoService;

/**
 * 意见反馈Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@Service
public class TOpinionInfoServiceImpl implements ITOpinionInfoService 
{
    @Autowired
    private TOpinionInfoMapper tOpinionInfoMapper;
    @Autowired
    private TOpinionImgInfoMapper tOpinionImgInfoMapper;

    /**
     * 查询意见反馈
     * 
     * @param id 意见反馈主键
     * @return 意见反馈
     */
    @Override
    public TOpinionInfo selectTOpinionInfoById(String id)
    {
        TOpinionInfo tOpinionInfo = tOpinionInfoMapper.selectTOpinionInfoById(id);
        tOpinionInfo.setReplyStateName(ReplyStateEnums.getEnumsByCode(tOpinionInfo.getReplyState()).getValue());

        TOpinionImgInfo tOpinionImgInfo =  new TOpinionImgInfo();
        tOpinionImgInfo.setOpinionId(tOpinionInfo.getId());
        List<TOpinionImgInfo> imgs = tOpinionImgInfoMapper.selectTOpinionImgInfoList(tOpinionImgInfo);
        String collect = imgs.stream().map(TOpinionImgInfo::getOpinionImg).collect(Collectors.joining(","));
        tOpinionInfo.setOpinionImg(collect);
        return tOpinionInfo;
    }

    /**
     * 查询意见反馈列表
     * 
     * @param tOpinionInfo 意见反馈
     * @return 意见反馈
     */
    @Override
    public List<TOpinionInfo> selectTOpinionInfoList(TOpinionInfo tOpinionInfo)
    {
        List<TOpinionInfo> tOpinionInfos = tOpinionInfoMapper.selectTOpinionInfoList(tOpinionInfo);
        Optional.ofNullable(tOpinionInfos).ifPresent(map->{
            map.stream().forEach(bean ->{
                bean.setReplyStateName(Optional.ofNullable(ReplyStateEnums.getEnumsByCode(bean.getReplyState())).map(item ->item.getValue()).orElse(bean.getReplyState()));
            });
        });
        return tOpinionInfos;
    }

    /**
     * 新增意见反馈
     * 
     * @param tOpinionInfo 意见反馈
     * @return 结果
     */
    @Override
    public int insertTOpinionInfo(TOpinionInfo tOpinionInfo)
    {
        tOpinionInfo.setCreateTime(DateUtils.getNowDate());
        return tOpinionInfoMapper.insertTOpinionInfo(tOpinionInfo);
    }

    /**
     * 修改意见反馈
     * 
     * @param tOpinionInfo 意见反馈
     * @return 结果
     */
    @Override
    public int updateTOpinionInfo(TOpinionInfo tOpinionInfo)
    {
        return tOpinionInfoMapper.updateTOpinionInfo(tOpinionInfo);
    }

    /**
     * 批量删除意见反馈
     * 
     * @param ids 需要删除的意见反馈主键
     * @return 结果
     */
    @Override
    public int deleteTOpinionInfoByIds(String[] ids)
    {
        return tOpinionInfoMapper.deleteTOpinionInfoByIds(ids);
    }

    /**
     * 删除意见反馈信息
     * 
     * @param id 意见反馈主键
     * @return 结果
     */
    @Override
    public int deleteTOpinionInfoById(String id)
    {
        return tOpinionInfoMapper.deleteTOpinionInfoById(id);
    }
}
