package com.ruoyi.pos.web.mapper;

import java.util.List;

import com.ruoyi.pos.api.dto.BaseDto;
import com.ruoyi.pos.api.dto.user.UserDefaultDto;
import com.ruoyi.pos.api.vo.user.UserDefaultVo;
import com.ruoyi.pos.web.domain.TUserDefaultInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 用户出入账信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public interface TUserDefaultInfoMapper 
{
    /**
     * 查询用户出入账信息
     * 
     * @param id 用户出入账信息主键
     * @return 用户出入账信息
     */
    public TUserDefaultInfo selectTUserDefaultInfoById(String id);

    /**
     * 查询用户出入账信息列表
     * 
     * @param tUserDefaultInfo 用户出入账信息
     * @return 用户出入账信息集合
     */
    public List<TUserDefaultInfo> selectTUserDefaultInfoList(TUserDefaultInfo tUserDefaultInfo);

    /**
     * 新增用户出入账信息
     * 
     * @param tUserDefaultInfo 用户出入账信息
     * @return 结果
     */
    public int insertTUserDefaultInfo(TUserDefaultInfo tUserDefaultInfo);

    /**
     * 修改用户出入账信息
     * 
     * @param tUserDefaultInfo 用户出入账信息
     * @return 结果
     */
    public int updateTUserDefaultInfo(TUserDefaultInfo tUserDefaultInfo);

    /**
     * 删除用户出入账信息
     * 
     * @param id 用户出入账信息主键
     * @return 结果
     */
    public int deleteTUserDefaultInfoById(String id);

    /**
     * 批量删除用户出入账信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTUserDefaultInfoByIds(String[] ids);


    public TUserDefaultInfo queryMoney(@Param("userId") String userId, @Param("brandId") String brandId, @Param("transOperType") List<String> transOperType, @Param("transType") String transType, @Param("dateTime") String dateTime);

    public List<TUserDefaultInfo> queryMyCommonDetails(UserDefaultDto userDefaultDto);

    public List<TUserDefaultInfo> queryUserWithdraw(UserDefaultDto userDefaultDto);


    public List<TUserDefaultInfo> queryMySerialDetails(@Param("userId") String userId, @Param("transOperType") List<String> transOperType, @Param("transType") String transType, @Param("dateTime") String dateTime);

    public TUserDefaultInfo queryMoneyIf(@Param("userId") String userId, @Param("brandId") String brandId,  @Param("transOperType") String transOperType, @Param("transType") String transType, @Param("dateTime") String dateTime);

    public List<UserDefaultVo> queryDetailList(@Param("userId") String userId, @Param("brandId") String brandId, @Param("transOperType") List<String> transOperType, @Param("transType") String transType, @Param("dateTime") String dateTime);

    List<TUserDefaultInfo> selectTUserDefaultInfoListForWeb(TUserDefaultInfo tUserDefaultInfo);
    
    List<TUserDefaultInfo> selectRecordList(TUserDefaultInfo tUserDefaultInfo);

    List<TUserDefaultInfo> exchangeRecordList(TUserDefaultInfo userDefaultInfo);
}
