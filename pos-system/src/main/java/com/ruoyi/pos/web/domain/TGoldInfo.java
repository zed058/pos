package com.ruoyi.pos.web.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 交易金序列号对象 t_gold_info
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
public class TGoldInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键（自增）【交易金序列号】 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 是否已转化余额(0：是，1:否) */
    @Excel(name = "是否已转化余额(0：是，1:否)")
    private String exchange;

    /** 兑换时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "兑换时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date exchangeTime;

    /** 序列号id */
    @Excel(name = "序列号id")
    private Long serialId;

    /** 序列号类型（1、交易金序列号，2、奖励金序列号） */
    @Excel(name = "序列号类型", readConverterExp = "1=、交易金序列号，2、奖励金序列号")
    private String serialType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setExchange(String exchange) 
    {
        this.exchange = exchange;
    }

    public String getExchange() 
    {
        return exchange;
    }
    public void setExchangeTime(Date exchangeTime) 
    {
        this.exchangeTime = exchangeTime;
    }

    public Date getExchangeTime() 
    {
        return exchangeTime;
    }
    public void setSerialId(Long serialId) 
    {
        this.serialId = serialId;
    }

    public Long getSerialId() 
    {
        return serialId;
    }
    public void setSerialType(String serialType) 
    {
        this.serialType = serialType;
    }

    public String getSerialType() 
    {
        return serialType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("exchange", getExchange())
            .append("exchangeTime", getExchangeTime())
            .append("createTime", getCreateTime())
            .append("serialId", getSerialId())
            .append("serialType", getSerialType())
            .toString();
    }
}
