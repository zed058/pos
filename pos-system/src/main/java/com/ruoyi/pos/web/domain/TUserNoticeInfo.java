package com.ruoyi.pos.web.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户阅读信息对象 t_user_notice_info
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public class TUserNoticeInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 消息id */
    @Excel(name = "消息id")
    private String noticeId;

    /** 是否已读信息 */
    @Excel(name = "是否已读信息")
    private String isRead;
    private String isReadName;

    /** 阅读时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "阅读时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date readTime;

    /** 是否有效 */
    @Excel(name = "是否有效")
    private String isValid;
    private String validName;

    /*用户名*/
    private String userName;

    /*标题*/
    private String noticeTitle;

    public String getIsReadName() {
        return isReadName;
    }

    public void setIsReadName(String isReadName) {
        this.isReadName = isReadName;
    }

    public String getValidName() {
        return validName;
    }

    public void setValidName(String validName) {
        this.validName = validName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setNoticeId(String noticeId) 
    {
        this.noticeId = noticeId;
    }

    public String getNoticeId() 
    {
        return noticeId;
    }
    public void setIsRead(String isRead) 
    {
        this.isRead = isRead;
    }

    public String getIsRead() 
    {
        return isRead;
    }
    public void setReadTime(Date readTime) 
    {
        this.readTime = readTime;
    }

    public Date getReadTime() 
    {
        return readTime;
    }
    public void setIsValid(String isValid) 
    {
        this.isValid = isValid;
    }

    public String getIsValid() 
    {
        return isValid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("noticeId", getNoticeId())
            .append("isRead", getIsRead())
            .append("readTime", getReadTime())
            .append("isValid", getIsValid())
            .append("createTime", getCreateTime())
            .toString();
    }
}
