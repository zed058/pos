package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户vip等级对象 t_user_grade_info
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public class TGradeInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 等级（v1-v2-v3。。。。。。v-10） */
    @Excel(name = "等级", readConverterExp = "v=1-v2-v3。。。。。。v-10")
    private String gradeCode;

    /** 等级名称(骑士。。。。。。。司令) */
    @Excel(name = "等级名称(骑士。。。。。。。司令)")
    private String gradeName;


    /** 直属分润 */
    @Excel(name = "直属分润")
    private BigDecimal shareProfit;

    /** 分润差额 */
    @Excel(name = "分润差额")
    private BigDecimal shareProfitBalance;

    /** 绑定终端数量 */
    @Excel(name = "绑定终端数量")
    private BigDecimal terminalSum;

    /** 交易金额 */
    @Excel(name = "交易金额")
    private BigDecimal badSum;

    /** 排序 */
    @Excel(name = "排序")
    private int sort;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setGradeCode(String gradeCode) 
    {
        this.gradeCode = gradeCode;
    }

    public String getGradeCode() 
    {
        return gradeCode;
    }
    public void setGradeName(String gradeName) 
    {
        this.gradeName = gradeName;
    }

    public String getGradeName() 
    {
        return gradeName;
    }
    public void setShareProfit(BigDecimal shareProfit) 
    {
        this.shareProfit = shareProfit;
    }

    public BigDecimal getShareProfit() 
    {
        return shareProfit;
    }
    public void setShareProfitBalance(BigDecimal shareProfitBalance) 
    {
        this.shareProfitBalance = shareProfitBalance;
    }

    public BigDecimal getShareProfitBalance() 
    {
        return shareProfitBalance;
    }
    public void setTerminalSum(BigDecimal terminalSum) 
    {
        this.terminalSum = terminalSum;
    }

    public BigDecimal getTerminalSum() 
    {
        return terminalSum;
    }
    public void setBadSum(BigDecimal badSum)
    {
        this.badSum = badSum;
    }

    public BigDecimal getBadSum()
    {
        return badSum;
    }
    public void setSort(int sort)
    {
        this.sort = sort;
    }

    public int getSort()
    {
        return sort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("gradeCode", getGradeCode())
            .append("gradeName", getGradeName())
            .append("shareProfit", getShareProfit())
            .append("shareProfitBalance", getShareProfitBalance())
            .append("terminalSum", getTerminalSum())
            .append("badSum", getBadSum())
            .append("sort", getSort())
            .append("createTime", getCreateTime())
            .toString();
    }
}
