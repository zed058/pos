package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TOpinionImgInfo;

/**
 * 意见反馈图片Service接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface ITOpinionImgInfoService 
{
    /**
     * 查询意见反馈图片
     * 
     * @param id 意见反馈图片主键
     * @return 意见反馈图片
     */
    public TOpinionImgInfo selectTOpinionImgInfoById(String id);

    /**
     * 查询意见反馈图片列表
     * 
     * @param tOpinionImgInfo 意见反馈图片
     * @return 意见反馈图片集合
     */
    public List<TOpinionImgInfo> selectTOpinionImgInfoList(TOpinionImgInfo tOpinionImgInfo);

    /**
     * 新增意见反馈图片
     * 
     * @param tOpinionImgInfo 意见反馈图片
     * @return 结果
     */
    public int insertTOpinionImgInfo(TOpinionImgInfo tOpinionImgInfo);

    /**
     * 修改意见反馈图片
     * 
     * @param tOpinionImgInfo 意见反馈图片
     * @return 结果
     */
    public int updateTOpinionImgInfo(TOpinionImgInfo tOpinionImgInfo);

    /**
     * 批量删除意见反馈图片
     * 
     * @param ids 需要删除的意见反馈图片主键集合
     * @return 结果
     */
    public int deleteTOpinionImgInfoByIds(String[] ids);

    /**
     * 删除意见反馈图片信息
     * 
     * @param id 意见反馈图片主键
     * @return 结果
     */
    public int deleteTOpinionImgInfoById(String id);
}
