package com.ruoyi.pos.web.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 工单管理对象 t_user_work_order_info
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public class TUserWorkOrderInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private String id;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String userName;

    /** 工单管理标题 */
    @Excel(name = "工单管理标题")
    private String title;

    /** 工单管理类别id */
    @Excel(name = "工单管理类别id")
    private String workOrderId;

    /** 工单管理类别名称 */
    @Excel(name = "工单管理类别名称")
    private String workOrderName;

    /** 申请理由 */
    @Excel(name = "申请理由")
    private String reason;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String terminalName;

    /** 上传机器SN图片 */
    @Excel(name = "上传机器SN图片")
    private String terminalImg;

    /** 工单描述 */
    @Excel(name = "工单描述")
    private String workOrderDescribe;

    /** 工单描述图片 */
    @Excel(name = "工单描述图片")
    private String describeImg;

    /** 工单审核（0：待审核，1：审核通过，2：审核驳回） */
    @Excel(name = "工单审核", readConverterExp = "0=：待审核，1：审核通过，2：审核驳回")
    private String audit;

    /** 驳回理由 */
    @Excel(name = "驳回理由")
    private String rejectReason;

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date auditTime;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setWorkOrderId(String workOrderId) 
    {
        this.workOrderId = workOrderId;
    }

    public String getWorkOrderId() 
    {
        return workOrderId;
    }
    public void setWorkOrderName(String workOrderName) 
    {
        this.workOrderName = workOrderName;
    }

    public String getWorkOrderName() 
    {
        return workOrderName;
    }
    public void setReason(String reason) 
    {
        this.reason = reason;
    }

    public String getReason() 
    {
        return reason;
    }
    public void setTerminalName(String terminalName) 
    {
        this.terminalName = terminalName;
    }

    public String getTerminalName() 
    {
        return terminalName;
    }
    public void setTerminalImg(String terminalImg) 
    {
        this.terminalImg = terminalImg;
    }

    public String getTerminalImg() 
    {
        return terminalImg;
    }

    public String getWorkOrderDescribe() {
        return workOrderDescribe;
    }

    public void setWorkOrderDescribe(String workOrderDescribe) {
        this.workOrderDescribe = workOrderDescribe;
    }

    public void setDescribeImg(String describeImg)
    {
        this.describeImg = describeImg;
    }

    public String getDescribeImg() 
    {
        return describeImg;
    }
    public void setAudit(String audit) 
    {
        this.audit = audit;
    }

    public String getAudit() 
    {
        return audit;
    }
    public void setRejectReason(String rejectReason) 
    {
        this.rejectReason = rejectReason;
    }

    public String getRejectReason() 
    {
        return rejectReason;
    }
    public void setAuditTime(Date auditTime) 
    {
        this.auditTime = auditTime;
    }

    public Date getAuditTime() 
    {
        return auditTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("title", getTitle())
            .append("workOrderId", getWorkOrderId())
            .append("workOrderName", getWorkOrderName())
            .append("reason", getReason())
            .append("terminalName", getTerminalName())
            .append("terminalImg", getTerminalImg())
            .append("workOrderDescribe", getWorkOrderDescribe())
            .append("describeImg", getDescribeImg())
            .append("audit", getAudit())
            .append("rejectReason", getRejectReason())
            .append("createTime", getCreateTime())
            .append("auditTime", getAuditTime())
            .toString();
    }
}
