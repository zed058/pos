package com.ruoyi.pos.web.webDto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Administrator
 * @title: TTerminalInfoWebDto
 * @projectName ruoyi
 * @description: TODO
 */
@Data
public class TTerminalInfoWebDto {
    /** 商品品牌id */
    private String brandId;
    /** 商品id */
    private String goodsId;
    /** 终端机型 */
    private String terminalModel;
    /** 终端SN码 */
    private String snCode;

    /** 开始终端SN码 */
    private String beginSnCode;
    /** 结束终端SN码 */
    private String endSnCode;
    /** 结束终端SN码 */
    private BigDecimal terminalGmv;
    /*添加状态（0：字符串添加，1：循环添加）*/
    private String isValid;

    /*绑定时间*/
    private Date bindingTime;
}
