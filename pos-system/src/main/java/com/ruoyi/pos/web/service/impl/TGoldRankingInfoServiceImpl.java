package com.ruoyi.pos.web.service.impl;

import java.util.List;

import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TGoldRankingInfoMapper;
import com.ruoyi.pos.web.domain.TGoldRankingInfo;
import com.ruoyi.pos.web.service.ITGoldRankingInfoService;

/**
 * 交易金/奖励金（排行榜）回台添加app显示的假数据Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-20
 */
@Service
public class TGoldRankingInfoServiceImpl implements ITGoldRankingInfoService 
{
    @Autowired
    private TGoldRankingInfoMapper tGoldRankingInfoMapper;

    /**
     * 查询交易金/奖励金（排行榜）回台添加app显示的假数据
     * 
     * @param id 交易金/奖励金（排行榜）回台添加app显示的假数据主键
     * @return 交易金/奖励金（排行榜）回台添加app显示的假数据
     */
    @Override
    public TGoldRankingInfo selectTGoldRankingInfoById(String id)
    {
        return tGoldRankingInfoMapper.selectTGoldRankingInfoById(id);
    }

    /**
     * 查询交易金/奖励金（排行榜）回台添加app显示的假数据列表
     * 
     * @param tGoldRankingInfo 交易金/奖励金（排行榜）回台添加app显示的假数据
     * @return 交易金/奖励金（排行榜）回台添加app显示的假数据
     */
    @Override
    public List<TGoldRankingInfo> selectTGoldRankingInfoList(TGoldRankingInfo tGoldRankingInfo)
    {
        return tGoldRankingInfoMapper.selectTGoldRankingInfoList(tGoldRankingInfo);
    }

    /**
     * 新增交易金/奖励金（排行榜）回台添加app显示的假数据
     * 
     * @param tGoldRankingInfo 交易金/奖励金（排行榜）回台添加app显示的假数据
     * @return 结果
     */
    @Override
    public int insertTGoldRankingInfo(TGoldRankingInfo tGoldRankingInfo)
    {
        tGoldRankingInfo.setId(IdUtils.simpleUUID());
        return tGoldRankingInfoMapper.insertTGoldRankingInfo(tGoldRankingInfo);
    }

    /**
     * 修改交易金/奖励金（排行榜）回台添加app显示的假数据
     * 
     * @param tGoldRankingInfo 交易金/奖励金（排行榜）回台添加app显示的假数据
     * @return 结果
     */
    @Override
    public int updateTGoldRankingInfo(TGoldRankingInfo tGoldRankingInfo)
    {
        return tGoldRankingInfoMapper.updateTGoldRankingInfo(tGoldRankingInfo);
    }

    /**
     * 批量删除交易金/奖励金（排行榜）回台添加app显示的假数据
     * 
     * @param ids 需要删除的交易金/奖励金（排行榜）回台添加app显示的假数据主键
     * @return 结果
     */
    @Override
    public int deleteTGoldRankingInfoByIds(String[] ids)
    {
        return tGoldRankingInfoMapper.deleteTGoldRankingInfoByIds(ids);
    }

    /**
     * 删除交易金/奖励金（排行榜）回台添加app显示的假数据信息
     * 
     * @param id 交易金/奖励金（排行榜）回台添加app显示的假数据主键
     * @return 结果
     */
    @Override
    public int deleteTGoldRankingInfoById(String id)
    {
        return tGoldRankingInfoMapper.deleteTGoldRankingInfoById(id);
    }
}
