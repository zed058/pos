package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商品基础对象 t_terminal_goods_info
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public class TTerminalGoodsInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 终端购买品牌id（兑换商城商品id) */
    @Excel(name = "终端购买品牌id", readConverterExp = "终端购买品牌id（兑换商城商品id)")
    private String brandId;
    /*显示*/
    private String brandIdShow;

    /** 商品类型（0：终端商品，1：兑换商品） */
    @Excel(name = "商品类型", readConverterExp = "0=：终端商品，1：兑换商品")
    private String goodsType;

    /** 商品交易类型（10:采购,20:积分兑换，21:交易金兑换，22:奖励金兑换，23:金币兑换） */
    @Excel(name = "商品交易类型", readConverterExp = "1=0:采购,20:积分兑换，21:交易金兑换，22:奖励金兑换，23:金币兑换")
    private String goodsStatus;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodsName;

    /** 商品图片 */
    @Excel(name = "商品图片")
    private String goodsImg;

    /** 商品所有人 */
    @Excel(name = "商品所有人")
    private String goodsUserId;

    /** 兑换/购买金额 */
    @Excel(name = "兑换/购买金额")
    private BigDecimal amount;

    /** 兑换商城商品（配送费） */
    @Excel(name = "兑换商城商品", readConverterExp = "配=送费")
    private BigDecimal deliveryFee;

    /** 商品单位 */
    @Excel(name = "商品单位")
    private String unit;

    /** 兑换单位（购买时默认为人民币） */
    @Excel(name = "兑换单位", readConverterExp = "购=买时默认为人民币")
    private String exchangeUnit;

    /** 商品排序 */
    @Excel(name = "商品排序")
    private String sort;

    /** 商品状态（0：正常，1：禁用） */
    @Excel(name = "商品状态", readConverterExp = "0=：正常，1：禁用")
    private String isValid;

    /** 文字介绍 */
    @Excel(name = "文字介绍")
    private String goodsDescribe;

    /** 产品兑换说明(兑换商城) */
    @Excel(name = "产品兑换说明(兑换商城)")
    private String conversionExplain;

    /** 产品说明（富文本） */
    @Excel(name = "产品说明", readConverterExp = "富=文本")
    private String goodsExplain;

    /** 是否热门推荐（0：是，1：不是） */
    @Excel(name = "是否热门推荐", readConverterExp = "0=：是，1：不是")
    private String isPopular;


    /** 商品轮播图 */
    @Excel(name = "商品轮播图")
    private String goodsImgS;

    /*终端SN码*/
    private String snCode;

    public String getSnCode() {
        return snCode;
    }

    public void setSnCode(String snCode) {
        this.snCode = snCode;
    }

    public String getGoodsImgS() {
        return goodsImgS;
    }

    public void setGoodsImgS(String goodsImgS) {
        this.goodsImgS = goodsImgS;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setBrandId(String brandId)
    {
        this.brandId = brandId;
    }

    public String getBrandId()
    {
        return brandId;
    }
    public void setGoodsType(String goodsType)
    {
        this.goodsType = goodsType;
    }

    public String getGoodsType()
    {
        return goodsType;
    }
    public void setGoodsStatus(String goodsStatus)
    {
        this.goodsStatus = goodsStatus;
    }

    public String getGoodsStatus()
    {
        return goodsStatus;
    }
    public void setGoodsName(String goodsName)
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName()
    {
        return goodsName;
    }
    public void setGoodsImg(String goodsImg)
    {
        this.goodsImg = goodsImg;
    }

    public String getGoodsImg()
    {
        return goodsImg;
    }
    public void setGoodsUserId(String goodsUserId)
    {
        this.goodsUserId = goodsUserId;
    }

    public String getGoodsUserId()
    {
        return goodsUserId;
    }
    public void setAmount(BigDecimal amount)
    {
        this.amount = amount;
    }

    public BigDecimal getAmount()
    {
        return amount;
    }
    public void setDeliveryFee(BigDecimal deliveryFee)
    {
        this.deliveryFee = deliveryFee;
    }

    public BigDecimal getDeliveryFee()
    {
        return deliveryFee;
    }
    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public String getUnit()
    {
        return unit;
    }
    public void setExchangeUnit(String exchangeUnit)
    {
        this.exchangeUnit = exchangeUnit;
    }

    public String getExchangeUnit()
    {
        return exchangeUnit;
    }
    public void setSort(String sort)
    {
        this.sort = sort;
    }

    public String getSort()
    {
        return sort;
    }
    public void setIsValid(String isValid)
    {
        this.isValid = isValid;
    }

    public String getIsValid()
    {
        return isValid;
    }
    public void setGoodsDescribe(String goodsDescribe)
    {
        this.goodsDescribe = goodsDescribe;
    }

    public String getGoodsDescribe()
    {
        return goodsDescribe;
    }
    public void setConversionExplain(String conversionExplain)
    {
        this.conversionExplain = conversionExplain;
    }

    public String getConversionExplain()
    {
        return conversionExplain;
    }
    public void setGoodsExplain(String goodsExplain)
    {
        this.goodsExplain = goodsExplain;
    }

    public String getGoodsExplain()
    {
        return goodsExplain;
    }
    public void setIsPopular(String isPopular)
    {
        this.isPopular = isPopular;
    }

    public String getIsPopular()
    {
        return isPopular;
    }

    public String getBrandIdShow() {
        return brandIdShow;
    }

    public void setBrandIdShow(String brandIdShow) {
        this.brandIdShow = brandIdShow;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("brandId", getBrandId())
                .append("goodsType", getGoodsType())
                .append("goodsStatus", getGoodsStatus())
                .append("goodsName", getGoodsName())
                .append("goodsImg", getGoodsImg())
                .append("goodsUserId", getGoodsUserId())
                .append("amount", getAmount())
                .append("deliveryFee", getDeliveryFee())
                .append("unit", getUnit())
                .append("exchangeUnit", getExchangeUnit())
                .append("sort", getSort())
                .append("isValid", getIsValid())
                .append("goodsDescribe", getGoodsDescribe())
                .append("createTime", getCreateTime())
                .append("conversionExplain", getConversionExplain())
                .append("goodsExplain", getGoodsExplain())
                .append("isPopular", getIsPopular())
                .append("goodsImgS", getGoodsImgS())
                .toString();
    }
}
