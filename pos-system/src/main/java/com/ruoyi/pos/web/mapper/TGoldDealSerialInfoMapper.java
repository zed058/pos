package com.ruoyi.pos.web.mapper;

import java.util.List;

import com.ruoyi.pos.api.vo.user.GoldserialVo;
import com.ruoyi.pos.web.domain.TGoldAwardSerialInfo;
import com.ruoyi.pos.web.domain.TGoldDealSerialInfo;
import org.apache.ibatis.annotations.Param;

/**
 * (金币序列号Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-20
 */
public interface TGoldDealSerialInfoMapper 
{
    /**
     * 查询(金币序列号
     * 
     * @param id (金币序列号主键
     * @return (金币序列号
     */
    public TGoldDealSerialInfo selectTGoldDealSerialInfoById(Long id);

    /**
     * 查询(金币序列号列表
     * 
     * @param tGoldDealSerialInfo (金币序列号
     * @return (金币序列号集合
     */
    public List<TGoldDealSerialInfo> selectTGoldDealSerialInfoList(TGoldDealSerialInfo tGoldDealSerialInfo);
    public List<TGoldDealSerialInfo> selectTGoldDealSerialInfoList2(TGoldDealSerialInfo tGoldDealSerialInfo);

    /**
     * 新增(金币序列号
     * 
     * @param tGoldDealSerialInfo (金币序列号
     * @return 结果
     */
    public int insertTGoldDealSerialInfo(TGoldDealSerialInfo tGoldDealSerialInfo);

    /**
     * 修改(金币序列号
     * 
     * @param tGoldDealSerialInfo (金币序列号
     * @return 结果
     */
    public int updateTGoldDealSerialInfo(TGoldDealSerialInfo tGoldDealSerialInfo);

    /**
     * 删除(金币序列号
     * 
     * @param id (金币序列号主键
     * @return 结果
     */
    public int deleteTGoldDealSerialInfoById(Long id);

    /**
     * 批量删除(金币序列号
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTGoldDealSerialInfoByIds(Long[] ids);


//    public List<TGoldDealSerialInfo> queryTGoldDealSerialMaxList(TGoldDealSerialInfo tGoldDealSerialInfo);


    //平台最大交易金序列号
    public TGoldDealSerialInfo queryDealMax();
    //个人可消费交易金序列号
    public TGoldDealSerialInfo queryDealExchangeMax();
    //平台最大交易金序列号
    public TGoldDealSerialInfo queryDealAllMax();

    //我个人可消费所有的交易金序列号
    public int queryDealCount(@Param("userId")String userId);

    //平台动态所有已兑奖的交易金序列号
    public List<GoldserialVo> queryDealRoll();

    public List<TGoldDealSerialInfo> selectTGoldDealExchangeTimeList(@Param("userId") String userId, @Param("exchange") String exchange, @Param("createTime") String createTime, @Param("exchangeTime") String exchangeTime);
    //已中奖序列号倒序排列
    public List<TGoldDealSerialInfo> selectTGoldDealSerialInfoDesc(TGoldDealSerialInfo tGoldDealSerialInfo);


}
