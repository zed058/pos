package com.ruoyi.pos.web.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TTerminalBrandInfoMapper;
import com.ruoyi.pos.web.domain.TTerminalBrandInfo;
import com.ruoyi.pos.web.service.ITTerminalBrandInfoService;

/**
 * 商品品牌Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@Service
public class TTerminalBrandInfoServiceImpl implements ITTerminalBrandInfoService 
{
    @Autowired
    private TTerminalBrandInfoMapper tTerminalBrandInfoMapper;

    /**
     * 查询商品品牌
     * 
     * @param id 商品品牌主键
     * @return 商品品牌
     */
    @Override
    public TTerminalBrandInfo selectTTerminalBrandInfoById(String id)
    {
        return tTerminalBrandInfoMapper.selectTTerminalBrandInfoById(id);
    }

    /**
     * 查询商品品牌列表
     * 
     * @param tTerminalBrandInfo 商品品牌
     * @return 商品品牌
     */
    @Override
    public List<TTerminalBrandInfo> selectTTerminalBrandInfoList(TTerminalBrandInfo tTerminalBrandInfo)
    {
        return tTerminalBrandInfoMapper.selectTTerminalBrandInfoList(tTerminalBrandInfo);
    }

    /**
     * 新增商品品牌
     * 
     * @param tTerminalBrandInfo 商品品牌
     * @return 结果
     */
    @Override
    public int insertTTerminalBrandInfo(TTerminalBrandInfo tTerminalBrandInfo)
    {
        tTerminalBrandInfo.setId(IdUtils.simpleUUID());
        tTerminalBrandInfo.setCreateTime(DateUtils.getNowDate());
        return tTerminalBrandInfoMapper.insertTTerminalBrandInfo(tTerminalBrandInfo);
    }

    /**
     * 修改商品品牌
     * 
     * @param tTerminalBrandInfo 商品品牌
     * @return 结果
     */
    @Override
    public int updateTTerminalBrandInfo(TTerminalBrandInfo tTerminalBrandInfo)
    {
        return tTerminalBrandInfoMapper.updateTTerminalBrandInfo(tTerminalBrandInfo);
    }

    /**
     * 批量删除商品品牌
     * 
     * @param ids 需要删除的商品品牌主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalBrandInfoByIds(String[] ids)
    {
        return tTerminalBrandInfoMapper.deleteTTerminalBrandInfoByIds(ids);
    }

    /**
     * 删除商品品牌信息
     * 
     * @param id 商品品牌主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalBrandInfoById(String id)
    {
        return tTerminalBrandInfoMapper.deleteTTerminalBrandInfoById(id);
    }
}
