package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TStatisticInfo;

/**
 * 统计信息Service接口
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public interface ITStatisticInfoService 
{
    /**
     * 查询统计信息
     * 
     * @param id 统计信息主键
     * @return 统计信息
     */
    public TStatisticInfo selectTStatisticInfoById(String id);

    /**
     * 查询统计信息列表
     * 
     * @param tStatisticInfo 统计信息
     * @return 统计信息集合
     */
    public List<TStatisticInfo> selectTStatisticInfoList(TStatisticInfo tStatisticInfo);

    /**
     * 新增统计信息
     * 
     * @param tStatisticInfo 统计信息
     * @return 结果
     */
    public int insertTStatisticInfo(TStatisticInfo tStatisticInfo);

    /**
     * 修改统计信息
     * 
     * @param tStatisticInfo 统计信息
     * @return 结果
     */
    public int updateTStatisticInfo(TStatisticInfo tStatisticInfo);

    /**
     * 批量删除统计信息
     * 
     * @param ids 需要删除的统计信息主键集合
     * @return 结果
     */
    public int deleteTStatisticInfoByIds(String[] ids);

    /**
     * 删除统计信息信息
     * 
     * @param id 统计信息主键
     * @return 结果
     */
    public int deleteTStatisticInfoById(String id);
}
