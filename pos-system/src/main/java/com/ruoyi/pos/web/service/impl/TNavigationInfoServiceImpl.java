package com.ruoyi.pos.web.service.impl;

import java.util.List;

import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TNavigationInfoMapper;
import com.ruoyi.pos.web.domain.TNavigationInfo;
import com.ruoyi.pos.web.service.ITNavigationInfoService;

/**
 * 金刚导航栏Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
@Service
public class TNavigationInfoServiceImpl implements ITNavigationInfoService 
{
    @Autowired
    private TNavigationInfoMapper tNavigationInfoMapper;

    /**
     * 查询金刚导航栏
     * 
     * @param id 金刚导航栏主键
     * @return 金刚导航栏
     */
    @Override
    public TNavigationInfo selectTNavigationInfoById(String id)
    {
        return tNavigationInfoMapper.selectTNavigationInfoById(id);
    }

    /**
     * 查询金刚导航栏列表
     * 
     * @param tNavigationInfo 金刚导航栏
     * @return 金刚导航栏
     */
    @Override
    public List<TNavigationInfo> selectTNavigationInfoList(TNavigationInfo tNavigationInfo)
    {
        List<TNavigationInfo> list = tNavigationInfoMapper.selectTNavigationInfoList(tNavigationInfo);
        list.stream().forEach(bean ->{
            if (YesOrNoEnums.NO.getCode().equals(bean.getLinkType())) {
                bean.setLinkType("外链");
            } else {
                bean.setLinkType("内链");
            }
        });
        return list;
    }

    /**
     * 新增金刚导航栏
     * 
     * @param tNavigationInfo 金刚导航栏
     * @return 结果
     */
    @Override
    public int insertTNavigationInfo(TNavigationInfo tNavigationInfo)
    {
        tNavigationInfo.setCreateTime(DateUtils.getNowDate());
        tNavigationInfo.setId(IdUtils.simpleUUID());
        return tNavigationInfoMapper.insertTNavigationInfo(tNavigationInfo);
    }

    /**
     * 修改金刚导航栏
     * 
     * @param tNavigationInfo 金刚导航栏
     * @return 结果
     */
    @Override
    public int updateTNavigationInfo(TNavigationInfo tNavigationInfo)
    {
        return tNavigationInfoMapper.updateTNavigationInfo(tNavigationInfo);
    }

    /**
     * 批量删除金刚导航栏
     * 
     * @param ids 需要删除的金刚导航栏主键
     * @return 结果
     */
    @Override
    public int deleteTNavigationInfoByIds(String[] ids)
    {
        return tNavigationInfoMapper.deleteTNavigationInfoByIds(ids);
    }

    /**
     * 删除金刚导航栏信息
     * 
     * @param id 金刚导航栏主键
     * @return 结果
     */
    @Override
    public int deleteTNavigationInfoById(String id)
    {
        return tNavigationInfoMapper.deleteTNavigationInfoById(id);
    }
}
