package com.ruoyi.pos.web.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * (金币序列号对象 t_gold_award_serial_info
 * 
 * @author ruoyi
 * @date 2021-11-20
 */
public class TGoldAwardSerialInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键（自增）【奖励金序列号】 */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 是否已兑换(0:是，1：否) */
    @Excel(name = "是否已兑换(0:是，1：否)")
    private String exchange;

    /** 兑换时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "兑换时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date exchangeTime;

    /** 兑换奖励金（金额） */
    @Excel(name = "兑换奖励金", readConverterExp = "金=额")
    private BigDecimal amount;
    //平台发放的奖励金序列号
    private Long serialNo;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setExchange(String exchange) 
    {
        this.exchange = exchange;
    }

    public String getExchange() 
    {
        return exchange;
    }
    public void setExchangeTime(Date exchangeTime) 
    {
        this.exchangeTime = exchangeTime;
    }

    public Date getExchangeTime() 
    {
        return exchangeTime;
    }
    public void setAmount(BigDecimal amount) 
    {
        this.amount = amount;
    }

    public BigDecimal getAmount() 
    {
        return amount;
    }

    public Long getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(Long serialNo) {
        this.serialNo = serialNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("exchange", getExchange())
            .append("exchangeTime", getExchangeTime())
            .append("amount", getAmount())
            .append("createTime", getCreateTime())
            .toString();
    }
}
