package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TAboutUsInfo;

/**
 * 平台设置（关于我们）Service接口
 * 
 * @author ruoyi
 * @date 2021-11-12
 */
public interface ITAboutUsInfoService 
{
    /**
     * 查询平台设置（关于我们）
     * 
     * @param id 平台设置（关于我们）主键
     * @return 平台设置（关于我们）
     */
    public TAboutUsInfo selectTAboutUsInfoById(String id);

    /**
     * 查询平台设置（关于我们）列表
     * 
     * @param tAboutUsInfo 平台设置（关于我们）
     * @return 平台设置（关于我们）集合
     */
    public List<TAboutUsInfo> selectTAboutUsInfoList(TAboutUsInfo tAboutUsInfo);

    /**
     * 新增平台设置（关于我们）
     * 
     * @param tAboutUsInfo 平台设置（关于我们）
     * @return 结果
     */
    public int insertTAboutUsInfo(TAboutUsInfo tAboutUsInfo);

    /**
     * 修改平台设置（关于我们）
     * 
     * @param tAboutUsInfo 平台设置（关于我们）
     * @return 结果
     */
    public int updateTAboutUsInfo(TAboutUsInfo tAboutUsInfo);

    /**
     * 批量删除平台设置（关于我们）
     * 
     * @param ids 需要删除的平台设置（关于我们）主键集合
     * @return 结果
     */
    public int deleteTAboutUsInfoByIds(String[] ids);

    /**
     * 删除平台设置（关于我们）信息
     * 
     * @param id 平台设置（关于我们）主键
     * @return 结果
     */
    public int deleteTAboutUsInfoById(String id);
}
