package com.ruoyi.pos.web.mapper;

import java.util.List;
import com.ruoyi.pos.web.domain.TRebateSetInfo;

/**
 * 基础配置Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public interface TRebateSetInfoMapper 
{
    /**
     * 查询基础配置
     * 
     * @param id 基础配置主键
     * @return 基础配置
     */
    public TRebateSetInfo selectTRebateSetInfoById(String id);

    /**
     * 查询基础配置列表
     * 
     * @param tRebateSetInfo 基础配置
     * @return 基础配置集合
     */
    public List<TRebateSetInfo> selectTRebateSetInfoList(TRebateSetInfo tRebateSetInfo);

    /**
     * 新增基础配置
     * 
     * @param tRebateSetInfo 基础配置
     * @return 结果
     */
    public int insertTRebateSetInfo(TRebateSetInfo tRebateSetInfo);

    /**
     * 修改基础配置
     * 
     * @param tRebateSetInfo 基础配置
     * @return 结果
     */
    public int updateTRebateSetInfo(TRebateSetInfo tRebateSetInfo);

    /**
     * 删除基础配置
     * 
     * @param id 基础配置主键
     * @return 结果
     */
    public int deleteTRebateSetInfoById(String id);

    /**
     * 批量删除基础配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTRebateSetInfoByIds(String[] ids);
}
