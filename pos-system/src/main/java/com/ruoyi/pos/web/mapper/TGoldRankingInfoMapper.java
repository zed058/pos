package com.ruoyi.pos.web.mapper;

import java.util.List;
import com.ruoyi.pos.web.domain.TGoldRankingInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 交易金/奖励金（排行榜）回台添加app显示的假数据Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-20
 */
public interface TGoldRankingInfoMapper 
{
    /**
     * 查询交易金/奖励金（排行榜）回台添加app显示的假数据
     * 
     * @param id 交易金/奖励金（排行榜）回台添加app显示的假数据主键
     * @return 交易金/奖励金（排行榜）回台添加app显示的假数据
     */
    public TGoldRankingInfo selectTGoldRankingInfoById(String id);

    /**
     * 查询交易金/奖励金（排行榜）回台添加app显示的假数据列表
     * 
     * @param tGoldRankingInfo 交易金/奖励金（排行榜）回台添加app显示的假数据
     * @return 交易金/奖励金（排行榜）回台添加app显示的假数据集合
     */
    public List<TGoldRankingInfo> selectTGoldRankingInfoList(TGoldRankingInfo tGoldRankingInfo);

    /**
     * 新增交易金/奖励金（排行榜）回台添加app显示的假数据
     * 
     * @param tGoldRankingInfo 交易金/奖励金（排行榜）回台添加app显示的假数据
     * @return 结果
     */
    public int insertTGoldRankingInfo(TGoldRankingInfo tGoldRankingInfo);

    /**
     * 修改交易金/奖励金（排行榜）回台添加app显示的假数据
     * 
     * @param tGoldRankingInfo 交易金/奖励金（排行榜）回台添加app显示的假数据
     * @return 结果
     */
    public int updateTGoldRankingInfo(TGoldRankingInfo tGoldRankingInfo);

    /**
     * 删除交易金/奖励金（排行榜）回台添加app显示的假数据
     * 
     * @param id 交易金/奖励金（排行榜）回台添加app显示的假数据主键
     * @return 结果
     */
    public int deleteTGoldRankingInfoById(String id);

    /**
     * 批量删除交易金/奖励金（排行榜）回台添加app显示的假数据
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTGoldRankingInfoByIds(String[] ids);
}
