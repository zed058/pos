package com.ruoyi.pos.web.service.impl;

import java.util.List;

import com.ruoyi.common.enums.IsReadEnums;
import com.ruoyi.common.enums.ValidEnums;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TUserNoticeInfoMapper;
import com.ruoyi.pos.web.domain.TUserNoticeInfo;
import com.ruoyi.pos.web.service.ITUserNoticeInfoService;

/**
 * 用户阅读信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
@Service
public class TUserNoticeInfoServiceImpl implements ITUserNoticeInfoService 
{
    @Autowired
    private TUserNoticeInfoMapper tUserNoticeInfoMapper;

    /**
     * 查询用户阅读信息
     * 
     * @param id 用户阅读信息主键
     * @return 用户阅读信息
     */
    @Override
    public TUserNoticeInfo selectTUserNoticeInfoById(String id)
    {
        return tUserNoticeInfoMapper.selectTUserNoticeInfoById(id);
    }

    /**
     * 查询用户阅读信息列表
     * 
     * @param tUserNoticeInfo 用户阅读信息
     * @return 用户阅读信息
     */
    @Override
    public List<TUserNoticeInfo> selectTUserNoticeInfoList(TUserNoticeInfo tUserNoticeInfo)
    {
        List<TUserNoticeInfo> tUserNoticeInfos = tUserNoticeInfoMapper.selectTUserNoticeInfoList(tUserNoticeInfo);
        tUserNoticeInfos.stream().forEach(bean -> {
            bean.setIsReadName(IsReadEnums.getEnumsByCode(bean.getIsRead()).getValue());
            bean.setValidName(ValidEnums.getEnumsByCode(bean.getIsValid()).getValue());
        });
        return tUserNoticeInfos;
    }

    /**
     * 新增用户阅读信息
     * 
     * @param tUserNoticeInfo 用户阅读信息
     * @return 结果
     */
    @Override
    public int insertTUserNoticeInfo(TUserNoticeInfo tUserNoticeInfo)
    {
        tUserNoticeInfo.setCreateTime(DateUtils.getNowDate());
        return tUserNoticeInfoMapper.insertTUserNoticeInfo(tUserNoticeInfo);
    }

    /**
     * 修改用户阅读信息
     * 
     * @param tUserNoticeInfo 用户阅读信息
     * @return 结果
     */
    @Override
    public int updateTUserNoticeInfo(TUserNoticeInfo tUserNoticeInfo)
    {
        return tUserNoticeInfoMapper.updateTUserNoticeInfo(tUserNoticeInfo);
    }

    /**
     * 批量删除用户阅读信息
     * 
     * @param ids 需要删除的用户阅读信息主键
     * @return 结果
     */
    @Override
    public int deleteTUserNoticeInfoByIds(String[] ids)
    {
        return tUserNoticeInfoMapper.deleteTUserNoticeInfoByIds(ids);
    }

    /**
     * 删除用户阅读信息信息
     * 
     * @param id 用户阅读信息主键
     * @return 结果
     */
    @Override
    public int deleteTUserNoticeInfoById(String id)
    {
        return tUserNoticeInfoMapper.deleteTUserNoticeInfoById(id);
    }
}
