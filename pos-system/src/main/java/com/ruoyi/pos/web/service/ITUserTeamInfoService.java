package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TUserTeamInfo;

/**
 * 用户团队关系Service接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface ITUserTeamInfoService 
{
    /**
     * 查询用户团队关系
     * 
     * @param id 用户团队关系主键
     * @return 用户团队关系
     */
    public TUserTeamInfo selectTUserTeamInfoById(String id);

    /**
     * 查询用户团队关系列表
     * 
     * @param tUserTeamInfo 用户团队关系
     * @return 用户团队关系集合
     */
    public List<TUserTeamInfo> selectTUserTeamInfoList(TUserTeamInfo tUserTeamInfo);

    /**
     * 新增用户团队关系
     * 
     * @param tUserTeamInfo 用户团队关系
     * @return 结果
     */
    public int insertTUserTeamInfo(TUserTeamInfo tUserTeamInfo);

    /**
     * 修改用户团队关系
     * 
     * @param tUserTeamInfo 用户团队关系
     * @return 结果
     */
    public int updateTUserTeamInfo(TUserTeamInfo tUserTeamInfo);

    /**
     * 批量删除用户团队关系
     * 
     * @param ids 需要删除的用户团队关系主键集合
     * @return 结果
     */
    public int deleteTUserTeamInfoByIds(String[] ids);

    /**
     * 删除用户团队关系信息
     * 
     * @param id 用户团队关系主键
     * @return 结果
     */
    public int deleteTUserTeamInfoById(String id);
}
