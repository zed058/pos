package com.ruoyi.pos.web.mapper;

import java.util.List;
import com.ruoyi.pos.web.domain.TGradeInfo;

/**
 * 用户vip等级Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
public interface TGradeInfoMapper
{
    /**
     * 查询用户vip等级
     * 
     * @param id 用户vip等级主键
     * @return 用户vip等级
     */
    public TGradeInfo selectTGradeInfoById(String id);

    /**
     * 查询用户vip等级列表
     * 
     * @param tGradeInfo 用户vip等级
     * @return 用户vip等级集合
     */
    public List<TGradeInfo> selectTGradeInfoList(TGradeInfo tGradeInfo);

    /**
     * 新增用户vip等级
     * 
     * @param tGradeInfo 用户vip等级
     * @return 结果
     */
    public int insertTGradeInfo(TGradeInfo tGradeInfo);

    /**
     * 修改用户vip等级
     * 
     * @param tGradeInfo 用户vip等级
     * @return 结果
     */
    public int updateTGradeInfo(TGradeInfo tGradeInfo);

    /**
     * 删除用户vip等级
     * 
     * @param id 用户vip等级主键
     * @return 结果
     */
    public int deleteTGradeInfoById(String id);

    /**
     * 批量删除用户vip等级
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTGradeInfoByIds(String[] ids);
//-----------------------------------------自定义--------------------------------------------
    //查询比当前级别高的所有会员列表
    public List<TGradeInfo> selectTGradeInfoListCustome(String gradeCode);
    public List<TGradeInfo> selectTGradeInfoListBySort(String sort);

    public List<TGradeInfo> queryUserVip();
}
