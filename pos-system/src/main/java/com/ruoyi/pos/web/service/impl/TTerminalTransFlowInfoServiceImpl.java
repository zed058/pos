package com.ruoyi.pos.web.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.enums.HandleStausEnum;
import com.ruoyi.common.enums.PayTypeEnum;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.pos.web.domain.TTerminalTransFlowShareInfo;
import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.mapper.TTerminalTransFlowShareInfoMapper;
import com.ruoyi.pos.web.mapper.TUserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TTerminalTransFlowInfoMapper;
import com.ruoyi.pos.web.domain.TTerminalTransFlowInfo;
import com.ruoyi.pos.web.service.ITTerminalTransFlowInfoService;

/**
 * 终端交易流水Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@Service
public class TTerminalTransFlowInfoServiceImpl implements ITTerminalTransFlowInfoService 
{
    @Autowired
    private TUserInfoMapper userInfoMapper;
    @Autowired
    private TTerminalTransFlowInfoMapper tTerminalTransFlowInfoMapper;
    @Autowired
    private TTerminalTransFlowShareInfoMapper tTerminalTransFlowShareInfoMapper;

    /**
     * 查询终端交易流水
     * 
     * @param id 终端交易流水主键
     * @return 终端交易流水
     */
    @Override
    public TTerminalTransFlowInfo selectTTerminalTransFlowInfoById(String id)
    {
        TTerminalTransFlowInfo mapper = new TTerminalTransFlowInfo();
        mapper.setId(id);
        TTerminalTransFlowInfo flow = tTerminalTransFlowInfoMapper.selectTTerminalTransFlowInfoListForWeb(mapper).get(0);
        flow.setTransAmount(flow.getTransAmount().divide(BigDecimal.valueOf(100L)));//换算成元
//        flow.setExtractionFee(flow.getExtractionFee().divide(BigDecimal.valueOf(100L)));//换算成元
        flow.setPayType(PayTypeEnum.getEnumsByCode(flow.getPayType()).getValue());
        flow.setHandleStaus(HandleStausEnum.getEnumsByCode(flow.getHandleStaus()).getValue());
        TTerminalTransFlowShareInfo shareMapper = new TTerminalTransFlowShareInfo();
        shareMapper.setFlowId(id);
        List<TTerminalTransFlowShareInfo> shareList = tTerminalTransFlowShareInfoMapper.selectTTerminalTransFlowShareInfoList(shareMapper);
        flow.setShareList(shareList);
        List<TUserInfo> userList = userInfoMapper.selectTUserInfoList(new TUserInfo());
        shareList.stream().forEach(share ->{
            TUserInfo user = userList.stream().filter(bean ->bean.getUserId().equals(share.getShareUserId())).collect(Collectors.toList()).get(0);
            share.setShareUserId(user.getUserName() + user.getUserPhone());
        });
        return flow;
    }

    /**
     * 查询终端交易流水列表
     * 
     * @param tTerminalTransFlowInfo 终端交易流水
     * @return 终端交易流水
     */
    @Override
    public List<TTerminalTransFlowInfo> selectTTerminalTransFlowInfoList(TTerminalTransFlowInfo tTerminalTransFlowInfo)
    {
        if (null != tTerminalTransFlowInfo.getQueryCreateTimeStr()) {
            tTerminalTransFlowInfo.setCreateTimeStart(tTerminalTransFlowInfo.getQueryCreateTimeStr()[0] + " 00:00:00");
            tTerminalTransFlowInfo.setCreateTimeEnd(tTerminalTransFlowInfo.getQueryCreateTimeStr()[1] + " 23:59:59");
        }
        List<TTerminalTransFlowInfo> flowList = tTerminalTransFlowInfoMapper.selectTTerminalTransFlowInfoListForWeb(tTerminalTransFlowInfo);
        List<TUserInfo> userList = userInfoMapper.selectTUserInfoList(new TUserInfo());
        flowList.stream().forEach(flow ->{
            flow.setTransAmount(flow.getTransAmount().divide(BigDecimal.valueOf(100L)));//换算成元
//            flow.setExtractionFee(flow.getExtractionFee().divide(BigDecimal.valueOf(100L)));//换算成元
            flow.setPayType(PayTypeEnum.getEnumsByCode(flow.getPayType()).getValue());
            flow.setHandleStaus(HandleStausEnum.getEnumsByCode(flow.getHandleStaus()).getValue());
        });
        return flowList;
    }

    /**
     * 新增终端交易流水
     * 
     * @param tTerminalTransFlowInfo 终端交易流水
     * @return 结果
     */
    @Override
    public int insertTTerminalTransFlowInfo(TTerminalTransFlowInfo tTerminalTransFlowInfo)
    {
        tTerminalTransFlowInfo.setCreateTime(DateUtils.getNowDate());
        return tTerminalTransFlowInfoMapper.insertTTerminalTransFlowInfo(tTerminalTransFlowInfo);
    }

    /**
     * 修改终端交易流水
     * 
     * @param tTerminalTransFlowInfo 终端交易流水
     * @return 结果
     */
    @Override
    public int updateTTerminalTransFlowInfo(TTerminalTransFlowInfo tTerminalTransFlowInfo)
    {
        return tTerminalTransFlowInfoMapper.updateTTerminalTransFlowInfo(tTerminalTransFlowInfo);
    }

    /**
     * 批量删除终端交易流水
     * 
     * @param ids 需要删除的终端交易流水主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalTransFlowInfoByIds(String[] ids)
    {
        return tTerminalTransFlowInfoMapper.deleteTTerminalTransFlowInfoByIds(ids);
    }

    /**
     * 删除终端交易流水信息
     * 
     * @param id 终端交易流水主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalTransFlowInfoById(String id)
    {
        return tTerminalTransFlowInfoMapper.deleteTTerminalTransFlowInfoById(id);
    }
}
