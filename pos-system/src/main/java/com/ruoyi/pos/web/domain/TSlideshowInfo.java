package com.ruoyi.pos.web.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 引导页面对象 t_slideshow_info
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public class TSlideshowInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private String id;

    /** 图片 */
    @Excel(name = "图片")
    private String img;

    /** 图片类型(1:首页弹窗,2:首页轮播图,3:广告展示,4:分享图片,5:腰部广告图) */
    @Excel(name = "图片类型(1:首页弹窗,2:首页轮播图,3:广告展示,4:分享图片,5:腰部广告图)")
    private String imgType;

    @Excel(name = "图片类型名称")
    private String imgTypeName;

    /** 排序 */
    @Excel(name = "排序")
    private String sort;

    /** 图片调整url路径 */
    @Excel(name = "图片调整url路径")
    private String imgUrl;

    /** 状态（0：正常，1：禁用） */
    @Excel(name = "状态", readConverterExp = "0=：正常，1：禁用")
    private String isValid;

    /** 跳转状态(0：内链 1外链)*/
    @Excel(name = "跳转状态", readConverterExp = "0=：内链，1：外链")
    private String linkType;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setImg(String img) 
    {
        this.img = img;
    }

    public String getImg() 
    {
        return img;
    }
    public void setImgType(String imgType) 
    {
        this.imgType = imgType;
    }

    public String getImgType() 
    {
        return imgType;
    }
    public void setSort(String sort) 
    {
        this.sort = sort;
    }

    public String getSort() 
    {
        return sort;
    }
    public void setImgUrl(String imgUrl) 
    {
        this.imgUrl = imgUrl;
    }

    public String getImgUrl() 
    {
        return imgUrl;
    }
    public void setIsValid(String isValid) 
    {
        this.isValid = isValid;
    }

    public String getIsValid() 
    {
        return isValid;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("img", getImg())
            .append("imgType", getImgType())
            .append("sort", getSort())
            .append("imgUrl", getImgUrl())
            .append("createTime", getCreateTime())
            .append("isValid", getIsValid())
            .append("linkType", getLinkType())
            .toString();
    }

    public String getImgTypeName() {
        return imgTypeName;
    }

    public void setImgTypeName(String imgTypeName) {
        this.imgTypeName = imgTypeName;
    }
}
