package com.ruoyi.pos.web.service;

import java.util.List;
import com.ruoyi.pos.web.domain.TUserNoticeInfo;

/**
 * 用户阅读信息Service接口
 * 
 * @author ruoyi
 * @date 2021-11-11
 */
public interface ITUserNoticeInfoService 
{
    /**
     * 查询用户阅读信息
     * 
     * @param id 用户阅读信息主键
     * @return 用户阅读信息
     */
    public TUserNoticeInfo selectTUserNoticeInfoById(String id);

    /**
     * 查询用户阅读信息列表
     * 
     * @param tUserNoticeInfo 用户阅读信息
     * @return 用户阅读信息集合
     */
    public List<TUserNoticeInfo> selectTUserNoticeInfoList(TUserNoticeInfo tUserNoticeInfo);

    /**
     * 新增用户阅读信息
     * 
     * @param tUserNoticeInfo 用户阅读信息
     * @return 结果
     */
    public int insertTUserNoticeInfo(TUserNoticeInfo tUserNoticeInfo);

    /**
     * 修改用户阅读信息
     * 
     * @param tUserNoticeInfo 用户阅读信息
     * @return 结果
     */
    public int updateTUserNoticeInfo(TUserNoticeInfo tUserNoticeInfo);

    /**
     * 批量删除用户阅读信息
     * 
     * @param ids 需要删除的用户阅读信息主键集合
     * @return 结果
     */
    public int deleteTUserNoticeInfoByIds(String[] ids);

    /**
     * 删除用户阅读信息信息
     * 
     * @param id 用户阅读信息主键
     * @return 结果
     */
    public int deleteTUserNoticeInfoById(String id);
}
