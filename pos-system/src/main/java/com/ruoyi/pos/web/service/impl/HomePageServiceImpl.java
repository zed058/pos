package com.ruoyi.pos.web.service.impl;

import com.ruoyi.common.enums.OrderStatusEnums;
import com.ruoyi.common.enums.OrderTypeEnum;
import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.pos.web.domain.TOrderInfo;
import com.ruoyi.pos.web.mapper.TOrderInfoMapper;
import com.ruoyi.pos.web.mapper.TUserInfoMapper;
import com.ruoyi.pos.web.service.HomePageService;
import com.ruoyi.pos.web.weVo.HomePageVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author Administrator
 * @title: HomePageServiceImpl
 * @projectName ruoyi
 * @description: TODO
 */
@Service
public class HomePageServiceImpl implements HomePageService {
    @Autowired
    private TUserInfoMapper tUserInfoMapper;
    @Autowired
    private TOrderInfoMapper tOrderInfoMapper;

    /*首页头部数据统计*/
    @Override
    public Map<String, Object> quantitySum() throws Exception{
        Map<String, Object> returnMap = new HashMap<>();

        returnMap.put("userCount", tUserInfoMapper.queryUserCount());//查询所有用户数量
        TOrderInfo order = new TOrderInfo();

        List<String> orderStatus =  new ArrayList<>();
        orderStatus.add(OrderStatusEnums.WAIT_DELIVERED.getCode());
        orderStatus.add(OrderStatusEnums.HAS_RECEIPT.getCode());
        orderStatus.add(OrderStatusEnums.HAS_OVER.getCode());
        order.setOrderStatus(String.join(",",orderStatus));
        List<TOrderInfo> tOrderInfos = tOrderInfoMapper.querySum(order);//查询终端数量
        BigDecimal terminalSum = new BigDecimal("0");//终端订单数量
        BigDecimal exchangeSum = new BigDecimal("0");//兑换订单数量
        Integer purchaseSum = 0;//采购订单数量
        Integer orderExchangeSum = 0;//兑换订单数量
        for (TOrderInfo info : tOrderInfos) {
            if (OrderTypeEnum.TERMINALPURCHASE.getCode().equals(info.getOrderType())){
                terminalSum = terminalSum.add(info.getGoodsNum() == null ? BigDecimal.ONE : info.getGoodsNum());
            }else {
                exchangeSum = exchangeSum.add(info.getGoodsNum() == null ? BigDecimal.ONE : info.getGoodsNum());
            }
            if (info.getOrderType().equals(OrderTypeEnum.TERMINALPURCHASE.getCode())){//采购订单
                purchaseSum ++;
            }else {
                orderExchangeSum++;
            }
        }
        returnMap.put("terminalSum", terminalSum);//终端商品数量
        returnMap.put("exchangeSum", exchangeSum);//兑换商品数量
        returnMap.put("purchaseSum", purchaseSum);//终端订单数量
        returnMap.put("orderExchangeSum", orderExchangeSum);//兑换订单数量
        return returnMap;
    }

    /*用户统计图*/
    @Override
    public Map<String, Object> queryUserChart() throws Exception{
        Map<String, Object> returnMap = new HashMap<>();
        List<HomePageVo> homePageVos = tUserInfoMapper.queryUserChart();
        List<Object> userSum = new ArrayList<>();
        List<Object> createTime = new ArrayList<>();
        Optional.ofNullable(homePageVos).ifPresent(map ->{
            map.stream().forEach(bean->{
                userSum.add(bean.getUserSum());
                createTime.add(bean.getCreateTime());
            });
        });
        returnMap.put("userSum",userSum);
        returnMap.put("createTime",createTime);
        return returnMap;
    }

    /*订单统计图*/
    @Override
    public Map<String, Object> queryOrderChart() throws Exception{
        Map<String, Object> returnMap = new HashMap<>();
        List<Object> orderSum = new ArrayList<>();
        List<Object> createTime = new ArrayList<>();
        List<HomePageVo> homePageVos = queryChart("");
        Optional.ofNullable(homePageVos).ifPresent(map ->{
            map.stream().forEach(bean->{
                orderSum.add(bean.getOrderSum());
                createTime.add(bean.getCreateTime());
            });
        });
        returnMap.put("orderSum",orderSum);
        returnMap.put("createTime",createTime);
        return returnMap;
    }

    /*采购商统计图*/
    @Override
    public Map<String, Object> queryOrderTerminalChart() {
        Map<String, Object> returnMap = new HashMap<>();
        List<Object> terminalSum = new ArrayList<>();
        List<Object> createTime = new ArrayList<>();
        List<Object> goodsNum = new ArrayList<>();
        List<HomePageVo> homePageVos = queryChart(OrderTypeEnum.TERMINALPURCHASE.getCode());
        Optional.ofNullable(homePageVos).ifPresent(map ->{
            map.stream().forEach(bean->{
                terminalSum.add(bean.getOrderSum());
                goodsNum.add(bean.getGoodsNum());
                createTime.add(bean.getCreateTime());
            });
        });
        returnMap.put("terminalSum",terminalSum);
        returnMap.put("createTime",createTime);
        returnMap.put("goodsNum",goodsNum);
        return returnMap;
    }

    /*兑换商统计图*/
    @Override
    public Map<String, Object> queryOrderExchangeChart() {
        Map<String, Object> returnMap = new HashMap<>();
        List<Object> exchangeSum = new ArrayList<>();
        List<Object> createTime = new ArrayList<>();
        List<Object> goodsNum = new ArrayList<>();
        List<HomePageVo> homePageVos = queryChart("-1");
        Optional.ofNullable(homePageVos).ifPresent(map ->{
            map.stream().forEach(bean->{
                exchangeSum.add(bean.getOrderSum());
                goodsNum.add(bean.getGoodsNum());
                createTime.add(bean.getCreateTime());
            });
        });
        returnMap.put("exchangeSum",exchangeSum);
        returnMap.put("createTime",createTime);
        returnMap.put("goodsNum",goodsNum);
        return returnMap;
    }


    public List<HomePageVo> queryChart(String orderType){
        List<String> orderStatus =  new ArrayList<>();
        if (StringUtils.isNotEmpty(orderType)){
            orderStatus.add(OrderStatusEnums.WAIT_DELIVERED.getCode());
            orderStatus.add(OrderStatusEnums.HAS_RECEIPT.getCode());
            orderStatus.add(OrderStatusEnums.HAS_OVER.getCode());
        }
        List<HomePageVo> homePageVos = tOrderInfoMapper.queryHomePageCount(orderType,orderStatus);
        return homePageVos;
    }

}
