package com.ruoyi.pos.web.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TGradeInfoMapper;
import com.ruoyi.pos.web.domain.TGradeInfo;
import com.ruoyi.pos.web.service.ITGradeInfoService;

/**
 * 用户vip等级Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-15
 */
@Service
public class TGradeInfoServiceImpl implements ITGradeInfoService
{
    @Autowired
    private TGradeInfoMapper tGradeInfoMapper;

    /**
     * 查询用户vip等级
     * 
     * @param id 用户vip等级主键
     * @return 用户vip等级
     */
    @Override
    public TGradeInfo selectTGradeInfoById(String id)
    {
        return tGradeInfoMapper.selectTGradeInfoById(id);
    }

    /**
     * 查询用户vip等级列表
     * 
     * @param tGradeInfo 用户vip等级
     * @return 用户vip等级
     */
    @Override
    public List<TGradeInfo> selectTGradeInfoList(TGradeInfo tGradeInfo)
    {
        return tGradeInfoMapper.selectTGradeInfoList(tGradeInfo);
    }

    /**
     * 新增用户vip等级
     * 
     * @param tGradeInfo 用户vip等级
     * @return 结果
     */
    @Override
    public int insertTGradeInfo(TGradeInfo tGradeInfo)
    {
        tGradeInfo.setCreateTime(DateUtils.getNowDate());
        return tGradeInfoMapper.insertTGradeInfo(tGradeInfo);
    }

    /**
     * 修改用户vip等级
     * 
     * @param tGradeInfo 用户vip等级
     * @return 结果
     */
    @Override
    public int updateTGradeInfo(TGradeInfo tGradeInfo)
    {
        return tGradeInfoMapper.updateTGradeInfo(tGradeInfo);
    }

    /**
     * 批量删除用户vip等级
     * 
     * @param ids 需要删除的用户vip等级主键
     * @return 结果
     */
    @Override
    public int deleteTGradeInfoByIds(String[] ids)
    {
        return tGradeInfoMapper.deleteTGradeInfoByIds(ids);
    }

    /**
     * 删除用户vip等级信息
     * 
     * @param id 用户vip等级主键
     * @return 结果
     */
    @Override
    public int deleteTGradeInfoById(String id)
    {
        return tGradeInfoMapper.deleteTGradeInfoById(id);
    }
}
