package com.ruoyi.pos.web.service.impl;

import java.nio.file.OpenOption;
import java.util.List;
import java.util.Optional;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.mapper.TUserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.web.mapper.TTerminalTransFlowShareInfoMapper;
import com.ruoyi.pos.web.domain.TTerminalTransFlowShareInfo;
import com.ruoyi.pos.web.service.ITTerminalTransFlowShareInfoService;

/**
 * 第三方交易流水分润详情Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-24
 */
@Service
public class TTerminalTransFlowShareInfoServiceImpl implements ITTerminalTransFlowShareInfoService 
{
    @Autowired
    private TTerminalTransFlowShareInfoMapper tTerminalTransFlowShareInfoMapper;
    @Autowired
    private TUserInfoMapper tUserInfoMapper;
    /**
     * 查询第三方交易流水分润详情
     * 
     * @param id 第三方交易流水分润详情主键
     * @return 第三方交易流水分润详情
     */
    @Override
    public TTerminalTransFlowShareInfo selectTTerminalTransFlowShareInfoById(String id)
    {
        return tTerminalTransFlowShareInfoMapper.selectTTerminalTransFlowShareInfoById(id);
    }

    /**
     * 查询第三方交易流水分润详情列表
     * 
     * @param tTerminalTransFlowShareInfo 第三方交易流水分润详情
     * @return 第三方交易流水分润详情
     */
    @Override
    public List<TTerminalTransFlowShareInfo> selectTTerminalTransFlowShareInfoList(TTerminalTransFlowShareInfo tTerminalTransFlowShareInfo)
    {
       return  tTerminalTransFlowShareInfoMapper.queryTransFlowUser(tTerminalTransFlowShareInfo);
    }

    /**
     * 新增第三方交易流水分润详情
     * 
     * @param tTerminalTransFlowShareInfo 第三方交易流水分润详情
     * @return 结果
     */
    @Override
    public int insertTTerminalTransFlowShareInfo(TTerminalTransFlowShareInfo tTerminalTransFlowShareInfo)
    {
        tTerminalTransFlowShareInfo.setCreateTime(DateUtils.getNowDate());
        return tTerminalTransFlowShareInfoMapper.insertTTerminalTransFlowShareInfo(tTerminalTransFlowShareInfo);
    }

    /**
     * 修改第三方交易流水分润详情
     * 
     * @param tTerminalTransFlowShareInfo 第三方交易流水分润详情
     * @return 结果
     */
    @Override
    public int updateTTerminalTransFlowShareInfo(TTerminalTransFlowShareInfo tTerminalTransFlowShareInfo)
    {
        return tTerminalTransFlowShareInfoMapper.updateTTerminalTransFlowShareInfo(tTerminalTransFlowShareInfo);
    }

    /**
     * 批量删除第三方交易流水分润详情
     * 
     * @param ids 需要删除的第三方交易流水分润详情主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalTransFlowShareInfoByIds(String[] ids)
    {
        return tTerminalTransFlowShareInfoMapper.deleteTTerminalTransFlowShareInfoByIds(ids);
    }

    /**
     * 删除第三方交易流水分润详情信息
     * 
     * @param id 第三方交易流水分润详情主键
     * @return 结果
     */
    @Override
    public int deleteTTerminalTransFlowShareInfoById(String id)
    {
        return tTerminalTransFlowShareInfoMapper.deleteTTerminalTransFlowShareInfoById(id);
    }
}
