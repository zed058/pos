package com.ruoyi.pos.web.service;


import com.ruoyi.pos.web.domain.TBankInfo;

import java.util.List;

/**
 * 银行信息维护Service接口
 * 
 * @author ruoyi
 * @date 2021-12-06
 */
public interface ITBankInfoService 
{
    /**
     * 查询银行信息维护
     * 
     * @param bankId 银行信息维护主键
     * @return 银行信息维护
     */
    public TBankInfo selectTBankInfoByBankId(String bankId);

    /**
     * 查询银行信息维护列表
     * 
     * @param tBankInfo 银行信息维护
     * @return 银行信息维护集合
     */
    public List<TBankInfo> selectTBankInfoList(TBankInfo tBankInfo);

    /**
     * 新增银行信息维护
     * 
     * @param tBankInfo 银行信息维护
     * @return 结果
     */
    public int insertTBankInfo(TBankInfo tBankInfo);

    /**
     * 修改银行信息维护
     * 
     * @param tBankInfo 银行信息维护
     * @return 结果
     */
    public int updateTBankInfo(TBankInfo tBankInfo);

    /**
     * 批量删除银行信息维护
     * 
     * @param bankIds 需要删除的银行信息维护主键集合
     * @return 结果
     */
    public int deleteTBankInfoByBankIds(String[] bankIds);

    /**
     * 删除银行信息维护信息
     * 
     * @param bankId 银行信息维护主键
     * @return 结果
     */
    public int deleteTBankInfoByBankId(String bankId);
}
