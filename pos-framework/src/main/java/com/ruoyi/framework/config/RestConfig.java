package com.ruoyi.framework.config;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.ruoyi.common.utils.rest.RestTemplateUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @ClassName RestConfig
 * @Description
 * @author luobo
 * @Date 2020年6月4日 下午1:51:09
 * @version 1.0.0
 */
@Configuration
public class RestConfig {

	@Value("${rest.connect.timeout}")
	private int connectTimeout;

	@Value("${rest.read.timeout}")
	private int readTimeout;

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
		RestTemplate restTemplate = restTemplateBuilder
				.setConnectTimeout(Duration.ofSeconds(connectTimeout))
				.setReadTimeout(Duration.ofSeconds(readTimeout))
				.build();
        
        // 设置编码
		/*StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter(Charset.forName("UTF-8"));
		stringHttpMessageConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));*/
		FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
		//支持所有类型、fastjson新版本不能使用All
		List<MediaType> mediaTypes = generateMediaTypes();
		converter.setSupportedMediaTypes(mediaTypes);

        for (int i = 0; i < restTemplate.getMessageConverters().size(); i++) {
            /*if (restTemplate.getMessageConverters().get(i) instanceof stringHttpMessageConverter) {
                restTemplate.getMessageConverters().remove(i);
                restTemplate.getMessageConverters().add(i, stringHttpMessageConverter);
            }*/
            if (restTemplate.getMessageConverters().get(i) instanceof FastJsonHttpMessageConverter) {
                restTemplate.getMessageConverters().remove(i);
                restTemplate.getMessageConverters().add(i, converter);
            }
        }
		return restTemplate;
	}

	//初始化允许请求的类型
	public List<MediaType> generateMediaTypes() {
		List<MediaType> mediaTypes = new ArrayList<>(16);
		mediaTypes.add(MediaType.APPLICATION_JSON);
		mediaTypes.add(MediaType.APPLICATION_ATOM_XML);
		mediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
		mediaTypes.add(MediaType.APPLICATION_OCTET_STREAM);
		mediaTypes.add(MediaType.APPLICATION_PDF);
		mediaTypes.add(MediaType.APPLICATION_RSS_XML);
		mediaTypes.add(MediaType.APPLICATION_XHTML_XML);
		mediaTypes.add(MediaType.APPLICATION_XML);
		mediaTypes.add(MediaType.IMAGE_GIF);
		mediaTypes.add(MediaType.IMAGE_JPEG);
		mediaTypes.add(MediaType.IMAGE_PNG);
		mediaTypes.add(MediaType.TEXT_EVENT_STREAM);
		mediaTypes.add(MediaType.TEXT_HTML);
		mediaTypes.add(MediaType.TEXT_MARKDOWN);
		mediaTypes.add(MediaType.TEXT_PLAIN);
		mediaTypes.add(MediaType.TEXT_XML);
		return mediaTypes;
	}
	
	/**
	 * 
	 * @Description restTemplate工具类
	 * @return
	 */
	@Bean
	public RestTemplateUtil restTemplateUtil(RestTemplateBuilder restTemplateBuilder) {
		return new RestTemplateUtil(restTemplate(restTemplateBuilder));
	}
	
}
