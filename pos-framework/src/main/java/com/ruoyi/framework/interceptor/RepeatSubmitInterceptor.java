package com.ruoyi.framework.interceptor;

import java.lang.reflect.Method;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.enums.YesOrNoEnums;
import com.ruoyi.pos.api.utils.CommonUtils;
import com.ruoyi.pos.web.domain.TUserInfo;
import com.ruoyi.pos.web.mapper.TUserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.ServletUtils;

/**
 * 防止重复提交拦截器
 *
 * @author ruoyi
 */
@Component
public abstract class RepeatSubmitInterceptor implements HandlerInterceptor
{
    @Autowired
    private CommonUtils commonUtils;

    @Autowired
    private TUserInfoMapper userInfoMapper;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        if (handler instanceof HandlerMethod)
        {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            RepeatSubmit annotation = method.getAnnotation(RepeatSubmit.class);
            if (annotation != null)
            {
                if (this.isRepeatSubmit(request, annotation))
                {
                    AjaxResult ajaxResult = AjaxResult.error(annotation.message());
                    ServletUtils.renderString(response, JSONObject.toJSONString(ajaxResult));
                    return false;
                }
            }
            String uri = request.getRequestURI();
            // 判请先登录断是否是公共的逻辑
            if (!isPassUri(uri)) {
                //需要进行鉴权校验
                /*if (1 == 1) { //测试代码
                    return true;
                }*/
                String userId = commonUtils.getUserId();
                TUserInfo user = userInfoMapper.selectTUserInfoByUserId(userId);
                if (null == user) {
                    AjaxResult ajaxResult = AjaxResult.error("登录过期", "登录过期");
                    ServletUtils.renderString(response, JSONObject.toJSONString(ajaxResult));
                    return false;
                } else {
                    if (YesOrNoEnums.NO.getCode().equals(user.getUserStatus())) {
                        AjaxResult ajaxResult = AjaxResult.error("您已被禁用", "您已被禁用");
                        ServletUtils.renderString(response, JSONObject.toJSONString(ajaxResult));
                        return false;
                    }
                }
            }
            return true;
        } else {
            return true;
        }
    }

    /**
     * 判断是否放行的uri
     *
     * @param uri
     * @return
     */
    private static boolean isPassUri(String uri) {
        if (uri.matches(".*\\..*") || !uri.startsWith("/api"))
            // 包含点,查静态文件,直接放行
            return true;
        if ( (uri.matches(".*swagger-resources.*") ||
                uri.matches("/v2/api-docs.*") ||
                uri.matches("/swagger-ui.html")) ||
                uri.matches(".*userRegister") ||
                uri.matches(".*loginByOpenId") ||
                uri.matches(".*loginByPhone") ||
                uri.matches(".*loginByAccount") ||
                uri.matches(".*loginOut") ||
                uri.matches(".*sendMqtt") ||
                uri.matches(".*sendMqtt2") ||
                uri.matches(".*sendMessage") ||
                uri.matches(".*saveUserInfo") ||
                uri.matches(".*forgetPassword")  ||
                uri.matches(".*api/common.*") ||
                uri.matches(".*register"))
            return true;
        return false;
    }

    /**
     * 验证是否重复提交由子类实现具体的防重复提交的规则
     *
     * @param request
     * @return
     * @throws Exception
     */
    public abstract boolean isRepeatSubmit(HttpServletRequest request, RepeatSubmit annotation);
}
