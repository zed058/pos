package com.ruoyi.framework.web.exception;

import javax.servlet.http.HttpServletRequest;

import com.ruoyi.common.core.domain.api.ApiResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.exception.DemoModeException;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.text.MessageFormat;

/**
 * 全局异常处理器
 * 
 * @author ruoyi
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 权限校验异常
     */
    @ExceptionHandler(AccessDeniedException.class)
    public AjaxResult handleAccessDeniedException(AccessDeniedException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',权限校验失败'{}'", requestURI, e.getMessage());
        return AjaxResult.error(HttpStatus.FORBIDDEN, "没有权限，请联系管理员授权");
    }

    /**
     * 请求方式不支持
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public Object handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException e,
            HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',不支持'{}'请求", requestURI, e.getMethod());
        if (requestURI.startsWith("/api")){
            return ApiResult.error(MessageFormat.format("请求地址{0},不支持{1}请求", requestURI, e.getMethod()));
        }
        return AjaxResult.error(e.getMessage());
    }

    /**
     * 业务异常
     */
    @ExceptionHandler(ServiceException.class)
    public Object handleServiceException(ServiceException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        if (requestURI.startsWith("/api")){
            return ApiResult.error("业务逻辑异常：" + e.getMessage());
        }
        log.error(e.getMessage(), e);
        Integer code = e.getCode();
        return StringUtils.isNotNull(code) ? AjaxResult.error(code, e.getMessage()) : AjaxResult.error(e.getMessage());
    }

    /**
     * 拦截未知的运行时异常
     */
    @ExceptionHandler(RuntimeException.class)
    public Object handleRuntimeException(RuntimeException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        String message = e.getMessage();
        if (requestURI.startsWith("/api")){
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            String exceptionType = "";
            if (e instanceof IllegalArgumentException) {
                exceptionType = "带处理数据错误异常：";
            } else if (e instanceof ArithmeticException) {
                exceptionType = "算术运算异常：";
            } else if (e instanceof NullPointerException) {
                exceptionType = "空指针异常：";
            } else if (e instanceof IndexOutOfBoundsException) {
                exceptionType = "数组下标越界异常：";
            } else {
                exceptionType = "运行时异常：";
            }
            log.error("请求地址'{}'，{}", requestURI, exceptionType, e);
            return ApiResult.error(exceptionType + message);
        }
        log.error("请求地址'{}',发生未知异常.", requestURI, e);
        return AjaxResult.error(e.getMessage());
    }

    /**
     * 文件处理异常
     */
    @ExceptionHandler(IOException.class)
    public Object handleIOException(IOException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        String message = e.getMessage();
        if (requestURI.startsWith("/api")){
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            String exceptionType = "文件处理异常：";
            log.error("请求地址'{}'，{}", requestURI, exceptionType, e);
            return ApiResult.error(exceptionType + message);
        }
        log.error("请求地址'{}',发生文件处理异常.", requestURI, e);
        return AjaxResult.error(e.getMessage());
    }

    /**
     * 数据库异常
     */
    @ExceptionHandler(SQLException.class)
    public Object handleSQLException(SQLException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        String message = e.getMessage();
        if (requestURI.startsWith("/api")){
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw, true));
            String exceptionType = "数据库异常：";
            log.error("请求地址'{}'，{}", requestURI, exceptionType, e);
            return ApiResult.error(exceptionType + message);
        }
        log.error("请求地址'{}',发生数据库异常.", requestURI, e);
        return AjaxResult.error(e.getMessage());
    }

    /**
     * 系统异常
     */
    @ExceptionHandler(Exception.class)
    public AjaxResult handleException(Exception e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        log.error("请求地址'{}',发生系统异常.", requestURI, e);
        return AjaxResult.error(e.getMessage());
    }

    /**
     * 自定义验证异常
     */
    @ExceptionHandler(BindException.class)
    public AjaxResult handleBindException(BindException e) {
        log.error(e.getMessage(), e);
        String message = e.getAllErrors().get(0).getDefaultMessage();
        return AjaxResult.error(message);
    }

    /**
     * 自定义验证异常
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Object handleMethodArgumentNotValidException(MethodArgumentNotValidException e, HttpServletRequest request) {
        String message = e.getBindingResult().getFieldError().getDefaultMessage();
        if (request.getRequestURI().startsWith("/api")){
            log.error(e.getMessage(), e);
            return ApiResult.error("参数错误异常：" + message);
        }
        log.error(e.getMessage(), e);
        return AjaxResult.error(message);
    }

    /**
     * 演示模式异常
     */
    @ExceptionHandler(DemoModeException.class)
    public AjaxResult handleDemoModeException(DemoModeException e) {
        return AjaxResult.error("演示模式，不允许操作");
    }
}
